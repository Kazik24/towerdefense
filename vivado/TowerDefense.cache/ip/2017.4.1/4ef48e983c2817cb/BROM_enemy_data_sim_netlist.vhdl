-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4.1 (lin64) Build 2117270 Tue Jan 30 15:31:13 MST 2018
-- Date        : Wed Jun  6 12:59:17 2018
-- Host        : X751LX running 64-bit Ubuntu 17.10
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ BROM_enemy_data_sim_netlist.vhdl
-- Design      : BROM_enemy_data
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_bindec is
  port (
    ena_array : out STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_bindec;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_bindec is
begin
\ENOUT_inferred__9/i_\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => addra(2),
      I1 => addra(0),
      I2 => addra(3),
      I3 => addra(1),
      O => ena_array(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_bindec_0 is
  port (
    enb_array : out STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_bindec_0 : entity is "bindec";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_bindec_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_bindec_0 is
begin
\ENOUT_inferred__9/i_\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => addrb(2),
      I1 => addrb(0),
      I2 => addrb(3),
      I3 => addrb(1),
      O => enb_array(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_mux is
  port (
    douta : out STD_LOGIC_VECTOR ( 10 downto 0 );
    addra : in STD_LOGIC_VECTOR ( 3 downto 0 );
    clka : in STD_LOGIC;
    DOPADOP : in STD_LOGIC_VECTOR ( 0 to 0 );
    p_7_out : in STD_LOGIC_VECTOR ( 8 downto 0 );
    DOADO : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_3\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_4\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_5\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_6\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_1\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_mux;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_mux is
  signal \douta[10]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \douta[10]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \douta[2]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \douta[2]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \douta[3]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \douta[3]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \douta[4]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \douta[4]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \douta[5]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \douta[5]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \douta[6]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \douta[6]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \douta[7]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \douta[7]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \douta[8]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \douta[8]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \douta[9]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \douta[9]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal sel_pipe : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal sel_pipe_d1 : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
\douta[0]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F40"
    )
        port map (
      I0 => sel_pipe_d1(2),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\(0),
      I2 => sel_pipe_d1(3),
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_0\(0),
      O => douta(0)
    );
\douta[10]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \douta[10]_INST_0_i_1_n_0\,
      I1 => \douta[10]_INST_0_i_2_n_0\,
      O => douta(10),
      S => sel_pipe_d1(3)
    );
\douta[10]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_3\(0),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_4\(0),
      I2 => sel_pipe_d1(2),
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_5\(0),
      I4 => sel_pipe_d1(1),
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_6\(0),
      O => \douta[10]_INST_0_i_1_n_0\
    );
\douta[10]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOPADOP(0),
      I1 => sel_pipe_d1(2),
      I2 => sel_pipe_d1(1),
      I3 => p_7_out(8),
      I4 => sel_pipe_d1(0),
      O => \douta[10]_INST_0_i_2_n_0\
    );
\douta[1]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F40"
    )
        port map (
      I0 => sel_pipe_d1(2),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\(1),
      I2 => sel_pipe_d1(3),
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_1\(0),
      O => douta(1)
    );
\douta[2]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \douta[2]_INST_0_i_1_n_0\,
      I1 => \douta[2]_INST_0_i_2_n_0\,
      O => douta(2),
      S => sel_pipe_d1(3)
    );
\douta[2]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(0),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(0),
      I2 => sel_pipe_d1(2),
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(0),
      I4 => sel_pipe_d1(1),
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(0),
      O => \douta[2]_INST_0_i_1_n_0\
    );
\douta[2]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOADO(0),
      I1 => sel_pipe_d1(2),
      I2 => sel_pipe_d1(1),
      I3 => p_7_out(0),
      I4 => sel_pipe_d1(0),
      O => \douta[2]_INST_0_i_2_n_0\
    );
\douta[3]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \douta[3]_INST_0_i_1_n_0\,
      I1 => \douta[3]_INST_0_i_2_n_0\,
      O => douta(3),
      S => sel_pipe_d1(3)
    );
\douta[3]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(1),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(1),
      I2 => sel_pipe_d1(2),
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(1),
      I4 => sel_pipe_d1(1),
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(1),
      O => \douta[3]_INST_0_i_1_n_0\
    );
\douta[3]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOADO(1),
      I1 => sel_pipe_d1(2),
      I2 => sel_pipe_d1(1),
      I3 => p_7_out(1),
      I4 => sel_pipe_d1(0),
      O => \douta[3]_INST_0_i_2_n_0\
    );
\douta[4]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \douta[4]_INST_0_i_1_n_0\,
      I1 => \douta[4]_INST_0_i_2_n_0\,
      O => douta(4),
      S => sel_pipe_d1(3)
    );
\douta[4]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(2),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(2),
      I2 => sel_pipe_d1(2),
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(2),
      I4 => sel_pipe_d1(1),
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(2),
      O => \douta[4]_INST_0_i_1_n_0\
    );
\douta[4]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOADO(2),
      I1 => sel_pipe_d1(2),
      I2 => sel_pipe_d1(1),
      I3 => p_7_out(2),
      I4 => sel_pipe_d1(0),
      O => \douta[4]_INST_0_i_2_n_0\
    );
\douta[5]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \douta[5]_INST_0_i_1_n_0\,
      I1 => \douta[5]_INST_0_i_2_n_0\,
      O => douta(5),
      S => sel_pipe_d1(3)
    );
\douta[5]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(3),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(3),
      I2 => sel_pipe_d1(2),
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(3),
      I4 => sel_pipe_d1(1),
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(3),
      O => \douta[5]_INST_0_i_1_n_0\
    );
\douta[5]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOADO(3),
      I1 => sel_pipe_d1(2),
      I2 => sel_pipe_d1(1),
      I3 => p_7_out(3),
      I4 => sel_pipe_d1(0),
      O => \douta[5]_INST_0_i_2_n_0\
    );
\douta[6]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \douta[6]_INST_0_i_1_n_0\,
      I1 => \douta[6]_INST_0_i_2_n_0\,
      O => douta(6),
      S => sel_pipe_d1(3)
    );
\douta[6]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(4),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(4),
      I2 => sel_pipe_d1(2),
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(4),
      I4 => sel_pipe_d1(1),
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(4),
      O => \douta[6]_INST_0_i_1_n_0\
    );
\douta[6]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOADO(4),
      I1 => sel_pipe_d1(2),
      I2 => sel_pipe_d1(1),
      I3 => p_7_out(4),
      I4 => sel_pipe_d1(0),
      O => \douta[6]_INST_0_i_2_n_0\
    );
\douta[7]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \douta[7]_INST_0_i_1_n_0\,
      I1 => \douta[7]_INST_0_i_2_n_0\,
      O => douta(7),
      S => sel_pipe_d1(3)
    );
\douta[7]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(5),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(5),
      I2 => sel_pipe_d1(2),
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(5),
      I4 => sel_pipe_d1(1),
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(5),
      O => \douta[7]_INST_0_i_1_n_0\
    );
\douta[7]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOADO(5),
      I1 => sel_pipe_d1(2),
      I2 => sel_pipe_d1(1),
      I3 => p_7_out(5),
      I4 => sel_pipe_d1(0),
      O => \douta[7]_INST_0_i_2_n_0\
    );
\douta[8]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \douta[8]_INST_0_i_1_n_0\,
      I1 => \douta[8]_INST_0_i_2_n_0\,
      O => douta(8),
      S => sel_pipe_d1(3)
    );
\douta[8]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(6),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(6),
      I2 => sel_pipe_d1(2),
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(6),
      I4 => sel_pipe_d1(1),
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(6),
      O => \douta[8]_INST_0_i_1_n_0\
    );
\douta[8]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOADO(6),
      I1 => sel_pipe_d1(2),
      I2 => sel_pipe_d1(1),
      I3 => p_7_out(6),
      I4 => sel_pipe_d1(0),
      O => \douta[8]_INST_0_i_2_n_0\
    );
\douta[9]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \douta[9]_INST_0_i_1_n_0\,
      I1 => \douta[9]_INST_0_i_2_n_0\,
      O => douta(9),
      S => sel_pipe_d1(3)
    );
\douta[9]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(7),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(7),
      I2 => sel_pipe_d1(2),
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(7),
      I4 => sel_pipe_d1(1),
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(7),
      O => \douta[9]_INST_0_i_1_n_0\
    );
\douta[9]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOADO(7),
      I1 => sel_pipe_d1(2),
      I2 => sel_pipe_d1(1),
      I3 => p_7_out(7),
      I4 => sel_pipe_d1(0),
      O => \douta[9]_INST_0_i_2_n_0\
    );
\no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clka,
      CE => '1',
      D => sel_pipe(0),
      Q => sel_pipe_d1(0),
      R => '0'
    );
\no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clka,
      CE => '1',
      D => sel_pipe(1),
      Q => sel_pipe_d1(1),
      R => '0'
    );
\no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clka,
      CE => '1',
      D => sel_pipe(2),
      Q => sel_pipe_d1(2),
      R => '0'
    );
\no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clka,
      CE => '1',
      D => sel_pipe(3),
      Q => sel_pipe_d1(3),
      R => '0'
    );
\no_softecc_sel_reg.ce_pri.sel_pipe_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clka,
      CE => '1',
      D => addra(0),
      Q => sel_pipe(0),
      R => '0'
    );
\no_softecc_sel_reg.ce_pri.sel_pipe_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clka,
      CE => '1',
      D => addra(1),
      Q => sel_pipe(1),
      R => '0'
    );
\no_softecc_sel_reg.ce_pri.sel_pipe_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clka,
      CE => '1',
      D => addra(2),
      Q => sel_pipe(2),
      R => '0'
    );
\no_softecc_sel_reg.ce_pri.sel_pipe_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clka,
      CE => '1',
      D => addra(3),
      Q => sel_pipe(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_mux__parameterized0\ is
  port (
    doutb : out STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    clkb : in STD_LOGIC;
    DOPBDOP : in STD_LOGIC_VECTOR ( 0 to 0 );
    p_6_out : in STD_LOGIC_VECTOR ( 8 downto 0 );
    DOBDO : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_3\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_4\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_5\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_6\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_1\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_mux__parameterized0\ : entity is "blk_mem_gen_mux";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_mux__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_mux__parameterized0\ is
  signal \doutb[10]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \doutb[10]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \doutb[2]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \doutb[2]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \doutb[3]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \doutb[3]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \doutb[4]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \doutb[4]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \doutb[5]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \doutb[5]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \doutb[6]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \doutb[6]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \doutb[7]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \doutb[7]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \doutb[8]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \doutb[8]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \doutb[9]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \doutb[9]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[0]\ : STD_LOGIC;
  signal \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\ : STD_LOGIC;
  signal \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\ : STD_LOGIC;
  signal \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[3]\ : STD_LOGIC;
  signal \no_softecc_sel_reg.ce_pri.sel_pipe_reg_n_0_[0]\ : STD_LOGIC;
  signal \no_softecc_sel_reg.ce_pri.sel_pipe_reg_n_0_[1]\ : STD_LOGIC;
  signal \no_softecc_sel_reg.ce_pri.sel_pipe_reg_n_0_[2]\ : STD_LOGIC;
  signal \no_softecc_sel_reg.ce_pri.sel_pipe_reg_n_0_[3]\ : STD_LOGIC;
begin
\doutb[0]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F40"
    )
        port map (
      I0 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\(0),
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[3]\,
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_0\(0),
      O => doutb(0)
    );
\doutb[10]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \doutb[10]_INST_0_i_1_n_0\,
      I1 => \doutb[10]_INST_0_i_2_n_0\,
      O => doutb(10),
      S => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[3]\
    );
\doutb[10]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_3\(0),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_4\(0),
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_5\(0),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_6\(0),
      O => \doutb[10]_INST_0_i_1_n_0\
    );
\doutb[10]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOPBDOP(0),
      I1 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I3 => p_6_out(8),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[0]\,
      O => \doutb[10]_INST_0_i_2_n_0\
    );
\doutb[1]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F40"
    )
        port map (
      I0 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\(1),
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[3]\,
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_1\(0),
      O => doutb(1)
    );
\doutb[2]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \doutb[2]_INST_0_i_1_n_0\,
      I1 => \doutb[2]_INST_0_i_2_n_0\,
      O => doutb(2),
      S => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[3]\
    );
\doutb[2]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(0),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(0),
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(0),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(0),
      O => \doutb[2]_INST_0_i_1_n_0\
    );
\doutb[2]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOBDO(0),
      I1 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I3 => p_6_out(0),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[0]\,
      O => \doutb[2]_INST_0_i_2_n_0\
    );
\doutb[3]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \doutb[3]_INST_0_i_1_n_0\,
      I1 => \doutb[3]_INST_0_i_2_n_0\,
      O => doutb(3),
      S => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[3]\
    );
\doutb[3]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(1),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(1),
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(1),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(1),
      O => \doutb[3]_INST_0_i_1_n_0\
    );
\doutb[3]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOBDO(1),
      I1 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I3 => p_6_out(1),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[0]\,
      O => \doutb[3]_INST_0_i_2_n_0\
    );
\doutb[4]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \doutb[4]_INST_0_i_1_n_0\,
      I1 => \doutb[4]_INST_0_i_2_n_0\,
      O => doutb(4),
      S => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[3]\
    );
\doutb[4]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(2),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(2),
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(2),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(2),
      O => \doutb[4]_INST_0_i_1_n_0\
    );
\doutb[4]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOBDO(2),
      I1 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I3 => p_6_out(2),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[0]\,
      O => \doutb[4]_INST_0_i_2_n_0\
    );
\doutb[5]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \doutb[5]_INST_0_i_1_n_0\,
      I1 => \doutb[5]_INST_0_i_2_n_0\,
      O => doutb(5),
      S => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[3]\
    );
\doutb[5]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(3),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(3),
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(3),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(3),
      O => \doutb[5]_INST_0_i_1_n_0\
    );
\doutb[5]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOBDO(3),
      I1 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I3 => p_6_out(3),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[0]\,
      O => \doutb[5]_INST_0_i_2_n_0\
    );
\doutb[6]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \doutb[6]_INST_0_i_1_n_0\,
      I1 => \doutb[6]_INST_0_i_2_n_0\,
      O => doutb(6),
      S => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[3]\
    );
\doutb[6]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(4),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(4),
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(4),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(4),
      O => \doutb[6]_INST_0_i_1_n_0\
    );
\doutb[6]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOBDO(4),
      I1 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I3 => p_6_out(4),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[0]\,
      O => \doutb[6]_INST_0_i_2_n_0\
    );
\doutb[7]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \doutb[7]_INST_0_i_1_n_0\,
      I1 => \doutb[7]_INST_0_i_2_n_0\,
      O => doutb(7),
      S => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[3]\
    );
\doutb[7]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(5),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(5),
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(5),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(5),
      O => \doutb[7]_INST_0_i_1_n_0\
    );
\doutb[7]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOBDO(5),
      I1 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I3 => p_6_out(5),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[0]\,
      O => \doutb[7]_INST_0_i_2_n_0\
    );
\doutb[8]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \doutb[8]_INST_0_i_1_n_0\,
      I1 => \doutb[8]_INST_0_i_2_n_0\,
      O => doutb(8),
      S => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[3]\
    );
\doutb[8]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(6),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(6),
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(6),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(6),
      O => \doutb[8]_INST_0_i_1_n_0\
    );
\doutb[8]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOBDO(6),
      I1 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I3 => p_6_out(6),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[0]\,
      O => \doutb[8]_INST_0_i_2_n_0\
    );
\doutb[9]_INST_0\: unisim.vcomponents.MUXF7
     port map (
      I0 => \doutb[9]_INST_0_i_1_n_0\,
      I1 => \doutb[9]_INST_0_i_2_n_0\,
      O => doutb(9),
      S => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[3]\
    );
\doutb[9]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(7),
      I1 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(7),
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I3 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(7),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I5 => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(7),
      O => \doutb[9]_INST_0_i_1_n_0\
    );
\doutb[9]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02023202"
    )
        port map (
      I0 => DOBDO(7),
      I1 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      I2 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      I3 => p_6_out(7),
      I4 => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[0]\,
      O => \doutb[9]_INST_0_i_2_n_0\
    );
\no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clkb,
      CE => '1',
      D => \no_softecc_sel_reg.ce_pri.sel_pipe_reg_n_0_[0]\,
      Q => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[0]\,
      R => '0'
    );
\no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clkb,
      CE => '1',
      D => \no_softecc_sel_reg.ce_pri.sel_pipe_reg_n_0_[1]\,
      Q => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[1]\,
      R => '0'
    );
\no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clkb,
      CE => '1',
      D => \no_softecc_sel_reg.ce_pri.sel_pipe_reg_n_0_[2]\,
      Q => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[2]\,
      R => '0'
    );
\no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clkb,
      CE => '1',
      D => \no_softecc_sel_reg.ce_pri.sel_pipe_reg_n_0_[3]\,
      Q => \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg_n_0_[3]\,
      R => '0'
    );
\no_softecc_sel_reg.ce_pri.sel_pipe_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clkb,
      CE => '1',
      D => addrb(0),
      Q => \no_softecc_sel_reg.ce_pri.sel_pipe_reg_n_0_[0]\,
      R => '0'
    );
\no_softecc_sel_reg.ce_pri.sel_pipe_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clkb,
      CE => '1',
      D => addrb(1),
      Q => \no_softecc_sel_reg.ce_pri.sel_pipe_reg_n_0_[1]\,
      R => '0'
    );
\no_softecc_sel_reg.ce_pri.sel_pipe_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clkb,
      CE => '1',
      D => addrb(2),
      Q => \no_softecc_sel_reg.ce_pri.sel_pipe_reg_n_0_[2]\,
      R => '0'
    );
\no_softecc_sel_reg.ce_pri.sel_pipe_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clkb,
      CE => '1',
      D => addrb(3),
      Q => \no_softecc_sel_reg.ce_pri.sel_pipe_reg_n_0_[3]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init is
  port (
    \douta[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \doutb[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    \addra[14]\ : in STD_LOGIC;
    \addrb[14]\ : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 13 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 13 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 1,
      DOB_REG => 1,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"FF7FFFFFFF7FFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_01 => X"FEB9F32FFEB9F3AFFF7FBFDFFF7779DFFEEEE6EFFEFDF7EFFEFFFFEFFEFFFFEF",
      INIT_02 => X"FFB5F5BFFFFFFFFFFFEFBEFFFFEF5EFFFFDAEB7FFFEBFAFFFFD7FD7FFF19F31F",
      INIT_03 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF83FFFFFC3BFFFFFFE7FFFFFFFFFFFFCFFE7F",
      INIT_04 => X"FEFFFFFFFEFFFFFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_05 => X"FF03CEFFFE4FCEFFFCC7CEFFFDEDFEFFFDFDBB7FFDFF777FFDFEEFBFFDFFFFBF",
      INIT_06 => X"FFEF8FFFFFEC7FFFFFC8FFFFFFBCE7FFFFAFFBFFFFD7B5FFFF87FCFFFF0AFEFF",
      INIT_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFF4BFFFFFF507FFFFFFFFFFFFFFF7FFFFEFFFFF",
      INIT_08 => X"FFFFFFBFFFFFFFBFFFFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_09 => X"FFB9E07FFFB9F93FFFB9F19FFFBFDBDFFF6EDFDFFF777FDFFEFBBFDFFEFFFFDF",
      INIT_0A => X"FFF8FBFFFFFF1BFFFFFF89FFFFF39EFFFFEFFAFFFFD6F5FFFF9FF0FFFFBFA87F",
      INIT_0B => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFE97FFFFF057FFFFFFFFFFFFF7FFFFFFFFFBFF",
      INIT_0C => X"FF7FFFFFFF7FFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_0D => X"FE9F7FAFFE92BFAFFF6DFFDFFF7FFFDFFEFFFFEFFEFFFFEFFEFFFFEFFEFFFFEF",
      INIT_0E => X"FFE5F4FFFFFFFFFFFFF7FDFFFFFFFFFFFFDBFB7FFFE060FFFFCDF27FFF1FFB1F",
      INIT_0F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF73FFFFF80BFFFFF7FFFFFFEFFEFFFFEFFEFF",
      INIT_10 => X"FE00001FFF00003FFF00003FFF80007FFF80007FFFC000FFFFF003FFFFFC0FFF",
      INIT_11 => X"FC00000FFE00001FFF00003FFF00003FFE00001FFE00001FFE00001FFE00001F",
      INIT_12 => X"FDF7FAFFF9CDEEFFF9F80103FBF0007BF800003BF8000017F8000007F8000007",
      INIT_13 => X"FFFFFFFFFFFFFFFFFFFEEFFFFFF003FFFFE2D1FFFFC0C0FFFE02D097FDE000FF",
      INIT_14 => X"FF00000FFF00000FFF80001FFF80003FFFC0003FFFE0007FFFF001FFFFFC07FF",
      INIT_15 => X"FF00001FFF80003FFF80003FFF00003FFF00003FFF00001FFF00000FFF00000F",
      INIT_16 => X"F800F9FFF806F8FFFC0C787FFC0E003FFE04003FFF00007FFF00003FFF00001F",
      INIT_17 => X"FFFFFFFFFFFFFFFFFFDEF0FFFFDC0FFFFFC001FFFE8014FFF8006CFFF800F0FF",
      INIT_18 => X"FC00003FFC00003FFE00007FFF00007FFF0000FFFF8001FFFFE003FFFFF81FFF",
      INIT_19 => X"FE00003FFF00007FFF00007FFF00003FFF00003FFE00003FFC00003FFC00003F",
      INIT_1A => X"FFE78007FFC7B007FF87180FFF00380FFF00101FFF80003FFF00003FFE00003F",
      INIT_1B => X"FFFFFFFFFFFFFFFFFFC3BEFFFFFC1EFFFFE000FFFFCA005FFFCD0007FFC38007",
      INIT_1C => X"FE00001FFF00003FFF00003FFF80007FFF80007FFFC000FFFFE001FFFFF807FF",
      INIT_1D => X"FC00000FFE00001FFF00003FFF00003FFE00001FFE00001FFE00001FFE00001F",
      INIT_1E => X"FCC60007F9CE0013F9E0001BF8C0003BF8000013F8000007F8000007F8000007",
      INIT_1F => X"FFFFFFFFFFFFFFFFFFFEE7FFFFF083FFFFE001FFFFC001FFFFC010FFFE00004F",
      INIT_20 => X"FF00001FFF00001FFF00001FFF80003FFF80003FFFC0007FFFF001FFFFFC07FF",
      INIT_21 => X"FF80003FFF80003FFF00001FFF00001FFF00001FFF00001FFF00001FFF00001F",
      INIT_22 => X"F8000003FC000003FC000007FC000007FE00000FFE00000FFF00001FFF00001F",
      INIT_23 => X"FFFFFFFFFFFFFFFFFFFE1FFFFFE1E1FFFF9FFE5FF7FC0F9FF7300339F9000011",
      INIT_24 => X"FFC00007FFC00007FFC00007FFE0000FFFE0000FFFF0001FFFF8007FFFFE01FF",
      INIT_25 => X"FE00000FFF00000FFF00000FFF80000FFF800007FFC00007FFC00007FFC00007",
      INIT_26 => X"F260007FEE20007FEE00007FE40000FFF000007FF000003FF800001FFC00000F",
      INIT_27 => X"FFFFFFFFFFFFFFFFFFFE7FFFFFFD8E7FFF9B003FFF66003FFE60003FFDE0003F",
      INIT_28 => X"F80000FFF80000FFF80000FFFC0001FFFC0001FFFE0003FFFF8007FFFFE01FFF",
      INIT_29 => X"FC00001FFC00003FFC00003FFC00007FF800007FF80000FFF80000FFF80000FF",
      INIT_2A => X"FF800193FF80011DFF80001DFFC00009FF800003FF000003FE000007FC00000F",
      INIT_2B => X"FFFFFFFFFFFFFFFFFFFF9FFFFF9C6FFFFF00367FFF0019BFFF00019FFF0001EF",
      INIT_2C => X"FE00003FFE00003FFE00003FFF00007FFF00007FFF8000FFFFE003FFFFF80FFF",
      INIT_2D => X"FF00007FFF00003FFE00003FFE00003FFE00003FFE00003FFE00003FFE00003F",
      INIT_2E => X"E4000047E0000007F0000007F000000FF800000FFC00001FFE00001FFE00003F",
      INIT_2F => X"FFFFFFFFFFFFFFFFFFF1A7FFFC0E5A67F09FFD9FE3FC0F77EEF00663EE400463",
      INIT_30 => X"FF3FFFFFFF3FFFBFFF7FFF3FFF9FFFFFFFBFFE7FFFDFFDFFFFF7FFFFFFFDF7FF",
      INIT_31 => X"FF1B5E3FFF85F07FFF91F27FFF81F07FFF20E13FFF5DF6BFFF3EFF3FFF1FFE7F",
      INIT_32 => X"FD300123F0600163F977B987F88AE897FC140217FC8AAC4FFCE001CFFE42A89F",
      INIT_33 => X"FFFFFFFFFFFFFFFFFFFF67FFFFF807FFFFF877FFFF100787FE033103FC251943",
      INIT_34 => X"FFC37FEFFFC7FFFFFFF7FFDFFFF7FFFFFFFBFFFFFFFFFFFFFFFFFDFFFFFFFFFF",
      INIT_35 => X"FFF89ABFFFF89F1FFFF10F5FFFE5D1FFFFEB819FFFC0209FFFD0D3EFFFC0BFCF",
      INIT_36 => X"FF3D59FFFFB81AFFFFDA19FFFFFA03FFFFF1BFFFFFF751FFFFFBC07FFFF8953F",
      INIT_37 => X"FFFFFFFFFFFFFFFFFFFBFFFFFEF9E0FFFDFB01FFFD7E30FFFF7C05FFFF7801FF",
      INIT_38 => X"FDFFB0FFFFFFF8FFFEFFFBFFFFFFFBFFFFFFF7FFFFFFFFFFFFEFFFFFFFFFFFFF",
      INIT_39 => X"FF5647FFFEA907FFFEBC23FFFFE2E9FFFE6075FFFE4100FFFDF2C2FFFCFF40FF",
      INIT_3A => X"FFE6AF3FFFD6077FFFE616FFFFF017FFFFFF63FFFFE2BBFFFF80F7FFFF2A47FF",
      INIT_3B => X"FFFFFFFFFFFFFFFFFFFFB3BFFFC183DFFFE0306FFFC31B2FFFE80BBFFFE007BF",
      INIT_3C => X"FF3FFFFFFF3FFFBFFF7FFF3FFF9FFFFFFFBFFE7FFFDFFDFFFFF7F7FFFFFDEFFF",
      INIT_3D => X"FF7EFFFFFF02C03FFF03103FFF00DD3FFF07FEBFFF4FFFBFFF1FFF3FFF3FFF7F",
      INIT_3E => X"FFF8C619FFF3F21BFFF7FC33FFFFFE27FFFFFF0FFFFF9F9FFFFE03BFFFFFFDFF",
      INIT_3F => X"FFFFFFFFFFFFFFFFFFF8037FE7F238B7FB7FFFDFFF3E798FFF3C388DFFB81C0D",
      INIT_A => X"00000",
      INIT_B => X"00000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 1,
      READ_WIDTH_B => 1,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 1,
      WRITE_WIDTH_B => 1
    )
        port map (
      ADDRARDADDR(13 downto 0) => addra(13 downto 0),
      ADDRBWRADDR(13 downto 0) => addrb(13 downto 0),
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DIADI(15 downto 0) => B"0000000000000000",
      DIBDI(15 downto 0) => B"0000000000000000",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"00",
      DOADO(15 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOADO_UNCONNECTED\(15 downto 1),
      DOADO(0) => \douta[0]\(0),
      DOBDO(15 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED\(15 downto 1),
      DOBDO(0) => \doutb[0]\(0),
      DOPADOP(1 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOPADOP_UNCONNECTED\(1 downto 0),
      DOPBDOP(1 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED\(1 downto 0),
      ENARDEN => \addra[14]\,
      ENBWREN => \addrb[14]\,
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1 downto 0) => B"00",
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized0\ is
  port (
    \douta[1]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \doutb[1]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    \addra[14]\ : in STD_LOGIC;
    \addrb[14]\ : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 12 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 12 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized0\ : entity is "blk_mem_gen_prim_wrapper_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized0\ is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 15 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 15 downto 2 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 1,
      DOB_REG => 1,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"FFFFF3CC2C2FFFFFFFFFFFF033FFFFFFFFFFFFFCF3FFFFFFFFFFFFFFFFFFFFFF",
      INIT_01 => X"FFFF38AAAA803FFFFFFCE3AAAABF3FFFFFFFF32AAA33FFFFFFFFF2F003EFFFFF",
      INIT_02 => X"FF0003155733020FFFFF3339570F303FFFFCF09A5D80FFFFFFFFC0A7F68A8FFF",
      INIT_03 => X"F280F130CFD2C007F2800F1FFD380003F00004F7F7CC2583FC0001DD5DD00583",
      INIT_04 => X"FFC00008A002803FFF400000030021FFFD0000030200007FFC0034330207001F",
      INIT_05 => X"FC280000A0020003FC00000260000003FF00000980000083FFF0000A0000000F",
      INIT_06 => X"FFC00015000017FFFF000000A000057FFC0A0002800A000FFC28000800098003",
      INIT_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD557F50017FFF",
      INIT_08 => X"FFFFFFFFEFFFFFFFFFFFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_09 => X"FFFFFFFFCAAA57FFFFFFFFF33AAAAFFFFFFFFFC8FAAA8FFFFFFFFFF0BCA8FFFF",
      INIT_0A => X"FFC2A03C7005550FFFC958337AA565FFFFC158CBCAA7E5FFFFF003F0CAA9FFFF",
      INIT_0B => X"FFC0000403C3000FFFFC000137FD50A0FFFC003C4FFDF450FFF000331C075563",
      INIT_0C => X"FF0160009800080FFF0000108000003FFF000010240C00FFFFC000040200C0FF",
      INIT_0D => X"F0000000200980FFF00080028002803FFC0280000000000FFF0160002000020F",
      INIT_0E => X"FFFFD555F557FFFFFF05400010017FFFFC00000000000FFFF0000000000A03FF",
      INIT_0F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_10 => X"FFFFFFFF2FFFFFFFFFFFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_11 => X"FFFD5AAA8FFFFFFFFFFFAAAAB33FFFFFFFFF2AAA3C8FFFFFFFFFF2A3F83FFFFF",
      INIT_12 => X"FC955D03D33000FFFF05550034F0A83FFFF5BDAA8F82543FFFFFF6AA8C3C00FF",
      INIT_13 => X"FF000C3C0040003FFC080157000000FFF0A057FD310003FFF051F7FF44F003FF",
      INIT_14 => X"FF0200025800940FFFC000000810000FFFF00301A010000FFFF030080040003F",
      INIT_15 => X"FFF02600A0000000FFC028000A002000FF00000000002803FF080000A000940F",
      INIT_16 => X"FFFFFD557D557FFFFFFFD4001000150FFFFF000000000003FFFC0A0000000000",
      INIT_17 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_18 => X"FFFFFFF2A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_19 => X"FFFC3CAAAA8C3FFFFFFFFCAAAA8FFFFFFFFFFF2AAA0FFFFFFFFFFF2AAA3FFFFF",
      INIT_1A => X"F09401C000FF000FFC2033000034F03FFF00FC0AA80F0FFFFFFF0C2AAA0CFFFF",
      INIT_1B => X"FD0034203207000FF800E1FCC313C0A3F0000B17FD3C00A3F0960C7FFFD40003",
      INIT_1C => X"FC000000280003FFFF00A000080000FFFFD200302000007FFF4000202000001F",
      INIT_1D => X"F000000008000A0FF000000280000A0FF00000026000000FF00000009800003F",
      INIT_1E => X"FFFF50017F555FFFFFF50000150000FFFF5400028000003FFC000000A000280F",
      INIT_1F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_20 => X"FFFFFFF003FFFFFFFFFFFFC113FFFFFFFFFFFFD51FFFFFFFFFFFFFD557FFFFFF",
      INIT_21 => X"FFFFFC0344FFFFFFFFFFFC0D54FFFFFFFFFFFCF557FFFFFFFFFFFFFFFFFFFFFF",
      INIT_22 => X"FFFFFFC55C0FFFFFFFFFFFF557CFFFFFFFFFFFFFFFFFFFFFFFFFFFF30CFFFFFF",
      INIT_23 => X"FFFFFCD55F7CFFFFFFFFFFFFFFFFFFFFFFFFFFCC33FFFFFFFFFFFFC4700FFFFF",
      INIT_24 => X"FFFFFFFFFFFFFFFFFFFFFFC3F3FFFFFFFFFFFFC4000FFFFFFFFFFFF57FF3FFFF",
      INIT_25 => X"FFFFFF08200FFFFFFFFFFC2599A3FFFFFFFFF2ABFAA8D5FFFFFC022FBBA8693F",
      INIT_26 => X"FFFFFC508AA8FFFFFFF3F1000698FFFFFFC8F00453F80FFFFFFFFFF153FFFFFF",
      INIT_27 => X"FFFFF2690004FCFFFFFF02FC4500F23FFFFFFFFFF155FFFFFFFFFF0F0054FFFF",
      INIT_28 => X"FFFFF20AB200FFFFFFFFF554FFFFFFFFFFFFF1503F0FFFFFFFFFF2AA0053FFFF",
      INIT_29 => X"FFFFFFC953FFFFFFFFFFFF0A800FFFFFFFFFFC80AAA3FFFFFFFFF02AA028FFFF",
      INIT_2A => X"F5D57FF0003FD575F5555551F0155555F555014200054895FD54080AC3C10207",
      INIT_2B => X"FFF17FC80A1EFFFFFFF135C008BBFFFFFFF515CAD283FFFFF7D7FFF0D3FFFD7D",
      INIT_2C => X"FFFFFFB2000504FFFFFFFF08788545FFFFFFFFFFF214FFFFFFFC7FFA32A3FFFF",
      INIT_2D => X"F555055005404051FFFFFC58FFFFCFFFFFFFFF28CABFD3FFFFFFFEDA008014FF",
      INIT_2E => X"FFFD7FC0000F37FFFC3D57040540451FFD45155555555155FD55055415414055",
      INIT_2F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"00000",
      INIT_B => X"00000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 2,
      READ_WIDTH_B => 2,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 2,
      WRITE_WIDTH_B => 2
    )
        port map (
      ADDRARDADDR(13 downto 1) => addra(12 downto 0),
      ADDRARDADDR(0) => '0',
      ADDRBWRADDR(13 downto 1) => addrb(12 downto 0),
      ADDRBWRADDR(0) => '0',
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DIADI(15 downto 0) => B"0000000000000000",
      DIBDI(15 downto 0) => B"0000000000000000",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"00",
      DOADO(15 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOADO_UNCONNECTED\(15 downto 2),
      DOADO(1 downto 0) => \douta[1]\(1 downto 0),
      DOBDO(15 downto 2) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED\(15 downto 2),
      DOBDO(1 downto 0) => \doutb[1]\(1 downto 0),
      DOPADOP(1 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOPADOP_UNCONNECTED\(1 downto 0),
      DOPBDOP(1 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED\(1 downto 0),
      ENARDEN => \addra[14]\,
      ENBWREN => \addrb[14]\,
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1 downto 0) => B"00",
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized1\ is
  port (
    \douta[1]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \doutb[1]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    \addra[14]\ : in STD_LOGIC;
    \addrb[14]\ : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 13 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 13 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized1\ : entity is "blk_mem_gen_prim_wrapper_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized1\ is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 1,
      DOB_REG => 1,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"FF00001FFF00001FFF80003FFFC0007FFFE000FFFFF803FFFFFFFFFFFFFFFFFF",
      INIT_01 => X"FE10010FFE10010FFF0E0E1FFF10011FFE02000FFE00000FFE00000FFE00000F",
      INIT_02 => X"FF8BFA3FFF08021FFF2FBE9FFF9E0F3FFFBC07BFFFC3F87FFFC7FC7FFF00001F",
      INIT_03 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF83FFFFFC03FFFFFEC3FFFFFC03FFFFC3F87F",
      INIT_04 => X"FE00003FFE00003FFF00007FFF8000FFFFC001FFFFF00FFFFFFFFFFFFFFFFFFF",
      INIT_05 => X"FF0000FFFE0080FFFC0080FFFC0072FFFC00807FFC01107FFC00003FFC00003F",
      INIT_06 => X"FFE077FFFFE3A7FFFFC7FBFFFF83E3FFFF981BFFFFCF31FFFF82FCFFFF00FEFF",
      INIT_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFF4BFFFFFF407FFFFF87FFFFFF007FFFFEF87FF",
      INIT_08 => X"FE00003FFE00003FFF00007FFF8000FFFFC001FFFFF807FFFFFFFFFFFFFFFFFF",
      INIT_09 => X"FF80007FFF80803FFF80801FFFA7001FFF00801FFF04401FFE00001FFE00001F",
      INIT_0A => X"FFF703FFFFF2E3FFFFEFF1FFFFE3E0FFFFEC0CFFFFC679FFFF9FA0FFFFBF807F",
      INIT_0B => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFE97FFFFF017FFFFFF0FFFFFF007FFFFF0FBFF",
      INIT_0C => X"FF00001FFF00001FFF80003FFFC0007FFFE000FFFFF803FFFFFFFFFFFFFFFFFF",
      INIT_0D => X"FE00000FFE00000FFF00001FFF00001FFE00000FFE00000FFE00000FFE00000F",
      INIT_0E => X"FFEBFEFFFFC0007FFF83F83FFF8FFE3FFFBC07BFFFC0007FFFC0007FFF00001F",
      INIT_0F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF73FFFFF803FFFFF7C7FFFFED82FFFFE802FF",
      INIT_10 => X"FF06195FFFADE13FFFB3823FFFBC64FFFFD8007FFFEC02FFFFFA0BFFFFFCCFFF",
      INIT_11 => X"FFB8033FFF6C1ABFFFB0037FFF5003BFFE7A155FFE7BF1DFFF5802DFFF2C04DF",
      INIT_12 => X"FC000403FA33F003FE07FD7FFF07F8FFFFF313FFFE9C025FF8E1C947FD6E1B3F",
      INIT_13 => X"FFFFFFFFFFFFFFFFFFFE0FFFFFF0B3FFFFED29FFFFDFFE8FFE1FFA67FC0FFC07",
      INIT_14 => X"FF9A582FFF9D0F8FFFD280DFFFC063BFFFE800FFFFFC02FFFFF30BFFFFFC07FF",
      INIT_15 => X"FFD8CFDFFFBB807FFFDC007FFF54407FFFC5C87FFF8B9CDFFF99818FFF93403F",
      INIT_16 => X"FC7F01FFFCAF00FFFEAE037FFE4F07FFFF6E51FFFF9686FFFF4C23BFFFBF007F",
      INIT_17 => X"FFFFFFFFFFFFFFFFFFC0F0FFFFC000FFFFD2FDFFFE9C0AFFF9710EFFFD6706FF",
      INIT_18 => X"FD072E7FFC7C5E7FFEC0A6FFFF7104FFFFC00DFFFFD01BFFFFF467FFFFF81FFF",
      INIT_19 => X"FF807F7FFF80EF7FFF801EFFFF8116BFFF85D4FFFECEE87FFC60CA7FFF01667F",
      INIT_1A => X"FFE07B8FFFC07D4FFFB03D5FFFF8789FFFE339BFFFD8B67FFF701CBFFEC703BF",
      INIT_1B => X"FFFFFFFFFFFFFFFFFFC380FFFFC000FFFFEFA6FFFFD41A5FFFDC43A7FFD875AF",
      INIT_1C => X"FF64015FFFBC013FFFBC023FFFA004FFFFD0007FFFE802FFFFEA09FFFFF8C7FF",
      INIT_1D => X"FFB3AB3FFF6D5ABFFFB0037FFF61F4BFFEC3F61FFE43FF1FFF473E5FFF460CDF",
      INIT_1E => X"FDF857B7FBE25D7BFBEE9DFFF9E18BFFF9F2EBFFFC90FE5FFEE11D57FD6CF33F",
      INIT_1F => X"FFFFFFFFFFFFFFFFFFFE17FFFFF073FFFFEFBDFFFFDDFFFFFFDCFFFFFE1C77CF",
      INIT_20 => X"FF00001FFF80003FFF80003FFF80003FFFC0007FFFF001FFFFFC07FFFFFFFFFF",
      INIT_21 => X"FFC000FFFFC0007FFF860C3FFF860C3FFF00603FFF00801FFF01901FFF00E01F",
      INIT_22 => X"FF2CF6FFFF197D3FFE31F68FFD771C5FFFFCE73FFEFF11AFFFF804FFFF6001DF",
      INIT_23 => X"FFFFFFFFFFFFFFFFFFFE1FFFFE60019FFE00002FF803F063F8CC0CC7FEB803EF",
      INIT_24 => X"FFC0006FFFE0000FFFE0000FFFF0001FFFF0001FFFF8007FFFFE03FFFFFFFFFF",
      INIT_25 => X"FFB8001FFFF0000FFF60060FFFE0060FFF8000EFFFC0019FFFC0011FFFC0009F",
      INIT_26 => X"F19B9CFFE1D66E7FF1EC837FFB0923FFFE028FFFF90580FFFC06003FFE1C001F",
      INIT_27 => X"FFFFFFFFFFFFFFFFFFFE0E7FFFFC71BFFF80FFBFFF19C0FFFE1F3C3FFC1CE0BF",
      INIT_28 => X"FD8000FFFC0001FFFC0001FFFE0003FFFE0003FFFF8007FFFFF01FFFFFFFFFFF",
      INIT_29 => X"FE00077FFC0003FFFC1801BFFC1801FFFDC0007FFE6000FFFE2000FFFE4000FF",
      INIT_2A => X"FFCE7663FF9D9AE1FFB04DE3FFF12437FFFC501FFFC06827FF00180FFE000E1F",
      INIT_2B => X"FFFFFFFFFFFFFFFFFF9C1FFFFF638FFFFF7FC07FFFC0E63FFF0F3E1FFF41CE0F",
      INIT_2C => X"FE00003FFF00007FFF00007FFF00007FFF8000FFFFE003FFFFF80FFFFFFFFFFF",
      INIT_2D => X"FFFFFFFFFFFFFFFFFFFF18FFFFFE007FFE60003FFE00003FFE00003FFE00003F",
      INIT_2E => X"FAD7F3B7F63E18FFFA6BC567F8E91A9FFFE6064FFD9FC15FFF3FF8DFFE7FFFBF",
      INIT_2F => X"FFFFFFFFFFFFFFFFFFF1A7FFFC000267F0000007E003F08BF10CF99FF1B3FB97",
      INIT_30 => X"FF40007FFF40003FFF4000BFFFA000FFFFA0017FFFD003FFFFF407FFFFFDF7FF",
      INIT_31 => X"FF1C063FFF9E0E7FFFB0037FFFA0017FFF21113FFF6319BFFF4100BFFF6001FF",
      INIT_32 => X"F0AC080BF08800ABF9B8E067F8B3B817FC1A0E17FCCB5CCFFD02A82FFEA1515F",
      INIT_33 => X"FFFFFFFFFFFFFFFFD9FF27E6C8780784C0006000C0001028E0232811E14B1009",
      INIT_34 => X"FFDC800FFFD8001FFFF8003FFFF8003FFFFC003FFFFC007FFFFE03FFFFFFFFFF",
      INIT_35 => X"FFF8FABFFFF820DFFFF1B05FFFE7F23FFFEFE05FFFCFE15FFFDFF32FFFDFC12F",
      INIT_36 => X"FC07B9FFFE00A2FFFE027BFFFF021DFFFF8183FFFFC0D1FFFFE0807FFFF01FBF",
      INIT_37 => X"FFFFFFFFFFFFFFFFFFFCFFFFFE7BE1FFFC7D8BFFFC4A31FFFC081DFFFC0025FF",
      INIT_38 => X"FC004EFFFE0006FFFF0007FFFF0007FFFF000FFFFF800FFFFFF01FFFFFFFFFFF",
      INIT_39 => X"FF57C7FFFE5747FFFE8363FFFF13F9FFFE81FDFFFEA1FCFFFD33FEFFFD20FEFF",
      INIT_3A => X"FFE7780FFFD1401FFFF7901FFFEE103FFFF0607FFFE2C0FFFF8041FFFF7E03FF",
      INIT_3B => X"FFFFFFFFFFFFFFFFFFFF8BBFFFE1939FFFF4680FFFE3100FFFEE000FFFE9000F",
      INIT_3C => X"FF40007FFF40003FFF4000BFFFA000FFFFA0017FFFD003FFFFF40FFFFFFDEFFF",
      INIT_3D => X"FF00203FFF02003FFF0FDC3FFF1FE33FFF3801BFFF7000BFFF6000BFFF4000FF",
      INIT_3E => X"C0000000E0000001E0000001F0000003F0000003F8000007FC00000FFE00001F",
      INIT_3F => X"FFFFFFFFFFFFFFFFFE78035FE6100003E0000000E0000000C0000000C0000000",
      INIT_A => X"00000",
      INIT_B => X"00000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 1,
      READ_WIDTH_B => 1,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 1,
      WRITE_WIDTH_B => 1
    )
        port map (
      ADDRARDADDR(13 downto 0) => addra(13 downto 0),
      ADDRBWRADDR(13 downto 0) => addrb(13 downto 0),
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DIADI(15 downto 0) => B"0000000000000000",
      DIBDI(15 downto 0) => B"0000000000000000",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"00",
      DOADO(15 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOADO_UNCONNECTED\(15 downto 1),
      DOADO(0) => \douta[1]\(0),
      DOBDO(15 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED\(15 downto 1),
      DOBDO(0) => \doutb[1]\(0),
      DOPADOP(1 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOPADOP_UNCONNECTED\(1 downto 0),
      DOPBDOP(1 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED\(1 downto 0),
      ENARDEN => \addra[14]\,
      ENBWREN => \addrb[14]\,
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1 downto 0) => B"00",
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized2\ is
  port (
    \douta[9]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \doutb[9]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \douta[10]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \doutb[10]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized2\ : entity is "blk_mem_gen_prim_wrapper_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized2\ is
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1__3_n_0\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2__3_n_0\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 1,
      DOB_REG => 1,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"FFFFFFDFFFBFFFFFFFBFFFBFFFDFFF7FFFE7FCFFFFF803FFFFFFFFFFFFFFFFFF",
      INITP_01 => X"FE97FDAFFED60D6FFF0E4E1FFFD0C57FFE6674CFFE6CEECFFF7DF7FFFF7FFFFF",
      INITP_02 => X"FF8FFE3FFF7C07DFFF7A2BDFFF9A0B3FFFBFFFBFFFCFFE7FFFDFFF7FFF27FC9F",
      INITP_03 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF83FFFFFC3BFFFFF93DFFFFF3C5FFFFC1107F",
      INITP_04 => X"FFFFDFBFFF7FFFBFFFFFFF7FFFBFFEFFFFCFF9FFFFF00FFFFFFFFFFFFFFFFFFF",
      INITP_05 => X"FF7FBEFFFEABBEFFFD2CB0FFFC3473FFFC6C847FFE6D73FFFEFEE77FFEFFEFFF",
      INITP_06 => X"FFE077FFFFE3BFFFFFC7FFFFFF83BFFFFF981BFFFFC87DFFFFBBFEFFFF75FEFF",
      INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFF4BFFFFFF707FFFFE587FFFFEC77FFFFE007FF",
      INITP_08 => X"FEFDFFFFFEFFFF7FFF7FFFFFFFBFFEFFFFCFF9FFFFF807FFFFFFFFFFFFFFFFFF",
      INITP_09 => X"FFBEFF7FFFBEEABFFF869A5FFFE7161FFF109B1FFFE75B3FFF73BFBFFFFBFFBF",
      INITP_0A => X"FFF703FFFFFEE3FFFFFFF1FFFFFEE0FFFFEC0CFFFFDF09FFFFBFEEFFFFBFD77F",
      INITP_0B => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFE97FFFFF077FFFFF0D3FFFFF71BFFFFF003FF",
      INITP_0C => X"FFFFFFDFFFBFFFFFFFBFFFBFFFDFFF7FFFE7FCFFFFF803FFFFFFFFFFFFFFFFFF",
      INITP_0D => X"FEA0802FFEED50EFFF16B99FFF8DFDFFFE2FFFCFFE2FFFCFFF7FFFFFFF7FFFFF",
      INITP_0E => X"FFEF1AFFFFC4047FFF82083FFF88023FFFBC07BFFFC79C7FFFD20D7FFF20049F",
      INITP_0F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF73FFFFF80BFFFFF039FFFFE244FFFFE484FF",
      INIT_00 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_01 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_02 => X"FFFFFFFFFFFFFFFFFFFFFFFFFF545454545454545454FFFFFFFFFFFFFFFFFFFF",
      INIT_03 => X"FFFFFFFFFFFFFFFFFFFFFF54545C5C5CECECECEC5C5C5454FFFFFFFFFFFFFFFF",
      INIT_04 => X"FFFFFFFFFFFFFFFFFFFF545C5C5CECECECECECECECECEC5C54FFFFFFFFFFFFFF",
      INIT_05 => X"FFFFFFFFFFFFFFFFFF545C5C5CECECECECECECECECECECEC5C54FFFFFFFFFFFF",
      INIT_06 => X"FFFFFFFFFFFFFFFF4C545C5CECECECECECECEC5CECECECECEC5C4CFFFFFFFFFF",
      INIT_07 => X"FFFFFFFFFFFFFFFF4C5C5C5CECEC5CECECECECEC5CECECECEC5C54FFFFFFFFFF",
      INIT_08 => X"FFFFFFFFFFFFFF4C545C5C5CECEC5CECECECECEC5CECEC5CECEC5C4CFFFFFFFF",
      INIT_09 => X"FFFFFFFFFFFFFF4C545C5C5C5CEC545CECECEC5C54ECEC5CECEC5C4CFFFFFFFF",
      INIT_0A => X"FFFFFFFFFFFFFF00545C5C545C5C00545CEC5C544CEC5C54ECEC5400FFFFFFFF",
      INIT_0B => X"FFFFFFFFFFFFFF00545C5C00545CD000545C5C4C005C5400EC5C5400FFFFFFFF",
      INIT_0C => X"FFFFFFFFFFFFFFFF4C5C54D000545D5D4C5C545D5D4C00D0545C4CFFFFFFFFFF",
      INIT_0D => X"FFFFFFFFFFFFFFFF0054545DD0D0D05D5D4C5D5DD0D0D05D545400FFFFFFFFFF",
      INIT_0E => X"FFFFFFFFFFFFFF00654C54D06651515D5D5D5D5D515166D0544C6500FFFFFFFF",
      INIT_0F => X"FFFFFFFFFFFFFF00650054D06651516565656565515166D04C006500FFFFFFFF",
      INIT_10 => X"FFFFFFFFFFFFFFFF00004C5D66515165656565655151665D4C0000FFFFFFFFFF",
      INIT_11 => X"FFFFFFFFFFFFFFFFFFFF0065EEFAFAFAFAFAFAFAFAFAEE6500FFFFFFFFFFFFFF",
      INIT_12 => X"FFFFFFFFFFFFFFFFFFFF900065EEFAFAFAFAFAFAFAEE650090FFFFFFFFFFFFFF",
      INIT_13 => X"FFFFFFFFFFFFFFFFFF9018E4E41865EEC4C4C4EE6518E4E41890FFFFFFFFFFFF",
      INIT_14 => X"FFFFFFFFFFFFFFFFFF909018E470E49000900090E470E4189090FFFFFFFFFFFF",
      INIT_15 => X"FFFFFFFFFFFFFFFF9065FAEED08DE4707000E470E48DD0EEFA6590FFFFFFFFFF",
      INIT_16 => X"FFFFFFFFFFFFFFFF90656565D01A909090909090901AD065656590FFFFFFFFFF",
      INIT_17 => X"FFFFFFFFFFFFFFFFFF009090181A18E4E4E4E4E4181A18909000FFFFFFFFFFFF",
      INIT_18 => X"FFFFFFFFFFFFFFFFFFFF000090668DE4707070E48D66900000FFFFFFFFFFFFFF",
      INIT_19 => X"FFFFFFFFFFFFFFFFFFFFFFFF8D8D1A1A1A1A2F2F2F1A8DFFFFFFFFFFFFFFFFFF",
      INIT_1A => X"FFFFFFFFFFFFFFFFFFFFFFFFFF8D8D1A8D8D65EEEE1A8DFFFFFFFFFFFFFFFFFF",
      INIT_1B => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000065656500FFFFFFFFFFFFFFFFFFFF",
      INIT_1C => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0015151500FFFFFFFFFFFFFFFFFFFF",
      INIT_1D => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_1E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_1F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_20 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_21 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_22 => X"FFFFFFFFFFFFFFFFFFFFFFFF5454545454545454FFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_23 => X"FFFFFFFFFFFFFFFFFFFF54545C5CECECECECEC5C5C5454FFFFFFFFFFFFFFFFFF",
      INIT_24 => X"FFFFFFFFFFFFFFFFFF545CECECECECECECECECECECEC5C54FFFFFFFFFFFFFFFF",
      INIT_25 => X"FFFFFFFFFFFFFFFF4C5CECECECECECECECECECECECECECEC54FFFFFFFFFFFFFF",
      INIT_26 => X"FFFFFFFFFFFFFF4C545CECECECECECECECEC5CECECECECECEC54FFFFFFFFFFFF",
      INIT_27 => X"FFFFFFFFFFFFFF4C5CECECECECECEC5CECEC54ECECECECECEC54FFFFFFFFFFFF",
      INIT_28 => X"FFFFFFFFFFFF4C545CECECECECECEC5CECEC5C545CECECEC5C4CFFFFFFFFFFFF",
      INIT_29 => X"FFFFFFFFFFFF4C545C5CEC5CECEC5C005CECEC00545CECEC544CFFFFFFFFFFFF",
      INIT_2A => X"FFFFFFFFFFFF4C54545CEC54ECEC54D0005CECD00054EC5C4CFFFFFFFFFFFFFF",
      INIT_2B => X"FFFFFFFFFFFF0054545C5C545CEC005DD000545D5D4C545400FFFFFFFFFFFFFF",
      INIT_2C => X"FFFFFFFFFFFF005454545C4C545C005D5DD0D0D05D5DD04CFFFFFFFFFFFFFFFF",
      INIT_2D => X"FFFFFFFFFFFF004C54544C004C5C545DD06651515D5D5D00FFFFFFFFFFFFFFFF",
      INIT_2E => X"FFFFFFFFFFFFFF004C544C0065545C65D066515165656500FFFFFFFFFFFFFFFF",
      INIT_2F => X"FFFFFFFFFFFFFFFF004C4C4CEE4C5C656566515165656500FFFFFFFFFFFFFFFF",
      INIT_30 => X"FFFFFFFFFFFFFFFF004C4C4C444C54EEFAFAFAFAFAFAFA00FFFFFFFFFFFFFFFF",
      INIT_31 => X"FFFFFFFFFFFFFFFFFF004C4C4C44D065FAFAFAFAFAFAEE00FFFFFFFFFFFFFFFF",
      INIT_32 => X"FFFFFFFFFFFFFFFFFFFF00901870707090EEFAFAEE6500FFFFFFFFFFFFFFFFFF",
      INIT_33 => X"FFFFFFFFFFFFFFFFFF009018E4909090909090D0D000FFFFFFFFFFFFFFFFFFFF",
      INIT_34 => X"FFFFFFFFFFFFFFFFFF00909090901818E470D0EEEE65FFFFFFFFFFFFFFFFFFFF",
      INIT_35 => X"FFFFFFFFFFFFFFFFFFFF000090181818E4E4D0FAFA65FFFFFFFFFFFFFFFFFFFF",
      INIT_36 => X"FFFFFFFFFFFFFFFFFFFFFF00909018181890D06565D0FFFFFFFFFFFFFFFFFFFF",
      INIT_37 => X"FFFFFFFFFFFFFFFFFFFFFF00909090906618181890FFFFFFFFFFFFFFFFFFFFFF",
      INIT_38 => X"FFFFFFFFFFFFFFFFFFFFFF008D8D8D8D8D90909090FFFFFFFFFFFFFFFFFFFFFF",
      INIT_39 => X"FFFFFFFFFFFFFFFFFFFFFF8D1A1A2F2F2F1A1A1A00FFFFFFFFFFFFFFFFFFFFFF",
      INIT_3A => X"FFFFFFFFFFFFFFFFFFFFFF8D8D1A2F1A1A8D8D8D8DFFFFFFFFFFFFFFFFFFFFFF",
      INIT_3B => X"FFFFFFFFFFFFFFFFFFFFFFFF000CEE650000000000FFFFFFFFFFFFFFFFFFFFFF",
      INIT_3C => X"FFFFFFFFFFFFFFFFFFFFFFFF000C15150C00FFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_3D => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_3E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_3F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_40 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_41 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_42 => X"FFFFFFFFFFFFFFFFFFFFFFFFFF5454545454545454FFFFFFFFFFFFFFFFFFFFFF",
      INIT_43 => X"FFFFFFFFFFFFFFFFFFFF54545C5CECECECECEC5C5C5454FFFFFFFFFFFFFFFFFF",
      INIT_44 => X"FFFFFFFFFFFFFFFFFF545CECECECECECECECECECECEC5C54FFFFFFFFFFFFFFFF",
      INIT_45 => X"FFFFFFFFFFFFFFFF54ECECECECECECECECECECECECECEC5C4CFFFFFFFFFFFFFF",
      INIT_46 => X"FFFFFFFFFFFFFF54ECECECECECEC5CECECECECECECECEC5C544CFFFFFFFFFFFF",
      INIT_47 => X"FFFFFFFFFFFFFF54ECECECECECEC54ECEC5CECECECECECEC5C4CFFFFFFFFFFFF",
      INIT_48 => X"FFFFFFFFFFFFFF4C5CECECEC5C545CECEC5CECECECECECEC5C544CFFFFFFFFFF",
      INIT_49 => X"FFFFFFFFFFFFFF4C54ECEC5C5400ECEC5C005CECEC5CEC5C5C544CFFFFFFFFFF",
      INIT_4A => X"FFFFFFFFFFFFFFFF4C5CEC5400D0EC5C00D054ECEC54EC5C54544CFFFFFFFFFF",
      INIT_4B => X"FFFFFFFFFFFFFFFF0054544C5D5D5400D05D00EC5C545C5C545400FFFFFFFFFF",
      INIT_4C => X"FFFFFFFFFFFFFFFFFF4CD05D5DD0D0D05D5D005C544C5C54545400FFFFFFFFFF",
      INIT_4D => X"FFFFFFFFFFFFFFFFFF005D5D5D515166D05D545C4C004C54544C00FFFFFFFFFF",
      INIT_4E => X"FFFFFFFFFFFFFFFFFF00656565515166D0655C5465004C544C00FFFFFFFFFFFF",
      INIT_4F => X"FFFFFFFFFFFFFFFFFF0065656551516665655C4CEE4C4C4C00FFFFFFFFFFFFFF",
      INIT_50 => X"FFFFFFFFFFFFFFFFFF00FAFAFAFAFAFAFAEE544C444C4C4C00FFFFFFFFFFFFFF",
      INIT_51 => X"FFFFFFFFFFFFFFFFFF00EEFAFAFAFAFAFA65D0444C4C4C00FFFFFFFFFFFFFFFF",
      INIT_52 => X"FFFFFFFFFFFFFFFFFFFF0065EEFAFAEE90707070189000FFFFFFFFFFFFFFFFFF",
      INIT_53 => X"FFFFFFFFFFFFFFFFFFFFFF00D0D0909090909090E4189000FFFFFFFFFFFFFFFF",
      INIT_54 => X"FFFFFFFFFFFFFFFFFFFFFF65EEEED070E418189090909000FFFFFFFFFFFFFFFF",
      INIT_55 => X"FFFFFFFFFFFFFFFFFFFFFF65FAFAD0E4E4181818900000FFFFFFFFFFFFFFFFFF",
      INIT_56 => X"FFFFFFFFFFFFFFFFFFFFFFD06565D090181818909000FFFFFFFFFFFFFFFFFFFF",
      INIT_57 => X"FFFFFFFFFFFFFFFFFFFFFFFF90181818669090909000FFFFFFFFFFFFFFFFFFFF",
      INIT_58 => X"FFFFFFFFFFFFFFFFFFFFFFFF909090908D8D8D8D8D00FFFFFFFFFFFFFFFFFFFF",
      INIT_59 => X"FFFFFFFFFFFFFFFFFFFFFFFF001A1A1A2F2F2F1A1A8DFFFFFFFFFFFFFFFFFFFF",
      INIT_5A => X"FFFFFFFFFFFFFFFFFFFFFFFF8D8D8D8D1A1A2F1A8D8DFFFFFFFFFFFFFFFFFFFF",
      INIT_5B => X"FFFFFFFFFFFFFFFFFFFFFFFF000000000065EE0C00FFFFFFFFFFFFFFFFFFFFFF",
      INIT_5C => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000C15150C00FFFFFFFFFFFFFFFFFFFFFF",
      INIT_5D => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_5E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_5F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_60 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_61 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_62 => X"FFFFFFFFFFFFFFFFFFFFFFFFFF545454545454545454FFFFFFFFFFFFFFFFFFFF",
      INIT_63 => X"FFFFFFFFFFFFFFFFFFFFFF54545C5C5CECECECEC5C5C5454FFFFFFFFFFFFFFFF",
      INIT_64 => X"FFFFFFFFFFFFFFFFFFFF545C5C5CECECECECECECECECEC5C54FFFFFFFFFFFFFF",
      INIT_65 => X"FFFFFFFFFFFFFFFFFF545C5C5CECECECECECECECECECECEC5C54FFFFFFFFFFFF",
      INIT_66 => X"FFFFFFFFFFFFFFFF4C545C5CECECECECECECECECECECECECEC5C4CFFFFFFFFFF",
      INIT_67 => X"FFFFFFFFFFFFFFFF4C5C5C5CECEC5CECECECECECECECECECEC5C54FFFFFFFFFF",
      INIT_68 => X"FFFFFFFFFFFFFF4C545C5C5CECEC5CECECECECECECECECECECEC5C4CFFFFFFFF",
      INIT_69 => X"FFFFFFFFFFFFFF4C545C5C5C5CEC5C5CECECECECECECECECECEC5C4CFFFFFFFF",
      INIT_6A => X"FFFFFFFFFFFFFF0054545C545C5C5C5C5CEC5CECECECECECECEC5400FFFFFFFF",
      INIT_6B => X"FFFFFFFFFFFFFF0054545C545C5C5C5C5C5C5CECECEC5C5C5C5C5400FFFFFFFF",
      INIT_6C => X"FFFFFFFFFFFFFFFF4C5454545C5C545C5C5C5CECEC5C545C5C5C4CFFFFFFFFFF",
      INIT_6D => X"FFFFFFFFFFFFFFFF0054544C545C4C545C545C5C5C54545C5C5400FFFFFFFFFF",
      INIT_6E => X"FFFFFFFFFFFFFF00654C4C544C4C544C544C545C545454545C4C6500FFFFFFFF",
      INIT_6F => X"FFFFFFFFFFFFFF0065004C54545454544C5454545454545454006500FFFFFFFF",
      INIT_70 => X"FFFFFFFFFFFFFFFF00004C545454545454545454544C54544C0000FFFFFFFFFF",
      INIT_71 => X"FFFFFFFFFFFFFFFFFFFF004C54544C54545454544C4C544C00FFFFFFFFFFFFFF",
      INIT_72 => X"FFFFFFFFFFFFFFFFFFFF9000004C4C4C4C54544C4C4C000090FFFFFFFFFFFFFF",
      INIT_73 => X"FFFFFFFFFFFFFFFFFF9018E4E4189090909090909018E4E41890FFFFFFFFFFFF",
      INIT_74 => X"FFFFFFFFFFFFFFFFFF909090E4707070707070707070E4909090FFFFFFFFFFFF",
      INIT_75 => X"FFFFFFFFFFFFFFFFFF9090900090E47070707070E49000909090FFFFFFFFFFFF",
      INIT_76 => X"FFFFFFFFFFFFFFFFFFFF9090901A909090909090901A909090FFFFFFFFFFFFFF",
      INIT_77 => X"FFFFFFFFFFFFFFFFFFFFFF00181A18E4707070E4188D1800FFFFFFFFFFFFFFFF",
      INIT_78 => X"FFFFFFFFFFFFFFFFFFFFFF008D1A2F2F1A2F2F2F2F1A8D00FFFFFFFFFFFFFFFF",
      INIT_79 => X"FFFFFFFFFFFFFFFFFFFFFF008D8D1A8D8D1A2F2F2F1A8D00FFFFFFFFFFFFFFFF",
      INIT_7A => X"FFFFFFFFFFFFFFFFFFFFFFFF008D8D8D8D8D65651A8D8DFFFFFFFFFFFFFFFFFF",
      INIT_7B => X"FFFFFFFFFFFFFFFFFFFFFFFFFF000000001515156500FFFFFFFFFFFFFFFFFFFF",
      INIT_7C => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000C0C0C1500FFFFFFFFFFFFFFFFFFFF",
      INIT_7D => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_7E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_7F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 3) => addra(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 3) => addrb(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 0) => B"00000000000000000000000000000000",
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 8) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 8),
      DOADO(7 downto 0) => \douta[9]\(7 downto 0),
      DOBDO(31 downto 8) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 8),
      DOBDO(7 downto 0) => \doutb[9]\(7 downto 0),
      DOPADOP(3 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 1),
      DOPADOP(0) => \douta[10]\(0),
      DOPBDOP(3 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 1),
      DOPBDOP(0) => \doutb[10]\(0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1__3_n_0\,
      ENBWREN => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2__3_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3 downto 0) => B"0000",
      WEBWE(7 downto 0) => B"00000000"
    );
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => addra(13),
      I1 => addra(14),
      I2 => addra(12),
      O => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1__3_n_0\
    );
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => addrb(13),
      I1 => addrb(14),
      I2 => addrb(12),
      O => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2__3_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized3\ is
  port (
    \douta[9]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \doutb[9]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \douta[10]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \doutb[10]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized3\ : entity is "blk_mem_gen_prim_wrapper_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized3\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized3\ is
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1__1_n_0\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2__1_n_0\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 1,
      DOB_REG => 1,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"FFFE197FFFFFE17FFFFF827FFFFFE4FFFFF800FFFFFC03FFFFFE0FFFFFFFFFFF",
      INITP_01 => X"FFF803FFFFFC1EFFFFF003FFFF7003BFFEFE1DDFFEFBF1DFFFF802FFFFFC04FF",
      INITP_02 => X"FD0330E3F947F88BF8C21103FB37F87BF83F1F3BF8FC03D7F9FFFF67FFFE1FFF",
      INITP_03 => X"FFFFFFFFFFFFFFFFFFFEAFFFFFF0F3FFFFE925FFFFDAD6EFFE1AD617FC000017",
      INITP_04 => X"FFFE783FFFFF0FFFFFF380FFFFF063FFFFF800FFFFFC03FFFFFF0FFFFFFFFFFF",
      INITP_05 => X"FFF8FFFFFFBF807FFFFC007FFF7FC07FFFFFF87FFFFF9FDFFFFF81EFFFFFC03F",
      INITP_06 => X"FF6049FFFFA680FFFFAC2B7FFFCE07FFFFE471FFFFF387FFFF7C3FBFFFFF007F",
      INITP_07 => X"FFFFFFFFFFFFFFFFFFC8F0FFFFC006FFFFDE01FFFE9E0AFFF9584CFFFF4024FF",
      INITP_08 => X"FF073FFFFFFC7FFFFFC0E7FFFFF107FFFFC00FFFFFF01FFFFFFC7FFFFFFFFFFF",
      INITP_09 => X"FF807FFFFF80FF7FFF801FFFFF81FFBFFF87FFFFFEFEFFFFFDE0FFFFFF01FFFF",
      INITP_0A => X"FFE501BFFFC0B17FFFB4197FFFF838FFFFE311FFFFF8E7FFFF7E1FBFFEC7FFBF",
      INITP_0B => X"FFFFFFFFFFFFFFFFFFC38CFFFFD800FFFFE03EFFFFD43E5FFFCD0EA7FFC804BF",
      INITP_0C => X"FFFC017FFFFC017FFFFC027FFFE004FFFFF000FFFFF803FFFFEE0DFFFFFBF7FF",
      INITP_0D => X"FFF3BFFFFFFFFEFFFFFFFFFFFF7FFFBFFEFFFFDFFEFFFFDFFFFF3EFFFFFE0CFF",
      INITP_0E => X"FCC2D087F9CED013F9E0901BF8C1813BF83EE113FCF0EBC7FFFFCB77FFFCD7FF",
      INITP_0F => X"FFFFFFFFFFFFFFFFFFFEE7FFFFF013FFFFEBDDFFFFD9DFFFFFD9DBFFFE19C1CF",
      INIT_00 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF5151D9D95151FFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_01 => X"FFFFFFFFFFFFFFFFFFFFFFFF0851D96262626262D951FFFFFFFFFFFFFFFFFFFF",
      INIT_02 => X"FFFFFFFFFFFFFFFFFFFF0851D9D96262626262626262D951FFFFFFFFFFFFFFFF",
      INIT_03 => X"FFFFFFFFFFFFFFFFFF0851D9D9626262626262626262626251FFFFFFFFFFFFFF",
      INIT_04 => X"FFFFFFFFFFFFFFFFFF51D9D9D9D9515151D9D96262D96262D9FFFFFFFFFFFFFF",
      INIT_05 => X"FFFFFFFFFFFFFFFF0851D9D95151D9D9D96262626262D9626251FFFFFFFFFFFF",
      INIT_06 => X"FFFFFFFFFFFFFFFF0851D951D9D951D9D9D9D962626262D96251FFFFFFFFFFFF",
      INIT_07 => X"FFFFFFFFFFFFFF085151515151D9D962626262D9D96262D962D951FFFFFFFFFF",
      INIT_08 => X"FFFFFFFFFFFFFF0851510851D9D962626262626262D96262D9D951FFFFFFFFFF",
      INIT_09 => X"FFFFFFFFFFFFFF08510851D9D9626262626262626262D962D9D951FFFFFFFFFF",
      INIT_0A => X"FFFFFFFFFFFFFF005108D9D9D962D9D9D9D9D9D9626262D9D9D900FFFFFFFFFF",
      INIT_0B => X"FFFFFFFFFFFFFF005108D9D9D95108000000000851D962D951D900FFFFFFFFFF",
      INIT_0C => X"FFFFFFFFFFFFFFFF000851080000000000000000000008D90800FFFFFFFFFFFF",
      INIT_0D => X"FFFFFFFFFFFFFFFF085108080000000000000000000008085108FFFFFFFFFFFF",
      INIT_0E => X"FFFFFFFFFFFFFF0851D9D95108080000000000080851D962D95108FFFFFFFFFF",
      INIT_0F => X"FFFFFFFFFFFF08080851D9D9D9626262626262626262D9D951510808FFFFFFFF",
      INIT_10 => X"FFFFFFFFFF0851D951080851D9D9D962626262D9D95108085151D9D908FFFFFF",
      INIT_11 => X"FFFFFFFFFF000051D9D9D951515151D9D9D95151085151D962D9510000FFFFFF",
      INIT_12 => X"FFFFFFFFFF8C8C00085151D9D9D96262626262626262D9515108000C8CFFFFFF",
      INIT_13 => X"FFFFFFFFFF8C8C8C8C8C08085151D9D9626262D9515108088C8C0C0C0C8CFFFF",
      INIT_14 => X"FFFFFFFFFF8C0C0C54C890900008080808080808080000008C0C0C0C0C8CFFFF",
      INIT_15 => X"FFFFFFFFFF8C8C5490905454C88C598C8C8C8C598C8C00FF008C8C8C8C8CFFFF",
      INIT_16 => X"FFFFFFFFFF008C90C890DCDCC890590C0C0C0C5990C8C8009054545490C8FFFF",
      INIT_17 => X"FFFFFFFFFFFF0090C8C8545400C8909054549090C88CC800909090C854C8FFFF",
      INIT_18 => X"FFFFFFFFFFFF00C8C8C8C8008C8C8C8C8C8C8C8C8C8C0000C8545490C8FFFFFF",
      INIT_19 => X"FFFFFFFFFFFFFF0000000008088C0C8C0C0C8C0C8C510800C8DCDC9000FFFFFF",
      INIT_1A => X"FFFFFFFFFFFFFFFFFFFF0008088C598C0C0C8C598CD90800FF9090C8FFFFFFFF",
      INIT_1B => X"FFFFFFFFFFFFFFFFFFFFFF00088C2659262659268C5100FFFFFFFFFFFFFFFFFF",
      INIT_1C => X"FFFFFFFFFFFFFFFFFFFFFFFF00000000085108080000FFFFFFFFFFFFFFFFFFFF",
      INIT_1D => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0090549000FFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_1E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_1F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_20 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF51515151515151FFFFFFFFFFFFFFFFFFFFFF",
      INIT_21 => X"FFFFFFFFFFFFFFFFFFFFFFFF5151D9D962626262D95108FFFFFFFFFFFFFFFFFF",
      INIT_22 => X"FFFFFFFFFFFFFFFFFFFFFF08D9D96262626262626262D95108FFFFFFFFFFFFFF",
      INIT_23 => X"FFFFFFFFFFFFFFFFFFFF0851D96262626262626262626262D908FFFFFFFFFFFF",
      INIT_24 => X"FFFFFFFFFFFFFFFFFF0851516262626262D9D9626262D9D9D951FFFFFFFFFFFF",
      INIT_25 => X"FFFFFFFFFFFFFFFFFF0851D96262D951D962626262626262D9D951FFFFFFFFFF",
      INIT_26 => X"FFFFFFFFFFFFFFFF085151D9D9D951D962626262D9D9D9D9D9515151FFFFFFFF",
      INIT_27 => X"FFFFFFFFFFFFFFFF085151D9D951D96262D951D9D96262626262D951FFFFFFFF",
      INIT_28 => X"FFFFFFFFFFFFFFFF085151D95151D9D951D96262626262626262D908FFFFFFFF",
      INIT_29 => X"FFFFFFFFFFFFFFFF085151D908515108D9626262626262D9D9515100FFFFFFFF",
      INIT_2A => X"FFFFFFFFFFFFFFFF08515151085108D9D96262D9D9D95151080800FFFFFFFFFF",
      INIT_2B => X"FFFFFFFFFFFFFFFF08085151510851D9D9D95151080000000008FFFFFFFFFFFF",
      INIT_2C => X"FFFFFFFFFFFFFFFF000851085108515151080000000000000008FFFFFFFFFFFF",
      INIT_2D => X"FFFFFFFFFFFFFFFFFF0851080808000000000000000000000008FFFFFFFFFFFF",
      INIT_2E => X"FFFFFFFFFFFFFFFFFF0008080851080808000000000000000008FFFFFFFFFFFF",
      INIT_2F => X"FFFFFFFFFFFFFFFF080851D9D9626262D9D951510808080808D951FFFFFFFFFF",
      INIT_30 => X"FFFFFFFFFFFFFFFF0851D9D9D9D9D9D9626262626262626262D908FFFFFFFFFF",
      INIT_31 => X"FFFFFFFFFFFFFFFF00085151D9D962626262D951515108080800FFFFFFFFFFFF",
      INIT_32 => X"FFFFFFFFFFFFFFFF08515108008C0851D962626262D9D95108FFFFFFFFFFFFFF",
      INIT_33 => X"FFFFFFFFFFFFFF08510808008C0C8C00000851D9626262D9D908FFFFFFFFFFFF",
      INIT_34 => X"FFFFFFFFFFFF0851510800000C0C0C8C000000000008D9D9D908FFFFFFFFFFFF",
      INIT_35 => X"FFFFFFFFFFFF0851080008000C0C8C0000C890549000080800FFFFFFFFFFFFFF",
      INIT_36 => X"FFFFFFFFFF085151080008008C0C0C8C90C8545454000000FFFFFFFFFFFFFFFF",
      INIT_37 => X"FFFFFFFFFF0851510008088C8C8C8C8C5490C854900000FFFFFFFFFFFFFFFFFF",
      INIT_38 => X"FFFFFFFFFF08510800088C00008C8C8CC85490C800598C00FFFFFFFFFFFFFFFF",
      INIT_39 => X"FFFFFFFFFF00000800088C085100008C0090C8000C0C8C00FFFFFFFFFFFFFFFF",
      INIT_3A => X"FFFFFFFFFFFFFF00FF000008D9D951000000002659265900FFFFFFFFFFFFFFFF",
      INIT_3B => X"FFFFFFFFFFFFFFFFFFFF0008515108008C8C8C8C8C8C00FFFFFFFFFFFFFFFFFF",
      INIT_3C => X"FFFFFFFFFFFFFFFFFFFF00C8C8C8000000000000C89090C8FFFFFFFFFFFFFFFF",
      INIT_3D => X"FFFFFFFFFFFFFFFFFFFF00C890545400FFFFFFFF00000000FFFFFFFFFFFFFFFF",
      INIT_3E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_3F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_40 => X"FFFFFFFFFFFFFFFFFFFFFFFFFF515151515151FFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_41 => X"FFFFFFFFFFFFFFFFFFFFFF0851D9626262D9D9515108FFFFFFFFFFFFFFFFFFFF",
      INIT_42 => X"FFFFFFFFFFFFFFFFFF0851D962626262626262D9D95108FFFFFFFFFFFFFFFFFF",
      INIT_43 => X"FFFFFFFFFFFFFFFF08D962626262626262626262D9D95108FFFFFFFFFFFFFFFF",
      INIT_44 => X"FFFFFFFFFFFFFFFF51D9D9D9626262D96262626262D9515108FFFFFFFFFFFFFF",
      INIT_45 => X"FFFFFFFFFFFFFF51D9D9626262626262D951D96262D9D95108FFFFFFFFFFFFFF",
      INIT_46 => X"FFFFFFFFFFFF515151D9D9D9D9D9626262D951D9D9D9D9515108FFFFFFFFFFFF",
      INIT_47 => X"FFFFFFFFFFFF51D96262626262D9D9D96262D951D9D9D9515108FFFFFFFFFFFF",
      INIT_48 => X"FFFFFFFFFFFF08D962626262626262D951D9D95151D9D9515108FFFFFFFFFFFF",
      INIT_49 => X"FFFFFFFFFFFF005151D9D96262626262D90851510851D9515108FFFFFFFFFFFF",
      INIT_4A => X"FFFFFFFFFFFFFF0008085151D9D9D962D9D90851085151515108FFFFFFFFFFFF",
      INIT_4B => X"FFFFFFFFFFFFFFFF08000000000851D9D9D95108510851510808FFFFFFFFFFFF",
      INIT_4C => X"FFFFFFFFFFFFFFFF080000000000000851515108510808510800FFFFFFFFFFFF",
      INIT_4D => X"FFFFFFFFFFFFFFFF0800000000000000000000080808085108FFFFFFFFFFFFFF",
      INIT_4E => X"FFFFFFFFFFFFFFFF0800000000000000080808510808080800FFFFFFFFFFFFFF",
      INIT_4F => X"FFFFFFFFFFFFFF08D96262626262626262D9D9D9D9D9D9D95108FFFFFFFFFFFF",
      INIT_50 => X"FFFFFFFFFFFFFF00D9D9626262D9D9D951515151515108080800FFFFFFFFFFFF",
      INIT_51 => X"FFFFFFFFFFFFFFFF0008080851515162626262D9D9D951510800FFFFFFFFFFFF",
      INIT_52 => X"FFFFFFFFFFFFFFFFFF0851D9D9626262D951088C000808515108FFFFFFFFFFFF",
      INIT_53 => X"FFFFFFFFFFFFFFFF08D9D9626262D90800008C0C8C000008085108FFFFFFFFFF",
      INIT_54 => X"FFFFFFFFFFFFFFFF08D9D9D908000000008C0C0C0C00000008515108FFFFFFFF",
      INIT_55 => X"FFFFFFFFFFFFFFFFFF000808009054C800008C0C0C8C000800085108FFFFFFFF",
      INIT_56 => X"FFFFFFFFFFFFFFFFFFFF0000005454C8908C0C0C8C8C00080008515108FFFFFF",
      INIT_57 => X"FFFFFFFFFFFFFFFFFFFFFF0000905490548C8C8C8C008C080800515108FFFFFF",
      INIT_58 => X"FFFFFFFFFFFFFFFFFFFF008C5900C854C88C8C8C0008008C0800085108FFFFFF",
      INIT_59 => X"FFFFFFFFFFFFFFFFFFFF008C0C0C0090008C00005151088C0800080000FFFFFF",
      INIT_5A => X"FFFFFFFFFFFFFFFFFFFF005926592600000051D9D951080000FF00FFFFFFFFFF",
      INIT_5B => X"FFFFFFFFFFFFFFFFFFFFFF008C8C8C8C8C00085151080800FFFFFFFFFFFFFFFF",
      INIT_5C => X"FFFFFFFFFFFFFFFFFFFFC89090C80000000000C8C8C8C800FFFFFFFFFFFFFFFF",
      INIT_5D => X"FFFFFFFFFFFFFFFFFFFF00000000FFFFFF0054549090C800FFFFFFFFFFFFFFFF",
      INIT_5E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_5F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_60 => X"FFFFFFFFFFFFFFFFFFFFFFFFFF005151D9D9515100FFFFFFFFFFFFFFFFFFFFFF",
      INIT_61 => X"FFFFFFFFFFFFFFFFFFFFFF000851D96262626262D95100FFFFFFFFFFFFFFFFFF",
      INIT_62 => X"FFFFFFFFFFFFFFFFFFFF0851D9626262626262626262D951FFFFFFFFFFFFFFFF",
      INIT_63 => X"FFFFFFFFFFFFFFFFFF0851D962626262626262626262626251FFFFFFFFFFFFFF",
      INIT_64 => X"FFFFFFFFFFFFFFFFFF51D962626262626262626262D96262D9FFFFFFFFFFFFFF",
      INIT_65 => X"FFFFFFFFFFFFFFFF0851D9D9D9D96262626262626262D9626251FFFFFFFFFFFF",
      INIT_66 => X"FFFFFFFFFFFFFFFF0851D9D9D9D9626262626262626262D96251FFFFFFFFFFFF",
      INIT_67 => X"FFFFFFFFFFFFFF0851D9D95151D9626262626262626262D962D951FFFFFFFFFF",
      INIT_68 => X"FFFFFFFFFFFFFF0851D9515151D9D96262626262D9D96262D9D951FFFFFFFFFF",
      INIT_69 => X"FFFFFFFFFFFFFF085108515151D9D9D96262D9D9D9D9D96251D951FFFFFFFFFF",
      INIT_6A => X"FFFFFFFFFFFFFF00510851515151D9D9D9D9D9D9D9D9D9D9515100FFFFFFFFFF",
      INIT_6B => X"FFFFFFFFFFFFFF00080851515151D9D9D9D9D9D951D9D951515100FFFFFFFFFF",
      INIT_6C => X"FFFFFFFFFFFFFFFF00080851515151D9D9D9D9D951D951510800FFFFFFFFFFFF",
      INIT_6D => X"FFFFFFFFFFFFFFFF085108085151515151515151515108085108FFFFFFFFFFFF",
      INIT_6E => X"FFFFFFFFFFFFFF0851D9D9510808510851D951D90851D962D95108FFFFFFFFFF",
      INIT_6F => X"FFFFFFFFFFFF08080851D9D96262D9D90862D951D951D9D951510808FFFFFFFF",
      INIT_70 => X"FFFFFFFFFF0851D951080851D9D96262D908EAD9625108085151D9D908FFFFFF",
      INIT_71 => X"FFFFFFFFFF080851D9D9D951515151D9515162EAD9EA51D962D9510800FFFFFF",
      INIT_72 => X"FFFFFFFFFF080000085151D962626262D9D908EAD9EAD9515108008C8CFFFFFF",
      INIT_73 => X"FFFFFFFFFF00008C8C8C08085151D962D9D90862EA62EA088C8C8C0C8C8CFFFF",
      INIT_74 => X"FFFFFFFFFF00008C0C0C8C000000000808000062EA62EAD98C8C0C0C0C8CFFFF",
      INIT_75 => X"FFFFFFFFFF008C0C0C0C0C008C8C8C0008000008EAEA62EA8C8C8C0C0C8CFFFF",
      INIT_76 => X"FFFFFFFFFF008C0C0C0C8C0090900C0051080008EAEA62EA008C8C0C8C00FFFF",
      INIT_77 => X"FFFFFFFFFFFF008C0C0C8C8C8CC890005108000862EAEAEAD9008C8C00FFFFFF",
      INIT_78 => X"FFFFFFFFFFFFFF0000000008088C005151D98C8C62EAEAD9D9FF0000FFFFFFFF",
      INIT_79 => X"FFFFFFFFFFFFFFFFFFFF0008088C0051D9D98C0C08EAD908FFFFFFFFFFFFFFFF",
      INIT_7A => X"FFFFFFFFFFFFFFFFFFFF0008088C0008D9D98C5908D908FFFFFFFFFFFFFFFFFF",
      INIT_7B => X"FFFFFFFFFFFFFFFFFFFFFF00088C595908518C59080800FFFFFFFFFFFFFFFFFF",
      INIT_7C => X"FFFFFFFFFFFFFFFFFFFFFFFF0000000054DCDC080000FFFFFFFFFFFFFFFFFFFF",
      INIT_7D => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00909090DC00FFFFFFFFFFFFFFFFFFFFFF",
      INIT_7E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_7F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 3) => addra(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 3) => addrb(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 0) => B"00000000000000000000000000000000",
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 8) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 8),
      DOADO(7 downto 0) => \douta[9]\(7 downto 0),
      DOBDO(31 downto 8) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 8),
      DOBDO(7 downto 0) => \doutb[9]\(7 downto 0),
      DOPADOP(3 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 1),
      DOPADOP(0) => \douta[10]\(0),
      DOPBDOP(3 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 1),
      DOPBDOP(0) => \doutb[10]\(0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1__1_n_0\,
      ENBWREN => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2__1_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3 downto 0) => B"0000",
      WEBWE(7 downto 0) => B"00000000"
    );
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => addra(13),
      I1 => addra(14),
      I2 => addra(12),
      O => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1__1_n_0\
    );
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => addrb(13),
      I1 => addrb(14),
      I2 => addrb(12),
      O => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2__1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized4\ is
  port (
    \douta[9]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \doutb[9]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \douta[10]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \doutb[10]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized4\ : entity is "blk_mem_gen_prim_wrapper_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized4\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized4\ is
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1_n_0\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2_n_0\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 1,
      DOB_REG => 1,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"FF00001FFF00001FFF00001FFF80003FFF80003FFFC0007FFFF001FFFFFC07FF",
      INITP_01 => X"FF80003FFF80003FFF060C1FFF060C1FFF09121FFF0F7E1FFF066C1FFF00001F",
      INITP_02 => X"FBF3F9FFFFE79EFBFDCE0F77FC8803A7FE0000CFFE00084FFF00021FFF00001F",
      INITP_03 => X"FFFFFFFFFFFFFFFFFFFE1FFFFE60019FFE00002FF803F063F8CFFCC7FEFFFFED",
      INITP_04 => X"FFC00007FFC00007FFC00007FFE0000FFFE0000FFFF0001FFFF8007FFFFE01FF",
      INITP_05 => X"FE40000FFF00000FFF00060FFF80060FFF800907FFC00E67FFC000E7FFC00067",
      INITP_06 => X"F19FE37FE1DF917FF1FF7C7FFBFEC0FFFFFD007FF7FA003FFBF8001FFDE0000F",
      INITP_07 => X"FFFFFFFFFFFFFFFFFFFE0E7FFFFC71BFFF80FFFFFF19FFBFFE1FFFBFFC1FFF3F",
      INITP_08 => X"F80000FFF80000FFF80000FFFC0001FFFC0001FFFE0003FFFF8007FFFFE01FFF",
      INITP_09 => X"FC00009FFC00003FFC18003FFC18007FF824007FF99C00FFF9C000FFF98000FF",
      INITP_0A => X"FFB1FE63FFA27EE1FF8FBFE3FFC0DFF7FF802FFFFF0017FBFE0007F7FC0001EF",
      INITP_0B => X"FFFFFFFFFFFFFFFFFF9C1FFFFF638FFFFFFFC07FFF7FE63FFF7FFE1FFF3FFE0F",
      INITP_0C => X"FE00003FFE00003FFE00003FFF00007FFF00007FFF8000FFFFE003FFFFF80FFF",
      INITP_0D => X"FF00007FFF00003FFE00003FFE00003FFE00003FFE00003FFE00003FFE00003F",
      INITP_0E => X"FBEFFFBFEFCFFFF7F79C3EF7F710E76FF801F9AFFC003E9FFE00071FFE00003F",
      INITP_0F => X"FFFFFFFFFFFFFFFFFFF1A7FFFC000267F0000007E003F08BF10FF99BF1BFFB9B",
      INIT_00 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C8C8C8C8CFFFFFFFFFFFFFFFFFFFFFF",
      INIT_01 => X"FFFFFFFFFFFFFFFFFFFFFFFF8C8C000000000000008C8CFFFFFFFFFFFFFFFFFF",
      INIT_02 => X"FFFFFFFFFFFFFFFFFFFF8C8C00000000000000000000008C8CFFFFFFFFFFFFFF",
      INIT_03 => X"FFFFFFFFFFFFFFFFFF8C0000000000000000000000000000008CFFFFFFFFFFFF",
      INIT_04 => X"FFFFFFFFFFFFFFFFFF0000000000000000000000000000000000FFFFFFFFFFFF",
      INIT_05 => X"FFFFFFFFFFFFFFFF8C00000000000000000000000000000000008CFFFFFFFFFF",
      INIT_06 => X"FFFFFFFFFFFFFFFF8C00000000000000000000000000000000008CFFFFFFFFFF",
      INIT_07 => X"FFFFFFFFFFFFFFFF00000000000000000000000000000000000000FFFFFFFFFF",
      INIT_08 => X"FFFFFFFFFFFFFFFF00000000000000008C8C8C0000000000000000FFFFFFFFFF",
      INIT_09 => X"FFFFFFFFFFFFFFFF000000000044448C8C11118C44440000000000FFFFFFFFFF",
      INIT_0A => X"FFFFFFFFFFFFFFFF00000000444444448C11114444444400000000FFFFFFFFFF",
      INIT_0B => X"FFFFFFFFFFFFFFFF0000000044484844008C8C444848440000008CFFFFFFFFFF",
      INIT_0C => X"FFFFFFFFFFFFFFFF8C00000048D9D94800000048D9D9480000008CFFFFFFFFFF",
      INIT_0D => X"FFFFFFFFFFFFFFFF8C00000048D9D94800000048D9D9480000008CFFFFFFFFFF",
      INIT_0E => X"FFFFFFFFFFFFFFFFFF8C0000000000000000000000000000008CFFFFFFFFFFFF",
      INIT_0F => X"FFFFFFFFFFFFFFFFFF8C00000000000000000000000000008C8CFFFFFFFFFFFF",
      INIT_10 => X"FFFFFFFFFFFFFFFF008C8C0000000000000000000000008C8C8C00FFFFFFFFFF",
      INIT_11 => X"FFFFFFFFFFFFFFFF8C8C8C8C8C00000000000000008C11008C8C8CFFFFFFFFFF",
      INIT_12 => X"FFFFFFFFFFFFFF008C8C8C8C8C8C8C8C0000008C1100008C8C118C00FFFFFFFF",
      INIT_13 => X"FFFFFFFFFFFFFF8C8C8C8C8C8C8C00008C8C8C00008C8C8C11118C8CFFFFFFFF",
      INIT_14 => X"FFFFFFFFFFFF008C118C8C8C118C8C8C0000008C8C8C1111118C118C8CFFFFFF",
      INIT_15 => X"FFFFFFFFFFFF8C1111118C8C1111118C8C8C8C8C115959118C1111118CFFFFFF",
      INIT_16 => X"FFFFFFFFFFFF59591111118C8C111159118C8C595959118C11115959598CFFFF",
      INIT_17 => X"FFFFFFFFFF8C5959111159118C8C111159595959118C8C11595959595959FFFF",
      INIT_18 => X"FFFFFFFFFF595926591159595911111111111111111159595959592659598CFF",
      INIT_19 => X"FFFFFFFF592626265959262659591111111111115959262659592626265959FF",
      INIT_1A => X"FFFFFFFF5926262626262626262659595959595926262626265959262626FFFF",
      INIT_1B => X"FFFFFFFFFFFFFF2626F3F3262626262626262626262626F3F3265926FFFFFFFF",
      INIT_1C => X"FFFFFFFFFFFFFF2626FFFFF3F3F3F326262626F3F3F3F3FFFF2626FFFFFFFFFF",
      INIT_1D => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F3F3F3FFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_1E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_1F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_20 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C8C8C8C8C8CFFFFFFFFFFFFFFFFFF",
      INIT_21 => X"FFFFFFFFFFFFFFFFFFFFFFFFFF8C8C000000000000008C8C8CFFFFFFFFFFFFFF",
      INIT_22 => X"FFFFFFFFFFFFFFFFFFFFFFFF8C0000000000000000000000008C8CFFFFFFFFFF",
      INIT_23 => X"FFFFFFFFFFFFFFFFFFFFFF8C0000000000000000000000000000008CFFFFFFFF",
      INIT_24 => X"FFFFFFFFFFFFFFFFFFFFFF8C0000000000000000000000000000008CFFFFFFFF",
      INIT_25 => X"FFFFFFFFFFFFFFFFFFFF8C00000000000000000000000000000000008CFFFFFF",
      INIT_26 => X"FFFFFFFFFFFFFFFFFFFF8C00000000000000000000000000000000008CFFFFFF",
      INIT_27 => X"FFFFFFFFFFFFFFFFFFFF0000000000000000000000000000008C8C008CFFFFFF",
      INIT_28 => X"FFFFFFFFFFFFFFFFFFFF00000000000000000000000000008C11118C8CFFFFFF",
      INIT_29 => X"FFFFFFFFFFFFFFFFFFFF000000000000000000000000008C1111118C8CFFFFFF",
      INIT_2A => X"FFFFFFFFFFFFFFFFFFFF000000000000000000004444448C8C11118C8CFFFFFF",
      INIT_2B => X"FFFFFFFFFFFFFFFFFF0000000000000000000000444848448C8C8C008CFFFFFF",
      INIT_2C => X"FFFFFFFFFFFFFFFFFF8C8C00000000000000000048D9D94800000000FFFFFFFF",
      INIT_2D => X"FFFFFFFFFFFFFFFF008C8C00000000000000000048D9D94800000000FFFFFFFF",
      INIT_2E => X"FFFFFFFFFFFFFFFF8C8C8C8C00000000000000000048480000000000FFFFFFFF",
      INIT_2F => X"FFFFFFFFFFFFFF8C8C118C8C8C00000000000000000000000000008CFFFFFFFF",
      INIT_30 => X"FFFFFFFFFFFF8C111111118C8C8C000000000000000000000000008CFFFFFFFF",
      INIT_31 => X"FFFFFFFFFF8C111111111111118C8C00000000000000000000008CFFFFFFFFFF",
      INIT_32 => X"FFFFFFFF8C11115911111111118C118C8C000000000000008C8CFFFFFFFFFFFF",
      INIT_33 => X"FFFFFFFF595959111111111111118C118C0000008C8C8C8C8CFFFFFFFFFFFFFF",
      INIT_34 => X"FFFFFF5959265959111111115911118C11118C0000008C8CFFFFFFFFFFFFFFFF",
      INIT_35 => X"FFFFFF592626265959595911595911118C11111111118C8C00FFFFFFFFFFFFFF",
      INIT_36 => X"FFFFFFF3262626595959265911595911118C8C118C8C8C1100FFFFFFFFFFFFFF",
      INIT_37 => X"FFFFFFFFF3F3265959262659591159595911118C8C8C11118CFFFFFFFFFFFFFF",
      INIT_38 => X"FFFFFFFFFFFFF326262626595959111159595911111111118C00FFFFFFFFFFFF",
      INIT_39 => X"FFFFFFFFFFFFFFF3F32626595959595911115959595911111100FFFFFFFFFFFF",
      INIT_3A => X"FFFFFFFFFFFFFFFFF3262659592626595959111111111111598CFFFFFFFFFFFF",
      INIT_3B => X"FFFFFFFFFFFFFFFFFFF3F32626F3262659595959595959595911FFFFFFFFFFFF",
      INIT_3C => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFF32626595959262626595926FFFFFFFFFFFF",
      INIT_3D => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F3262626FFFFFF2626FFFFFFFFFFFFFF",
      INIT_3E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_3F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_40 => X"FFFFFFFFFFFFFFFFFFFFFF8C8C8C8C8C8C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_41 => X"FFFFFFFFFFFFFFFFFF8C8C8C000000000000008C8CFFFFFFFFFFFFFFFFFFFFFF",
      INIT_42 => X"FFFFFFFFFFFFFF8C8C0000000000000000000000008CFFFFFFFFFFFFFFFFFFFF",
      INIT_43 => X"FFFFFFFFFFFF8C0000000000000000000000000000008CFFFFFFFFFFFFFFFFFF",
      INIT_44 => X"FFFFFFFFFFFF8C0000000000000000000000000000008CFFFFFFFFFFFFFFFFFF",
      INIT_45 => X"FFFFFFFFFF8C00000000000000000000000000000000008CFFFFFFFFFFFFFFFF",
      INIT_46 => X"FFFFFFFFFF8C00000000000000000000000000000000008CFFFFFFFFFFFFFFFF",
      INIT_47 => X"FFFFFFFFFF8C008C8C000000000000000000000000000000FFFFFFFFFFFFFFFF",
      INIT_48 => X"FFFFFFFFFF8C8C11118C0000000000000000000000000000FFFFFFFFFFFFFFFF",
      INIT_49 => X"FFFFFFFFFF8C8C1111118C00000000000000000000000000FFFFFFFFFFFFFFFF",
      INIT_4A => X"FFFFFFFFFF8C8C11118C8C44444400000000000000000000FFFFFFFFFFFFFFFF",
      INIT_4B => X"FFFFFFFFFF8C008C8C8C444848440000000000000000000000FFFFFFFFFFFFFF",
      INIT_4C => X"FFFFFFFFFFFF0000000048D9D9480000000000000000008C8CFFFFFFFFFFFFFF",
      INIT_4D => X"FFFFFFFFFFFF0000000048D9D9480000000000000000008C8C00FFFFFFFFFFFF",
      INIT_4E => X"FFFFFFFFFFFF000000000048480000000000000000008C8C8C8CFFFFFFFFFFFF",
      INIT_4F => X"FFFFFFFFFFFF8C00000000000000000000000000008C8C8C118C8CFFFFFFFFFF",
      INIT_50 => X"FFFFFFFFFFFF8C000000000000000000000000008C8C8C111111118CFFFFFFFF",
      INIT_51 => X"FFFFFFFFFFFFFF8C00000000000000000000008C8C111111111111118CFFFFFF",
      INIT_52 => X"FFFFFFFFFFFFFFFF8C8C000000000000008C8C118C11111111115911118CFFFF",
      INIT_53 => X"FFFFFFFFFFFFFFFFFF8C8C8C8C8C0000008C118C11111111111111595959FFFF",
      INIT_54 => X"FFFFFFFFFFFFFFFFFFFF8C8C0000008C11118C111159111111115959265959FF",
      INIT_55 => X"FFFFFFFFFFFFFFFFFF008C8C11111111118C11115959115959595926262659FF",
      INIT_56 => X"FFFFFFFFFFFFFFFFFF00118C8C8C118C8C11115959115926595959262626F3FF",
      INIT_57 => X"FFFFFFFFFFFFFFFFFF8C11118C8C8C11115959591159592626595926F3F3FFFF",
      INIT_58 => X"FFFFFFFFFFFFFFFF008C1111111111595959111159595926262626F3FFFFFFFF",
      INIT_59 => X"FFFFFFFFFFFFFFFF0011111159595959111159595959592626F3F3FFFFFFFFFF",
      INIT_5A => X"FFFFFFFFFFFFFFFF8C59111111111111595959262659592626F3FFFFFFFFFFFF",
      INIT_5B => X"FFFFFFFFFFFFFFFF115959595959595959592626F32626F3F3FFFFFFFFFFFFFF",
      INIT_5C => X"FFFFFFFFFFFFFFFF2659592626265959592626F3FFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_5D => X"FFFFFFFFFFFFFFFFFF2626FFFFFF262626F3F3FFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_5E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_5F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_60 => X"FFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C8C8C8C8CFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_61 => X"FFFFFFFFFFFFFFFFFFFFFF8C8C000000000000008C8CFFFFFFFFFFFFFFFFFFFF",
      INIT_62 => X"FFFFFFFFFFFFFFFFFF8C8C00000000000000000000008C8CFFFFFFFFFFFFFFFF",
      INIT_63 => X"FFFFFFFFFFFFFFFF8C0000000000000000000000000000008CFFFFFFFFFFFFFF",
      INIT_64 => X"FFFFFFFFFFFFFFFF0000000000000000000000000000000000FFFFFFFFFFFFFF",
      INIT_65 => X"FFFFFFFFFFFFFF8C00000000000000000000000000000000008CFFFFFFFFFFFF",
      INIT_66 => X"FFFFFFFFFFFFFF8C00000000000000000000000000000000008CFFFFFFFFFFFF",
      INIT_67 => X"FFFFFFFFFFFFFF00000000000000000000000000000000000000FFFFFFFFFFFF",
      INIT_68 => X"FFFFFFFFFFFFFF00000000000000000000000000000000000000FFFFFFFFFFFF",
      INIT_69 => X"FFFFFFFFFFFFFF00000000000000000000000000000000000000FFFFFFFFFFFF",
      INIT_6A => X"FFFFFFFFFFFFFF00000000000000000000000000000000000000FFFFFFFFFFFF",
      INIT_6B => X"FFFFFFFFFFFFFF00008C8C000000000000000000000000000000FFFFFFFFFFFF",
      INIT_6C => X"FFFFFFFFFFFFFF8C8C8C8C8C8C8C8C000000000000000000008CFFFFFFFFFFFF",
      INIT_6D => X"FFFFFFFFFFFFFF8C8C8C8C8C8C8C8C8C0000008C8C0000008C8CFFFFFFFFFFFF",
      INIT_6E => X"FFFFFFFFFFFFFFFF8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8CFFFFFFFFFFFF",
      INIT_6F => X"FFFFFFFFFFFFFFFF8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8CFFFFFFFFFFFFFF",
      INIT_70 => X"FFFFFFFFFFFFFF00008C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C00FFFFFFFFFFFF",
      INIT_71 => X"FFFFFFFFFFFFFF8C00008C8C8C8C8C8C8C8C8C8C8C1111118C8C00FFFFFFFFFF",
      INIT_72 => X"FFFFFFFFFFFF008C8C00008C8C8C8C8C8C8C11111111118C118C00FFFFFFFFFF",
      INIT_73 => X"FFFFFFFFFF8C8C8C8C8C8C00008C8C1111111111118C8C11118C1100FFFFFFFF",
      INIT_74 => X"FFFFFFFF8C1111118C8C8C118C00008C1111118C8C1159118C11118CFFFFFFFF",
      INIT_75 => X"FFFFFFFF8C115911118C8C1159118C8C8C8C11111159118C1159591100FFFFFF",
      INIT_76 => X"FFFFFF8C1159591111118C8C595959111111115959111111595959598CFFFFFF",
      INIT_77 => X"FFFFFF59592659115959118C1159595959595959111159595926595911FFFFFF",
      INIT_78 => X"FFFFFF59262626595926595911115959595959595926595959262659118CFFFF",
      INIT_79 => X"FFFFFF59262626592626262659591111595959595926265959262659598CFFFF",
      INIT_7A => X"FFFFFFF3F3F3262626262626262659595959595926262626592626265926FFFF",
      INIT_7B => X"FFFFFFFFF3F3F3F326F3F32626262626262626262626F32626F3F32626FFFFFF",
      INIT_7C => X"FFFFFFFFFFFFF3F3F3F3F3F3262626F3F326F32626F3FFF3F3FFFFF3F3FFFFFF",
      INIT_7D => X"FFFFFFFFFFFFFFFFFFFFFFFFF3F3F3FFFFF3FFF3F3FFFFFFFFFFFFFFFFFFFFFF",
      INIT_7E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_7F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 3) => addra(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 3) => addrb(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 0) => B"00000000000000000000000000000000",
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 8) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 8),
      DOADO(7 downto 0) => \douta[9]\(7 downto 0),
      DOBDO(31 downto 8) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 8),
      DOBDO(7 downto 0) => \doutb[9]\(7 downto 0),
      DOPADOP(3 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 1),
      DOPADOP(0) => \douta[10]\(0),
      DOPBDOP(3 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 1),
      DOPBDOP(0) => \doutb[10]\(0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1_n_0\,
      ENBWREN => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3 downto 0) => B"0000",
      WEBWE(7 downto 0) => B"00000000"
    );
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => addra(13),
      I1 => addra(12),
      I2 => addra(14),
      O => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1_n_0\
    );
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => addrb(13),
      I1 => addrb(12),
      I2 => addrb(14),
      O => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized5\ is
  port (
    \douta[9]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \doutb[9]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \douta[10]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \doutb[10]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized5\ : entity is "blk_mem_gen_prim_wrapper_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized5\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized5\ is
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1__0_n_0\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2__0_n_0\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 1,
      DOB_REG => 1,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"FFBFFFFFFFBFFFFFFFFFFF7FFFDFFFFFFFFFFEFFFFFFFDFFFFFFFFFFFFFFFFFF",
      INITP_01 => X"FF7B5FBFFFA5F17FFF93FA7FFFD1F2FFFF60E1BFFF5DF6BFFFBEFF7FFF9FFE7F",
      INITP_02 => X"FCEAA55BF39841BBFBBFBDEFFBBEE89FFF15F237FDBAAF6FFEF957DFFF56AABF",
      INITP_03 => X"FFFFFFFFFFFFFFFFFFFF37FFFFF877FFFFF8EFFFFF1027BFFEE7317BFDDDBD3B",
      INITP_04 => X"FFE37FEFFFE7FFFFFFF7FFDFFFF7FFFFFFFBFFFFFFFFFFFFFFFFFDFFFFFFFFFF",
      INITP_05 => X"FFFA9FBFFFF9DF3FFFF74F7FFFEDD1FFFFEB81BFFFD020BFFFF0D3EFFFE0BFCF",
      INITP_06 => X"FF3DBFFFFFB8E6FFFFDA7DFFFFFB7FFFFFFFFFFFFFFF5DFFFFFF887FFFFC353F",
      INITP_07 => X"FFFFFFFFFFFFFFFFFFFCFFFFFEFFEFFFFDFD8FFFFD7B7BFFFF7C15FFFF7B7DFF",
      INITP_08 => X"FDFFB1FFFFFFF9FFFEFFFBFFFFFFFBFFFFFFF7FFFFFFFFFFFFEFFFFFFFFFFFFF",
      INITP_09 => X"FF7E57FFFFA997FFFFBCBBFFFFE2EDFFFF6075FFFF4102FFFDF2C3FFFCFF41FF",
      INITP_0A => X"FFFF6F3FFFD9C77FFFEF96FFFFFFB7FFFFFFFFFFFFEEBFFFFF847FFFFF2B0FFF",
      INITP_0B => X"FFFFFFFFFFFFFFFFFFFF8BBFFFFD9BDFFFFC686FFFF7B32FFFEA0BBFFFEFB7BF",
      INITP_0C => X"FFBFFFFFFFBFFFFFFFFFFF7FFFDFFFFFFFFFFEFFFFFFFDFFFFFFF7FFFFFFFFFF",
      INITP_0D => X"FF7FFFFFFF32D33FFF7333BFFF60DDBFFF47FEBFFF4FFFBFFF9FFF7FFFBFFF7F",
      INITP_0E => X"FFF8C619FFF3F21BFFF7FC33FFFFFE27FFFFFF0FFFFF9F9FFFFE03BFFFFFFDFF",
      INITP_0F => X"FFFFFFFFFFFFFFFFFFF8037FE7F238B7FB7FFFDFFF3E798FFF3C388DFFB81C0D",
      INIT_00 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF11999999999911FFFFFFFFFFFFFFFFFFFFFF",
      INIT_01 => X"FFFFFFFFFFFFFFFFFFFFFFFF11993333333333333399FFFFFFFFFFFFFFFFFFFF",
      INIT_02 => X"FFFFFFFFFFFFFFFFFFFF1199333333333333333333336A99FFFFFFFFFFFFFFFF",
      INIT_03 => X"FFFFFFFFFFFFFFFFFF11993333333333333333333333336A11FFFFFFFFFFFFFF",
      INIT_04 => X"FFFFFFFFFFFFFFFFFF116A3333333333333333333333333399FFFFFFFFFFFFFF",
      INIT_05 => X"FFFFFFFFFFFFFFFF119933333333333333333333333333336A11FFFFFFFFFFFF",
      INIT_06 => X"FFFFFFFFFFFFFFFF116A33333333333333333333333333333311FFFFFFFFFFFF",
      INIT_07 => X"FFFFFFFFFFFFFFFF116A33333333333333333333333333333399FFFFFFFFFFFF",
      INIT_08 => X"FFFFFFFFFFFFFFFF116A6A3333333333333333333333336A6A99FFFFFFFFFFFF",
      INIT_09 => X"FFFFFFFFFFFFFFFF116A33333333336A33333333333333336A11FFFFFFFFFFFF",
      INIT_0A => X"FFFFFFFFFFFFFFFF00996A3333336A99333333996A33336A9900FFFFFFFFFFFF",
      INIT_0B => X"FFFFFFFFFFFFFFFF001199000000006A3333336A000000991100FFFFFFFFFFFF",
      INIT_0C => X"FFFFFFFFFFFFFFFFFF116A1100000033333333330000116A11FFFFFFFFFFFFFF",
      INIT_0D => X"FFFFFFFFFFFFFFFFFF006A9900001133333333331100996A00FFFFFFFFFFFFFF",
      INIT_0E => X"FFFFFFFFFFFFFFFFFF00116A6A996A33333333336A6A6A1100FFFFFFFFFFFFFF",
      INIT_0F => X"FFFFFFFFFFFFFFFF00111199996A333300330033339999111100FFFFFFFFFFFF",
      INIT_10 => X"FFFFFFFFFFFFFF116A336A110011336A336A336A3300116A336A11FFFFFFFFFF",
      INIT_11 => X"FFFFFFFFFFFF116A3333331111006A116A116A116A11113333336A11FFFFFFFF",
      INIT_12 => X"FFFFFFFFFFFF0011996A11119900996A336A336A999911116A991100FFFFFFFF",
      INIT_13 => X"FFFFFFFFFFFF1111000000996A336A11111111116A6A99000000119900FFFFFF",
      INIT_14 => X"FFFFFFFFFF00111199000C0C0411996A9933996A990000000400009911FFFFFF",
      INIT_15 => X"FFFFFFFFFF0011990CDD69690C333333996A993333110004040C0C0011FFFFFF",
      INIT_16 => X"FFFFFFFF000011D10CDDDDD10C00000000110000000000040CDD69D10C00FFFF",
      INIT_17 => X"FFFFFFFF040400DD0CD169DD0C6A1100110011006A11000400D1DDD10C00FFFF",
      INIT_18 => X"FFFFFFC80404000CD10CDDD10C336A99110011993311000400DDD1D10C00C8FF",
      INIT_19 => X"FFFFFFC804040400D1D10C0000119999000099336A00000400D1D10CD100C8FF",
      INIT_1A => X"FFFFC8C80404040400000004000000000000116A0004040404000CD10C04C8C8",
      INIT_1B => X"FFFFC8C804C8C8040404040404000000D16969DDD1040404040404C8C804C8C8",
      INIT_1C => X"FFFFC804FFC8040404FFFFFFFF00000000D1D1D100FFFFFFFF040404C8FF04C8",
      INIT_1D => X"FFFF04FFFF0404FFFFFFFFFFFFFFFFFF00DD69D100FFFFFFFFFFFF0404FFFF04",
      INIT_1E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_1F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_20 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9999999999999999FFFFFFFFFFFFFFFFFF",
      INIT_21 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF99333333333333336A9999FFFFFFFFFFFFFF",
      INIT_22 => X"FFFFFFFFFFFFFFFFFFFFFFFFFF99333333333333333333333399FFFFFFFFFFFF",
      INIT_23 => X"FFFFFFFFFFFFFFFFFFFFFFFF996A33333333333333333333333399FFFFFFFFFF",
      INIT_24 => X"FFFFFFFFFFFFFFFFFFFFFF996A3333333333333333333333333399FFFFFFFFFF",
      INIT_25 => X"FFFFFFFFFFFFFFFFFFFFFF996A333333333333333333333333336A99FFFFFFFF",
      INIT_26 => X"FFFFFFFFFFFFFFFFFFFF116A6A333333333333333333333333333399FFFFFFFF",
      INIT_27 => X"FFFFFFFFFFFFFFFFFFFF116A6A6A33336A3333333333333333333300FFFFFFFF",
      INIT_28 => X"FFFFFFFFFFFFFFFFFFFF116A6A6A6A6A996A33333333339933336A00FFFFFFFF",
      INIT_29 => X"FFFFFFFFFFFFFFFFFFFF11996A6A6A6A99996A990000999933339900FFFFFFFF",
      INIT_2A => X"FFFFFFFFFFFFFFFFFFFF00116A6A6A6A6A6A99000000006A336A11FFFFFFFFFF",
      INIT_2B => X"FFFFFFFFFFFFFFFFFFFFFF00996A9999996A6A0000000033336A11FFFFFFFFFF",
      INIT_2C => X"FFFFFFFFFFFFFFFFFFFFFF0011996A9999996A9900006A33333399FFFFFFFFFF",
      INIT_2D => X"FFFFFFFFFFFFFFFFFFFFFFFF001111996A116A6A33333333009911FFFFFFFFFF",
      INIT_2E => X"FFFFFFFFFFFFFFFFFFFFFFFFFF00001133116A33333333336A6A11FFFFFFFFFF",
      INIT_2F => X"FFFFFFFFFFFFFFFFFFFFFFFFFF001100996A6A99991199119900FFFFFFFFFFFF",
      INIT_30 => X"FFFFFFFFFFFFFFFFFFFFFFFF04110000440011996A996A996A00FFFFFFFFFFFF",
      INIT_31 => X"FFFFFFFFFFFFFFFFFFFFFF0404113333994400001100000000FFFFFFFFFFFFFF",
      INIT_32 => X"FFFFFFFFFFFFFFFFFFFF0404113333336A990099111100FFFFFFFFFFFFFFFFFF",
      INIT_33 => X"FFFFFFFFFFFFFFFFFF04C8C81111119999113333333399FFFFFFFFFFFFFFFFFF",
      INIT_34 => X"FFFFFFFFFFFFFFFF04C8C8C8040099110011110C0C0C3399FFFFFFFFFFFFFFFF",
      INIT_35 => X"FFFFFFFFFFFFFF04C8C8D10404009900000C0C6969D16A99FFFFFFFFFFFFFFFF",
      INIT_36 => X"FFFFFFFFFFFFFF04C8D1C804040000000CD10CDDDDD19900FFFFFFFFFFFFFFFF",
      INIT_37 => X"FFFFFFFFFFFF04C8D1D1040404996A990CDD0C6969D111FFFFFFFFFFFFFFFFFF",
      INIT_38 => X"FFFFFFFFFFFF04C8D1C804040400111100D10CD1D10C00FFFFFFFFFFFFFFFFFF",
      INIT_39 => X"FFFFFFFFFFFF04C8D1C80404FF0400000000000C6A9900FFFFFFFFFFFFFFFFFF",
      INIT_3A => X"FFFFFFFFFFFF0004D1FF0404FFDD9911001199991100D10CFFFFFFFFFFFFFFFF",
      INIT_3B => X"FFFFFFFFFFFF0004C8FFFFFFFF0CDD690C0000000CD10CFFFFFFFFFFFFFFFFFF",
      INIT_3C => X"FFFFFFFFFFFFFF0004FFFFFFFFD10CFFFFFFFF00D1D1D10CFFFFFFFFFFFFFFFF",
      INIT_3D => X"FFFFFFFFFFFFFFFFFFFFFFFFFF0CDDDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_3E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_3F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_40 => X"FFFFFFFFFFFFFFFFFFFFFF9999999999999999FFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_41 => X"FFFFFFFFFFFFFFFFFF99996A3333333333333399FFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_42 => X"FFFFFFFFFFFFFFFF99333333333333333333333399FFFFFFFFFFFFFFFFFFFFFF",
      INIT_43 => X"FFFFFFFFFFFFFF993333333333333333333333336A99FFFFFFFFFFFFFFFFFFFF",
      INIT_44 => X"FFFFFFFFFFFFFF99333333333333333333333333336A99FFFFFFFFFFFFFFFFFF",
      INIT_45 => X"FFFFFFFFFFFF996A333333333333333333333333336A99FFFFFFFFFFFFFFFFFF",
      INIT_46 => X"FFFFFFFFFFFF9933333333333333333333333333336A6A11FFFFFFFFFFFFFFFF",
      INIT_47 => X"FFFFFFFFFFFF00333333333333333333336A33336A6A6A11FFFFFFFFFFFFFFFF",
      INIT_48 => X"FFFFFFFFFFFF006A33339933333333336A996A6A6A6A6A11FFFFFFFFFFFFFFFF",
      INIT_49 => X"FFFFFFFFFFFF0099333399990000996A99996A6A6A6A9911FFFFFFFFFFFFFFFF",
      INIT_4A => X"FFFFFFFFFFFFFF116A336A00000000996A6A6A6A6A6A1100FFFFFFFFFFFFFFFF",
      INIT_4B => X"FFFFFFFFFFFFFF116A3333000000006A6A9999996A9900FFFFFFFFFFFFFFFFFF",
      INIT_4C => X"FFFFFFFFFFFFFF993333336A0000996A9999996A991100FFFFFFFFFFFFFFFFFF",
      INIT_4D => X"FFFFFFFFFFFFFF119900333333336A6A116A99111100FFFFFFFFFFFFFFFFFFFF",
      INIT_4E => X"FFFFFFFFFFFFFF11336A336A336A6A99116A001100FFFFFFFFFFFFFFFFFFFFFF",
      INIT_4F => X"FFFFFFFFFFFFFFFF009911991199996A6A99001100FFFFFFFFFFFFFFFFFFFFFF",
      INIT_50 => X"FFFFFFFFFFFFFFFF006A996A996A9911004400001104FFFFFFFFFFFFFFFFFFFF",
      INIT_51 => X"FFFFFFFFFFFFFFFFFF0000000011000044993333110404FFFFFFFFFFFFFFFFFF",
      INIT_52 => X"FFFFFFFFFFFFFFFFFFFFFF0011119900996A333333110404FFFFFFFFFFFFFFFF",
      INIT_53 => X"FFFFFFFFFFFFFFFFFFFFFF9933333333119999111111C8C804FFFFFFFFFFFFFF",
      INIT_54 => X"FFFFFFFFFFFFFFFFFFFF99330C0C0C11110011990004C8C8C804FFFFFFFFFFFF",
      INIT_55 => X"FFFFFFFFFFFFFFFFFFFF996AD169690C0C000099000404D1C8C804FFFFFFFFFF",
      INIT_56 => X"FFFFFFFFFFFFFFFFFFFF0099D1DDDD0CD10C0000000404C8D1C804FFFFFFFFFF",
      INIT_57 => X"FFFFFFFFFFFFFFFFFFFFFF11D169690CDD0C996A99040404D1D1C804FFFFFFFF",
      INIT_58 => X"FFFFFFFFFFFFFFFFFFFFFF000CD1D10CD100111100040404C8D1C804FFFFFFFF",
      INIT_59 => X"FFFFFFFFFFFFFFFFFFFFFF00996A0C000000000004000404C8D1C804FFFFFFFF",
      INIT_5A => X"FFFFFFFFFFFFFFFFFFFF0CD10011999911001199DD00040400D10400FFFFFFFF",
      INIT_5B => X"FFFFFFFFFFFFFFFFFFFFFF0CD10C0000000C69DD0C00000000C80400FFFFFFFF",
      INIT_5C => X"FFFFFFFFFFFFFFFFFFFF0CD1D1D100FFFF00000CD100FFFFFF0400FFFFFFFFFF",
      INIT_5D => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00DDDD0C00FFFFFF00FFFFFFFFFFFF",
      INIT_5E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_5F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_60 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF119999999911FFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_61 => X"FFFFFFFFFFFFFFFFFFFFFFFF11993333333333336A99FFFFFFFFFFFFFFFFFFFF",
      INIT_62 => X"FFFFFFFFFFFFFFFFFFFF1199333333333333333333336A99FFFFFFFFFFFFFFFF",
      INIT_63 => X"FFFFFFFFFFFFFFFFFF11993333333333333333333333336A11FFFFFFFFFFFFFF",
      INIT_64 => X"FFFFFFFFFFFFFFFFFF116A3333333333333333333333333399FFFFFFFFFFFFFF",
      INIT_65 => X"FFFFFFFFFFFFFFFF119933333333333333333333333333336A11FFFFFFFFFFFF",
      INIT_66 => X"FFFFFFFFFFFFFFFF116A33333333333333333333333333333311FFFFFFFFFFFF",
      INIT_67 => X"FFFFFFFFFFFFFFFF116A33333333333333333333333333333399FFFFFFFFFFFF",
      INIT_68 => X"FFFFFFFFFFFFFFFF116A33333333333333333333333333336A99FFFFFFFFFFFF",
      INIT_69 => X"FFFFFFFFFFFFFFFF116A6A333333333333333333333333336A11FFFFFFFFFFFF",
      INIT_6A => X"FFFFFFFFFFFFFFFF00996A6A3333333333333333333333339900FFFFFFFFFFFF",
      INIT_6B => X"FFFFFFFFFFFFFFFF00116A6A6A333333333333333333336A9900FFFFFFFFFFFF",
      INIT_6C => X"FFFFFFFFFFFFFFFF0011116A6A6A6A6A99996A3333336A991100FFFFFFFFFFFF",
      INIT_6D => X"FFFFFFFFFFFFFFFF001111116A6A99996A6A11996A6A11111100FFFFFFFFFFFF",
      INIT_6E => X"FFFFFFFFFFFFFFFF000011110000990033330011000011110000FFFFFFFFFFFF",
      INIT_6F => X"FFFFFFFFFFFFFFFF000404040404041133339904040404040404FFFFFFFFFFFF",
      INIT_70 => X"FFFFFFFFFFFFFF040404C8C804040404040404040404D1C804C804FFFFFFFFFF",
      INIT_71 => X"FFFFFFFFFFFF04C8C8040404C8C8C8D1D1D1D1D1D1D1C80404D1C804FFFFFFFF",
      INIT_72 => X"FFFFFFFFFF04C8C8C8C8040404C8C8C8C8D1D1C8C8040404C8D1D1C804FFFFFF",
      INIT_73 => X"FFFFFFFF04C8C8C8C8C8C804040404C8C8C8C804040404C8D1D1D1D1C804FFFF",
      INIT_74 => X"FFFFFFFF04C8C8C8C8C8C804C8040404040404040404C8D1D1D1C8D1D1C8FFFF",
      INIT_75 => X"FFFFFF04C8C8C8C8C8C8C8C8D1C804040404040404C8D1D1D1D1C8C8D1D104FF",
      INIT_76 => X"FFFFFF04C8C8C804C8C8C8C8D1D1C804040404C8D1D1C8D1D1D1D104C8D1C8FF",
      INIT_77 => X"FFFF04C8C8C80404C8C8C8C8C8D1D1D1C8C8D1D1D1C8C8D1D1D1D1C804D1D104",
      INIT_78 => X"FFFF04C8C80404C8C8D1C8C8C8D1D1D1D1D1D1C8C8C8D1D1D1D1D1D104C8D104",
      INIT_79 => X"FFFF04C8C80404C8D1D1C8C8C8C8D1D1D1D1C804C8D1D1D1C8D1D1D10404D104",
      INIT_7A => X"FFFFFF04040404C8D1D1C8C8C8C8C8D1D1C80404C8D1D1C8C8D1D1D10404C804",
      INIT_7B => X"FFFFFF0404000404D1C8C8040404C8C8C8040404C8C8C804C8C8D1C804040404",
      INIT_7C => X"FFFFFF0000FFFF04C80404FF0000040000000404040000000400C8040004FFFF",
      INIT_7D => X"FFFFFFFFFFFFFF0404FFFFFFFF000000000000000000FFFF00FF04FFFFFFFFFF",
      INIT_7E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_7F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 3) => addra(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 3) => addrb(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 0) => B"00000000000000000000000000000000",
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 8) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 8),
      DOADO(7 downto 0) => \douta[9]\(7 downto 0),
      DOBDO(31 downto 8) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 8),
      DOBDO(7 downto 0) => \doutb[9]\(7 downto 0),
      DOPADOP(3 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 1),
      DOPADOP(0) => \douta[10]\(0),
      DOPBDOP(3 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 1),
      DOPBDOP(0) => \doutb[10]\(0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1__0_n_0\,
      ENBWREN => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2__0_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3 downto 0) => B"0000",
      WEBWE(7 downto 0) => B"00000000"
    );
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => addra(14),
      I1 => addra(13),
      I2 => addra(12),
      O => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1__0_n_0\
    );
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => addrb(14),
      I1 => addrb(13),
      I2 => addrb(12),
      O => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2__0_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized6\ is
  port (
    DOADO : out STD_LOGIC_VECTOR ( 7 downto 0 );
    DOBDO : out STD_LOGIC_VECTOR ( 7 downto 0 );
    DOPADOP : out STD_LOGIC_VECTOR ( 0 to 0 );
    DOPBDOP : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized6\ : entity is "blk_mem_gen_prim_wrapper_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized6\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized6\ is
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1__2_n_0\ : STD_LOGIC;
  signal \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2__2_n_0\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 1,
      DOB_REG => 1,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"FF7FFC7FFECFFB7FFFDFFDFFFFDBEFFFFFDA67FFFFFC5FFFFFFEDFFFFFFFFFFF",
      INITP_01 => X"DDC4B9F9DBB416B9D84222FDEC5414FDF08EA93BFF4BEF47FEDFDCFFFF9E3FBF",
      INITP_02 => X"F797FBF3F757F74DF89BD0BDFF3FCD5BFC3FEBC7F07310EFE069DF17EED55583",
      INITP_03 => X"FFFFFFFFFFFFFFFFFFFFFFFFFE073E7FFF187C9FFB8DFF47F7C3C7BFF71BABBB",
      INITP_04 => X"FFFF7FEFFFFD3FDFFFFABFDFFFFCDF3FFFFFE0FFFFFFBFFFFFFFFFFFFFFFFFFF",
      INITP_05 => X"FC7D192FFF1E61FEFFDE013EFF0D5E9DFFEEBFD3FBFDBFDFFBEB7E7FFC1C7F0F",
      INITP_06 => X"E5E3E3FFF7CB8DEFFBD269D7FBD5ECB7F9DFF7F7F88BF76FF91AEF5FFCFC969F",
      INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80C1FFFC40A07FFF701E7FE6F9CFBF",
      INITP_08 => X"FDFFDFFFFEFF97FFFEFFABFFFF3E67FFFFC17FFFFFFFBFFFFFFFFFFFFFFFFFFF",
      INITP_09 => X"FD26178FF71F5F1FDFE14E3FDF208EFFEE5E543FF2FFADFFFF9FD9F7FC3FC60F",
      INITP_0A => X"FFF1F9E9FDEC38FBFAE5CAF7FB4DF6F7FBFBFEE7FDBBF847FEBDEA27FE5A27CF",
      INITP_0B => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0607FFF81A08FFF9E03BFFF7CF3D9",
      INITP_0C => X"FE5FFC7FFFDFFDFFFFEFFDFFFFEFFBFFFFF3E7FFFFFC1FFFFFFFFFFFFFFFFFFF",
      INITP_0D => X"E0D555BBEFCE91DDCEB616EDDFA804CDDF97F31BEE4FFAC7F0DFFD3FFF1FFCFF",
      INITP_0E => X"EE4BEC77E70BF4F7D105F577D886EC8FED59FE7FF1E8FE1FFB846747F47DCB33",
      INITP_0F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E703FFC9F077FF17F9FEFFE71F8F7",
      INIT_00 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_01 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8DFFFF8DFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_02 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF8D8D8D158DFFFF15FFFFFFFFFFFFFFFFFFFF",
      INIT_03 => X"FFFFFFFFFFFFFFFFFFFF8D15FF8D158D8DE2158D8DE215FFFFFFFFFFFFFFFFFF",
      INIT_04 => X"FFFFFFFFFFFFFFFFFFFF8DE2152133333333332115E215FFFFFFFFFFFFFFFFFF",
      INIT_05 => X"FFFFFFFFFFFFFFFFFF158D1533BBBBBBBBBBBBBB33158DFF15FFFFFFFFFFFFFF",
      INIT_06 => X"FFFFFFFFFFFFFF8D15E28D21BBBBBBBBBBBBBBBBBB2115158DFFFFFFFFFFFFFF",
      INIT_07 => X"FFFFFFFFFFFFFFFF8D15E233BBBBBBBBBBBBBBBBBB338D8D8D1515FFFFFFFFFF",
      INIT_08 => X"FFFFFFFFFFFFFFFFFF8D8D33BBBBE68C8C8CE6BBBB33E2E2E28DFFFFFFFFFFFF",
      INIT_09 => X"FFFFFFFFFFFFFF8D15158D33BBE6BBBBC4C48CE6BB338D8D1515FFFFFFFFFFFF",
      INIT_0A => X"FFFFFFFFFFFFFFFF8D158D21338CBBE6C4C4C48C333315158DFF484848FFFFFF",
      INIT_0B => X"FFFFFFFF48484848448D8D2133C4C444C444C48C33218D158D4851D95148FFFF",
      INIT_0C => X"FFFFFF485151484848448DD921C48C4444448CC421D98D8D4451E6E6D95148FF",
      INIT_0D => X"FFFF4851514848484844448D2121C48C8C8CC421218D158D44D9E6E6D95148FF",
      INIT_0E => X"FFFF48D9D9484444448D15158DD92121212121D98D15E28D44485151514848FF",
      INIT_0F => X"FFFF48D9D9514844FF158D448D158D8D158D151515448DE2FF444444444844FF",
      INIT_10 => X"FFFFFF485151514844FF44448D158D158D448DE28D444415444848484844FFFF",
      INIT_11 => X"FFFFFF444848484848444448448D4815445148E2444444444848485144FFFFFF",
      INIT_12 => X"FFFFFFFF444848484844444448485144484848154848484851D95144FFFFFFFF",
      INIT_13 => X"FFFFFFFFFF444848484844444451D951D9D95148444851D9D951484848FFFFFF",
      INIT_14 => X"FFFFFFFFFFFF4444484844444451D9D95151484844444851484448515148FFFF",
      INIT_15 => X"FFFFFFFF44484848444848444448D9E6D95148444848484844485151D95148FF",
      INIT_16 => X"FFFFFF444851515148444844485151D9E6D951444851515148444848515148FF",
      INIT_17 => X"FFFFFF4448D9D9515148484448515151D9D95151444851D951444444484844FF",
      INIT_18 => X"FFFFFF4448D9D951484848444448D951514851484448D9E6D9484444444844FF",
      INIT_19 => X"FFFFFF444851D9D951514848484851D9D95148484851D9D9514844444444FFFF",
      INIT_1A => X"FFFFFFFF444851515148484844444851D9D95151515151514844444444FFFFFF",
      INIT_1B => X"FFFFFFFFFF44444448484844444444444851515151514848444444FFFFFFFFFF",
      INIT_1C => X"FFFFFFFFFFFFFF444444444444FFFFFF444444444444444444FFFFFFFFFFFFFF",
      INIT_1D => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_1E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_1F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_20 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_21 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_22 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8DFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_23 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF15E2FF2121212121FFFFFFFFFFFFFFFF",
      INIT_24 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF8D8DE2152133BBBBBB332121FFFFFFFFFFFF",
      INIT_25 => X"FFFFFFFFFFFFFFFFFFFFFFFFFF8DE28D1521BBBBBBBBBBBBBB3321FFFFFFFFFF",
      INIT_26 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF8D158D21BBBBBBBBBBBBBBBB21FFFFFFFFFF",
      INIT_27 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF152133BBBBBBBBBBBBE6E6E621FFFFFFFF",
      INIT_28 => X"FFFFFFFFFFFF4848484848FFFFFF8D8D2133BBBBBBBBBBE68C8C8C8CFFFFFFFF",
      INIT_29 => X"FFFFFFFFFF4851E6E6E6D948FF8DE2152133BBBBBBBBE68C8CBBC4C4FFFFFFFF",
      INIT_2A => X"FFFFFFFFFF48D9E6E6E6D95144FF8D15D921BBBBBBBBC4C4E6BB44C4FFFFFFFF",
      INIT_2B => X"FFFFFFFFFF4451D9D9D9514844FFFF8DD92133333333C4C4C4C444C44848FFFF",
      INIT_2C => X"FFFFFFFFFFFF44444848484844FF8D158DD921333333C48CC44444C4E6D948FF",
      INIT_2D => X"FFFFFFFFFFFFFF444444484444FFFF8D448D2121212121C48C8CC444E6E65148",
      INIT_2E => X"FFFFFFFFFFFFFF4448484844444444448D15D921212121D9C4C44451D9D95148",
      INIT_2F => X"FFFFFFFFFF444848484444444444444448488D15158D8D158D4851484444FFFF",
      INIT_30 => X"FFFFFFFFFF444848444444444444444851488DE28D44448D15484844FFFFFFFF",
      INIT_31 => X"FFFFFFFF44484844484848444444444851D9E68D5144FF4448514844FFFFFFFF",
      INIT_32 => X"FFFFFFFF444848485148484844444451D9515151484444444851514844FFFFFF",
      INIT_33 => X"FFFFFFFF444848E6E6D9484444444451D9E6D951484444444451D9514844FFFF",
      INIT_34 => X"FFFFFFFF444851E6E6D948444844485151D9514844444848444851D94844FFFF",
      INIT_35 => X"FFFFFF44444851D9D9514844484851484851514844484851514448514844FFFF",
      INIT_36 => X"FFFF444448445151D9514848444851D9D9484848444448D9D951444844FFFFFF",
      INIT_37 => X"FFFF444848444851515151484848515151D951484848D9E6D9514444FFFFFFFF",
      INIT_38 => X"FFFF4448484444485151515151484851515148485151D9D9514844FFFFFFFFFF",
      INIT_39 => X"FFFFFF4444444444485151514848484848484851515151484844FFFFFFFFFFFF",
      INIT_3A => X"FFFFFFFF444444444444484848484848444444484848484444FFFFFFFFFFFFFF",
      INIT_3B => X"FFFFFFFFFFFFFFFFFF44444444444444FFFF4444444444FFFFFFFFFFFFFFFFFF",
      INIT_3C => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_3D => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_3E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_3F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_40 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_41 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_42 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8DFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_43 => X"FFFFFFFFFFFFFFFFFFFF2121212121FF8DE215FFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_44 => X"FFFFFFFFFFFFFFFF212133BBBBBB33212115E28D8DFFFFFFFFFFFFFFFFFFFFFF",
      INIT_45 => X"FFFFFFFFFFFFFF2133BBBBBBBBBBBBBB3321158DE28DFFFFFFFFFFFFFFFFFFFF",
      INIT_46 => X"FFFFFFFFFFFFFF21BBBBBBBBBBBBBBBBBB218D158DFFFFFFFFFFFFFFFFFFFFFF",
      INIT_47 => X"FFFFFFFFFFFF21E6E6E6BBBBBBBBBBBBBB332115FFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_48 => X"FFFFFFFFFFFF8C8C8C8CE6BBBBBBBBBBBB33218D8DFFFF4848484848FFFFFFFF",
      INIT_49 => X"FFFFFFFFFFFFC4C4BB8C8CE6BBBBBBBBBB332115E28D48D9E6E6E65148FFFFFF",
      INIT_4A => X"FFFFFFFF4848C444C4C4C4C4333333333321D98DFFFF4851D9D9D95144FFFFFF",
      INIT_4B => X"FFFFFF48D9E6C44444C48CC43333332121D98D158DFF484848484444FFFFFFFF",
      INIT_4C => X"FFFF4851E6E644C48C8CC42121212121D98D448DFFFF4448444444FFFFFFFFFF",
      INIT_4D => X"FFFF4851D9D95144C4C4D921212121D98D158D4444444448484844FFFFFFFFFF",
      INIT_4E => X"FFFFFF444851D951488D8DD9D9D9D9158D4448444444444448484844FFFFFFFF",
      INIT_4F => X"FFFFFFFF44444851488D158D8D15158D48484844444444444448484844FFFFFF",
      INIT_50 => X"FFFFFFFFFFFF444848158D44448DE28D48485148444444444444484844FFFFFF",
      INIT_51 => X"FFFFFFFFFFFF4448514844FF44518DE6D9D9514844444448484844484844FFFF",
      INIT_52 => X"FFFFFFFFFF44485151484444444851515151D95144444848485148484844FFFF",
      INIT_53 => X"FFFFFFFF444851D951444444444851D9E6E6D95144444448D9E6E6484844FFFF",
      INIT_54 => X"FFFFFFFF4448D9514844484844444851D9D9515148444448D9E6E6514844FFFF",
      INIT_55 => X"FFFFFFFF444851484451514848444851515148485148444851D9D951484444FF",
      INIT_56 => X"FFFFFFFFFF44484451D9D948444448484848D9D95148484851D9515144484444",
      INIT_57 => X"FFFFFFFFFFFF444451D9E6D948484851D9D95151514848515151514844484844",
      INIT_58 => X"FFFFFFFFFFFFFF444851D9D95151484851515151484851515151484444484844",
      INIT_59 => X"FFFFFFFFFFFFFFFF4448485151515148484848484848515151484444444444FF",
      INIT_5A => X"FFFFFFFFFFFFFFFFFF444448484848444444444848484848444444444444FFFF",
      INIT_5B => X"FFFFFFFFFFFFFFFFFFFFFF444444444444FFFF444444444444FFFFFFFFFFFFFF",
      INIT_5C => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_5D => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_5E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_5F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_60 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_61 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_62 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF2121212121FFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_63 => X"FFFFFFFFFFFFFFFFFFFFFFFF212133BBBBBB332121FFFFFFFFFFFFFFFFFFFFFF",
      INIT_64 => X"FFFFFFFFFFFFFFFFFFFFFF2133BBBBBBBBBBBBBB3321FFFFFFFFFFFFFFFFFFFF",
      INIT_65 => X"FFFFFFFFFFFFFFFFFFFFFF2133BBBBBBBBBBBBBB333321FFFFFFFFFFFFFFFFFF",
      INIT_66 => X"FFFFFFFFFFFFFF1515152133BBBBBBBBBBBBBBBBBB33211515FFFFFFFFFFFFFF",
      INIT_67 => X"FFFFFFFFFFFFFF8D8D152133BBBBBBBBBBBBBBBBBB33218D8DFFFFFFFFFFFFFF",
      INIT_68 => X"FFFFFFFFFFFFFFFF8D8D213333BBBBBBBBBBBBBB3333218DFFFFFFFFFFFFFFFF",
      INIT_69 => X"FFFFFFFF4848488D151521333333BBBBBBBBBB33333321158D8DFFFFFFFFFFFF",
      INIT_6A => X"FFFFFF4851D951488D158D2133333333333333333321D98D15158D4848FFFFFF",
      INIT_6B => X"FFFF4851D9E6E651448D8DD92133333333333333212115158D8D48515148FFFF",
      INIT_6C => X"FFFF4851D9E6E6D9448D158DD92121212121212121D9448D44444848515148FF",
      INIT_6D => X"FFFF484851515148448DE2158DD9D921212121D98D15158D44444448D9D948FF",
      INIT_6E => X"FFFF994844444444FFE28D441515158D158D8D158D448D15FF444851D9D948FF",
      INIT_6F => X"FFFFFF4448484848441544448DE28D448D158DE28D4444FF444851515148FFFF",
      INIT_70 => X"FFFFFFFF445148484844444444E2485144E2488D44484444484851514844FFFF",
      INIT_71 => X"FFFFFFFFFF4451D9514848484815484848E25148484444444851484844FFFFFF",
      INIT_72 => X"FFFFFFFF48484851D9D95148444848485151D9514444444848484844FFFFFFFF",
      INIT_73 => X"FFFFFF4851514844485148444448485151D9D95144444448484444FFFFFFFFFF",
      INIT_74 => X"FFFF4851514848484448484848445148D9E6D948444448484448484844FFFFFF",
      INIT_75 => X"FFFF48514848484448484848484448D9E6D9515148444844485151514844FFFF",
      INIT_76 => X"FFFF44484844444448484848444851D9D9515151484448485151D9D94844FFFF",
      INIT_77 => X"FFFF44484444444848514848444851515151D948444448484851D9D94844FFFF",
      INIT_78 => X"FFFFFF44444444484851515148484851D9D951515148484851D9D9514844FFFF",
      INIT_79 => X"FFFFFFFF4444444448515151515151D9D9484851515151515151514844FFFFFF",
      INIT_7A => X"FFFFFFFFFFFF44444448485151515151484444444851515148444444FFFFFFFF",
      INIT_7B => X"FFFFFFFFFFFFFFFF444444444444444444FFFFFF444444444444FFFFFFFFFFFF",
      INIT_7C => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_7D => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_7E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_7F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 3) => addra(11 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 3) => addrb(11 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 0) => B"00000000000000000000000000000000",
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 8) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 8),
      DOADO(7 downto 0) => DOADO(7 downto 0),
      DOBDO(31 downto 8) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 8),
      DOBDO(7 downto 0) => DOBDO(7 downto 0),
      DOPADOP(3 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 1),
      DOPADOP(0) => DOPADOP(0),
      DOPBDOP(3 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 1),
      DOPBDOP(0) => DOPBDOP(0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1__2_n_0\,
      ENBWREN => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2__2_n_0\,
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3 downto 0) => B"0000",
      WEBWE(7 downto 0) => B"00000000"
    );
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => addra(13),
      I1 => addra(12),
      I2 => addra(14),
      O => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_1__2_n_0\
    );
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => addrb(13),
      I1 => addrb(12),
      I2 => addrb(14),
      O => \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_i_2__2_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized7\ is
  port (
    p_7_out : out STD_LOGIC_VECTOR ( 8 downto 0 );
    p_6_out : out STD_LOGIC_VECTOR ( 8 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena_array : in STD_LOGIC_VECTOR ( 0 to 0 );
    enb_array : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized7\ : entity is "blk_mem_gen_prim_wrapper_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized7\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized7\ is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 15 downto 8 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 15 downto 8 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 to 1 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 1,
      DOB_REG => 1,
      INITP_00 => X"FFE1EFFFFFE10FFFFFE3AFFFFFFFFFFFFFFC1FFFFFF9DFFFFFF7C3FFFFF421FF",
      INITP_01 => X"FFE448FFFFFFFFFFFFFA5FFFFFFBC3FFFFF843FFFFFAE3FFFFFFFFFFFFFD2FFF",
      INITP_02 => X"FFF3C3FFFFE8A5FFFFDBD6EFFE1B5617FFFFFFFFFFF9DFFFFFFA03FFFFF381FF",
      INITP_03 => X"FFD600EFFF0EA0D7FFFFCCFFFFF330FFFFE0E0FFFDC01AFFFAC0DC3FFFFC5FFF",
      INITP_04 => X"FFF99FFFFFF203FFFFE8F5FFFFD7C6FFFFD7E0FFFFCCFFFFFFC373FFFFC181FF",
      INITP_05 => X"FDFA3BFFFD78F7FFFF7BA5FFFFFD9FFFFFFDC7FFFFFEE7FFFF1883BFFEE4997B",
      INITP_06 => X"FF3C388DFFE2FFBFFFF6BFDFFFFB086FFFFFC32FFFF57BBFFFFFD8FFFEFF5DFF",
      INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8037FE7F238B7FB7FFFDFFF3E798F",
      INIT_00 => X"FFFFFFFFFFFFFFFFFFFFFFFF8D1A2F2F2F2F1A8D8D8D8DFFFFFFFFFFFFFFFFFF",
      INIT_01 => X"FFFFFFFFFFFFFFFFFFFFFFFF8D1A1A65EE658D8D8D8DFFFFFFFFFFFFFFFFFFFF",
      INIT_02 => X"FFFFFFFFFFFFFFFFFFFFFFFFFF000065EE6500FFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_03 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF0015151500FFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_04 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_05 => X"FFFFFFFFFFFFFFFFFFFFFF008D8D1A1A1A2F1A8DFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_06 => X"FFFFFFFFFFFFFFFFFFFFFF0000008D1A2F2F2F00FFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_07 => X"FFFFFFFFFFFFFFFFFFFFFF00000000D065EE6500FFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_08 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF000C15150C00FFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_09 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_0A => X"FFFFFFFFFFFFFFFFFFFFFFFFFF8D1A2F1A1A1A8D8D00FFFFFFFFFFFFFFFFFFFF",
      INIT_0B => X"FFFFFFFFFFFFFFFFFFFFFFFFFF002F2F2F1A8D000000FFFFFFFFFFFFFFFFFFFF",
      INIT_0C => X"FFFFFFFFFFFFFFFFFFFFFFFFFF0065EE65D000000000FFFFFFFFFFFFFFFFFFFF",
      INIT_0D => X"FFFFFFFFFFFFFFFFFFFFFFFFFF000C15150C00FFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_0E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_0F => X"FFFFFFFFFFFFFFFFFFFFFF008D1A2F2F2F1A8D8D1A8D8D00FFFFFFFFFFFFFFFF",
      INIT_10 => X"FFFFFFFFFFFFFFFFFFFFFFFF8D8D1A65658D8D8D8D8D00FFFFFFFFFFFFFFFFFF",
      INIT_11 => X"FFFFFFFFFFFFFFFFFFFFFFFFFF006515151500000000FFFFFFFFFFFFFFFFFFFF",
      INIT_12 => X"FFFFFFFFFFFFFFFFFFFFFFFFFF00150C0C0C00FFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_13 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_14 => X"FFFFFFFFFFFFFF0000000008518C0C0C8C0C8C0C8C080800C8DCDC9000FFFFFF",
      INIT_15 => X"FFFFFFFFFFFFFFFFFFFF0008D98C590C0C0C8C598C080800FF9090C8FFFFFFFF",
      INIT_16 => X"FFFFFFFFFFFFFFFFFFFFFF00518C2626592659268C0800FFFFFFFFFFFFFFFFFF",
      INIT_17 => X"FFFFFFFFFFFFFFFFFFFFFFFF00000851510800000000FFFFFFFFFFFFFFFFFFFF",
      INIT_18 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF0054549000FFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_19 => X"FFFFFFFFFF000800FFFF00000000C8009090000C0C0C8C000000FFFFFFFFFFFF",
      INIT_1A => X"FFFFFFFFFFFF00FFFFFF00C8000000000000265959265900FFFFFFFFFFFFFFFF",
      INIT_1B => X"FFFFFFFFFFFFFFFFFFFFFF00C8C800000851D98C8C8C8C00FFFFFFFFFFFFFFFF",
      INIT_1C => X"FFFFFFFFFFFFFFFFFFFFFFFF0000FFFF00005151C8C8C800FFFFFFFFFFFFFFFF",
      INIT_1D => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00C890905454FFFFFFFFFFFFFFFF",
      INIT_1E => X"FFFFFFFFFFFFFFFF0000008C0C0C0C00900090C800000000FFFF000800FFFFFF",
      INIT_1F => X"FFFFFFFFFFFFFFFFFFFF005926595926000000000000C800FFFFFF00FFFFFFFF",
      INIT_20 => X"FFFFFFFFFFFFFFFFFFFF008C8C8C8CD951000000C8C800FFFFFFFFFFFFFFFFFF",
      INIT_21 => X"FFFFFFFFFFFFFFFFFFFF00C8C8C8515100FFFFFF0000FFFFFFFFFFFFFFFFFFFF",
      INIT_22 => X"FFFFFFFFFFFFFFFFFFFF54549090C800FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_23 => X"FFFFFFFFFFFFFFFFFFFF00080051D9D9D90C51EA62000000FFFFFFFFFFFFFFFF",
      INIT_24 => X"FFFFFFFFFFFFFFFFFFFF00510008D9D9D959000000080800FFFFFFFFFFFFFFFF",
      INIT_25 => X"FFFFFFFFFFFFFFFFFFFFFF0008000000085959598C0800FFFFFFFFFFFFFFFFFF",
      INIT_26 => X"FFFFFFFFFFFFFFFFFFFFFFFF000008DCDC0000000000FFFFFFFFFFFFFFFFFFFF",
      INIT_27 => X"FFFFFFFFFFFFFFFFFFFFFFFFFF00DC90905400FFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_28 => X"FFFFFFC804040400D1D10C0000116A6A990000999900000400D1D10CD100C8FF",
      INIT_29 => X"FFFFC8C804040404000000040400006A110000000000040404000CD10C04C8C8",
      INIT_2A => X"FFFFC8C804C8C804040404040404D1DD6969D10000040404040404C8C804C8C8",
      INIT_2B => X"FFFFC804FFC8040404FFFFFFFFFF00D1D1D1000000FFFFFFFF040404C8FF04C8",
      INIT_2C => X"FFFF04FFFF0404FFFFFFFFFFFFFF00D169DD00FFFFFFFFFFFFFFFF0404FFFF04",
      INIT_2D => X"FFFFFFFFFFFF04C8D1C80404FF000C0C69DDD16A6A1100FFFFFFFFFFFFFFFFFF",
      INIT_2E => X"FFFFFFFFFFFF0004D1FF0404FF000000D1D10C116A990CFFFFFFFFFFFFFFFFFF",
      INIT_2F => X"FFFFFFFFFFFF0004C8FFFFFFFF000C0000000C0CD1DD690CFFFFFFFFFFFFFFFF",
      INIT_30 => X"FFFFFFFFFFFFFF0004FFFFFFFFFF0C0C00FF000C0C0C00FFFFFFFFFFFFFFFFFF",
      INIT_31 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000CD1DDDD00FFFFFFFFFFFFFFFF",
      INIT_32 => X"FFFFFFFFFFFFFFFFFFFFFFFF00116AD1DD690CD10C000404C8D1C804FFFFFFFF",
      INIT_33 => X"FFFFFFFFFFFFFFFFFFFFFFFF0C99110CD1D100000000040400D10400FFFFFFFF",
      INIT_34 => X"FFFFFFFFFFFFFFFFFFFFFF0C69DD0C0C000000000C00000000C80400FFFFFFFF",
      INIT_35 => X"FFFFFFFFFFFFFFFFFFFFFFFF000C0C00FF000C0C0CFFFFFFFF0400FFFFFFFFFF",
      INIT_36 => X"FFFFFFFFFFFFFFFFFFFFFF00DDDD0C00FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF",
      INIT_37 => X"FFFF04C8C80404C8D1D1C8C8C8C8D1D1D1D1C804C8D1D1D1C8D1D1D10404D104",
      INIT_38 => X"FFFFFF04040404C8D1D1C8C8C8C8C8D1D1C80404C8D1D1C8C8D1D1D10404C804",
      INIT_39 => X"FFFFFF0404000404D1C8C8040404C8C8C8040404C8C8C804C8C8D1C804040404",
      INIT_3A => X"FFFFFF0000FFFF04C80404FF0000040000000404040000000400C8040004FFFF",
      INIT_3B => X"FFFFFFFFFFFFFF0404FFFFFFFF000000000000000000FFFF00FF04FFFFFFFFFF",
      INIT_3C => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_3D => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_3E => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_3F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_A => X"00000",
      INIT_B => X"00000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(13 downto 3) => addra(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"000",
      ADDRBWRADDR(13 downto 3) => addrb(10 downto 0),
      ADDRBWRADDR(2 downto 0) => B"000",
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DIADI(15 downto 0) => B"0000000000000000",
      DIBDI(15 downto 0) => B"0000000000000000",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"00",
      DOADO(15 downto 8) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOADO_UNCONNECTED\(15 downto 8),
      DOADO(7 downto 0) => p_7_out(7 downto 0),
      DOBDO(15 downto 8) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED\(15 downto 8),
      DOBDO(7 downto 0) => p_6_out(7 downto 0),
      DOPADOP(1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOPADOP_UNCONNECTED\(1),
      DOPADOP(0) => p_7_out(8),
      DOPBDOP(1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED\(1),
      DOPBDOP(0) => p_6_out(8),
      ENARDEN => ena_array(0),
      ENBWREN => enb_array(0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1 downto 0) => B"00",
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized8\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 0 to 0 );
    doutb : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized8\ : entity is "blk_mem_gen_prim_wrapper_init";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized8\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized8\ is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 1,
      DOB_REG => 1,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"FF7FFFFFFF7FFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_01 => X"FEAFFE2FFEAFFEAFFF71B1DFFF6778DFFEECE6EFFEFDF7EFFEFFFFEFFEFFFFEF",
      INIT_02 => X"FF80003FFF7001DFFF7191DFFF84043FFF83183FFFCFFE7FFFDFFF7FFF1FFF1F",
      INIT_03 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFBBFFFFFC3BFFFFF839FFFFF039FFFFC4E47F",
      INIT_04 => X"FEFFFFFFFEFFFFFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_05 => X"FF0BFEFFFE4F7EFFFCC77EFFFDED8CFFFDFD3B7FFDFE677FFDFEEFBFFDFFFFBF",
      INIT_06 => X"FFE087FFFFE01BFFFFC01FFFFF805FFFFF8003FFFFC77DFFFF81FEFFFF03FEFF",
      INIT_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFF33FFFFFF307FFFFE207FFFFE387FFFFE007FF",
      INIT_08 => X"FFFFFFBFFFFFFFBFFFFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_09 => X"FFBFE87FFFBF793FFFBF719FFF98DBDFFF6E5FDFFF733FDFFEFBBFDFFEFFFFDF",
      INIT_0A => X"FFF083FFFFEC03FFFFFC01FFFFFD00FFFFE000FFFFDF71FFFFBFC0FFFFBFE07F",
      INIT_0B => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFE67FFFFF067FFFFF023FFFFF0E3FFFFF003FF",
      INIT_0C => X"FF7FFFFFFF7FFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_0D => X"FE9F7FAFFE92BFAFFF6DFFDFFF7FFFDFFEFFFFEFFEFFFFEFFEFFFFEFFEFFFFEF",
      INIT_0E => X"FFE0E0FFFFC0007FFF81F03FFF87FC3FFF80003FFFC0607FFFCDF27FFF1FFB1F",
      INIT_0F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF0BFFFFF87BFFFFF031FFFFE038FFFFE378FF",
      INIT_10 => X"FE01E69FFF001EBFFF007DBFFF801B7FFF87FF7FFFC3FCFFFFF1F3FFFFFC0FFF",
      INIT_11 => X"FC07FC0FFE00011FFF00003FFF00003FFE00021FFE040E1FFE07FD1FFE03FB1F",
      INIT_12 => X"FC30C00BF8300073F9300103F8800003F800E003F803FC07F8000087F801E007",
      INIT_13 => X"FFFFFFFFFFFFFFFFFFFE4FFFFFF003FFFFE2D1FFFFC0008FFE000067FC000067",
      INIT_14 => X"FF0187CFFF00F00FFF8C7F1FFF8F9C3FFFC7FF3FFFE3FC7FFFF0F1FFFFFC07FF",
      INIT_15 => X"FF07001FFF80003FFF80003FFF00003FFF00003FFF00601FFF007E0FFF003FCF",
      INIT_16 => X"F80091FFF80038FFFC00107FFC00003FFE000E3FFF00787FFF03C03FFF00FF9F",
      INIT_17 => X"FFFFFFFFFFFFFFFFFFC6F0FFFFC000FFFFC001FFFE8014FFF80000FFF80040FF",
      INIT_18 => X"FCF8C03FFC03803FFE3F187FFF0EF87FFF3FF0FFFF8FE1FFFFE383FFFFF81FFF",
      INIT_19 => X"FE7F803FFF00007FFF00007FFF00003FFF00003FFE01003FFC1F003FFCFE003F",
      INIT_1A => X"FFE28007FFC60007FF82000FFF00000FFF1C001FFF87003FFF01E03FFE38003F",
      INIT_1B => X"FFFFFFFFFFFFFFFFFFC3B0FFFFC000FFFFE000FFFFCA005FFFC00007FFC10007",
      INIT_1C => X"FE03FE9FFF03FEBFFF03FDBFFF9FFB7FFF8FFF7FFFC7FCFFFFE1F1FFFFF807FF",
      INIT_1D => X"FC0C400FFE00011FFF00003FFF00003FFE00001FFE00001FFE00C11FFE01F31F",
      INIT_1E => X"FC000F07F8000F03F8000F03F8001E03F8011E03F80F1407F8003487F8032807",
      INIT_1F => X"FFFFFFFFFFFFFFFFFFFE17FFFFF0E3FFFFE001FFFFC001FFFFC004FFFE000E4F",
      INIT_20 => X"FF00001FFF00001FFF00001FFF80003FFF80003FFFC0007FFFF001FFFFFC07FF",
      INIT_21 => X"FF80003FFF80003FFF0F1E1FFF0F1E1FFF060C1FFF00001FFF00001FFF00001F",
      INIT_22 => X"F8000003FC000003FC000007FC000007FE00000FFE00000FFF00001FFF00001F",
      INIT_23 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFF7FC0F9FF7300339F9000011",
      INIT_24 => X"FFC00007FFC00007FFC00007FFE0000FFFE0000FFFF0001FFFF8007FFFFE01FF",
      INIT_25 => X"FE00000FFF00060FFF000F0FFF800F0FFF800607FFC00007FFC00007FFC00007",
      INIT_26 => X"FE60007FFE20007FEE00007FE40000FFF000007FF000003FF800001FFC00000F",
      INIT_27 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E7FFFFF003FFFE6003FFFE0003FFFE0003F",
      INIT_28 => X"F80000FFF80000FFF80000FFFC0001FFFC0001FFFE0003FFFF8007FFFFE01FFF",
      INIT_29 => X"FC00001FFC18003FFC3C003FFC3C007FF818007FF80000FFF80000FFF80000FF",
      INIT_2A => X"FF80019FFF80011FFF80001DFFC00009FF800003FF000003FE000007FC00000F",
      INIT_2B => X"FFFFFFFFFFFFFFFFFFFFFFFFFF9C7FFFFF003FFFFF0019FFFF0001FFFF0001FF",
      INIT_2C => X"FE00003FFE00003FFE00003FFF00007FFF00007FFF8000FFFFE003FFFFF80FFF",
      INIT_2D => X"FF00007FFF00003FFE00003FFE00003FFE00003FFE00003FFE00003FFE00003F",
      INIT_2E => X"E4000047E0000007F0000007F000000FF800000FFC00001FFE00001FFE00003F",
      INIT_2F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0F77EEF00663EE400463",
      INIT_30 => X"FF7FFFBFFF7FFFBFFF3FFFBFFFBFFF7FFF9FFF7FFFCFFEFFFFF3FBFFFFFC07FF",
      INIT_31 => X"FF07583FFF9BFE7FFFA1F17FFFA1F17FFF01F03FFF3EEF3FFF7FFFBFFF7FFFBF",
      INIT_32 => X"F1340823F0600063F8775807F8015007FC0E0C07FC41F08FFDE2A9EFFEE3F9DF",
      INIT_33 => X"FFFFFFFFFFFFFFFFD9FF67E6C8780784C0007000C0001000E0001801E0260841",
      INIT_34 => X"FFDFFFEFFFDFFFEFFFEFFFEFFFEFFFDFFFF7FFDFFFFBFFBFFFFDFE7FFFFE01FF",
      INIT_35 => X"FFF8603FFFF8BFDFFFF0BF1FFFE223DFFFE461DFFFCFC1DFFFCF20CFFFDF7EEF",
      INIT_36 => X"FCC259FFFE4018FFFE201AFFFF0002FFFF803DFFFFC781FFFFE3007FFFF00ABF",
      INIT_37 => X"FFFFFFFFFFFFFFFFFFFBFFFFFE79E0FFFC7B01FFFCCC00FFFC8809FFFC8001FF",
      INIT_38 => X"FDFFFEFFFDFFFEFFFDFFFDFFFEFFFDFFFEFFFBFFFF7FF7FFFF9FEFFFFFE01FFF",
      INIT_39 => X"FF0187FFFEFE47FFFE3F43FFFEF111FFFEE189FFFEE0FCFFFCC13CFFFDDFBEFF",
      INIT_3A => X"FFE690CFFFC6009FFFD6011FFFD0003FFFEF007FFFE078FFFF8031FFFF5403FF",
      INIT_3B => X"FFFFFFFFFFFFFFFFFFFFB3BFFFC1839FFFE0300FFFC0084FFFE4004FFFE0004F",
      INIT_3C => X"FF7FFFBFFF7FFFBFFF3FFFBFFFBFFF7FFF9FFF7FFFCFFEFFFFF3FBFFFFFC0FFF",
      INIT_3D => X"FF00C03FFF00C03FFF0CCC3FFF1F3E3FFF3FFF3FFF3FFF3FFF7FFFBFFF7FFFBF",
      INIT_3E => X"C00739E6E00C0DE5E00803CDF00001DBF00000F3F8006067FC01FC4FFE00021F",
      INIT_3F => X"FFFFFFFFFFFFFFFFFE78035FE6100003E0800020E0C18670C0C3C772C047E3F2",
      INIT_40 => X"FF1FFC1FFE1FFC7FFF8FF97FFFC7F1FFFFC801FFFFFC1BFFFFFEDFFFFFFFFFFF",
      INIT_41 => X"FE800085FC03E07FFF8DD87FFF8A287FFF181C7FFF1F1C7FFE1F3C3FFF9FFC3F",
      INIT_42 => X"EFEFF78DEFAFEFBFF767EF7FFCC7F3BFFBC7F7FFF78EEFEFEF9260F7FF40007B",
      INIT_43 => X"FFFFFFFFFFFFFFFFFFFFFFFFFE07007FF8E0FF1FF7F3FF87EFFFFFC3EFE7F7C5",
      INIT_44 => X"FFFEFFFFFFFC7FFFFFF87FFFFFFC3FFFFFFF3FFFFFFFBFFFFFFFFFFFFFFFFFFF",
      INIT_45 => X"FB80C073FEE01E1FFE263ECFFCF43D0FFBF67C0FFFF47CCFFFF8FFCFFFFCFFFF",
      INIT_46 => X"DBFFFFCFCBF7F3D7E7EFF7BBF7EBF37BF7E1F87BF7F1F8F7F6E1EAEFFB01C06F",
      INIT_47 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80C1FFF03F1E7FE0FFFFBFD9FFFFDF",
      INIT_48 => X"FFFFEFFFFFFFC7FFFFFFC3FFFFFF87FFFFFF1FFFFFFFBFFFFFFFFFFFFFFFFFFF",
      INIT_49 => X"F380E077EF8020EFFE1E01DFFCDF0D1FFC2F87CFFC0FCFF7FCFFE3FFFFFFE7FF",
      INIT_4A => X"FCFFFFF6FAF3FFF4F77BFDF9F7B3F9FBF787F1FBFBC7F3FBFDD5F1DBFD80F037",
      INIT_4B => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0607FFF9E1F03FF7FFFC1FEFFFFE6",
      INIT_4C => X"FE3FFE7FFE3FFE7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
      INIT_4D => X"EF00017FD08000BFFF01E01FFF07F83FFF0FFC3FFF1FFC1FFE3FFE3FFF3FFEFF",
      INIT_4E => X"D1F7F3FBD8F7FBFBFEFBFAFBFF7BF377FEE7F19FFFF7F1EFFBFBB8F7F78324FB",
      INIT_4F => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00703FFC7F8F8FF0FFFFF7E1FFFFFB",
      INIT_50 => X"FFE0EFFFFFE0EFFFFFE04FFFFFFFFFFFFFFDDFFFFFF9DFFFFFF1C3FFFFF3C1FF",
      INIT_51 => X"FFE380FFFFFFFFFFFFF99FFFFFFB83FFFFFB83FFFFF903FFFFFFFFFFFFFCCFFF",
      INIT_52 => X"FFF003FFFFE351FFFFC0008FFE000067FFFFFFFFFFFA1FFFFFFBC3FFFFF181FF",
      INIT_53 => X"FFC900EFFF0000C7FFFFC3FFFFF300FFFFE000FFFDC024FFF8C0003FFFFD9FFF",
      INIT_54 => X"FFFA5FFFFFF183FFFFE001FFFFC000FFFFC018FFFFF0FFFFFFC073FFFFC001FF",
      INIT_55 => X"FC7806FFFCC809FFFC88D9FFD9FCDFE6C87C0784C001C000C0010000E0030001",
      INIT_56 => X"C0C3C772FFECFFBFFFF0879FFFEC000FFFF0004FFFF2C04FFFFFC6FFFE7C41FF",
      INIT_57 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE78035FE6100003E0800020E0C18670",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 1,
      READ_WIDTH_B => 1,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 1,
      WRITE_WIDTH_B => 1
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 0) => addra(14 downto 0),
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 0) => addrb(14 downto 0),
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clkb,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 0) => B"00000000000000000000000000000000",
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 1),
      DOADO(0) => douta(0),
      DOBDO(31 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 1),
      DOBDO(0) => doutb(0),
      DOPADOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 0),
      DOPBDOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => '1',
      ENBWREN => '1',
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '1',
      REGCEB => '1',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3 downto 0) => B"0000",
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width is
  port (
    \douta[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \doutb[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    \addra[14]\ : in STD_LOGIC;
    \addrb[14]\ : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 13 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 13 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width is
begin
\prim_init.ram\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init
     port map (
      addra(13 downto 0) => addra(13 downto 0),
      \addra[14]\ => \addra[14]\,
      addrb(13 downto 0) => addrb(13 downto 0),
      \addrb[14]\ => \addrb[14]\,
      clka => clka,
      clkb => clkb,
      \douta[0]\(0) => \douta[0]\(0),
      \doutb[0]\(0) => \doutb[0]\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\ is
  port (
    \douta[1]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \doutb[1]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    \addra[14]\ : in STD_LOGIC;
    \addrb[14]\ : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 12 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 12 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\ is
begin
\prim_init.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized0\
     port map (
      addra(12 downto 0) => addra(12 downto 0),
      \addra[14]\ => \addra[14]\,
      addrb(12 downto 0) => addrb(12 downto 0),
      \addrb[14]\ => \addrb[14]\,
      clka => clka,
      clkb => clkb,
      \douta[1]\(1 downto 0) => \douta[1]\(1 downto 0),
      \doutb[1]\(1 downto 0) => \doutb[1]\(1 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\ is
  port (
    \douta[1]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \doutb[1]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    \addra[14]\ : in STD_LOGIC;
    \addrb[14]\ : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 13 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 13 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\ is
begin
\prim_init.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized1\
     port map (
      addra(13 downto 0) => addra(13 downto 0),
      \addra[14]\ => \addra[14]\,
      addrb(13 downto 0) => addrb(13 downto 0),
      \addrb[14]\ => \addrb[14]\,
      clka => clka,
      clkb => clkb,
      \douta[1]\(0) => \douta[1]\(0),
      \doutb[1]\(0) => \doutb[1]\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\ is
  port (
    \douta[9]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \doutb[9]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \douta[10]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \doutb[10]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\ is
begin
\prim_init.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized2\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      addrb(14 downto 0) => addrb(14 downto 0),
      clka => clka,
      clkb => clkb,
      \douta[10]\(0) => \douta[10]\(0),
      \douta[9]\(7 downto 0) => \douta[9]\(7 downto 0),
      \doutb[10]\(0) => \doutb[10]\(0),
      \doutb[9]\(7 downto 0) => \doutb[9]\(7 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized3\ is
  port (
    \douta[9]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \doutb[9]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \douta[10]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \doutb[10]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized3\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized3\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized3\ is
begin
\prim_init.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized3\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      addrb(14 downto 0) => addrb(14 downto 0),
      clka => clka,
      clkb => clkb,
      \douta[10]\(0) => \douta[10]\(0),
      \douta[9]\(7 downto 0) => \douta[9]\(7 downto 0),
      \doutb[10]\(0) => \doutb[10]\(0),
      \doutb[9]\(7 downto 0) => \doutb[9]\(7 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized4\ is
  port (
    \douta[9]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \doutb[9]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \douta[10]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \doutb[10]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized4\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized4\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized4\ is
begin
\prim_init.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized4\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      addrb(14 downto 0) => addrb(14 downto 0),
      clka => clka,
      clkb => clkb,
      \douta[10]\(0) => \douta[10]\(0),
      \douta[9]\(7 downto 0) => \douta[9]\(7 downto 0),
      \doutb[10]\(0) => \doutb[10]\(0),
      \doutb[9]\(7 downto 0) => \doutb[9]\(7 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized5\ is
  port (
    \douta[9]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \doutb[9]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \douta[10]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \doutb[10]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized5\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized5\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized5\ is
begin
\prim_init.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized5\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      addrb(14 downto 0) => addrb(14 downto 0),
      clka => clka,
      clkb => clkb,
      \douta[10]\(0) => \douta[10]\(0),
      \douta[9]\(7 downto 0) => \douta[9]\(7 downto 0),
      \doutb[10]\(0) => \doutb[10]\(0),
      \doutb[9]\(7 downto 0) => \doutb[9]\(7 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized6\ is
  port (
    DOADO : out STD_LOGIC_VECTOR ( 7 downto 0 );
    DOBDO : out STD_LOGIC_VECTOR ( 7 downto 0 );
    DOPADOP : out STD_LOGIC_VECTOR ( 0 to 0 );
    DOPBDOP : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized6\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized6\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized6\ is
begin
\prim_init.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized6\
     port map (
      DOADO(7 downto 0) => DOADO(7 downto 0),
      DOBDO(7 downto 0) => DOBDO(7 downto 0),
      DOPADOP(0) => DOPADOP(0),
      DOPBDOP(0) => DOPBDOP(0),
      addra(14 downto 0) => addra(14 downto 0),
      addrb(14 downto 0) => addrb(14 downto 0),
      clka => clka,
      clkb => clkb
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized7\ is
  port (
    p_7_out : out STD_LOGIC_VECTOR ( 8 downto 0 );
    p_6_out : out STD_LOGIC_VECTOR ( 8 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    ena_array : in STD_LOGIC_VECTOR ( 0 to 0 );
    enb_array : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 10 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized7\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized7\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized7\ is
begin
\prim_init.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized7\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      ena_array(0) => ena_array(0),
      enb_array(0) => enb_array(0),
      p_6_out(8 downto 0) => p_6_out(8 downto 0),
      p_7_out(8 downto 0) => p_7_out(8 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized8\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 0 to 0 );
    doutb : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized8\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized8\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized8\ is
begin
\prim_init.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper_init__parameterized8\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      addrb(14 downto 0) => addrb(14 downto 0),
      clka => clka,
      clkb => clkb,
      douta(0) => douta(0),
      doutb(0) => doutb(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr is
  port (
    douta : out STD_LOGIC_VECTOR ( 11 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 11 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr is
  signal ena_array : STD_LOGIC_VECTOR ( 10 to 10 );
  signal enb_array : STD_LOGIC_VECTOR ( 10 to 10 );
  signal p_6_out : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal p_7_out : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal ram_douta : STD_LOGIC;
  signal ram_doutb : STD_LOGIC;
  signal \ram_ena_inferred__0/i__n_0\ : STD_LOGIC;
  signal ram_ena_n_0 : STD_LOGIC;
  signal \ram_enb_inferred__0/i__n_0\ : STD_LOGIC;
  signal ram_enb_n_0 : STD_LOGIC;
  signal \ramloop[1].ram.r_n_0\ : STD_LOGIC;
  signal \ramloop[1].ram.r_n_1\ : STD_LOGIC;
  signal \ramloop[1].ram.r_n_2\ : STD_LOGIC;
  signal \ramloop[1].ram.r_n_3\ : STD_LOGIC;
  signal \ramloop[2].ram.r_n_0\ : STD_LOGIC;
  signal \ramloop[2].ram.r_n_1\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_0\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_1\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_10\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_11\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_12\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_13\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_14\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_15\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_16\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_17\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_2\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_3\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_4\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_5\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_6\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_7\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_8\ : STD_LOGIC;
  signal \ramloop[3].ram.r_n_9\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_0\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_1\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_10\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_11\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_12\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_13\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_14\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_15\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_16\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_17\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_2\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_3\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_4\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_5\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_6\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_7\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_8\ : STD_LOGIC;
  signal \ramloop[4].ram.r_n_9\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_0\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_1\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_10\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_11\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_12\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_13\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_14\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_15\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_16\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_17\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_2\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_3\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_4\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_5\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_6\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_7\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_8\ : STD_LOGIC;
  signal \ramloop[5].ram.r_n_9\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_0\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_1\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_10\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_11\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_12\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_13\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_14\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_15\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_16\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_17\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_2\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_3\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_4\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_5\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_6\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_7\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_8\ : STD_LOGIC;
  signal \ramloop[6].ram.r_n_9\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_0\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_1\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_10\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_11\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_12\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_13\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_14\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_15\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_16\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_17\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_2\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_3\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_4\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_5\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_6\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_7\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_8\ : STD_LOGIC;
  signal \ramloop[7].ram.r_n_9\ : STD_LOGIC;
begin
\bindec_a.bindec_inst_a\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_bindec
     port map (
      addra(3 downto 0) => addra(14 downto 11),
      ena_array(0) => ena_array(10)
    );
\bindec_b.bindec_inst_b\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_bindec_0
     port map (
      addrb(3 downto 0) => addrb(14 downto 11),
      enb_array(0) => enb_array(10)
    );
\has_mux_a.A\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_mux
     port map (
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\(1) => \ramloop[1].ram.r_n_0\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\(0) => \ramloop[1].ram.r_n_1\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_0\(0) => ram_douta,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_1\(0) => \ramloop[2].ram.r_n_0\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(7) => \ramloop[6].ram.r_n_0\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(6) => \ramloop[6].ram.r_n_1\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(5) => \ramloop[6].ram.r_n_2\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(4) => \ramloop[6].ram.r_n_3\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(3) => \ramloop[6].ram.r_n_4\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(2) => \ramloop[6].ram.r_n_5\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(1) => \ramloop[6].ram.r_n_6\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(0) => \ramloop[6].ram.r_n_7\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(7) => \ramloop[5].ram.r_n_0\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(6) => \ramloop[5].ram.r_n_1\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(5) => \ramloop[5].ram.r_n_2\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(4) => \ramloop[5].ram.r_n_3\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(3) => \ramloop[5].ram.r_n_4\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(2) => \ramloop[5].ram.r_n_5\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(1) => \ramloop[5].ram.r_n_6\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(0) => \ramloop[5].ram.r_n_7\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(7) => \ramloop[4].ram.r_n_0\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(6) => \ramloop[4].ram.r_n_1\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(5) => \ramloop[4].ram.r_n_2\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(4) => \ramloop[4].ram.r_n_3\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(3) => \ramloop[4].ram.r_n_4\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(2) => \ramloop[4].ram.r_n_5\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(1) => \ramloop[4].ram.r_n_6\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(0) => \ramloop[4].ram.r_n_7\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(7) => \ramloop[3].ram.r_n_0\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(6) => \ramloop[3].ram.r_n_1\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(5) => \ramloop[3].ram.r_n_2\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(4) => \ramloop[3].ram.r_n_3\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(3) => \ramloop[3].ram.r_n_4\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(2) => \ramloop[3].ram.r_n_5\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(1) => \ramloop[3].ram.r_n_6\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(0) => \ramloop[3].ram.r_n_7\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_3\(0) => \ramloop[6].ram.r_n_16\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_4\(0) => \ramloop[5].ram.r_n_16\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_5\(0) => \ramloop[4].ram.r_n_16\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_6\(0) => \ramloop[3].ram.r_n_16\,
      DOADO(7) => \ramloop[7].ram.r_n_0\,
      DOADO(6) => \ramloop[7].ram.r_n_1\,
      DOADO(5) => \ramloop[7].ram.r_n_2\,
      DOADO(4) => \ramloop[7].ram.r_n_3\,
      DOADO(3) => \ramloop[7].ram.r_n_4\,
      DOADO(2) => \ramloop[7].ram.r_n_5\,
      DOADO(1) => \ramloop[7].ram.r_n_6\,
      DOADO(0) => \ramloop[7].ram.r_n_7\,
      DOPADOP(0) => \ramloop[7].ram.r_n_16\,
      addra(3 downto 0) => addra(14 downto 11),
      clka => clka,
      douta(10 downto 0) => douta(10 downto 0),
      p_7_out(8 downto 0) => p_7_out(8 downto 0)
    );
\has_mux_b.B\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_mux__parameterized0\
     port map (
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\(1) => \ramloop[1].ram.r_n_2\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram\(0) => \ramloop[1].ram.r_n_3\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_0\(0) => ram_doutb,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM18.ram_1\(0) => \ramloop[2].ram.r_n_1\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(7) => \ramloop[6].ram.r_n_8\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(6) => \ramloop[6].ram.r_n_9\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(5) => \ramloop[6].ram.r_n_10\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(4) => \ramloop[6].ram.r_n_11\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(3) => \ramloop[6].ram.r_n_12\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(2) => \ramloop[6].ram.r_n_13\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(1) => \ramloop[6].ram.r_n_14\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram\(0) => \ramloop[6].ram.r_n_15\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(7) => \ramloop[5].ram.r_n_8\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(6) => \ramloop[5].ram.r_n_9\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(5) => \ramloop[5].ram.r_n_10\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(4) => \ramloop[5].ram.r_n_11\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(3) => \ramloop[5].ram.r_n_12\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(2) => \ramloop[5].ram.r_n_13\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(1) => \ramloop[5].ram.r_n_14\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_0\(0) => \ramloop[5].ram.r_n_15\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(7) => \ramloop[4].ram.r_n_8\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(6) => \ramloop[4].ram.r_n_9\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(5) => \ramloop[4].ram.r_n_10\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(4) => \ramloop[4].ram.r_n_11\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(3) => \ramloop[4].ram.r_n_12\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(2) => \ramloop[4].ram.r_n_13\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(1) => \ramloop[4].ram.r_n_14\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_1\(0) => \ramloop[4].ram.r_n_15\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(7) => \ramloop[3].ram.r_n_8\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(6) => \ramloop[3].ram.r_n_9\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(5) => \ramloop[3].ram.r_n_10\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(4) => \ramloop[3].ram.r_n_11\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(3) => \ramloop[3].ram.r_n_12\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(2) => \ramloop[3].ram.r_n_13\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(1) => \ramloop[3].ram.r_n_14\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_2\(0) => \ramloop[3].ram.r_n_15\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_3\(0) => \ramloop[6].ram.r_n_17\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_4\(0) => \ramloop[5].ram.r_n_17\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_5\(0) => \ramloop[4].ram.r_n_17\,
      \DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram_6\(0) => \ramloop[3].ram.r_n_17\,
      DOBDO(7) => \ramloop[7].ram.r_n_8\,
      DOBDO(6) => \ramloop[7].ram.r_n_9\,
      DOBDO(5) => \ramloop[7].ram.r_n_10\,
      DOBDO(4) => \ramloop[7].ram.r_n_11\,
      DOBDO(3) => \ramloop[7].ram.r_n_12\,
      DOBDO(2) => \ramloop[7].ram.r_n_13\,
      DOBDO(1) => \ramloop[7].ram.r_n_14\,
      DOBDO(0) => \ramloop[7].ram.r_n_15\,
      DOPBDOP(0) => \ramloop[7].ram.r_n_17\,
      addrb(3 downto 0) => addrb(14 downto 11),
      clkb => clkb,
      doutb(10 downto 0) => doutb(10 downto 0),
      p_6_out(8 downto 0) => p_6_out(8 downto 0)
    );
ram_ena: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => addra(14),
      O => ram_ena_n_0
    );
\ram_ena_inferred__0/i_\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => addra(13),
      I1 => addra(14),
      O => \ram_ena_inferred__0/i__n_0\
    );
ram_enb: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => addrb(14),
      O => ram_enb_n_0
    );
\ram_enb_inferred__0/i_\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => addrb(13),
      I1 => addrb(14),
      O => \ram_enb_inferred__0/i__n_0\
    );
\ramloop[0].ram.r\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width
     port map (
      addra(13 downto 0) => addra(13 downto 0),
      \addra[14]\ => ram_ena_n_0,
      addrb(13 downto 0) => addrb(13 downto 0),
      \addrb[14]\ => ram_enb_n_0,
      clka => clka,
      clkb => clkb,
      \douta[0]\(0) => ram_douta,
      \doutb[0]\(0) => ram_doutb
    );
\ramloop[1].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\
     port map (
      addra(12 downto 0) => addra(12 downto 0),
      \addra[14]\ => \ram_ena_inferred__0/i__n_0\,
      addrb(12 downto 0) => addrb(12 downto 0),
      \addrb[14]\ => \ram_enb_inferred__0/i__n_0\,
      clka => clka,
      clkb => clkb,
      \douta[1]\(1) => \ramloop[1].ram.r_n_0\,
      \douta[1]\(0) => \ramloop[1].ram.r_n_1\,
      \doutb[1]\(1) => \ramloop[1].ram.r_n_2\,
      \doutb[1]\(0) => \ramloop[1].ram.r_n_3\
    );
\ramloop[2].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\
     port map (
      addra(13 downto 0) => addra(13 downto 0),
      \addra[14]\ => ram_ena_n_0,
      addrb(13 downto 0) => addrb(13 downto 0),
      \addrb[14]\ => ram_enb_n_0,
      clka => clka,
      clkb => clkb,
      \douta[1]\(0) => \ramloop[2].ram.r_n_0\,
      \doutb[1]\(0) => \ramloop[2].ram.r_n_1\
    );
\ramloop[3].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      addrb(14 downto 0) => addrb(14 downto 0),
      clka => clka,
      clkb => clkb,
      \douta[10]\(0) => \ramloop[3].ram.r_n_16\,
      \douta[9]\(7) => \ramloop[3].ram.r_n_0\,
      \douta[9]\(6) => \ramloop[3].ram.r_n_1\,
      \douta[9]\(5) => \ramloop[3].ram.r_n_2\,
      \douta[9]\(4) => \ramloop[3].ram.r_n_3\,
      \douta[9]\(3) => \ramloop[3].ram.r_n_4\,
      \douta[9]\(2) => \ramloop[3].ram.r_n_5\,
      \douta[9]\(1) => \ramloop[3].ram.r_n_6\,
      \douta[9]\(0) => \ramloop[3].ram.r_n_7\,
      \doutb[10]\(0) => \ramloop[3].ram.r_n_17\,
      \doutb[9]\(7) => \ramloop[3].ram.r_n_8\,
      \doutb[9]\(6) => \ramloop[3].ram.r_n_9\,
      \doutb[9]\(5) => \ramloop[3].ram.r_n_10\,
      \doutb[9]\(4) => \ramloop[3].ram.r_n_11\,
      \doutb[9]\(3) => \ramloop[3].ram.r_n_12\,
      \doutb[9]\(2) => \ramloop[3].ram.r_n_13\,
      \doutb[9]\(1) => \ramloop[3].ram.r_n_14\,
      \doutb[9]\(0) => \ramloop[3].ram.r_n_15\
    );
\ramloop[4].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized3\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      addrb(14 downto 0) => addrb(14 downto 0),
      clka => clka,
      clkb => clkb,
      \douta[10]\(0) => \ramloop[4].ram.r_n_16\,
      \douta[9]\(7) => \ramloop[4].ram.r_n_0\,
      \douta[9]\(6) => \ramloop[4].ram.r_n_1\,
      \douta[9]\(5) => \ramloop[4].ram.r_n_2\,
      \douta[9]\(4) => \ramloop[4].ram.r_n_3\,
      \douta[9]\(3) => \ramloop[4].ram.r_n_4\,
      \douta[9]\(2) => \ramloop[4].ram.r_n_5\,
      \douta[9]\(1) => \ramloop[4].ram.r_n_6\,
      \douta[9]\(0) => \ramloop[4].ram.r_n_7\,
      \doutb[10]\(0) => \ramloop[4].ram.r_n_17\,
      \doutb[9]\(7) => \ramloop[4].ram.r_n_8\,
      \doutb[9]\(6) => \ramloop[4].ram.r_n_9\,
      \doutb[9]\(5) => \ramloop[4].ram.r_n_10\,
      \doutb[9]\(4) => \ramloop[4].ram.r_n_11\,
      \doutb[9]\(3) => \ramloop[4].ram.r_n_12\,
      \doutb[9]\(2) => \ramloop[4].ram.r_n_13\,
      \doutb[9]\(1) => \ramloop[4].ram.r_n_14\,
      \doutb[9]\(0) => \ramloop[4].ram.r_n_15\
    );
\ramloop[5].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized4\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      addrb(14 downto 0) => addrb(14 downto 0),
      clka => clka,
      clkb => clkb,
      \douta[10]\(0) => \ramloop[5].ram.r_n_16\,
      \douta[9]\(7) => \ramloop[5].ram.r_n_0\,
      \douta[9]\(6) => \ramloop[5].ram.r_n_1\,
      \douta[9]\(5) => \ramloop[5].ram.r_n_2\,
      \douta[9]\(4) => \ramloop[5].ram.r_n_3\,
      \douta[9]\(3) => \ramloop[5].ram.r_n_4\,
      \douta[9]\(2) => \ramloop[5].ram.r_n_5\,
      \douta[9]\(1) => \ramloop[5].ram.r_n_6\,
      \douta[9]\(0) => \ramloop[5].ram.r_n_7\,
      \doutb[10]\(0) => \ramloop[5].ram.r_n_17\,
      \doutb[9]\(7) => \ramloop[5].ram.r_n_8\,
      \doutb[9]\(6) => \ramloop[5].ram.r_n_9\,
      \doutb[9]\(5) => \ramloop[5].ram.r_n_10\,
      \doutb[9]\(4) => \ramloop[5].ram.r_n_11\,
      \doutb[9]\(3) => \ramloop[5].ram.r_n_12\,
      \doutb[9]\(2) => \ramloop[5].ram.r_n_13\,
      \doutb[9]\(1) => \ramloop[5].ram.r_n_14\,
      \doutb[9]\(0) => \ramloop[5].ram.r_n_15\
    );
\ramloop[6].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized5\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      addrb(14 downto 0) => addrb(14 downto 0),
      clka => clka,
      clkb => clkb,
      \douta[10]\(0) => \ramloop[6].ram.r_n_16\,
      \douta[9]\(7) => \ramloop[6].ram.r_n_0\,
      \douta[9]\(6) => \ramloop[6].ram.r_n_1\,
      \douta[9]\(5) => \ramloop[6].ram.r_n_2\,
      \douta[9]\(4) => \ramloop[6].ram.r_n_3\,
      \douta[9]\(3) => \ramloop[6].ram.r_n_4\,
      \douta[9]\(2) => \ramloop[6].ram.r_n_5\,
      \douta[9]\(1) => \ramloop[6].ram.r_n_6\,
      \douta[9]\(0) => \ramloop[6].ram.r_n_7\,
      \doutb[10]\(0) => \ramloop[6].ram.r_n_17\,
      \doutb[9]\(7) => \ramloop[6].ram.r_n_8\,
      \doutb[9]\(6) => \ramloop[6].ram.r_n_9\,
      \doutb[9]\(5) => \ramloop[6].ram.r_n_10\,
      \doutb[9]\(4) => \ramloop[6].ram.r_n_11\,
      \doutb[9]\(3) => \ramloop[6].ram.r_n_12\,
      \doutb[9]\(2) => \ramloop[6].ram.r_n_13\,
      \doutb[9]\(1) => \ramloop[6].ram.r_n_14\,
      \doutb[9]\(0) => \ramloop[6].ram.r_n_15\
    );
\ramloop[7].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized6\
     port map (
      DOADO(7) => \ramloop[7].ram.r_n_0\,
      DOADO(6) => \ramloop[7].ram.r_n_1\,
      DOADO(5) => \ramloop[7].ram.r_n_2\,
      DOADO(4) => \ramloop[7].ram.r_n_3\,
      DOADO(3) => \ramloop[7].ram.r_n_4\,
      DOADO(2) => \ramloop[7].ram.r_n_5\,
      DOADO(1) => \ramloop[7].ram.r_n_6\,
      DOADO(0) => \ramloop[7].ram.r_n_7\,
      DOBDO(7) => \ramloop[7].ram.r_n_8\,
      DOBDO(6) => \ramloop[7].ram.r_n_9\,
      DOBDO(5) => \ramloop[7].ram.r_n_10\,
      DOBDO(4) => \ramloop[7].ram.r_n_11\,
      DOBDO(3) => \ramloop[7].ram.r_n_12\,
      DOBDO(2) => \ramloop[7].ram.r_n_13\,
      DOBDO(1) => \ramloop[7].ram.r_n_14\,
      DOBDO(0) => \ramloop[7].ram.r_n_15\,
      DOPADOP(0) => \ramloop[7].ram.r_n_16\,
      DOPBDOP(0) => \ramloop[7].ram.r_n_17\,
      addra(14 downto 0) => addra(14 downto 0),
      addrb(14 downto 0) => addrb(14 downto 0),
      clka => clka,
      clkb => clkb
    );
\ramloop[8].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized7\
     port map (
      addra(10 downto 0) => addra(10 downto 0),
      addrb(10 downto 0) => addrb(10 downto 0),
      clka => clka,
      clkb => clkb,
      ena_array(0) => ena_array(10),
      enb_array(0) => enb_array(10),
      p_6_out(8 downto 0) => p_6_out(8 downto 0),
      p_7_out(8 downto 0) => p_7_out(8 downto 0)
    );
\ramloop[9].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized8\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      addrb(14 downto 0) => addrb(14 downto 0),
      clka => clka,
      clkb => clkb,
      douta(0) => douta(11),
      doutb(0) => doutb(11)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top is
  port (
    douta : out STD_LOGIC_VECTOR ( 11 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 11 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top is
begin
\valid.cstr\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      addrb(14 downto 0) => addrb(14 downto 0),
      clka => clka,
      clkb => clkb,
      douta(11 downto 0) => douta(11 downto 0),
      doutb(11 downto 0) => doutb(11 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1_synth is
  port (
    douta : out STD_LOGIC_VECTOR ( 11 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 11 downto 0 );
    clka : in STD_LOGIC;
    clkb : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1_synth;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1_synth is
begin
\gnbram.gnativebmg.native_blk_mem_gen\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      addrb(14 downto 0) => addrb(14 downto 0),
      clka => clka,
      clkb => clkb,
      douta(11 downto 0) => douta(11 downto 0),
      doutb(11 downto 0) => doutb(11 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 is
  port (
    clka : in STD_LOGIC;
    rsta : in STD_LOGIC;
    ena : in STD_LOGIC;
    regcea : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 11 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 11 downto 0 );
    clkb : in STD_LOGIC;
    rstb : in STD_LOGIC;
    enb : in STD_LOGIC;
    regceb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 11 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 11 downto 0 );
    injectsbiterr : in STD_LOGIC;
    injectdbiterr : in STD_LOGIC;
    eccpipece : in STD_LOGIC;
    sbiterr : out STD_LOGIC;
    dbiterr : out STD_LOGIC;
    rdaddrecc : out STD_LOGIC_VECTOR ( 14 downto 0 );
    sleep : in STD_LOGIC;
    deepsleep : in STD_LOGIC;
    shutdown : in STD_LOGIC;
    rsta_busy : out STD_LOGIC;
    rstb_busy : out STD_LOGIC;
    s_aclk : in STD_LOGIC;
    s_aresetn : in STD_LOGIC;
    s_axi_awid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wlast : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_arid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rlast : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_injectsbiterr : in STD_LOGIC;
    s_axi_injectdbiterr : in STD_LOGIC;
    s_axi_sbiterr : out STD_LOGIC;
    s_axi_dbiterr : out STD_LOGIC;
    s_axi_rdaddrecc : out STD_LOGIC_VECTOR ( 14 downto 0 )
  );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 15;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 15;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "4";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "6";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "Estimated Power for IP     :     12.941174 mW";
  attribute C_FAMILY : string;
  attribute C_FAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "artix7";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 1;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 1;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "BROM_enemy_data.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "BROM_enemy_data.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 4;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 22528;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 22528;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 12;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 12;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 22528;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 22528;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "WRITE_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 12;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is 12;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1 is
  signal \<const0>\ : STD_LOGIC;
begin
  dbiterr <= \<const0>\;
  rdaddrecc(14) <= \<const0>\;
  rdaddrecc(13) <= \<const0>\;
  rdaddrecc(12) <= \<const0>\;
  rdaddrecc(11) <= \<const0>\;
  rdaddrecc(10) <= \<const0>\;
  rdaddrecc(9) <= \<const0>\;
  rdaddrecc(8) <= \<const0>\;
  rdaddrecc(7) <= \<const0>\;
  rdaddrecc(6) <= \<const0>\;
  rdaddrecc(5) <= \<const0>\;
  rdaddrecc(4) <= \<const0>\;
  rdaddrecc(3) <= \<const0>\;
  rdaddrecc(2) <= \<const0>\;
  rdaddrecc(1) <= \<const0>\;
  rdaddrecc(0) <= \<const0>\;
  rsta_busy <= \<const0>\;
  rstb_busy <= \<const0>\;
  s_axi_arready <= \<const0>\;
  s_axi_awready <= \<const0>\;
  s_axi_bid(3) <= \<const0>\;
  s_axi_bid(2) <= \<const0>\;
  s_axi_bid(1) <= \<const0>\;
  s_axi_bid(0) <= \<const0>\;
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_bvalid <= \<const0>\;
  s_axi_dbiterr <= \<const0>\;
  s_axi_rdaddrecc(14) <= \<const0>\;
  s_axi_rdaddrecc(13) <= \<const0>\;
  s_axi_rdaddrecc(12) <= \<const0>\;
  s_axi_rdaddrecc(11) <= \<const0>\;
  s_axi_rdaddrecc(10) <= \<const0>\;
  s_axi_rdaddrecc(9) <= \<const0>\;
  s_axi_rdaddrecc(8) <= \<const0>\;
  s_axi_rdaddrecc(7) <= \<const0>\;
  s_axi_rdaddrecc(6) <= \<const0>\;
  s_axi_rdaddrecc(5) <= \<const0>\;
  s_axi_rdaddrecc(4) <= \<const0>\;
  s_axi_rdaddrecc(3) <= \<const0>\;
  s_axi_rdaddrecc(2) <= \<const0>\;
  s_axi_rdaddrecc(1) <= \<const0>\;
  s_axi_rdaddrecc(0) <= \<const0>\;
  s_axi_rdata(11) <= \<const0>\;
  s_axi_rdata(10) <= \<const0>\;
  s_axi_rdata(9) <= \<const0>\;
  s_axi_rdata(8) <= \<const0>\;
  s_axi_rdata(7) <= \<const0>\;
  s_axi_rdata(6) <= \<const0>\;
  s_axi_rdata(5) <= \<const0>\;
  s_axi_rdata(4) <= \<const0>\;
  s_axi_rdata(3) <= \<const0>\;
  s_axi_rdata(2) <= \<const0>\;
  s_axi_rdata(1) <= \<const0>\;
  s_axi_rdata(0) <= \<const0>\;
  s_axi_rid(3) <= \<const0>\;
  s_axi_rid(2) <= \<const0>\;
  s_axi_rid(1) <= \<const0>\;
  s_axi_rid(0) <= \<const0>\;
  s_axi_rlast <= \<const0>\;
  s_axi_rresp(1) <= \<const0>\;
  s_axi_rresp(0) <= \<const0>\;
  s_axi_rvalid <= \<const0>\;
  s_axi_sbiterr <= \<const0>\;
  s_axi_wready <= \<const0>\;
  sbiterr <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst_blk_mem_gen: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1_synth
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      addrb(14 downto 0) => addrb(14 downto 0),
      clka => clka,
      clkb => clkb,
      douta(11 downto 0) => douta(11 downto 0),
      doutb(11 downto 0) => doutb(11 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clka : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 11 downto 0 );
    clkb : in STD_LOGIC;
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 11 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "BROM_enemy_data,blk_mem_gen_v8_4_1,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "blk_mem_gen_v8_4_1,Vivado 2017.4.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal NLW_U0_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rsta_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rstb_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rlast_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 14 downto 0 );
  signal NLW_U0_s_axi_bid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_U0_s_axi_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 14 downto 0 );
  signal NLW_U0_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal NLW_U0_s_axi_rid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of U0 : label is 15;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of U0 : label is 15;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of U0 : label is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of U0 : label is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of U0 : label is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of U0 : label is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of U0 : label is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of U0 : label is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of U0 : label is "4";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of U0 : label is "6";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of U0 : label is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of U0 : label is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of U0 : label is 0;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of U0 : label is 0;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of U0 : label is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of U0 : label is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of U0 : label is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of U0 : label is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of U0 : label is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of U0 : label is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of U0 : label is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of U0 : label is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of U0 : label is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of U0 : label is "Estimated Power for IP     :     12.941174 mW";
  attribute C_FAMILY : string;
  attribute C_FAMILY of U0 : label is "artix7";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of U0 : label is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of U0 : label is 0;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of U0 : label is 0;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of U0 : label is 1;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of U0 : label is 1;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of U0 : label is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of U0 : label is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of U0 : label is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of U0 : label is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of U0 : label is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of U0 : label is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of U0 : label is "BROM_enemy_data.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of U0 : label is "BROM_enemy_data.mif";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of U0 : label is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of U0 : label is 1;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of U0 : label is 4;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of U0 : label is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of U0 : label is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of U0 : label is 22528;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of U0 : label is 22528;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of U0 : label is 12;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of U0 : label is 12;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of U0 : label is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of U0 : label is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of U0 : label is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of U0 : label is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of U0 : label is "ALL";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of U0 : label is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of U0 : label is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of U0 : label is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of U0 : label is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of U0 : label is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of U0 : label is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of U0 : label is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of U0 : label is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of U0 : label is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of U0 : label is 22528;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of U0 : label is 22528;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of U0 : label is 12;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of U0 : label is 12;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of clka : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of clka : signal is "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_WRITE_MODE READ_WRITE";
  attribute x_interface_info of clkb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK";
  attribute x_interface_parameter of clkb : signal is "XIL_INTERFACENAME BRAM_PORTB, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_WRITE_MODE READ_WRITE";
  attribute x_interface_info of addra : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR";
  attribute x_interface_info of addrb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR";
  attribute x_interface_info of douta : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT";
  attribute x_interface_info of doutb : signal is "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_1
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      addrb(14 downto 0) => addrb(14 downto 0),
      clka => clka,
      clkb => clkb,
      dbiterr => NLW_U0_dbiterr_UNCONNECTED,
      deepsleep => '0',
      dina(11 downto 0) => B"000000000000",
      dinb(11 downto 0) => B"000000000000",
      douta(11 downto 0) => douta(11 downto 0),
      doutb(11 downto 0) => doutb(11 downto 0),
      eccpipece => '0',
      ena => '0',
      enb => '0',
      injectdbiterr => '0',
      injectsbiterr => '0',
      rdaddrecc(14 downto 0) => NLW_U0_rdaddrecc_UNCONNECTED(14 downto 0),
      regcea => '0',
      regceb => '0',
      rsta => '0',
      rsta_busy => NLW_U0_rsta_busy_UNCONNECTED,
      rstb => '0',
      rstb_busy => NLW_U0_rstb_busy_UNCONNECTED,
      s_aclk => '0',
      s_aresetn => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arburst(1 downto 0) => B"00",
      s_axi_arid(3 downto 0) => B"0000",
      s_axi_arlen(7 downto 0) => B"00000000",
      s_axi_arready => NLW_U0_s_axi_arready_UNCONNECTED,
      s_axi_arsize(2 downto 0) => B"000",
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awburst(1 downto 0) => B"00",
      s_axi_awid(3 downto 0) => B"0000",
      s_axi_awlen(7 downto 0) => B"00000000",
      s_axi_awready => NLW_U0_s_axi_awready_UNCONNECTED,
      s_axi_awsize(2 downto 0) => B"000",
      s_axi_awvalid => '0',
      s_axi_bid(3 downto 0) => NLW_U0_s_axi_bid_UNCONNECTED(3 downto 0),
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_U0_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_U0_s_axi_bvalid_UNCONNECTED,
      s_axi_dbiterr => NLW_U0_s_axi_dbiterr_UNCONNECTED,
      s_axi_injectdbiterr => '0',
      s_axi_injectsbiterr => '0',
      s_axi_rdaddrecc(14 downto 0) => NLW_U0_s_axi_rdaddrecc_UNCONNECTED(14 downto 0),
      s_axi_rdata(11 downto 0) => NLW_U0_s_axi_rdata_UNCONNECTED(11 downto 0),
      s_axi_rid(3 downto 0) => NLW_U0_s_axi_rid_UNCONNECTED(3 downto 0),
      s_axi_rlast => NLW_U0_s_axi_rlast_UNCONNECTED,
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_U0_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_U0_s_axi_rvalid_UNCONNECTED,
      s_axi_sbiterr => NLW_U0_s_axi_sbiterr_UNCONNECTED,
      s_axi_wdata(11 downto 0) => B"000000000000",
      s_axi_wlast => '0',
      s_axi_wready => NLW_U0_s_axi_wready_UNCONNECTED,
      s_axi_wstrb(0) => '0',
      s_axi_wvalid => '0',
      sbiterr => NLW_U0_sbiterr_UNCONNECTED,
      shutdown => '0',
      sleep => '0',
      wea(0) => '0',
      web(0) => '0'
    );
end STRUCTURE;
