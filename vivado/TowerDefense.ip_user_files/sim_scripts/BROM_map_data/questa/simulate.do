onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib BROM_map_data_opt

do {wave.do}

view wave
view structure
view signals

do {BROM_map_data.udo}

run -all

quit -force
