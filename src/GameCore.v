`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.03.2018 11:59:06
// Design Name: 
// Module Name: GameCore
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// Wyswietla wieze, pociski i wrogow, oraz zarzadza ich stanem
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module GameCore(
        input wire pclk,
        input wire`VGA vga_in,
        output wire`VGA vga_out,
        
        
        //control signals
        input wire`GRID_XY placePos,
        input wire[`TOWER_MEMORY_BITS-1:0] placeTower,
        input wire`COMMAND command,
        input wire commandClk,
        input wire`GRID_XY memPos,
        output wire[`TOWER_MEMORY_BITS-1:0] memTower,
        output wire`GLOBAL_STATE globals
        
    );
    `include "mathfuncs.vh"
    wire logicClk,frameClk,enablePreload,enableTick,busyPreload,busyTick;
    assign logicClk = pclk;
    assign frameClk = vga_in`VGA_VBLANK & vga_out`VGA_VBLANK;
    
    wire coreBusy;
    Sequencer#(2) sequ(.clk(logicClk),
        .enable(frameClk),
        .busy(coreBusy),
        .seq_enable({enableTick,enablePreload}),
        .seq_busy({busyTick,busyPreload})
    );
    
    
    wire`GRID_XY gridPos;
    wire[`TOWER_MEMORY_BITS-1:0] gridRead,gridWrite;
    wire gridWriteEn;
    wire`VGA vga_grid;
    
    wire`GRID_XY pmemPos;
    wire[clog2(`MAX_ENEMIES-1)-1:0] pmemEnemy;
    wire[`ENEMY_MEMORY_BITS-1:0] pmemEnemyWrite;
    wire[`TOWER_MEMORY_BITS-1:0] pmemTowerWrite;
    wire pmemEnemyWEN,pmemGridWEN;
    
    wire`GRID_XY gridPosToRam;
    assign gridPosToRam = pmemPos | gridPos;
    MapGrid grid(
        .gpos(gridPosToRam),
        .rpos(gridPosToRam),
        .wen(gridWriteEn || pmemGridWEN),
        .readState(gridRead),
        .state(gridWrite | pmemTowerWrite),//.state(gridWrite),
        .rpos2(memPos),
        .readState2(memTower),
        .pclk(pclk),
        .vga_in(vga_in),
        .vga_out(vga_grid)
    );
    wire[`MAX_ENEMIES*`ENEMY_MEMORY_BITS-1:0] mappedEnemies;
    wire[clog2(`MAX_ENEMIES-1)-1:0] enemyAddr;
    wire enemyWriteEn;
    wire[`ENEMY_MEMORY_BITS-1:0] enemyRead,enemyWrite;
    
    //tymczasowe 
    wire [37:0] wdata;
    wire [8:0] waddr;
    
    
    wire[clog2(`MAX_ENEMIES-1)-1:0] enmAddressToRam;
    assign enmAddressToRam = enemyAddr | pmemEnemy;
    BusOutRam#(`MAX_ENEMIES,`ENEMY_MEMORY_BITS) enemiesRam(
        .clk(pclk),
        .waddr(enmAddressToRam),
        .wen(enemyWriteEn || pmemEnemyWEN),
        .wdata(enemyWrite | pmemEnemyWrite),
        .raddr(enmAddressToRam),
        .rdata(enemyRead),
        .mapped(mappedEnemies)
    );
    
    
    wire clearAll;
    //Delay#(10)(logicClk,enablePreload,busyPreload);//tymczasowe opoznienie
    
    wire anyAlive;
    wire`STATISTIC hitpointsLost,earnedFactor;
    PreloadMemory preload(
        .clk(logicClk),
        .enable(enablePreload),
        .busy(busyPreload),
        .coreBusy(coreBusy),
        
        .placePos(placePos),
        .placeTower(placeTower),
        .command(command),
        .commandEN(commandClk),
        
        .memPos(pmemPos),
        .memEnemy(pmemEnemy),
        .memEnemyRead(enemyRead),
        .memEnemyWrite(pmemEnemyWrite),
        .memTowerRead(gridRead),
        .memTowerWrite(pmemTowerWrite),
        .memEnemyWEN(pmemEnemyWEN),
        .memGridWEN(pmemGridWEN),
        
        .hitpointsLost(hitpointsLost),
        .earnedFactor(earnedFactor),
        .anyAlive(anyAlive),
        .clearAll(clearAll),
        .globals(globals)
    );
    
    
    
    wire[`MAX_BULLETS*`BULLET_MEMORY_BITS-1:0] bullets;
    CalcGameTick tick(
        .frame_clk(enableTick),
        .logic_clk(logicClk),
        .busy(busyTick),
        .memPos(gridPos),
        .memEnemy(enemyAddr),
        .memEnemyRead(enemyRead),
        .memEnemyWrite(enemyWrite),
        .memTowerRead(gridRead),
        .memTowerWrite(gridWrite),
        .memEnemyWriteEn(enemyWriteEn),
        .memGridWriteEn(gridWriteEn),
        .enemies(mappedEnemies),
        .bullets(bullets),
        .isAnyAlive(anyAlive),
        .hitpointsLost(hitpointsLost),
        .earnedFactor(earnedFactor),
        .clearBullets(clearAll)
    );
    
    wire`VGA vga_enemies;
    DisplayEnemies#(`MAX_ENEMIES) dispEnemy(
        .pclk(pclk),
        .enemies(mappedEnemies),
        .vga_in(vga_grid),
        .vga_out(vga_enemies)
    );
    
    wire`VGA vga_dbg;
    DisplayBullets#(`MAX_BULLETS) dispBullets(
        .pclk(pclk),
        .bullets(bullets),
        .vga_in(vga_enemies),
        .vga_out(vga_out)
    );
    
//    wire[`ENEMY_MEMORY_BITS-1:0] tclose;
//    wire[7:0] taddr;
//    wire[9:0] tdist;
//    reg[15:0] ctr[4:0];
    
//    wire en,bs;
//    Enumerator#(16) testEnum(.clk(pclk),.enable(frameClk),.busy(),.cnt(),.subenable(en),.subbusy(bs));
//    Delay#(10)(pclk,en,bs);
    
//    reg[15:0] tcount,tcount2;
//    //enablePreload,enableTick,busyPreload,busyTick
////    always @(posedge debug[0]) ctr[0] = ctr[0] + 1;
////    always @(posedge debug[1]) ctr[1] = ctr[1] + 1;
////    always @(posedge debug[2]) ctr[2] = ctr[2] + 1;
////    always @(posedge debug[3]) ctr[3] = ctr[3] + 1;
//    always @(posedge debug[8]) ctr[0] = ctr[0] + 1;
//    always @(posedge debug[9]) ctr[1] = ctr[1] + 1;
//    always @(posedge debug[10]) ctr[2] = ctr[2] + 1;
//    always @(posedge debug[11]) ctr[3] = ctr[3] + 1;
//    always @(posedge debug[12]) ctr[4] = ctr[4] + 1;
//    ForEachGetClosest#(128,8) closest(.clk(pclk),
//        .enable(vga_in`VGA_VBLANK & vga_out`VGA_VBLANK),
//        .busy(),
//        .enemies(mappedEnemies),
//        .towerGridPos(10'b0011100001),
//        .closestEnemy(tclose),
//        .choosenEnemy(taddr),
//        .distance(tdist)
//    );
//    Debug_BinDisplay#(40,500,80) dbg(vga_dbg,vga_out,{ctr[4],ctr[3],ctr[2],ctr[1],ctr[0]});
    
endmodule
