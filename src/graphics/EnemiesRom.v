

module EnemiesRom (
    input wire clk ,
    input wire `ENEM_ROM_BUS enemy_rom_bus_in1,  //  = {addry[16:12], addrx[11:7],enable[6], typ[5:2], fac[1:0]]}
    input wire `ENEM_ROM_BUS enemy_rom_bus_in2,
    output wire [11:0] rgb1,
    output wire [11:0] rgb2
);


reg [9:0] ypos1, ypos2;

    BROM_enemy_data myBrom_enemy_data1(
        .clka(clk),
        .clkb(clk),
        .addra({ypos1[9:0],enemy_rom_bus_in1[`ADDRX]}),       
        .addrb({ypos2[9:0],enemy_rom_bus_in2[`ADDRX]}),  
        .douta(rgb1),
        .doutb(rgb2)
    );
 

always@*begin
    if(enemy_rom_bus_in1[`ENABLE_BIT]==1)
        if( (enemy_rom_bus_in1[`ADDRY] >= 25) && (enemy_rom_bus_in1[`ADDRY] <= 29) && (enemy_rom_bus_in1[`ENEMY_LEFT_RIGHT_BIT] ==1)) //jeśli piksele zawierające stopy
            if(enemy_rom_bus_in1[`ENEM_TYPE]==`EN_LVL1)
                case(enemy_rom_bus_in1[`ENEM_FACING])
                `FACING_DOWN: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*20 -25;
                `FACING_LEFT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*20 -25 + 5;
                `FACING_RIGHT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*20 -25 + 10;
                `FACING_UP: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*20 -25 + 15;
                endcase
            else if(enemy_rom_bus_in1[`ENEM_TYPE]==`EN_LVL2)
                case(enemy_rom_bus_in1[`ENEM_FACING])
                `FACING_DOWN: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*20 -25 + 20;
                `FACING_LEFT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*20 -25 + 25;
                `FACING_RIGHT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*20 -25 + 30;
                `FACING_UP: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*20 -25 + 35;
                endcase
            else if(enemy_rom_bus_in1[`ENEM_TYPE]==`EN_LVL3)
                case(enemy_rom_bus_in1[`ENEM_FACING])
                `FACING_DOWN: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*8;
                `FACING_LEFT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*9;
                `FACING_RIGHT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*10;
                `FACING_UP: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*11;
                endcase
            else if(enemy_rom_bus_in1[`ENEM_TYPE]==`EN_LVL4)
                case(enemy_rom_bus_in1[`ENEM_FACING])
                `FACING_DOWN: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*20 -25 + 40;
                `FACING_LEFT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*20 -25 + 45;
                `FACING_RIGHT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*20 -25 + 50;
                `FACING_UP: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*20 -25 + 55;
                endcase
            else if(enemy_rom_bus_in1[`ENEM_TYPE]==`EN_BOSS)
                case(enemy_rom_bus_in1[`ENEM_FACING])
                `FACING_DOWN: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*16;
                `FACING_LEFT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*17;
                `FACING_RIGHT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*18;
                `FACING_UP: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*19;
                endcase
            else
                ypos1 = 0;
        else
            if(enemy_rom_bus_in1[`ENEM_TYPE]==`EN_LVL1)
                case(enemy_rom_bus_in1[`ENEM_FACING])
                `FACING_UP: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*3;
                `FACING_DOWN: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*0;
                `FACING_LEFT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*1;
                `FACING_RIGHT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*2;
                endcase
           else if(enemy_rom_bus_in1[`ENEM_TYPE]==`EN_LVL2)
                case(enemy_rom_bus_in1[`ENEM_FACING])
                `FACING_UP: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*7;
                `FACING_DOWN: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*4;
                `FACING_LEFT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*5;
                `FACING_RIGHT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*6;
                endcase
            else if(enemy_rom_bus_in1[`ENEM_TYPE]==`EN_LVL3)
                case(enemy_rom_bus_in1[`ENEM_FACING])
                `FACING_UP: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*11;
                `FACING_DOWN: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*8;
                `FACING_LEFT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*9;
                `FACING_RIGHT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*10;
                endcase
            else if(enemy_rom_bus_in1[`ENEM_TYPE]==`EN_LVL4)
                case(enemy_rom_bus_in1[`ENEM_FACING])
                `FACING_UP: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*15;
                `FACING_DOWN: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*12;
                `FACING_LEFT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*13;
                `FACING_RIGHT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*14;
                endcase
            else if(enemy_rom_bus_in1[`ENEM_TYPE]==`EN_BOSS)
                case(enemy_rom_bus_in1[`ENEM_FACING])
                `FACING_UP: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*19;
                `FACING_DOWN: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*16;
                `FACING_LEFT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*17;
                `FACING_RIGHT: ypos1 = enemy_rom_bus_in1[`ADDRY] + 32*18;
                endcase
            else
                ypos1 = 0;
    else
        ypos1 = 0;
        
    if(enemy_rom_bus_in2[`ENABLE_BIT]==1)
        if( (enemy_rom_bus_in2[`ADDRY] >= 25) && (enemy_rom_bus_in2[`ADDRY] <= 29) && (enemy_rom_bus_in2[`ENEMY_LEFT_RIGHT_BIT] ==1)) //jeśli piksele zawierające stopy
            if(enemy_rom_bus_in2[`ENEM_TYPE]==`EN_LVL1)
                case(enemy_rom_bus_in2[`ENEM_FACING])
                `FACING_DOWN: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*20 -25;
                `FACING_LEFT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*20 -25 + 5;
                `FACING_RIGHT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*20 -25 + 10;
                `FACING_UP: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*20 -25 + 15;
                endcase
            else if(enemy_rom_bus_in2[`ENEM_TYPE]==`EN_LVL2)
                case(enemy_rom_bus_in2[`ENEM_FACING])
                `FACING_DOWN: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*20 -25 + 20;
                `FACING_LEFT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*20 -25 + 25;
                `FACING_RIGHT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*20 -25 + 30;
                `FACING_UP: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*20 -25 + 35;
                endcase
            else if(enemy_rom_bus_in2[`ENEM_TYPE]==`EN_LVL3)
                case(enemy_rom_bus_in2[`ENEM_FACING])
                `FACING_DOWN: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*8;
                `FACING_LEFT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*9;
                `FACING_RIGHT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*10;
                `FACING_UP: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*11;
                endcase
            else if(enemy_rom_bus_in2[`ENEM_TYPE]==`EN_LVL4)
                case(enemy_rom_bus_in2[`ENEM_FACING])
                `FACING_DOWN: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*20 -25 + 40;
                `FACING_LEFT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*20 -25 + 45;
                `FACING_RIGHT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*20 -25 + 50;
                `FACING_UP: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*20 -25 + 55;
                endcase
            else if(enemy_rom_bus_in2[`ENEM_TYPE]==`EN_BOSS)
                case(enemy_rom_bus_in2[`ENEM_FACING])
                `FACING_DOWN: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*16;
                `FACING_LEFT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*17;
                `FACING_RIGHT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*18;
                `FACING_UP: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*19;
                endcase
            else
                ypos2 = 0;
        else
            if(enemy_rom_bus_in2[`ENEM_TYPE]==`EN_LVL1)
                case(enemy_rom_bus_in2[`ENEM_FACING])
                `FACING_UP: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*3;
                `FACING_DOWN: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*0;
                `FACING_LEFT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*1;
                `FACING_RIGHT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*2;
                endcase
            else if(enemy_rom_bus_in2[`ENEM_TYPE]==`EN_LVL2)
                case(enemy_rom_bus_in2[`ENEM_FACING])
                `FACING_UP: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*7;
                `FACING_DOWN: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*4;
                `FACING_LEFT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*5;
                `FACING_RIGHT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*6;
                endcase
            else if(enemy_rom_bus_in2[`ENEM_TYPE]==`EN_LVL3)
                case(enemy_rom_bus_in2[`ENEM_FACING])
                `FACING_UP: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*11;
                `FACING_DOWN: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*8;
                `FACING_LEFT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*9;
                `FACING_RIGHT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*10;
                endcase
            else if(enemy_rom_bus_in2[`ENEM_TYPE]==`EN_LVL4)
                case(enemy_rom_bus_in2[`ENEM_FACING])
                `FACING_UP: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*15;
                `FACING_DOWN: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*12;
                `FACING_LEFT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*13;
                `FACING_RIGHT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*14;
                endcase
            else if(enemy_rom_bus_in2[`ENEM_TYPE]==`EN_BOSS)
                case(enemy_rom_bus_in2[`ENEM_FACING])
                `FACING_UP: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*19;
                `FACING_DOWN: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*16;
                `FACING_LEFT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*17;
                `FACING_RIGHT: ypos2 = enemy_rom_bus_in2[`ADDRY] + 32*18;
                endcase
            else
                ypos2 = 0;
    else
        ypos2 = 0;
end

endmodule
