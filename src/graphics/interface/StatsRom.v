`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.04.2018 08:48:03
// Design Name: 
// Module Name: char_rom_16x16
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module StatsRom(
    input wire [7:0] char_xy,
    output reg [6:0] char_code
    );
    reg [127:0] data;
    
    reg [127:0] line1 = "Stats           ";
    reg [127:0] line2 = "Map:            ";
    reg [127:0] line3 = "Wave:           ";
    reg [127:0] line4 = "Lifes:          ";
    reg [127:0] line5 = "Money:          ";
    reg [127:0] line6 = "                ";
    
    always@* begin
        case(char_xy[3:0])
            4'b0000: data = line1;
            4'b0001: data = line2;
            4'b0010: data = line3;
            4'b0011: data = line4;
            4'b0100: data = line5;
            4'b0101: data = line6;
            default: data = line1;

        endcase
        
        case(char_xy[7:4])
            4'b0000: char_code = data[127:120];
            4'b0001: char_code = data[119:112];
            4'b0010: char_code = data[111:104];
            4'b0011: char_code = data[103:96];
            4'b0100: char_code = data[95:88];
            4'b0101: char_code = data[87:80];
            4'b0110: char_code = data[79:72];
            4'b0111: char_code = data[71:64];
            4'b1000: char_code = data[63:56];
            4'b1001: char_code = data[55:48];
            4'b1010: char_code = data[47:40];
            4'b1011: char_code = data[39:32];
            4'b1100: char_code = data[31:24];
            4'b1101: char_code = data[23:16];
            4'b1110: char_code = data[15:8];
            4'b1111: char_code = data[7:0];
        endcase
    end
    
endmodule
