`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.04.2018 08:48:03
// Design Name: 
// Module Name: char_rom_16x16
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Bin2Dec6Bit#(
    parameter CHARACTERS = 2
    )(
    input  wire [5:0] bin,
    output reg  [8*CHARACTERS-1:0] string_out
    );

    integer i;
    reg  [3:0]  digit1;
    reg  [3:0]  digit2;  // MSB
    
    always @(bin)
    begin

        digit1 = 0;
        digit2 = 0;

        for ( i = 5; i >= 0; i = i - 1 )
        begin
            if( digit1 > 4 ) digit1 = digit1 + 3;
            if( digit2 > 4 ) digit2 = digit2 + 3;
            { digit2[3:0], digit1[3:0] } =
            { digit2[2:0], digit1[3:0],bin[i] };
        end
        
        
        case(digit1)
            4'b0000: string_out[7:0]    = "0";
            4'b0001: string_out[7:0]    = "1";
            4'b0010: string_out[7:0]    = "2";
            4'b0011: string_out[7:0]    = "3";
            4'b0100: string_out[7:0]    = "4";
            4'b0101: string_out[7:0]    = "5";
            4'b0110: string_out[7:0]    = "6";
            4'b0111: string_out[7:0]    = "7";
            4'b1000: string_out[7:0]    = "8";
            4'b1001: string_out[7:0]    = "9";
            default: string_out[7:0]    = "X";     
        endcase
        case(digit2)
            4'b0000: string_out[15:8]    = "0";
            4'b0001: string_out[15:8]    = "1";
            4'b0010: string_out[15:8]    = "2";
            4'b0011: string_out[15:8]    = "3";
            4'b0100: string_out[15:8]    = "4";
            4'b0101: string_out[15:8]    = "5";
            4'b0110: string_out[15:8]    = "6";
            4'b0111: string_out[15:8]    = "7";
            4'b1000: string_out[15:8]    = "8";
            4'b1001: string_out[15:8]    = "9";
            default: string_out[15:8]    = "X";     
        endcase
    end
    
endmodule
