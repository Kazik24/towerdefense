`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 27.04.2018 19:37:43
// Design Name: 
// Module Name: mouse_rom
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 30x30 px cell which stores image dependent on state
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module MouseRom(
        input wire [3:0] x,
        input wire [3:0] y,
        input wire mouse_type,
        output reg`RGB rgb
    );
    
    reg [11:0] mouse_rom [0:256];
    reg [11:0] mousex_rom [0:256];
    
    initial begin
        $readmemh("mouse.data", mouse_rom);
        $readmemh("mousex.data", mousex_rom);
    end
    
    always @* begin
    if (mouse_type==0)
 	   rgb = mouse_rom[{y,x}];
 	else
 	   rgb = mousex_rom[{y,x}];
    end
    
endmodule

