module DrawTitleScreen(
    input wire pclk,
    input wire `VGA vga_in,
    output reg `VGA vga_out,
    input wire enable
    );
wire `VGA vga1, vga2, vga3, vga4, vga5, vga6;

always@(posedge pclk)begin
    if(enable==1)
        vga_out<=vga6;
    else
        vga_out<=vga_in;
end
    

    DrawRectTransparent#(49,49,500,700) DrawTitleBackground(
        .pclk(pclk),
        .vga_in(vga_in),
        .vga_out(vga1)
    );
    
    DrawLogo#(258,107,64,256) MyDrawLogo(
        .pclk(pclk),
        .vga_in(vga1),
        .vga_out(vga2)
    );
    
    DrawRect#(269,339,70,70,12'h0_0_0) DrawMap1Button(
        .pclk(pclk),
        .vga_in(vga2),
        .vga_out(vga3)
    );
    
    DrawMap1#(272,342,64,64) MyDrawMap1(
        .pclk(pclk),
        .vga_in(vga3),
        .vga_out(vga4)
    );
    
    DrawRect#(442,339,70,70,12'h0_0_0) DrawMap2eButton(
        .pclk(pclk),
        .vga_in(vga4),
        .vga_out(vga5)
    );
    
    DrawMap2#(445,342,64,64) MyDrawMap2(
        .pclk(pclk),
        .vga_in(vga5),
        .vga_out(vga6)
    );
endmodule
