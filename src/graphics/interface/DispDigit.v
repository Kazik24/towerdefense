`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.04.2018 20:17:59
// Design Name: 
// Module Name: draw_rect_char
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module DispDigit#(
    parameter COLOUR = 12'hf_f_f
    )(
    input wire pclk,
    input wire `VGA vga_in,
    output reg `VGA vga_out,
    input wire [7:0] char_line_pixels,
    input wire digit_to_disp,
    input wire [2:0] xoffset,
    input wire red_colour
    );
    
    reg `VGA vga_nxt;
    
    localparam HEIGHT = 16;
    
    reg [`VGA_COUNTING_SIZE-1:0] vcount_pos, hcount_pos;
    reg [6:0] char_code;
    
    always@(posedge pclk)begin
    
        vga_out <= vga_nxt;
        
    end
    
    always@*begin
        if (digit_to_disp==0)
            vga_nxt = vga_in;
        else if((char_line_pixels[7-xoffset] == 1'b1)&&(red_colour==0))
            vga_nxt = {vga_in[`VGA_HCOUNT_MSB:`VGA_VCOUNT_LSB],COLOUR,vga_in[3:0]}; 
        else if((char_line_pixels[7-xoffset] == 1'b1)&&(red_colour==1))
            vga_nxt = {vga_in[`VGA_HCOUNT_MSB:`VGA_VCOUNT_LSB],12'hF_0_0,vga_in[3:0]};
        else //tło napisu
            //vga_nxt = {vga_in[`VGA_HCOUNT_MSB:`VGA_VCOUNT_LSB],12'h0_0_0,vga_in[3:0]}; 
            vga_nxt = vga_in;
    end
    
endmodule
