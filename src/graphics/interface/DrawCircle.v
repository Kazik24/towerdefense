`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.04.2018 20:17:59
// Design Name: 
// Module Name: draw_rect_char
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module DrawCircle#(
    parameter COLOUR = 12'hf_f_f
    )(
    input wire pclk,
    input wire [`DISP_TOWER_BITS-1:0] tower_moving_type,
    input wire `VGA vga_in,
    output reg `VGA vga_out,
    input wire `PX_COORD xpos,
    input wire `PX_COORD ypos
    );
    `include "mathfuncs.vh" //dodalem zeby dzialala symulacja (musi byc w srodku)
   
    reg `VGA vga_nxt;
    reg [`VGA_COUNTING_SIZE-1:0] vcount_pos, hcount_pos;
    reg [9:0] diameter;
    
    always@(posedge pclk)begin
    
        vga_out <= vga_nxt;
        
    end
    
    always@* begin
        
        if (tower_moving_type == `DISP_TOWER1)
            diameter = `TOWER1_DIAMETER;
        else if (tower_moving_type == `DISP_TOWER2)
            diameter = `TOWER2_DIAMETER;
        else
            diameter = 0;
            
        if ((diameter > approxDistance( xpos+`TOWER1_BUTTON_LENGTH/2 , ypos+`TOWER1_BUTTON_HEIGHT/2 , vga_in[`VGA_HCOUNT] , vga_in[`VGA_VCOUNT])) && (tower_moving_type != `DISP_TOWER_NON)&& (vga_in[`VGA_HCOUNT_MSB:`VGA_HCOUNT_LSB]<600))
            vga_nxt = {vga_in[`VGA_HCOUNT_MSB:`VGA_VCOUNT_LSB], blendColors(vga_in[`VGA_RGB] ,COLOUR) ,vga_in[3:0]}; 
        else
            vga_nxt = vga_in;
    end
endmodule
