`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.04.2018 20:17:59
// Design Name: 
// Module Name: draw_rect_char
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module PlacingCtl(
    input wire pclk,
    
    
    input wire [1:0] buttons,
    input wire [`DISP_TOWER_BITS:0] selected_tower,
    output reg `GRID_XY pointed_grid_addres,
    input wire `TOWER_TYPE map_data_from_mem,
    output reg placed,
    output reg canceled,
    input wire `PX_COORD xpos,
    input wire `PX_COORD ypos,
    output reg `PX_COORD xpos_out,
    output reg `PX_COORD ypos_out,
    output reg highlight_tower1,
    output reg highlight_tower2,
    output reg [`DISP_TOWER_BITS-1:0] tower_moving_type,
    output reg `GRID_XY new_tower_pos,
    output reg mouse_type
    );
    `include "mathfuncs.vh" //dodalem zeby dzialala symulacja (musi byc w srodku)
   
    reg `GRID_X XGridExport;
    reg `GRID_X YGridExport;
    reg mouse_type_nxt;
    reg `GRID_X Xcal, Ycal, Xresz, Yresz; 
    reg highlight_tower1_nxt, highlight_tower2_nxt, placed_nxt, canceled_nxt;
    reg [`DISP_TOWER_BITS:0] tower_moving_type_nxt;
    reg `PX_COORD xpos_nxt, ypos_nxt;
    reg [4:0] XGridExport_nxt, YGridExport_nxt;
    
    
    always@(posedge pclk)begin
        placed <= placed_nxt;
        canceled <= canceled_nxt;
        highlight_tower1 <= highlight_tower1_nxt;
        highlight_tower2 <= highlight_tower2_nxt;
        tower_moving_type <= tower_moving_type_nxt;
        xpos_out <= xpos_nxt;
        ypos_out <= ypos_nxt;
        XGridExport <= XGridExport_nxt;
        YGridExport <= YGridExport_nxt;
        mouse_type <= mouse_type_nxt;
        new_tower_pos <= {YGridExport,XGridExport};
    end
    
    always@* begin
    
        {Xresz,Xcal} = divBy30(xpos);
        {Yresz,Ycal} = divBy30(ypos);
        pointed_grid_addres = {Ycal,Xcal};
        
        
        if (selected_tower == `DISP_TOWER1)begin
            highlight_tower1_nxt = 1;
            highlight_tower2_nxt = 0;
                if( xpos >= 600)begin
                    mouse_type_nxt = 0;
                    tower_moving_type_nxt = `DISP_TOWER_NON;
                    placed_nxt = 0;
                    canceled_nxt = 0;
                    ypos_nxt = ypos;
                    xpos_nxt = xpos;
                    XGridExport_nxt = XGridExport;
                    YGridExport_nxt = YGridExport;
                    
                end
                else begin
                    ypos_nxt = ypos - Yresz;
                    xpos_nxt = xpos - Xresz;
                    
                    if(map_data_from_mem `TOWER_TYPE == `GR_GRASS) begin
                        tower_moving_type_nxt = `DISP_TOWER1;
                        mouse_type_nxt = 0;
                    end
                    else begin
                        mouse_type_nxt = 1;
                        tower_moving_type_nxt = `DISP_TOWER_NON;
                    end
                        
                    if ((buttons==`MOUSEBTN_LEFT) && (map_data_from_mem `TOWER_TYPE == `GR_GRASS)) begin
                        placed_nxt = 1;
                        canceled_nxt = 0;
                        XGridExport_nxt = Xcal;
                        YGridExport_nxt = Ycal;
                        
                    end
                    else begin
                        placed_nxt = 0;
                        canceled_nxt = 0;
                        XGridExport_nxt = XGridExport;
                        YGridExport_nxt = YGridExport;
                    end
                end
                    
        end
        else if (selected_tower == `DISP_TOWER2)begin
            highlight_tower1_nxt = 0;
            highlight_tower2_nxt = 1;
            
                if( xpos >= 600)begin
                    mouse_type_nxt = 0;
                    XGridExport_nxt = XGridExport;
                    YGridExport_nxt = YGridExport;
                    tower_moving_type_nxt = `DISP_TOWER_NON;
                    placed_nxt = 0;
                    canceled_nxt = 0;
                    ypos_nxt = ypos;
                    xpos_nxt = xpos;
                end
                else begin
                    ypos_nxt = ypos - Yresz;
                    xpos_nxt = xpos - Xresz;
                    
                    if(map_data_from_mem `TOWER_TYPE == `GR_GRASS) begin
                        tower_moving_type_nxt = `DISP_TOWER2;
                        mouse_type_nxt = 0;
                    end
                    else begin
                        tower_moving_type_nxt = `DISP_TOWER_NON;
                        mouse_type_nxt = 1;
                    end
                    
                    if ((buttons==`MOUSEBTN_LEFT) && (map_data_from_mem `TOWER_TYPE == `GR_GRASS)) begin
                        placed_nxt = 1;
                        canceled_nxt = 0;
                        XGridExport_nxt = Xcal;
                        YGridExport_nxt = Ycal;
                    end
                    else begin
                        placed_nxt = 0;
                        canceled_nxt = 0;
                        XGridExport_nxt = XGridExport;
                        YGridExport_nxt = YGridExport;
                    end
                end
        end
        else begin
           XGridExport_nxt = XGridExport;
           YGridExport_nxt = YGridExport;
           ypos_nxt = ypos;
           xpos_nxt = xpos;
           highlight_tower1_nxt = 0;
           highlight_tower2_nxt = 0; 
           placed_nxt = 0;
           canceled_nxt = 0;
           tower_moving_type_nxt = `DISP_TOWER_NON;
           mouse_type_nxt = 0;
        end
        
    if (buttons == `MOUSEBTN_RIGHT)begin
        placed_nxt = 0;
        canceled_nxt = 1;
    end
    else begin
        placed_nxt = placed_nxt;
        canceled_nxt = canceled_nxt;
    end
    end
    
endmodule