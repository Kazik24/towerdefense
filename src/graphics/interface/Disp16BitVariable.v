`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.04.2018 20:17:59
// Design Name: 
// Module Name: draw_rect_char
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module Disp16BitVariable#(
    parameter XPOS = 0,
    parameter YPOS = 0,
    parameter CHARACTERS = 5,
    parameter BITS_OFF_CHARACTER = 3 //2^BITS_OFF_CHARACTER >= CHARACTERS
    )(
    input wire [`VGA_COUNTING_SIZE-1:0] vcount,
    input wire [`VGA_COUNTING_SIZE-1:0] hcount,
    input wire enable_in,
    output reg [`TEXT_BUS_SIZE-1:0] text_bus,
    input wire [15:0] value,
    input wire red_colour
    );
    
    localparam LENGTH = CHARACTERS*8;
    
    wire [CHARACTERS*8-1:0] text;
    integer i;
    wire [7:0] char_pixels;
    reg [BITS_OFF_CHARACTER-1:0] char_x;
    reg [3:0] char_line;
    
    localparam HEIGHT = 16;
    
    reg [`VGA_COUNTING_SIZE-1:0] vcount_pos, hcount_pos;
    reg [6:0] char_code;
    
    Bin2Dec16Bit MyBin2Dec16Bit(
        .bin(value),
        .string_out(text)
    );
    
    wire[7:0] mappedText[CHARACTERS-1:0];
    generate//mapowanie tekstu na tablice 2d, dzieki temu do kazdego jest dostep po indeksie
        genvar m;
        for(m=0;m<CHARACTERS;m=m+1) begin
            assign mappedText[m] = text[ ((CHARACTERS*8)-1 -(8*m)): ((CHARACTERS*8)-8-(8*m))];
        end
    endgenerate 
    
    always@*begin
        text_bus[`TEXT_RED_BIT] = red_colour;
        vcount_pos = vcount-YPOS;
        hcount_pos = hcount-XPOS;
        text_bus[`TEXT_OFFSET] = hcount_pos[2:0];
            
        //addr = {48 + hcount_in[9:3],vcount_in[3:0]};
        char_line = {vcount_pos[3:0]};
        char_x = hcount_pos[8:3];
        
        char_code=0;
        for(i=0;i<CHARACTERS;i=i+1) begin
            if(char_x == i)
                char_code = mappedText[i];
            else
                char_code=char_code;
        end
        
        text_bus[`TEXT_ADDR]={char_code,char_line};
        
        if ((hcount_pos>=0)&&(hcount_pos<=LENGTH-1)&&(vcount_pos>=0)&&(vcount_pos<=HEIGHT-1)&&(enable_in==1))
            text_bus[`TEXT_ENABLE_BIT]=1;
        else
            text_bus[`TEXT_ENABLE_BIT]=0;
    end

endmodule
