`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.03.2018 20:10:48
// Design Name: 
// Module Name: draw_rect
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module DrawButtons(
    input wire pclk,
    input wire [`DISP_TOWER_BITS-1:0] tower_moving_type,
    input wire `PX_COORD xpos,
    input wire `PX_COORD ypos,
    input wire `VGA vga_in,
    input wire wave_running,
    input wire highlight_tower1,
    input wire highlight_tower2,
    output wire `VGA vga_out
        );

    wire `VGA vga1, vga2, vga3, vga4, vga5, vga6, vga7, vga8, vga9;
    wire `TOWER_ROM_BUS addr1, addr2,addr3;
    wire [11:0] rgb1, rgb2, rgb3;
    
    TowersRom MyTowersRom(
        .pclk(pclk),
        .tower1_bus(addr1),
        .tower2_bus(addr2),
        .tower_moving_bus(addr3),
        .tower_moving_type(tower_moving_type),
        .rgb_tower1(rgb1),
        .rgb_tower2(rgb2),
        .rgb_tower_moving(rgb3)
    );
   
    DrawRectTwoColours#(617,170,22,87,12'h0_5_0,12'h0_0_0) DrawStartWaveButton(
        .pclk(pclk),
        .vga_in(vga_in),
        .vga_out(vga1),
        .colour2(wave_running)
    );
    
    DrawRect#(718,170,22,63,12'h0_5_0) DrawGiveUpButton(
        .pclk(pclk),
        .vga_in(vga1),
        .vga_out(vga2)
    );
    
    DrawRectEn#(618,261,37,37,12'h8_f_0) HighlightTower1(
        .pclk(pclk),
        .vga_in(vga2),
        .vga_out(vga5),
        .enable(highlight_tower1)//
    );
    DrawRectEn#(670,261,37,37,12'h8_f_0) HighlightTower2(
         .pclk(pclk),
         .vga_in(vga5),
         .vga_out(vga6),
         .enable(highlight_tower2)
        );
    
    DrawImage#(`TOWER1_BUTTON_XPOS,`TOWER1_BUTTON_YPOS) DrawTower1(
        .pclk(pclk),
        .vga_in(vga6),
        .vga_out(vga7),
        .pixel_addr(addr1),
        .rgb_in(rgb1)
    );        
    DrawImage#(`TOWER2_BUTTON_XPOS,`TOWER1_BUTTON_YPOS) DrawTower2(
        .pclk(pclk),
        .vga_in(vga7),
        .vga_out(vga8),
        .pixel_addr(addr2),
        .rgb_in(rgb2)
    );  
    
    DrawCircle DrawTowerRange(
        .pclk(pclk),
        .vga_in(vga8),
        .vga_out(vga9),
        .tower_moving_type(tower_moving_type),
        .xpos(xpos),
        .ypos(ypos)
    );
    
    DrawMovingTower MyDrawMovingTower(
        .pclk(pclk),
        .vga_in(vga9),
        .vga_out(vga_out),
        .pixel_addr(addr3),
        .rgb_in(rgb3),
        .xpos(xpos),
        .ypos(ypos)
    );

endmodule
