`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.04.2018 23:59:47
// Design Name: 
// Module Name: test_draw_enemies
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TestDrawVariables(
    input wire pclk,
    output reg [5:0] stage,
    output reg [15:0] money,
    output reg [15:0] life
    );
    
    reg [15:0] main=0, main_nxt=0;
    reg [30:0] timer=0, timer_nxt=0;
    
    always@(posedge pclk)begin
        timer<=timer_nxt;
        main<=main_nxt;
        life<=main;
        money<=main+6;
        stage<=main[5:0];
    end
    
    
    always@*begin
        if(timer<1000000)begin
            timer_nxt=timer+1;
            main_nxt=main;
        end
        else begin
            timer_nxt=0;
            main_nxt = main+1;
        end
    end
    
    
endmodule
