`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 27.04.2018 15:18:27
// Design Name: 
// Module Name: Interface_state_machine
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module InterfaceStateMachine(
        input wire pclk,
        input wire [15:0] earnings,
        input wire [5:0] wave,
        output reg [15:0] money,
        input wire [15:0] lifesminus,
        output reg [15:0] lifes,
        output reg lifes_red,
        input wire wave_running,
        output reg `COMMAND command=0,
        output reg commandClk=0,
        output reg money_red,
        input wire `PX_COORD xpos,
        input wire `PX_COORD ypos,
        input wire [1:0] buttons,
        input wire placed,
        input wire canceled,
        output reg display_title_screen,
        output reg display_results,
        output reg `TOWER_TYPE new_tower_type,
        output reg [`INFO_WINDOW_BITS-1:0] info_number=0,
        output reg [`DISP_TOWER_BITS:0] selected_tower=0,
        output reg [15:0] results_money,
        output reg [5:0] results_wave
    );
    
    reg `TOWER_TYPE new_tower_type_nxt;
    reg [15:0] expenses=0, expenses_nxt;
    reg `COMMAND command_nxt;
    reg commandClk_nxt;
    reg [2:0] state=0, state_nxt=0;
    reg block_buttons=0, block_buttons_nxt=0;
    reg display_title_screen_nxt, display_results_nxt;
    reg [`INFO_WINDOW_BITS:0] info_number_nxt=0;
    reg [`DISP_TOWER_BITS:0] selected_tower_nxt=0;
    reg block_map_select=0, block_map_select_nxt=0;
    reg [15:0] results_money_nxt=0;
    reg [5:0] results_wave_nxt;
    
    localparam INIT = 3'b000;
    localparam TITLE = 3'b001;
    localparam MAP = 3'b010;
    localparam PLACE_TOWER = 3'b011;
    localparam RESULTS = 3'b100;
    
    always@(posedge pclk)begin
        state<=state_nxt;
        block_buttons<=block_buttons_nxt;
        display_title_screen<=display_title_screen_nxt;
        display_results <= display_results_nxt;
        info_number <= info_number_nxt;
        selected_tower <= selected_tower_nxt;
        command<=command_nxt;
        commandClk<=commandClk_nxt;
        expenses <= expenses_nxt;
        money <= earnings-expenses+`MONEY_ON_START; // expenses_nxt = expenses;  expenses_nxt = expenses + `TOWER1_COST;
        new_tower_type <= new_tower_type_nxt;
        block_map_select <= block_map_select_nxt;
        lifes <= `LIFE_ON_START-lifesminus;
        results_money <= results_money_nxt;
        results_wave <= results_wave_nxt;
    end
    
    always@*begin
        case(state)
        INIT: begin
            display_results_nxt=0;
            new_tower_type_nxt = 0;
            money_red = 0;
            expenses_nxt = 0;
            command_nxt = `CMD_START_LVL1;
            commandClk_nxt = 1;
            display_title_screen_nxt=0;
            info_number_nxt = `NO_INFO;
            selected_tower_nxt = `DISP_TOWER_NON;
            block_buttons_nxt=block_buttons;
            state_nxt= TITLE;
            block_map_select_nxt=block_map_select;
            results_money_nxt=results_money;
            results_wave_nxt = results_wave;
        end
        TITLE: begin
            display_results_nxt=0;
            results_money_nxt=results_money;
            results_wave_nxt = results_wave;
            new_tower_type_nxt = new_tower_type;
            money_red = 0;
            expenses_nxt = expenses;
            display_title_screen_nxt=1;
            info_number_nxt = `NO_INFO;
            selected_tower_nxt = `DISP_TOWER_NON;

            if ((xpos >= 269) && (xpos <= 269 + 70) && (ypos >= 339) && (ypos <= 339 + 70) && (block_map_select==0))begin
                command_nxt=`CMD_START_LVL1;
                commandClk_nxt=1;
                block_map_select_nxt=1;
            end
            else if ((xpos >= 442) && (xpos <= 442 + 70) && (ypos >= 339) && (ypos <= 339 + 70) && (block_map_select==0)) begin
                command_nxt=`CMD_START_LVL2;
                commandClk_nxt=1;
                block_map_select_nxt=1;
            end
            else begin
                command_nxt = command;
                commandClk_nxt = commandClk;
                block_map_select_nxt=block_map_select;
            end
            
            if((buttons==`MOUSEBTN_LEFT)&&(block_buttons==0))begin
                block_buttons_nxt=1;
                state_nxt= MAP;
            end
            else begin
                block_buttons_nxt=block_buttons;
                state_nxt= TITLE;
            end
        end
        MAP: begin 
            display_results_nxt=0;
            new_tower_type_nxt = new_tower_type;
            expenses_nxt = expenses;   
            results_money_nxt=results_money;
            results_wave_nxt = results_wave; 
            block_map_select_nxt=block_map_select;
            if ((xpos >= `TOWER1_BUTTON_XPOS) && (xpos <= `TOWER1_BUTTON_XPOS + `TOWER1_BUTTON_LENGTH) && (ypos >= `TOWER1_BUTTON_YPOS) && (ypos <= `TOWER1_BUTTON_YPOS + `TOWER1_BUTTON_HEIGHT))begin
                if (money>=`TOWER1_COST)
                    money_red = 0;
                else
                    money_red = 1;
                if((buttons==`MOUSEBTN_LEFT)&&(block_buttons==0)&&(money>=`TOWER1_COST))begin
                    info_number_nxt = `TOWER1_INFO;
                    block_buttons_nxt = 1;
                    selected_tower_nxt = `DISP_TOWER1;
                    state_nxt= PLACE_TOWER;
                    command_nxt = command;
                    commandClk_nxt = commandClk;
                end
                else begin
                    info_number_nxt = `TOWER1_INFO;
                    block_buttons_nxt = block_buttons;
                    selected_tower_nxt = `DISP_TOWER_NON;
                    state_nxt= MAP;
                    command_nxt = command;
                    commandClk_nxt = commandClk;
                end
            end
            else if ((xpos >= `TOWER2_BUTTON_XPOS) && (xpos <= `TOWER2_BUTTON_XPOS + `TOWER2_BUTTON_LENGTH) && (ypos >= `TOWER2_BUTTON_YPOS) && (ypos <= `TOWER2_BUTTON_YPOS + `TOWER2_BUTTON_HEIGHT))begin
                if (money>=`TOWER2_COST)
                    money_red = 0;
                else
                    money_red = 1;
                if((buttons==`MOUSEBTN_LEFT)&&(block_buttons==0)&&(money>=`TOWER2_COST))begin
                    info_number_nxt = `TOWER2_INFO;
                    block_buttons_nxt = 1;
                    selected_tower_nxt = `DISP_TOWER2;
                    state_nxt= PLACE_TOWER;
                    command_nxt = command;
                    commandClk_nxt = commandClk;
                end
                else begin
                    info_number_nxt = `TOWER2_INFO;
                    block_buttons_nxt = block_buttons;
                    selected_tower_nxt = `DISP_TOWER_NON;
                    state_nxt= MAP;
                    command_nxt = command;
                    commandClk_nxt = commandClk;
                end
            end
            else if ((xpos >= 617) && (xpos <= 617 + 87) && (ypos >= 170) && (ypos <= 170 + 22) && (buttons==`MOUSEBTN_LEFT)&&(block_buttons==0) && (wave_running==0) )begin
                block_buttons_nxt = 1;
                command_nxt = `CMD_START_WAVE;
                commandClk_nxt = 1;
                info_number_nxt = `NO_INFO;
                selected_tower_nxt = `DISP_TOWER_NON;
                state_nxt= MAP;
                money_red = 0;
            end
            else if ((xpos >= 718) && (xpos <= 718 + 64) && (ypos >= 170) && (ypos <= 170 + 22) && (buttons==`MOUSEBTN_LEFT)&&(block_buttons==0) )begin // || life==0
                block_buttons_nxt = 1;
                command_nxt = command;
                commandClk_nxt = commandClk;
                info_number_nxt = `NO_INFO;
                selected_tower_nxt = `DISP_TOWER_NON;
                results_money_nxt= earnings;
                results_wave_nxt = wave;
                state_nxt= RESULTS;   
                money_red = 0;
            end          
            else begin
                info_number_nxt = `NO_INFO;
                block_buttons_nxt = block_buttons;
                selected_tower_nxt = `DISP_TOWER_NON;
                state_nxt= MAP;
                command_nxt = command;
                commandClk_nxt = commandClk;
                money_red = 0;
            end     
            display_title_screen_nxt=0;
            if( ((lifes==0 || lifes > `LIFE_ON_START)) || ((wave==40) && (wave_running==0)) ) begin
                state_nxt = RESULTS;
                results_money_nxt= earnings;
                results_wave_nxt = wave;
            end
            else begin
                state_nxt = state_nxt;
            end
        end
        PLACE_TOWER: begin
            display_results_nxt=0;
            money_red = 0;
            results_money_nxt=results_money;
            results_wave_nxt = results_wave;
            display_title_screen_nxt=0;
            info_number_nxt = `PLACING_INFO;
            selected_tower_nxt = selected_tower;
            block_map_select_nxt=block_map_select;
            if ((xpos >= 718) && (xpos <= 718 + 64) && (ypos >= 170) && (ypos <= 170 + 22) && (buttons==`MOUSEBTN_LEFT)&&(block_buttons==0) )begin // || life==0
                block_buttons_nxt = 1;
                command_nxt = command;
                commandClk_nxt = commandClk;
                results_money_nxt= earnings;
                results_wave_nxt = wave;
                state_nxt= RESULTS;   
                new_tower_type_nxt = new_tower_type;
                expenses_nxt = expenses;
            end          
            else if(placed==0 && canceled==0) begin
                block_buttons_nxt=block_buttons;
                state_nxt= PLACE_TOWER;
                expenses_nxt = expenses;
                command_nxt = command;
                commandClk_nxt = commandClk;
                new_tower_type_nxt = new_tower_type;
            end
            else if (placed==1 && canceled==0)begin
                state_nxt= MAP;
                block_buttons_nxt =1;
                command_nxt = `CMD_PLACE_TOWER;
                commandClk_nxt = 1;
                if(selected_tower == `DISP_TOWER1) begin
                    expenses_nxt = expenses + `TOWER1_COST;
                    new_tower_type_nxt = `GR_TOWER1;
                end
                else begin // (selected_tower == `DISP_TOWER2)
                    expenses_nxt = expenses + `TOWER2_COST;
                    new_tower_type_nxt = `GR_TOWER2;
                end
            end
            else begin //(placed==0 && canceled==1)
                new_tower_type_nxt = new_tower_type;
                state_nxt= MAP;
                block_buttons_nxt =1;
                expenses_nxt = expenses;
                command_nxt = command;
                commandClk_nxt = commandClk;
            end
            if( ((lifes==0 || lifes > `LIFE_ON_START)) || ((wave==40) && (wave_running==0)) ) begin
                state_nxt = RESULTS;
                results_money_nxt= earnings;
                results_wave_nxt = wave;
            end
            else begin
                state_nxt = state_nxt;
            end
        end
        RESULTS: begin
            block_map_select_nxt=block_map_select;
            display_results_nxt=1;
            results_money_nxt=results_money;
            results_wave_nxt = results_wave;
            new_tower_type_nxt = new_tower_type;
            money_red = 0;
            expenses_nxt = expenses;
            command_nxt = command;
            commandClk_nxt = commandClk;
            display_title_screen_nxt=0;
            info_number_nxt = `NO_INFO;
            selected_tower_nxt = `DISP_TOWER_NON;
            if ((buttons==`MOUSEBTN_LEFT)&&(block_buttons==0)) begin
                state_nxt= INIT;
                block_buttons_nxt=1;
            end
            else begin
                state_nxt= RESULTS;
                block_buttons_nxt=block_buttons;
            end
        end
        default: begin
            display_results_nxt=0;
            new_tower_type_nxt = 0;
            block_map_select_nxt=block_map_select;
            results_money_nxt=results_money;
            results_wave_nxt = results_wave;
            money_red = 0;
            expenses_nxt = 0;
            command_nxt = `CMD_START_LVL1;
            commandClk_nxt = 1;
            display_title_screen_nxt=0;
            info_number_nxt = `NO_INFO;
            selected_tower_nxt = `DISP_TOWER_NON;
            block_buttons_nxt=block_buttons;
            state_nxt= TITLE;
        end
        
        endcase
    
    if((block_buttons==1) && (buttons==`MOUSEBTN_NONE))
        block_buttons_nxt=0;
    else
        block_buttons_nxt=block_buttons_nxt;
    
    if(commandClk==1)
        commandClk_nxt=0;
    else
        commandClk_nxt=commandClk_nxt;
    
    if  (!((((xpos >= 269) && (xpos <= 269 + 70)) || ((xpos >= 442) && (xpos <= 442 + 70))) && (ypos >= 339) && (ypos <= 339 + 70)))
        block_map_select_nxt=0;
    else
        block_map_select_nxt=block_map_select_nxt;
    
    if(lifes<=15)
        lifes_red=1;
    else
        lifes_red=0;
    
    
    end
    
    
    
endmodule