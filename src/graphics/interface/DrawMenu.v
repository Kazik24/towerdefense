`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.03.2018 20:10:48
// Design Name: 
// Module Name: draw_rect
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module DrawMenu(
    input wire pclk,
    input wire `VGA vga_in,
    output wire `VGA vga_out //C71 jasny // 841 sredni // ciemny 420
        );

    wire `VGA vga1, vga2, vga3, vga4, vga5, vga6, vga7, vga8, vga9, vga10, vga11, vga12, vga13;
    //sredni prostokat tło
    DrawRect#(600,0,600,200,12'h8_4_1) DrawMenuFrame(
        .pclk(pclk),
        .vga_in(vga_in),
        .vga_out(vga1)
    );
    //jasny prostokąt tło
    DrawRect#(602,2,595,195,12'hC_7_1) DrawMenuBackground(
        .pclk(pclk),
        .vga_in(vga1),
        .vga_out(vga2)
    );
    
    //srednie ramki
    DrawFrame MyDrawFrame(
        .pclk(pclk),
        .vga_in(vga2),
        .vga_out(vga3)
    );
    
    //pierwszy prostokąt jasny
    DrawRect#(620,17,121,121,12'h8_4_1) DrawRect1(
        .pclk(pclk),
        .vga_in(vga3),
        .vga_out(vga4)
    );
    //pierwszy prostokąt sredni
    DrawRect#(622,19,117,117,12'h4_2_0) DrawRect2(
        .pclk(pclk),
        .vga_in(vga4),
        .vga_out(vga5)
    );
    //drugi prostokąc ciemny
    DrawRect#(602,160,42,196,12'h4_2_0) DrawRect3(
        .pclk(pclk),
        .vga_in(vga5),
        .vga_out(vga6)
    );
    //trzeci prostokąt ciemny
    DrawRect#(602,247,66,196,12'h4_2_0) DrawRect4(
        .pclk(pclk),
        .vga_in(vga6),
        .vga_out(vga7)
    );
    
    //czwarty prostokąt sredni
    DrawRect#(629,370,202,137,12'h8_4_1) DrawRect5(
        .pclk(pclk),
        .vga_in(vga7),
        .vga_out(vga8)
    );
    //czwarty prostokąt ciemny 
    DrawRect#(631,372,198,133,12'h4_2_0) DrawRect6(
        .pclk(pclk),
        .vga_in(vga8),
        .vga_out(vga_out)
    );

endmodule
