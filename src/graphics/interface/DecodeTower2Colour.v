`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.03.2018 20:10:48
// Design Name: 
// Module Name: draw_rect
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"
`include "states.vh"

module DecodeTower2Colour(
    output reg [11:0] rgb_out,
    input wire [6:0] data
    );


always@* begin
            case (data)
            7'b0000000: rgb_out = 12'h000;
            7'b0000001: rgb_out = 12'h100;
            7'b0000010: rgb_out = 12'h101;
            7'b0000011: rgb_out = 12'h110;
            7'b0000100: rgb_out = 12'h130;
            7'b0000101: rgb_out = 12'h140;
            7'b0000110: rgb_out = 12'h150;
            7'b0000111: rgb_out = 12'h210;
            7'b0001000: rgb_out = 12'h211;
            7'b0001001: rgb_out = 12'h221;
            7'b0001010: rgb_out = 12'h230;
            7'b0001011: rgb_out = 12'h240;
            7'b0001100: rgb_out = 12'h241;
            7'b0001101: rgb_out = 12'h250;
            7'b0001110: rgb_out = 12'h251;
            7'b0001111: rgb_out = 12'h260;
            7'b0010000: rgb_out = 12'h270;
            7'b0010001: rgb_out = 12'h280;
            7'b0010010: rgb_out = 12'h310;
            7'b0010011: rgb_out = 12'h320;
            7'b0010100: rgb_out = 12'h321;
            7'b0010101: rgb_out = 12'h340;
            7'b0010110: rgb_out = 12'h341;
            7'b0010111: rgb_out = 12'h350;
            7'b0011000: rgb_out = 12'h351;
            7'b0011001: rgb_out = 12'h361;
            7'b0011010: rgb_out = 12'h370;
            7'b0011011: rgb_out = 12'h371;
            7'b0011100: rgb_out = 12'h380;
            7'b0011101: rgb_out = 12'h381;
            7'b0011110: rgb_out = 12'h390;
            7'b0011111: rgb_out = 12'h420;
            7'b0100000: rgb_out = 12'h421;
            7'b0100001: rgb_out = 12'h431;
            7'b0100010: rgb_out = 12'h432;
            7'b0100011: rgb_out = 12'h441;
            7'b0100100: rgb_out = 12'h451;
            7'b0100101: rgb_out = 12'h461;
            7'b0100110: rgb_out = 12'h471;
            7'b0100111: rgb_out = 12'h480;
            7'b0101000: rgb_out = 12'h481;
            7'b0101001: rgb_out = 12'h490;
            7'b0101010: rgb_out = 12'h491;
            7'b0101011: rgb_out = 12'h4A0;
            7'b0101100: rgb_out = 12'h531;
            7'b0101101: rgb_out = 12'h532;
            7'b0101110: rgb_out = 12'h542;
            7'b0101111: rgb_out = 12'h571;
            7'b0110000: rgb_out = 12'h572;
            7'b0110001: rgb_out = 12'h581;
            7'b0110010: rgb_out = 12'h591;
            7'b0110011: rgb_out = 12'h631;
            7'b0110100: rgb_out = 12'h632;
            7'b0110101: rgb_out = 12'h641;
            7'b0110110: rgb_out = 12'h642;
            7'b0110111: rgb_out = 12'h741;
            7'b0111000: rgb_out = 12'h742;
            7'b0111001: rgb_out = 12'h752;
            7'b0111010: rgb_out = 12'h753;
            7'b0111011: rgb_out = 12'h762;
            7'b0111100: rgb_out = 12'h852;
            7'b0111101: rgb_out = 12'h853;
            7'b0111110: rgb_out = 12'h862;
            7'b0111111: rgb_out = 12'h863;
            7'b1000000: rgb_out = 12'h952;
            7'b1000001: rgb_out = 12'h962;
            7'b1000010: rgb_out = 12'h963;
            7'b1000011: rgb_out = 12'h964;
            7'b1000100: rgb_out = 12'h974;
            7'b1000101: rgb_out = 12'h975;
            7'b1000110: rgb_out = 12'h985;
            7'b1000111: rgb_out = 12'hA63;
            7'b1001000: rgb_out = 12'hA73;
            7'b1001001: rgb_out = 12'hA74;
            7'b1001010: rgb_out = 12'hA75;
            7'b1001011: rgb_out = 12'hA84;
            7'b1001100: rgb_out = 12'hA85;
            7'b1001101: rgb_out = 12'hA96;
            7'b1001110: rgb_out = 12'hB73;
            7'b1001111: rgb_out = 12'hB74;
            7'b1010000: rgb_out = 12'hB75;
            7'b1010001: rgb_out = 12'hB84;
            7'b1010010: rgb_out = 12'hB85;
            7'b1010011: rgb_out = 12'hB95;
            7'b1010100: rgb_out = 12'hB96;
            7'b1010101: rgb_out = 12'hC74;
            7'b1010110: rgb_out = 12'hC84;
            7'b1010111: rgb_out = 12'hC85;
            7'b1011000: rgb_out = 12'hC86;
            7'b1011001: rgb_out = 12'hC95;
            7'b1011010: rgb_out = 12'hC96;
            7'b1011011: rgb_out = 12'hCA6;
            7'b1011100: rgb_out = 12'hCA7;
            7'b1011101: rgb_out = 12'hD96;
            7'b1011110: rgb_out = 12'hD97;
            7'b1011111: rgb_out = 12'hDA6;
            7'b1100000: rgb_out = 12'hDA7;
            7'b1100001: rgb_out = 12'hDB7;
            7'b1100010: rgb_out = 12'hEA6;
            7'b1100011: rgb_out = 12'hEA7;
            7'b1100100: rgb_out = 12'hEB6;
            7'b1100101: rgb_out = 12'hEB7;
            7'b1100110: rgb_out = 12'hEC7;
            7'b1100111: rgb_out = 12'hFB7;
            7'b1101000: rgb_out = 12'hFC7;
            7'b1101001: rgb_out = 12'hFC8;
            7'b1101010: rgb_out = 12'hFD8;
            7'b1101011: rgb_out = 12'hFD9;
            7'b1101100: rgb_out = 12'hFE9;
        default: rgb_out = 12'h000;
    endcase
end

endmodule
