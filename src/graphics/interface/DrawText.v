`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.03.2018 20:10:48
// Design Name: 
// Module Name: draw_rect
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module DrawText(
    input wire pclk,
    input wire money_red,
    input wire lifes_red,
    input wire [5:0] wave,
    input wire [5:0] map,
    input wire [15:0] money,
    input wire [15:0] life,
    input wire `VGA vga_in,
    output wire `VGA vga_out,
    input wire display_title_screen,
    input wire display_results,
    input wire [`INFO_WINDOW_BITS-1:0] info_number,
    input wire [15:0] results_money,
    input wire [5:0] results_wave
    );

    wire digit_to_disp;
    wire [7:0]char_line_pixels;
    wire [2:0] xoffset;
    wire [`TEXT_BUS_SIZE-1:0] text_bus [17:1];
    wire red_colour;
    
    FontRomGlobal MyFontRomGlobal(
        .text_bus1(text_bus[1]),
        .text_bus2(text_bus[2]),
        .text_bus3(text_bus[3]),
        .text_bus4(text_bus[4]),
        .text_bus5(text_bus[5]),
        .text_bus6(text_bus[6]),
        .text_bus7(text_bus[7]),
        .text_bus8(text_bus[8]),
        .text_bus9(text_bus[9]),
        .text_bus10(text_bus[10]),
        .text_bus11(text_bus[11]),
        .text_bus12(text_bus[12]),
        .text_bus13(text_bus[13]),
        .text_bus14(text_bus[14]),
        .text_bus15(text_bus[15]),
        .text_bus16(text_bus[16]),
        .text_bus17(text_bus[17]),
        .char_line_pixels(char_line_pixels),
        .digit_to_disp(digit_to_disp),
        .xoffset_out(xoffset),
        .red_colour(red_colour)
    );
    
    DispDigit MyDispDigit(
        .pclk(pclk),
        .vga_in(vga_in),
        .vga_out(vga_out),
        .char_line_pixels(char_line_pixels),
        .digit_to_disp(digit_to_disp),
        .xoffset(xoffset),
        .red_colour(red_colour)
    );
    
    DispOneLineOfText#(10,4,620,173,"Start wave") DisplayStartWave(
        .vcount(vga_in[`VGA_VCOUNT]),
        .hcount(vga_in[`VGA_HCOUNT]),
        .enable_in((!display_results)&&(!display_title_screen)),
        .text_bus(text_bus[1])
    );
    
    DispOneLineOfText#(7,3,721,173,"Give up") DisplayGiveUp(
        .vcount(vga_in[`VGA_VCOUNT]),
        .hcount(vga_in[`VGA_HCOUNT]),
        .enable_in((!display_results)&&(!display_title_screen)),
        .text_bus(text_bus[2])
    );
    
     DispOneLineOfText#(26,5,292,487,"Press LMB to start game...") DispClickInfo(
        .vcount(vga_in[`VGA_VCOUNT]),
        .hcount(vga_in[`VGA_HCOUNT]),
        .enable_in(display_title_screen),
        .text_bus(text_bus[3])
   );
   
    DispOneLineOfText#(9,4,344,150,"Game Over") DispGameOver(
        .vcount(vga_in[`VGA_VCOUNT]),
        .hcount(vga_in[`VGA_HCOUNT]),
        .enable_in(display_results),
        .text_bus(text_bus[4])
    );
    
    DispOneLineOfText#(23,5,304,450,"Press LMB to restart...") DispClickInfoo(
        .vcount(vga_in[`VGA_VCOUNT]),
        .hcount(vga_in[`VGA_HCOUNT]),
        .enable_in(display_results),
        .text_bus(text_bus[5])
    );
    
    DispOneLineOfText#(8,3,344,250,"Results:") DispRes(
        .vcount(vga_in[`VGA_VCOUNT]),
        .hcount(vga_in[`VGA_HCOUNT]),
        .enable_in(display_results),
        .text_bus(text_bus[6])
    );
    
    DispOneLineOfText#(13,4,300,300,"Earned money:") DispEarnedMoney(
        .vcount(vga_in[`VGA_VCOUNT]),
        .hcount(vga_in[`VGA_HCOUNT]),
        .enable_in(display_results),
        .text_bus(text_bus[7])
    );
    
    Disp16BitVariable#(412,300) DisplayEarnedMoney(
        .vcount(vga_in[`VGA_VCOUNT]),
        .hcount(vga_in[`VGA_HCOUNT]),
        .enable_in(display_results),
        .text_bus(text_bus[8]),
        .value(results_money),
        .red_colour(1'b0)
    );
    
    DispOneLineOfText#(17,5,300,316,"Last wave number:") DispWaveNumber(
        .vcount(vga_in[`VGA_VCOUNT]),
        .hcount(vga_in[`VGA_HCOUNT]),
        .enable_in(display_results),
        .text_bus(text_bus[9])
    );
    
    Disp6BitVariable#(444,316) DisplayLastWave(
        .vcount(vga_in[`VGA_VCOUNT]),
        .hcount(vga_in[`VGA_HCOUNT]),
        .enable_in(display_results),
        .text_bus(text_bus[10]),
        .value(results_wave),
        .red_colour(1'b0)
    );
    
    Disp6BitVariable#(680,37) DisplayMap(
        .vcount(vga_in[`VGA_VCOUNT]),
        .hcount(vga_in[`VGA_HCOUNT]),
        .enable_in((!display_results)&&(!display_title_screen)),
        .text_bus(text_bus[11]),
        .value({map+1}),
        .red_colour(1'b0)
    );
    
    Disp6BitVariable#(680,53) DisplayWave(
        .vcount(vga_in[`VGA_VCOUNT]),
        .hcount(vga_in[`VGA_HCOUNT]),
        .enable_in((!display_results)&&(!display_title_screen)),
        .text_bus(text_bus[12]),
        .value(wave),
        .red_colour(1'b0)
    );
    
    Disp16BitVariable#(680,69) DisplayLife(
        .vcount(vga_in[`VGA_VCOUNT]),
        .hcount(vga_in[`VGA_HCOUNT]),
        .enable_in((!display_results)&&(!display_title_screen)),
        .text_bus(text_bus[13]),
        .value(life),
        .red_colour(lifes_red)
    );
    
    Disp16BitVariable#(680,85) DisplayMoney(
        .vcount(vga_in[`VGA_VCOUNT]),
        .hcount(vga_in[`VGA_HCOUNT]),
        .enable_in((!display_results)&&(!display_title_screen)),
        .text_bus(text_bus[14]),
        .value(money),
        .red_colour(money_red)
    );
    
    DrawTowerInfo#(633,374,128,192) MyDrawTower_info(
        .info_number(info_number),
        .vcount(vga_in[`VGA_VCOUNT]),
        .hcount(vga_in[`VGA_HCOUNT]),
        .enable_in((!display_results)&&(!display_title_screen)),
        .text_bus(text_bus[15])
    );
    
    DrawStats#(96,112,624,21) MyDrawS_stats(
        .vcount(vga_in[`VGA_VCOUNT]),
        .hcount(vga_in[`VGA_HCOUNT]),
        .enable_in((!display_results)&&(!display_title_screen)),
        .text_bus(text_bus[16])
    );
    
    DispOneLineOfText#(35,6,250,266,"Place mouse on button to select map") DispSelectInfo(
        .vcount(vga_in[`VGA_VCOUNT]),
        .hcount(vga_in[`VGA_HCOUNT]),
        .enable_in(display_title_screen),
        .text_bus(text_bus[17])
    );
endmodule
