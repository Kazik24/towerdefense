`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.04.2018 20:17:59
// Design Name: 
// Module Name: draw_rect_char
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module DrawRectTwoColours#(
    parameter XPOS = 150,
    parameter YPOS = 50,
    parameter HEIGHT = 256,
    parameter LENGTH = 128,
    parameter COLOUR1 = 12'h0_0_0,
    parameter COLOUR2 = 12'h0_0_0
    )(
    input wire pclk,
    input wire `VGA vga_in,
    input wire colour2,
    output reg `VGA vga_out
    );
    `include "mathfuncs.vh" //dodalem zeby dzialala symulacja (musi byc w srodku)
   
    reg `VGA vga_nxt;
    reg [`VGA_COUNTING_SIZE-1:0] vcount_pos, hcount_pos;
    
    always@(posedge pclk)begin
    
        vga_out <= vga_nxt;
        
    end
    
    always@* begin
        
        vcount_pos = vga_in[`VGA_VCOUNT]-YPOS;
        hcount_pos = vga_in[`VGA_HCOUNT]-XPOS;
        
        if ((hcount_pos>=0)&&(hcount_pos<=LENGTH-1)&&(vcount_pos>=0)&&(vcount_pos<=HEIGHT-1))
            if(colour2==0)
		vga_nxt = {vga_in[`VGA_HCOUNT_MSB:`VGA_VCOUNT_LSB], COLOUR1 ,vga_in[3:0]}; 
	    else
		vga_nxt = {vga_in[`VGA_HCOUNT_MSB:`VGA_VCOUNT_LSB], COLOUR2 ,vga_in[3:0]};
        else
            vga_nxt = vga_in;
    end
endmodule
