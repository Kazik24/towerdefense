`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.03.2018 20:10:48
// Design Name: 
// Module Name: draw_rect
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module DrawMouse(
    input wire pclk,
    input wire `VGA vga_in,
    input wire `PX_COORD xpos,
    input wire `PX_COORD ypos,
    input wire mouse_type,
    output reg `VGA vga_out
        );

reg `VGA vga_nxt;
reg [3:0] addry, addrx;
wire [11:0] rgb_from_rom;       
localparam WIDTH = 15;
localparam LENGTH = 15;

MouseRom MyMouseRom(
        .x(addrx),
        .y(addry),
        .rgb(rgb_from_rom),
        .mouse_type(mouse_type)
    );

always@(posedge pclk) begin
    vga_out<=vga_nxt;
  end
  
  
  
always@* begin
    addry = vga_in[`VGA_VCOUNT] - ypos;
    addrx = vga_in[`VGA_HCOUNT] - xpos;
    end
    
always@* begin
    if(vga_in[`VGA_HCOUNT] >= xpos && vga_in[`VGA_HCOUNT] < (xpos+LENGTH) && vga_in[`VGA_VCOUNT] >= ypos && vga_in[`VGA_VCOUNT] < (ypos+WIDTH))
        if(rgb_from_rom==0)
            vga_nxt=vga_in;
        else
            vga_nxt={vga_in[`VGA_HCOUNT_MSB:`VGA_VCOUNT_LSB], rgb_from_rom ,vga_in[3:0]};
    else
        vga_nxt=vga_in;
end
    
endmodule
