`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.03.2018 20:10:48
// Design Name: 
// Module Name: draw_rect
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module DrawMap1#(
    parameter XPOS = 30,
    parameter YPOS = 30,
    parameter HEIGHT = 30,
    parameter LENGTH = 30
    )(
    input wire pclk,
    input wire `VGA vga_in,
    output reg `VGA vga_out
    );

    reg `VGA vga1,vga2, vga_nxt;
    
always@(posedge pclk) begin
    vga1 <= vga_in;
    vga2 <= vga1;
    vga_out <= vga_nxt;
  end
  
  reg [5:0] addrx;
  reg [5:0] addry;
  wire [11:0] data;
  
  BROM_map1(
      .clka(pclk),
      .addra({addry,addrx}),
      .douta(data)
  );
  
always@* begin
    addry = vga_in[`VGA_VCOUNT] - YPOS;
    addrx = vga_in[`VGA_HCOUNT] - XPOS;
    
    if(vga2[`VGA_HCOUNT] >= XPOS && vga2[`VGA_HCOUNT] < (XPOS+LENGTH) && vga2[`VGA_VCOUNT] >= YPOS && vga2[`VGA_VCOUNT] < (YPOS+HEIGHT) && (data!=12'hFFF))
        vga_nxt = {vga2[`VGA_HCOUNT_MSB:`VGA_VCOUNT_LSB], data ,vga2[3:0]}; 
    else
        vga_nxt = vga2;
    end


endmodule
