`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.04.2018 20:17:59
// Design Name: 
// Module Name: draw_rect_char
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module DrawStats#(
    parameter HEIGHT = 256,
    parameter LENGTH = 128,
    parameter XPOS = 150,
    parameter YPOS = 50
    )(
    input wire [`VGA_COUNTING_SIZE-1:0] vcount,
    input wire [`VGA_COUNTING_SIZE-1:0] hcount,
    input wire enable_in,
    output reg [`TEXT_BUS_SIZE-1:0] text_bus
    );
    
    reg [7:0] char_xy;
    reg [3:0] char_line;
    
    reg [`VGA_COUNTING_SIZE-1:0] vcount_pos, hcount_pos;
    wire [6:0] char_code;
    
    StatsRom MyStatsRom(
        .char_xy(char_xy),
        .char_code(char_code)
    );
    

    always@*begin
        text_bus[`TEXT_RED_BIT] =0;
        vcount_pos = vcount-YPOS;
        hcount_pos = hcount-XPOS;
        text_bus[`TEXT_OFFSET] = hcount_pos[2:0];
            
        char_line = {vcount_pos[3:0]};
        char_xy = {hcount_pos[6:3],vcount_pos[7:4]};

        text_bus[`TEXT_ADDR]={char_code,char_line};
                        
        if ((hcount_pos>=0)&&(hcount_pos<=LENGTH-1)&&(vcount_pos>=0)&&(vcount_pos<=HEIGHT-1)&&(enable_in==1))
            text_bus[`TEXT_ENABLE_BIT]=1;
        else
            text_bus[`TEXT_ENABLE_BIT]=0;
    end
endmodule
