`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.04.2018 20:17:59
// Design Name: 
// Module Name: draw_rect_char
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module DrawFrame(
    input wire pclk,
    input wire `VGA vga_in,
    output reg `VGA vga_out
    );
    
    reg `VGA vga_nxt;
    
    always@(posedge pclk)
        vga_out <= vga_nxt;
        
    always@*begin
        if ((vga_in[`VGA_HCOUNT] == 0) || (vga_in[`VGA_VCOUNT] == 0) || (vga_in[`VGA_VCOUNT] == 599))
            vga_nxt = {vga_in[`VGA_HCOUNT_MSB:`VGA_VCOUNT_LSB], 12'h8_4_1 ,vga_in[3:0]}; 
        else if (((vga_in[`VGA_VCOUNT] == 158) || (vga_in[`VGA_VCOUNT] == 159) || (vga_in[`VGA_VCOUNT] == 201) ||
            (vga_in[`VGA_VCOUNT] == 202) || (vga_in[`VGA_VCOUNT] == 245) || (vga_in[`VGA_VCOUNT] == 246) || (vga_in[`VGA_VCOUNT] == 313) || 
            (vga_in[`VGA_VCOUNT] == 314) || (vga_in[`VGA_VCOUNT] == 351) || (vga_in[`VGA_VCOUNT] == 352) ) && (vga_in[`VGA_HCOUNT] >= 600) && (vga_in[`VGA_HCOUNT] <=799) )
            vga_nxt = {vga_in[`VGA_HCOUNT_MSB:`VGA_VCOUNT_LSB], 12'h8_4_1 ,vga_in[3:0]}; 
        else
            vga_nxt = vga_in;
    end    
endmodule
