`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 27.04.2018 15:18:27
// Design Name: 
// Module Name: Interface_state_machine
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module DrawResults(
    input wire pclk,
    input wire`VGA vga_in,
    output reg`VGA vga_out,
    input wire enable   
    );
    
    wire `VGA vga1;
    
    always@(posedge pclk)begin
        if(enable==1)
            vga_out<=vga1;
        else
            vga_out<=vga_in;
    end
  
    DrawRect#(0,0,600,800,12'h0_0_0) DrawResultsBackground(
        .pclk(pclk),
        .vga_in(vga_in),
        .vga_out(vga1)
    );
    
endmodule
