`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.04.2018 08:48:03
// Design Name: 
// Module Name: char_rom_16x16
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module TowerInfoRom(
    input wire [7:0] char_xy,
    output reg [6:0] char_code,
    input wire [`INFO_WINDOW_BITS-1:0] info_number
    );
    reg [127:0] data;
    
    reg [127:0] tow1_line1 = "   Gun tower    ";
    reg [127:0] tow1_line2 = "Range:          ";
    reg [127:0] tow1_line3 = "  low  60 pix   ";
    reg [127:0] tow1_line4 = "                ";
    reg [127:0] tow1_line5 = "Reloading time: ";
    reg [127:0] tow1_line6 = "     1 sec      ";
    reg [127:0] tow1_line7 = "                ";
    reg [127:0] tow1_line8 = "Cost:           ";
    reg [127:0] tow1_line9 = "       70       ";
    reg [127:0] tow1_line10 = "                ";
    reg [127:0] tow1_line11 = "                ";
    reg [127:0] tow1_line12 = "                ";
    reg [127:0] tow2_line1 = "  Sniper tower  ";
    reg [127:0] tow2_line2 = "Range:          ";
    reg [127:0] tow2_line3 = " medium 100 pix ";
    reg [127:0] tow2_line4 = "                ";
    reg [127:0] tow2_line5 = "reloading time: ";
    reg [127:0] tow2_line6 = "    0.41 sec    ";
    reg [127:0] tow2_line7 = "                ";
    reg [127:0] tow2_line8 = "cost:           ";
    reg [127:0] tow2_line9 = "       250      ";
    reg [127:0] tow2_line10 = "                ";
    reg [127:0] tow2_line11 = "                ";
    reg [127:0] tow2_line12 = "                ";
    reg [127:0] tow_non_line1 = "Select tower    ";
    reg [127:0] tow_non_line2 = "                ";
    reg [127:0] placing_tower_line1 = "Press LMB       ";
    reg [127:0] placing_tower_line2 = "to place tower  ";
    reg [127:0] placing_tower_line3 = "on grass        ";
    reg [127:0] placing_tower_line4 = "                ";
    reg [127:0] placing_tower_line5 = "Press RMB       ";
    reg [127:0] placing_tower_line6 = "to cancel       ";
    
    always@* begin
    
    if(info_number== `TOWER1_INFO) begin
        case(char_xy[3:0])
            4'b0000: data = tow1_line1;
            4'b0001: data = tow1_line2;
            4'b0010: data = tow1_line3;
            4'b0011: data = tow1_line4;
            4'b0100: data = tow1_line5;
            4'b0101: data = tow1_line6;
            4'b0110: data = tow1_line7;
            4'b0111: data = tow1_line8;
            4'b1000: data = tow1_line9;
            4'b1001: data = tow1_line10;
            4'b1010: data = tow1_line11;
            4'b1011: data = tow1_line12;
            default: data = tow1_line1;
            //4'b1100: data = line13;
            //4'b1101: data = line14;
            //4'b1110: data = line15;
            //4'b1111: data = line16;
        endcase
    end 
    else if(info_number== `TOWER2_INFO) begin
        case(char_xy[3:0])
           4'b0000: data = tow2_line1;
           4'b0001: data = tow2_line2;
           4'b0010: data = tow2_line3;
           4'b0011: data = tow2_line4;
           4'b0100: data = tow2_line5;
           4'b0101: data = tow2_line6;
           4'b0110: data = tow2_line7;
           4'b0111: data = tow2_line8;
           4'b1000: data = tow2_line9;
           4'b1001: data = tow2_line10;
           4'b1010: data = tow2_line11;
           4'b1011: data = tow2_line12;
           default: data = tow2_line1;
           //4'b1100: data = line13;
           //4'b1101: data = line14;
           //4'b1110: data = line15;
           //4'b1111: data = line16;
       endcase
   end
   else if(info_number== `PLACING_INFO) begin
       case(char_xy[3:0])
           4'b0000: data = placing_tower_line1;
           4'b0001: data = placing_tower_line2;
           4'b0010: data = placing_tower_line3;
           4'b0011: data = placing_tower_line4;
           4'b0100: data = placing_tower_line5;
           4'b0101: data = placing_tower_line6;
           default: data = placing_tower_line4;
       endcase
    end
    else begin // INFO_NON
        case(char_xy[3:0])
            4'b0000: data = tow_non_line1;
            default: data = tow_non_line2;
        endcase
    end 
        case(char_xy[7:4])
            4'b0000: char_code = data[127:120];
            4'b0001: char_code = data[119:112];
            4'b0010: char_code = data[111:104];
            4'b0011: char_code = data[103:96];
            4'b0100: char_code = data[95:88];
            4'b0101: char_code = data[87:80];
            4'b0110: char_code = data[79:72];
            4'b0111: char_code = data[71:64];
            4'b1000: char_code = data[63:56];
            4'b1001: char_code = data[55:48];
            4'b1010: char_code = data[47:40];
            4'b1011: char_code = data[39:32];
            4'b1100: char_code = data[31:24];
            4'b1101: char_code = data[23:16];
            4'b1110: char_code = data[15:8];
            4'b1111: char_code = data[7:0];
        endcase
    end
    
endmodule
