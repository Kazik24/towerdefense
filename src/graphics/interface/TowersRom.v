`include "maindefs.vh"

module TowersRom (
    input pclk,
    input wire `TOWER_ROM_BUS tower1_bus,
    input wire `TOWER_ROM_BUS tower2_bus,
    input wire `TOWER_ROM_BUS tower_moving_bus,  //  = {addry[9:5], addrx[4:0],}
    input wire [`DISP_TOWER_BITS-1:0] tower_moving_type,
    output wire [11:0] rgb_tower1,
    output wire [11:0] rgb_tower2,
    output wire [11:0] rgb_tower_moving
);

reg [6:0] data;
wire [6:0] data1, data3;
wire [7:0] data4,data2;

BROM_tower1 myBrom_tower1(
    .clka(pclk),
    .addra({tower1_bus[`TOWER_ADDRY],tower1_bus[`TOWER_ADDRX]}),
    .douta(data1)
);

BROM_tower2 myBrom_tower2(
    .clka(pclk),
    .addra({tower2_bus[`TOWER_ADDRY],tower2_bus[`TOWER_ADDRX]}),
    .douta(data2)
);

BROM_tower1 myBrom_tower_moving1(
    .clka(pclk),
    .addra({tower_moving_bus[`TOWER_ADDRY],tower_moving_bus[`TOWER_ADDRX]}),
    .douta(data3)
);

BROM_tower2 myBrom_tower_moving2(
    .clka(pclk),
    .addra({tower_moving_bus[`TOWER_ADDRY],tower_moving_bus[`TOWER_ADDRX]}),
    .douta(data4)
);

DecodeTowerColour MyDecodeTowerColour(
    .tower_moving_type(tower_moving_type),
    .data(data),
    .rgb_out(rgb_tower_moving)
);

DecodeTower1Colour MyDecodeTower1Colour(
    .data(data1),
    .rgb_out(rgb_tower1)
);

DecodeTower2Colour MyDecodeTower2Colour(
    .data(data2),
    .rgb_out(rgb_tower2)
);

always@*begin
    
    if(tower_moving_type==`DISP_TOWER1)
        data = data3;
    else if(tower_moving_type==`DISP_TOWER2)
        data = data4;
    else 
        data = 12'h0_0_0;
end

endmodule
