`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.03.2018 20:10:48
// Design Name: 
// Module Name: draw_rect
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"
`include "states.vh"

module DecodeTower1Colour(
    output reg [11:0] rgb_out,
    input wire [6:0] data
    );

always@* begin
        case (data)
        7'b0000000: rgb_out = 12'h000;
        7'b0000001: rgb_out = 12'h100;
        7'b0000010: rgb_out = 12'h110;
        7'b0000011: rgb_out = 12'h111;
        7'b0000100: rgb_out = 12'h140;
        7'b0000101: rgb_out = 12'h211;
        7'b0000110: rgb_out = 12'h221;
        7'b0000111: rgb_out = 12'h230;
        7'b0001000: rgb_out = 12'h231;
        7'b0001001: rgb_out = 12'h240;
        7'b0001010: rgb_out = 12'h241;
        7'b0001011: rgb_out = 12'h250;
        7'b0001100: rgb_out = 12'h251;
        7'b0001101: rgb_out = 12'h260;
        7'b0001110: rgb_out = 12'h270;
        7'b0001111: rgb_out = 12'h280;
        7'b0010000: rgb_out = 12'h322;
        7'b0010001: rgb_out = 12'h331;
        7'b0010010: rgb_out = 12'h332;
        7'b0010011: rgb_out = 12'h333;
        7'b0010100: rgb_out = 12'h341;
        7'b0010101: rgb_out = 12'h342;
        7'b0010110: rgb_out = 12'h351;
        7'b0010111: rgb_out = 12'h360;
        7'b0011000: rgb_out = 12'h361;
        7'b0011001: rgb_out = 12'h370;
        7'b0011010: rgb_out = 12'h371;
        7'b0011011: rgb_out = 12'h380;
        7'b0011100: rgb_out = 12'h381;
        7'b0011101: rgb_out = 12'h390;
        7'b0011110: rgb_out = 12'h432;
        7'b0011111: rgb_out = 12'h433;
        7'b0100000: rgb_out = 12'h442;
        7'b0100001: rgb_out = 12'h443;
        7'b0100010: rgb_out = 12'h452;
        7'b0100011: rgb_out = 12'h453;
        7'b0100100: rgb_out = 12'h471;
        7'b0100101: rgb_out = 12'h472;
        7'b0100110: rgb_out = 12'h480;
        7'b0100111: rgb_out = 12'h481;
        7'b0101000: rgb_out = 12'h482;
        7'b0101001: rgb_out = 12'h490;
        7'b0101010: rgb_out = 12'h491;
        7'b0101011: rgb_out = 12'h492;
        7'b0101100: rgb_out = 12'h4A0;
        7'b0101101: rgb_out = 12'h543;
        7'b0101110: rgb_out = 12'h544;
        7'b0101111: rgb_out = 12'h553;
        7'b0110000: rgb_out = 12'h554;
        7'b0110001: rgb_out = 12'h572;
        7'b0110010: rgb_out = 12'h573;
        7'b0110011: rgb_out = 12'h581;
        7'b0110100: rgb_out = 12'h582;
        7'b0110101: rgb_out = 12'h591;
        7'b0110110: rgb_out = 12'h592;
        7'b0110111: rgb_out = 12'h5A0;
        7'b0111000: rgb_out = 12'h654;
        7'b0111001: rgb_out = 12'h664;
        7'b0111010: rgb_out = 12'h665;
        7'b0111011: rgb_out = 12'h674;
        7'b0111100: rgb_out = 12'h684;
        7'b0111101: rgb_out = 12'h692;
        7'b0111110: rgb_out = 12'h693;
        7'b0111111: rgb_out = 12'h765;
        7'b1000000: rgb_out = 12'h775;
        7'b1000001: rgb_out = 12'h776;
        7'b1000010: rgb_out = 12'h7A4;
        7'b1000011: rgb_out = 12'h876;
        7'b1000100: rgb_out = 12'h886;
        7'b1000101: rgb_out = 12'h887;
        7'b1000110: rgb_out = 12'h895;
        7'b1000111: rgb_out = 12'h8A4;
        7'b1001000: rgb_out = 12'h8A5;
        7'b1001001: rgb_out = 12'h8A6;
        7'b1001010: rgb_out = 12'h986;
        7'b1001011: rgb_out = 12'h987;
        7'b1001100: rgb_out = 12'h997;
        7'b1001101: rgb_out = 12'h9A6;
        7'b1001110: rgb_out = 12'h9B6;
        7'b1001111: rgb_out = 12'hA97;
        7'b1010000: rgb_out = 12'hA98;
        7'b1010001: rgb_out = 12'hAA8;
        7'b1010010: rgb_out = 12'hBA8;
        7'b1010011: rgb_out = 12'hBA9;
        7'b1010100: rgb_out = 12'hBB9;
        7'b1010101: rgb_out = 12'hBC8;
        7'b1010110: rgb_out = 12'hCB9;
        7'b1010111: rgb_out = 12'hCBA;
        7'b1011000: rgb_out = 12'hCCA;
        7'b1011001: rgb_out = 12'hDCA;
        7'b1011010: rgb_out = 12'hDCB;
        7'b1011011: rgb_out = 12'hDDA;
        7'b1011100: rgb_out = 12'hDDB;
        7'b1011101: rgb_out = 12'hEDB;
        7'b1011110: rgb_out = 12'hEDC;
        7'b1011111: rgb_out = 12'hEEA;
        7'b1100000: rgb_out = 12'hEEB;
        7'b1100001: rgb_out = 12'hEEC;
        7'b1100010: rgb_out = 12'hFEC;
        7'b1100011: rgb_out = 12'hFFC;
        7'b1100100: rgb_out = 12'hFFD;
        default: rgb_out = 12'h000;
        endcase
end

endmodule
