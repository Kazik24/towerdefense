`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.03.2018 20:10:48
// Design Name: 
// Module Name: draw_rect
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module DrawRectTransparent#(
    parameter XPOS = 150,
    parameter YPOS = 50,
    parameter HEIGHT = 256,
    parameter LENGTH = 128
    )(
    input wire pclk,
    input wire `VGA vga_in,
    output reg `VGA vga_out
    );
    `include "mathfuncs.vh" //dodalem zeby dzialala symulacja (musi byc w srodku)
   
    reg `VGA vga_nxt;
    reg [`VGA_COUNTING_SIZE-1:0] vcount_pos, hcount_pos;
    
    always@(posedge pclk)begin
    
        vga_out <= vga_nxt;
        
    end
    
    always@* begin
        
        vcount_pos = vga_in[`VGA_VCOUNT]-YPOS;
        hcount_pos = vga_in[`VGA_HCOUNT]-XPOS;
        
        if ((hcount_pos>=0)&&(hcount_pos<=LENGTH-1)&&(vcount_pos>=0)&&(vcount_pos<=HEIGHT-1))
            vga_nxt = {vga_in[`VGA_HCOUNT_MSB:`VGA_VCOUNT_LSB], blendColors(blendColors(vga_in[`VGA_RGB],12'h000),12'h000) ,vga_in[3:0]}; 
        else
            vga_nxt = vga_in;
    end
endmodule

