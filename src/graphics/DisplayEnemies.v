`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.03.2018 17:54:42
// Design Name: 
// Module Name: DisplayEnemies
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


`include "maindefs.vh"
`include "states.vh"

module DisplayEnemies#(
        parameter ECOUNT = 128
    )(
        input wire pclk,
        input wire[ECOUNT*`ENEMY_MEMORY_BITS-1:0] enemies,
        input wire`VGA vga_in,
        output reg`VGA vga_out
    );
    
    wire`RGB rgb_in1, rgb_in2;
    reg`RGB rgb_nxt;
    reg`ENEM_ROM_BUS enemy_rom_bus_out1, enemy_rom_bus_out2;
    EnemiesRom MyEnemiesRom(
        .clk(pclk),
        .enemy_rom_bus_in1(enemy_rom_bus_out1),
        .enemy_rom_bus_in2(enemy_rom_bus_out2),
        .rgb1(rgb_in1),
        .rgb2(rgb_in2)
    );
    
    
    
    localparam ENEMY_WIDTH = 31;
    localparam ENEMY_HEIGHT = 31;
    reg`VGA vga_buf,vga_buf1,vga_buf2,vga_buf3,vga_buf4,vga_buf5,vga_buf6;
    integer i;
    reg[`ENEMY_MEMORY_BITS*2-1:0] currentEnemyData1_nxt,currentEnemyData2_nxt,currentEnemyData3_nxt,currentEnemyData4_nxt, currentEnemyData1,currentEnemyData2,currentEnemyData3,currentEnemyData4;
    reg`PX_COORD x_buf,y_buf;
    
    wire[`ENEMY_MEMORY_BITS-1:0] mappedEnemies[ECOUNT-1:0];
    generate//mapowanie wrogow na tablice 2d, dzieki temu do kazdego jest dostep po indeksie
        genvar m;
        for(m=0;m<ECOUNT;m=m+1) begin
            assign mappedEnemies[m] = enemies[`ENEMY_MEMORY_BITS*(m+1)-1:`ENEMY_MEMORY_BITS*m];
        end
    endgenerate
    
    
    always@(posedge pclk) begin
        currentEnemyData1 <= currentEnemyData1_nxt;
        currentEnemyData2 <= currentEnemyData2_nxt;
        currentEnemyData3 <= currentEnemyData3_nxt;
        currentEnemyData4 <= currentEnemyData4_nxt;
        //vga_buf <= vga_in;
        vga_buf1 <= vga_in;
        vga_buf2 <= vga_buf1;
        vga_buf3 <= vga_buf2;
        vga_buf4 <= vga_buf3;
        vga_buf5 <= vga_buf4;
        vga_buf6 <= vga_buf5;
        
        vga_out <= {vga_buf6[37:16],rgb_nxt,vga_buf6[3:0]};
        /*
        if((rgb_in1 == 12'hFFF)&&(rgb_in2 == 12'hFFF)) //osbluga bezbarwnego piksela
            vga_out <= vga_buf2;
        else if((rgb_in1 != 12'hFFF)&&(rgb_in2 == 12'hFFF))
            vga_out <= {vga_buf2[37:16],rgb_in1,vga_buf2[3:0]};
        else if((rgb_in1 == 12'hFFF)&&(rgb_in2 != 12'hFFF))
            vga_out <= {vga_buf2[37:16],rgb_in2,vga_buf2[3:0]};
        else
            vga_out <= {vga_buf2[37:16],rgb_in1,vga_buf2[3:0]};*/
    end
    
    always @* begin
        //wyzwrowanie danych wroga (dzieki temu vivado nie stwozy zatrzasku)
        currentEnemyData1_nxt = 0;
        for(i=0;i<ECOUNT/4;i=i+1) begin
            //przesuniecie koordynatow wroga ze srodka na lewy gorny rog
            x_buf = mappedEnemies[i]`ENEMY_POSX - (ENEMY_WIDTH-1)/2;
            y_buf = mappedEnemies[i]`ENEMY_POSY - (ENEMY_HEIGHT-1)/2;
            //porownanie pozycji lewego gornego rogu z aktuanlym pikselem
            x_buf = vga_in[37:27] - x_buf;
            y_buf = vga_in[26:16] - y_buf;
            if(x_buf < ENEMY_WIDTH && y_buf < ENEMY_HEIGHT && mappedEnemies[i]`ENEMY_TYPE != `EN_DEAD)begin
                currentEnemyData1_nxt = {currentEnemyData1_nxt[`ENEMY_MEMORY_BITS-1:0],mappedEnemies[i]};
            end
            else begin
                currentEnemyData1_nxt=currentEnemyData1_nxt;
            end
        end
        
        currentEnemyData2_nxt = currentEnemyData1;
        for(i=(ECOUNT/4);i<(ECOUNT/4)*2;i=i+1) begin
            //przesuniecie koordynatow wroga ze srodka na lewy gorny rog
            x_buf = mappedEnemies[i]`ENEMY_POSX - (ENEMY_WIDTH-1)/2;
            y_buf = mappedEnemies[i]`ENEMY_POSY - (ENEMY_HEIGHT-1)/2;
            //porownanie pozycji lewego gornego rogu z aktuanlym pikselem
            x_buf = vga_buf1[37:27] - x_buf;
            y_buf = vga_buf1[26:16] - y_buf;
            if(x_buf < ENEMY_WIDTH && y_buf < ENEMY_HEIGHT && mappedEnemies[i]`ENEMY_TYPE != `EN_DEAD)begin
                currentEnemyData2_nxt = {currentEnemyData2_nxt[`ENEMY_MEMORY_BITS-1:0],mappedEnemies[i]};
            end
            else begin
                currentEnemyData2_nxt = currentEnemyData2_nxt;
            end
        end
        
        currentEnemyData3_nxt = currentEnemyData2;
        for(i=(ECOUNT/4)*2;i<(ECOUNT/4)*3;i=i+1) begin
            //przesuniecie koordynatow wroga ze srodka na lewy gorny rog
            x_buf = mappedEnemies[i]`ENEMY_POSX - (ENEMY_WIDTH-1)/2;
            y_buf = mappedEnemies[i]`ENEMY_POSY - (ENEMY_HEIGHT-1)/2;
            //porownanie pozycji lewego gornego rogu z aktuanlym pikselem
            x_buf = vga_buf2[37:27] - x_buf;
            y_buf = vga_buf2[26:16] - y_buf;
            if(x_buf < ENEMY_WIDTH && y_buf < ENEMY_HEIGHT && mappedEnemies[i]`ENEMY_TYPE != `EN_DEAD)begin
                currentEnemyData3_nxt = {currentEnemyData3_nxt[`ENEMY_MEMORY_BITS-1:0],mappedEnemies[i]};
            end
            else begin
                currentEnemyData3_nxt = currentEnemyData3_nxt;
            end
        end
        
        currentEnemyData4_nxt = currentEnemyData3;
        for(i=(ECOUNT/4)*3;i<ECOUNT;i=i+1) begin
            //przesuniecie koordynatow wroga ze srodka na lewy gorny rog
            x_buf = mappedEnemies[i]`ENEMY_POSX - (ENEMY_WIDTH-1)/2;
            y_buf = mappedEnemies[i]`ENEMY_POSY - (ENEMY_HEIGHT-1)/2;
            //porownanie pozycji lewego gornego rogu z aktuanlym pikselem
            x_buf = vga_buf3[37:27] - x_buf;
            y_buf = vga_buf3[26:16] - y_buf;
            if(x_buf < ENEMY_WIDTH && y_buf < ENEMY_HEIGHT && mappedEnemies[i]`ENEMY_TYPE != `EN_DEAD)begin
                currentEnemyData4_nxt = {currentEnemyData4_nxt[`ENEMY_MEMORY_BITS-1:0],mappedEnemies[i]};
            end
            else begin
                currentEnemyData4_nxt = currentEnemyData4_nxt;
            end
        end
        
        //wyslanie do romu wrogow bezbarwnego piksela
        if(vga_buf4[0] || vga_buf4[1] || currentEnemyData4[`ENEMY_TYPE_MSB:`ENEMY_TYPE_LSB] == 0)
            enemy_rom_bus_out1 = 16'b0;
        else begin
            //wyznaczenie koordynatow do odczytu z romu
            x_buf = vga_buf4[37:27]+(ENEMY_WIDTH-1)/2 - currentEnemyData4[`ENEMY_POSX_MSB:`ENEMY_POSX_LSB];
            y_buf = vga_buf4[26:16]+(ENEMY_HEIGHT-1)/2 - currentEnemyData4[`ENEMY_POSY_MSB:`ENEMY_POSY_LSB];
            //wyslanie do romu koordynatow, bit aktywujacy, typ i zwrot grafiki
            enemy_rom_bus_out1 = { currentEnemyData4[2]^currentEnemyData4[12] ,y_buf[4:0], x_buf[4:0], 1'b1, currentEnemyData4[`ENEMY_TYPE_MSB:`ENEMY_TYPE_LSB], currentEnemyData4[`ENEMY_FACING_MSB:`ENEMY_FACING_LSB]};
        end
        
        if(vga_buf4[0] || vga_buf4[1] || currentEnemyData4[`ENEMY_TYPE_MSB+`ENEMY_MEMORY_BITS:`ENEMY_TYPE_LSB+`ENEMY_MEMORY_BITS] == 0)
            enemy_rom_bus_out2 = 16'b0;
        else begin
            //wyznaczenie koordynatow do odczytu z romu
            x_buf = vga_buf4[37:27]+(ENEMY_WIDTH-1)/2 - currentEnemyData4[`ENEMY_POSX_MSB+`ENEMY_MEMORY_BITS:`ENEMY_POSX_LSB+`ENEMY_MEMORY_BITS];
            y_buf = vga_buf4[26:16]+(ENEMY_HEIGHT-1)/2 - currentEnemyData4[`ENEMY_POSY_MSB+`ENEMY_MEMORY_BITS:`ENEMY_POSY_LSB+`ENEMY_MEMORY_BITS];
            //wyslanie do romu koordynatow, bit aktywujacy, typ i zwrot grafiki
            enemy_rom_bus_out2 = { currentEnemyData4[2+`ENEMY_MEMORY_BITS]^currentEnemyData4[12+`ENEMY_MEMORY_BITS] ,y_buf[4:0], x_buf[4:0], 1'b1, currentEnemyData4[`ENEMY_TYPE_MSB+`ENEMY_MEMORY_BITS:`ENEMY_TYPE_LSB+`ENEMY_MEMORY_BITS], currentEnemyData4[`ENEMY_FACING_MSB+`ENEMY_MEMORY_BITS:`ENEMY_FACING_LSB+`ENEMY_MEMORY_BITS]};
        end
    
        if((rgb_in1 == 12'hFFF)&&(rgb_in2 == 12'hFFF)) //osbluga bezbarwnego piksela
            rgb_nxt = vga_buf6[15:4];
        else if((rgb_in1 != 12'hFFF)&&(rgb_in2 == 12'hFFF))
            rgb_nxt = rgb_in1;
        else if((rgb_in1 == 12'hFFF)&&(rgb_in2 != 12'hFFF))
            rgb_nxt = rgb_in2;
        else
            rgb_nxt = rgb_in1;
    
    end
endmodule