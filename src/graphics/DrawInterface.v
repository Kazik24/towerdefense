`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 27.03.2018 22:14:54
// Design Name: 
// Module Name: EnemyCellRender
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"
`include "states.vh"

module DrawInterface(
        input wire pclk,
        output wire `COMMAND command,
        output wire commandClk,
        input wire`VGA vga_in,
        output reg`VGA vga_out,
        output wire `GRID_XY new_tower_pos,
        output wire `GRID_XY pointed_grid_addres,
        output wire `TOWER_TYPE new_tower_type,
        input wire `TOWER_TYPE map_data_from_mem,
        input wire `PX_COORD xpos,
        input wire `PX_COORD ypos,
        input wire [1:0] buttons,
        input wire`GLOBAL_STATE globals
        
    );
    
    wire [15:0] money;
    wire [4:0] XGridExport, YGridExport;
    wire `VGA vga1,vga2, vga3, vga4, vga5, vga6;
    wire display_title_screen, display_results;
    wire [`INFO_WINDOW_BITS-1:0] info_number;
    wire [`DISP_TOWER_BITS:0] selected_tower;
    wire placed, canceled;
    wire [`DISP_TOWER_BITS-1:0] tower_moving_type;
    wire highlight_tower1, highlight_tower2;
    wire `PX_COORD xpos_with_grid, ypos_with_grid;
    wire money_red;
    wire [15:0] lifes, results_money;
    wire [5:0] results_wave;
    wire lifes_red;
    always@(posedge pclk)begin
        vga_out <= vga6;
    end
            
    InterfaceStateMachine MyInterfaceStateMachine(
        .pclk(pclk),
        .wave_running(globals`GSTATE_RUNNING),
        .wave(globals`GSTATE_WAVE),
        .earnings(globals`GSTATE_MONEY),
        .lifesminus({8'b0,globals`GSTATE_LIFES}),
        .lifes(lifes),
        .lifes_red(lifes_red),
        .money(money),
        .money_red(money_red),
        .command(command),
        .commandClk(commandClk),
        .xpos(xpos),
        .ypos(ypos),
        .selected_tower(selected_tower),
        .new_tower_type(new_tower_type),
        .info_number(info_number),
        .buttons(buttons),
        .display_title_screen(display_title_screen),
        .display_results(display_results),
        .placed(placed),
        .canceled(canceled),
        .results_money(results_money),
        .results_wave(results_wave)
    );
    wire mouse_type;
    PlacingCtl TowerPlacingCtl(
        .pclk(pclk),
        .buttons(buttons),
        .xpos(xpos),
        .ypos(ypos),
        .selected_tower(selected_tower),
        .placed(placed),
        .canceled(canceled),
        .tower_moving_type(tower_moving_type),
        .highlight_tower1(highlight_tower1),
        .highlight_tower2(highlight_tower2),
        .xpos_out(xpos_with_grid),
        .ypos_out(ypos_with_grid),
        .new_tower_pos(new_tower_pos),
        .pointed_grid_addres(pointed_grid_addres),
        .map_data_from_mem(map_data_from_mem),
        .mouse_type(mouse_type)
    );
    
    DrawMenu MyDrawMenu(
        .pclk(pclk),
        .vga_in(vga_in),
        .vga_out(vga1)
    );
    
    DrawButtons MyDrawButtons(
        .pclk(pclk),
        .wave_running(globals`GSTATE_RUNNING),
        .tower_moving_type(tower_moving_type),
        .vga_in(vga1),
        .vga_out(vga2),
        .xpos(xpos_with_grid),
        .ypos(ypos_with_grid),
        .highlight_tower1(highlight_tower1),
        .highlight_tower2(highlight_tower2)
    );
    
    DrawTitleScreen MyDrawTitleScreen(
        .pclk(pclk),
        .vga_in(vga2),
        .vga_out(vga3),
        .enable(display_title_screen)
    );
    
    DrawResults MyDrawResults(
        .pclk(pclk),
        .vga_in(vga3),
        .vga_out(vga4),
        .enable(display_results)
    );
    
    DrawText MyDrawText(
        .pclk(pclk),
        .money_red(money_red),
        .lifes_red(lifes_red),
        .wave(globals`GSTATE_WAVE ),
        .map({4'b0,globals`GSTATE_LEVEL}),
        .money(money),
        .life(lifes),
        .vga_in(vga4),
        .vga_out(vga5),
        .display_title_screen(display_title_screen),
        .display_results(display_results),
        .info_number(info_number),
        .results_money(results_money),
        .results_wave(results_wave)
    );
    
    DrawMouse MyDrawMouse(
        .pclk(pclk),
        .vga_in(vga5),
        .vga_out(vga6),
        .xpos(xpos),
        .ypos(ypos),
        .mouse_type(mouse_type)
    );

endmodule