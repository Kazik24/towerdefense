`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.03.2018 19:37:43
// Design Name: 
// Module Name: GridCellRender
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 30x30 px cell which stores image dependent on state
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"


module GridCellRender(
        input wire pclk,
        input wire[`TOWER_MEMORY_BITS-1:0] state,
        input wire`GRID_COORD x,
        input wire`GRID_COORD y,
        output wire`RGB rgb
    );
    
    BROM_map_data myBrom_mapa_data(
        .clka(pclk),
        .addra({ypos[9:0],x[4:0]}),         
        .douta(rgb)
    );
    reg [9:0] ypos;

    always @* begin
        if(state`TOWER_TYPE == `GR_PATH_V) ypos=y+32*1;
        else if(state`TOWER_TYPE == `GR_PATH_H) ypos=y+32*2;
        else if(state`TOWER_TYPE == `GR_PATH_TL) ypos=y+32*3;
        else if(state`TOWER_TYPE == `GR_PATH_TR) ypos=y+32*4;
        else if(state`TOWER_TYPE == `GR_PATH_BL) ypos=y+32*5;
        else if(state`TOWER_TYPE == `GR_PATH_BR) ypos=y+32*6;
        else if(state`TOWER_TYPE == `GR_PATH_CR) ypos=y+32*7;
        else if(state`TOWER_TYPE == `GR_TOWER1)
            if((state`TOWER_COUNT <= `TOWER1_RESTORE_TIME) && (state`TOWER_COUNT >= `TOWER1_RESTORE_TIME-`FIRE_ANIM_TIME))
                ypos=y+32*18;
            else
                ypos=y+32*12;
        else if(state`TOWER_TYPE == `GR_TOWER2)
            if((state`TOWER_COUNT <= `TOWER2_RESTORE_TIME) && (state`TOWER_COUNT >= `TOWER2_RESTORE_TIME-`FIRE_ANIM_TIME))
                ypos=y+32*18;
            else
                ypos=y+32*13;
        else if(state`TOWER_TYPE == `GR_GRASS_STONE) ypos=y+32*8;
        else if(state`TOWER_TYPE == `GR_CASTLE_G) ypos=y+32*9;
        else if(state`TOWER_TYPE == `GR_CASTLE_D) ypos=y+32*10;
        else if(state`TOWER_TYPE == `GR_TREE_LG) ypos=y+32*14;
        else if(state`TOWER_TYPE == `GR_TREE_RG) ypos=y+32*15;
        else if(state`TOWER_TYPE == `GR_TREE_LD) ypos=y+32*16;
        else if(state`TOWER_TYPE == `GR_TREE_RD) ypos=y+32*17;
        else if(state`TOWER_TYPE == `GR_SPAWNER) ypos=y+32*11;
        else ypos=y+32*0; //nieokreslone       
    end
    
endmodule