`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.03.2018 14:59:54
// Design Name: 
// Module Name: BulletCellRender
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"
`include "states.vh"

module BulletCellRender(
        input wire pclk,
        input wire`VGA vga_in,
        output reg`VGA vga_out,
        input wire[`BULLET_MEMORY_BITS-1:0] inState
    );
    
    wire`VGA_COUNTING hc,vc;
    wire`RGB rgb_in;
    reg`RGB rgb_out;
    wire`VGA vga_out_nxt;
    PassVgaBus vga(
        .busIn(vga_in),
        .busOut(vga_out_nxt),
        .hcount(hc),
        .vcount(vc),
        .rgbSrc(rgb_in),
        .rgbDst(rgb_out)
    );
    `include "mathfuncs.vh"
    always @(posedge pclk) vga_out <= vga_out_nxt;
    
    
	
    wire[9:0] x,y,xm1,xp1,ym1,yp1,ltowX,ltowY;
	assign x = inState`BULLET_TPOSX;
	assign y = inState`BULLET_TPOSY;
	assign xm1 = x-1;
	assign xp1 = x+1;
	assign ym1 = y-1;
	assign yp1 = y+1;
	assign ltowX = toTowerOrgin(inState`BULLET_GPOSX);
	assign ltowY = toTowerOrgin(inState`BULLET_GPOSY);
	wire signed [9:0] cdx,cdy,dx,dy;
	assign dx = ltowX-x;
	assign dy = ltowY-y;
	assign cdx = ltowX-hc;
	assign cdy = ltowY-vc;
	
	
    always @* begin
        rgb_out = rgb_in;
		case(inState`BULLET_TYPE)
		`BLT_GUN: begin
			if((hc==xm1||hc==x||hc==xp1)&&(vc==ym1||vc==y||vc==yp1)) rgb_out = 0;
			if(hc==x&&vc==y) rgb_out = 12'h888;
		end
		`BLT_LASER: begin
            if((hc==xm1||hc==x||hc==xp1)&&(vc==ym1||vc==y||vc==yp1)) rgb_out = 12'h00f;
            if(hc==x&&vc==y) rgb_out = 12'hf00;
        end
//		`BLT_LASER: begin
//		    //dx*cdy == cdx*dy
//		    if($signed(dx*cdy) == $signed(cdx*dy)) rgb_out = 12'hf0f;
//		end
		endcase
    end
    
    
endmodule
