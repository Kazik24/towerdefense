`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.03.2018 15:00:27
// Design Name: 
// Module Name: DisplayBullets
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module DisplayBullets#(
        parameter COUNT = 4
    )(
        input wire pclk,
        input wire[COUNT*`BULLET_MEMORY_BITS-1:0] bullets,
        input wire`VGA vga_in,
        output wire`VGA vga_out
    );
    //render all enemies to vga
    wire[COUNT*`VGA_BUS_SIZE-1:0] mapped;
    generate
        genvar i;
        for(i=0;i<COUNT;i=i+1) begin
            if(i == 0) begin
                BulletCellRender render(
                    .pclk(pclk),
                    .vga_in(vga_in),
                    .vga_out(mapped[`VGA_BUS_SIZE-1:0]),
                    .inState(bullets[(i+1)*`BULLET_MEMORY_BITS-1:i*`BULLET_MEMORY_BITS])
                );
            end else begin
                BulletCellRender render(
                    .pclk(pclk),
                    .vga_in(mapped[`VGA_BUS_SIZE*i-1:`VGA_BUS_SIZE*(i-1)]),
                    .vga_out(mapped[`VGA_BUS_SIZE*(i+1)-1:`VGA_BUS_SIZE*i]),
                    .inState(bullets[(i+1)*`BULLET_MEMORY_BITS-1:i*`BULLET_MEMORY_BITS])
                );
            end
        end
    endgenerate
    assign vga_out = mapped[COUNT*`VGA_BUS_SIZE-1:(COUNT-1)*`VGA_BUS_SIZE];
    
    
endmodule
