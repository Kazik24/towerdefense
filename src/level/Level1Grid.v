`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 24.04.2018 11:12:25
// Design Name: 
// Module Name: Level1Grid
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module Level1Grid(
        input wire[1:0] level,
        input wire`GRID_XY pos,
        output wire[`TOWER_MEMORY_BITS-1:0] data
    );
    `include "mathfuncs.vh"
    localparam SIZE = `GRID_SIZE*`GRID_SIZE;
    wire[clog2(SIZE)-1:0] addr;
    assign addr = pos`GRID_X + ({pos`GRID_Y,2'h0}+{pos`GRID_Y,4'h0});
    reg[`TOWER_MEMORY_BITS-1:0] level1[0:SIZE-1];
	reg[`TOWER_MEMORY_BITS-1:0] level2[0:SIZE-1];
    assign data = level == 0 ? level1[addr] : level2[addr];
    
    initial begin
        $readmemh("map_level1.data",level1);
		$readmemh("map_level2.data",level2);
    end
    
    
    
endmodule
