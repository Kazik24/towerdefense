`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 24.04.2018 11:13:24
// Design Name: 
// Module Name: Level1Waves
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"
`include "states.vh"




module Level1Waves(
        input wire`LEVEL level,
        input wire`WAVE wave,
        input wire[clog2(`MAX_ENEMIES-1)-1:0] addr,
        output reg[15:0] activTick,
        output reg[`ENEMY_MEMORY_BITS-1:0] data,
        output reg[clog2(`MAX_ENEMIES-1)-1:0] maxAddr
    );
    `include "mathfuncs.vh"
    
    //speed 0 - zatrzymane
    //speed 1 - count = 3, add = 1 (15px/s)
    //speed 2 - count = 2, add = 1 (20px/s)
    //speed 3 - count = 1, add = 1 (30px/s)
    //speed 4 - count = 0, add = 1 (60px/s)
    //speed 5 - count = 0, add = 2 (120px/s)
    //speed 6 - count = 0, add = 3 (160px/s)
    //speed 7 - count = 0, add = 4 (220px/s)
    //pierwsze fale
    `define EN_LVL1_HP 0
    `define EN_LVL2_HP 1
    `define EN_LVL3_HP 2
    `define EN_LVL4_HP 3
    //fale >14
    `define EN2_LVL1_HP 0
    `define EN2_LVL2_HP 2
    `define EN2_LVL3_HP 3
    `define EN2_LVL4_HP 5
    //fale >20
    `define EN3_LVL1_HP 2
    `define EN3_LVL2_HP 4
    `define EN3_LVL3_HP 8
    `define EN3_LVL4_HP 11
    //fale >30
    `define EN4_LVL1_HP 7
    `define EN4_LVL2_HP 8
    `define EN4_LVL3_HP 14
    `define EN4_LVL4_HP 18
	//fale*********************************************************
    always @* begin
        maxAddr = 0;
        data = 0;
        activTick = 1;
        case(wave)
        1: begin
            maxAddr = 9;
            activTick = 120;
            data = newEnemy(`EN_LVL1,`EN_LVL1_HP,3);
        end
        2: begin
            maxAddr = 19;
            activTick = 100;
            data = newEnemy(`EN_LVL1,`EN_LVL1_HP,3);
        end
        3: begin
            maxAddr = 19;
            activTick = 90;
            data = newEnemy(`EN_LVL1,`EN_LVL1_HP,4);
        end
        4: begin
            first(15,newEnemy(`EN_LVL1,`EN_LVL1_HP,4),80,200);
            last(5,newEnemy(`EN_LVL2,`EN_LVL2_HP,3),100);
        end
        5: begin
            first(10,newEnemy(`EN_LVL1,`EN_LVL1_HP,4),70,200);
            last(10,newEnemy(`EN_LVL2,`EN_LVL2_HP,5),70);
        end
        6: begin
            first(10,newEnemy(`EN_LVL1,`EN_LVL1_HP,4),70,200);
            next(10,newEnemy(`EN_LVL2,`EN_LVL2_HP,5),70,300);
            next(10,newEnemy(`EN_LVL1,`EN_LVL1_HP,4),50,200);
            last(10,newEnemy(`EN_LVL2,`EN_LVL2_HP,5),50);
        end
        7: begin
            first(10,newEnemy(`EN_LVL1,`EN_LVL1_HP,4),60,200);
            next(10,newEnemy(`EN_LVL2,`EN_LVL2_HP,5),60,300);
            next(10,newEnemy(`EN_LVL1,`EN_LVL1_HP,4),50,200);
            next(10,newEnemy(`EN_LVL2,`EN_LVL2_HP,5),50,300);
            next(10,newEnemy(`EN_LVL1,`EN_LVL1_HP,4),40,200);
            last(10,newEnemy(`EN_LVL2,`EN_LVL2_HP,5),40);
        end
        8: begin
            first(10,newEnemy(`EN_LVL1,`EN_LVL1_HP,4),40,200);
            next(10,newEnemy(`EN_LVL2,`EN_LVL2_HP,5),40,200);
            last(5,newEnemy(`EN_LVL3,`EN_LVL3_HP,4),80);
        end
        9: begin
            first(10,newEnemy(`EN_LVL1,`EN_LVL1_HP,4),40,200);
            next(10,newEnemy(`EN_LVL2,`EN_LVL2_HP,5),40,200);
            last(10,newEnemy(`EN_LVL3,`EN_LVL3_HP,4),70);
        end
        10: begin
            maxAddr = 9;
            activTick = 60;
            data = newEnemy(`EN_LVL3,`EN_LVL3_HP,7);
        end
        11: begin
            first(10,newEnemy(`EN_LVL1,`EN_LVL1_HP,4),40,200);
            next(10,newEnemy(`EN_LVL2,`EN_LVL2_HP,5),40,200);
            next(10,newEnemy(`EN_LVL3,`EN_LVL3_HP,4),80,60);
            last(10,newEnemy(`EN_LVL3,`EN_LVL3_HP,7),60);
        end
        12: begin
            first(10,newEnemy(`EN_LVL1,`EN_LVL1_HP,7),40,200);
            next(10,newEnemy(`EN_LVL3,`EN_LVL3_HP,5),40,200);
            next(10,newEnemy(`EN_LVL2,`EN_LVL2_HP,5),80,60);
            next(10,newEnemy(`EN_LVL2,`EN_LVL2_HP,7),60,60);
            last(10,newEnemy(`EN_LVL3,`EN_LVL3_HP,7),60);
        end
		13: begin
			first(10,newEnemy(`EN_LVL1,`EN_LVL1_HP,7),40,200);
            next(10,newEnemy(`EN_LVL3,`EN_LVL3_HP,7),40,200);
            next(10,newEnemy(`EN_LVL2,`EN_LVL2_HP,7),80,60);
            next(10,newEnemy(`EN_LVL2,`EN_LVL2_HP,7),60,60);
            last(10,newEnemy(`EN_LVL3,`EN_LVL3_HP,7),60);
		end
		14: begin
			first(10,newEnemy(`EN_LVL3,`EN_LVL3_HP,4),50,100);
            next(10,newEnemy(`EN_LVL3,`EN_LVL3_HP,5),50,100);
            next(10,newEnemy(`EN_LVL3,`EN_LVL3_HP,6),50,100);
            last(10,newEnemy(`EN_LVL3,`EN_LVL3_HP,7),50);
		end
		15: begin
			first(10,newEnemy(`EN_LVL3,`EN2_LVL3_HP,4),15,60);
            next(10,newEnemy(`EN_LVL3,`EN2_LVL3_HP,5),15,60);
            next(10,newEnemy(`EN_LVL3,`EN2_LVL3_HP,6),15,60);
            last(10,newEnemy(`EN_LVL3,`EN2_LVL3_HP,7),15);
		end
		16: begin
	    first(10,newEnemy(`EN_LVL3,`EN2_LVL3_HP,4),40,60);
            next(10,newEnemy(`EN_LVL2,`EN2_LVL2_HP,4),40,60);
            next(40,newEnemy(`EN_LVL1,`EN2_LVL1_HP,7),4,60);
            next(10,newEnemy(`EN_LVL2,`EN2_LVL2_HP,4),40,100);
			last(10,newEnemy(`EN_LVL3,`EN2_LVL3_HP,7),40);
		end
		17: begin
			first(10,newEnemy(`EN_LVL3,`EN2_LVL3_HP,4),5,60);
            next(10,newEnemy(`EN_LVL2,`EN2_LVL2_HP,4),5,60);
            next(40,newEnemy(`EN_LVL1,`EN_LVL1_HP,7),4,60);
			next(10,newEnemy(`EN_LVL2,`EN2_LVL3_HP,4),5,100);
			last(10,newEnemy(`EN_LVL3,`EN2_LVL3_HP,7),5);
		end
		18: begin
			first(20,newEnemy(`EN_LVL3,`EN2_LVL3_HP,4),30,60);
            next(10,newEnemy(`EN_LVL1,`EN2_LVL1_HP,7),5,60);
            next(20,newEnemy(`EN_LVL3,`EN_LVL3_HP,4),30,60);
			next(10,newEnemy(`EN_LVL1,`EN2_LVL1_HP,7),5,60);
			last(20,newEnemy(`EN_LVL3,`EN2_LVL3_HP,4),30);
		end
		19: begin
			first(20,newEnemy(`EN_LVL2,`EN2_LVL2_HP,4),30,60);
            next(10,newEnemy(`EN_LVL1,`EN2_LVL1_HP,7),5,60);
            next(20,newEnemy(`EN_LVL3,`EN2_LVL3_HP,4),30,60);
			next(10,newEnemy(`EN_LVL1,`EN2_LVL1_HP,7),5,60);
			last(20,newEnemy(`EN_LVL3,`EN2_LVL3_HP,4),30);
		end
		20: begin
			first(3,newEnemy(`EN_LVL1,`EN2_LVL1_HP,7),30,60);
			next(3,newEnemy(`EN_LVL2,`EN2_LVL2_HP,7),30,60);
			next(3,newEnemy(`EN_LVL3,`EN2_LVL3_HP,7),30,60);
			last(3,newEnemy(`EN_BOSS,25,4),90);
		end
		21: begin
			first(20,newEnemy(`EN_LVL2,`EN2_LVL2_HP,5),15,100);
			next(20,newEnemy(`EN_LVL3,`EN2_LVL3_HP,5),15,100);
			next(20,newEnemy(`EN_LVL3,`EN2_LVL3_HP,5),15,100);
			last(15,newEnemy(`EN_LVL4,`EN2_LVL4_HP,4),35);
		end
		22: begin
			first(20,newEnemy(`EN_LVL2,`EN3_LVL2_HP,5),15,100);
			next(20,newEnemy(`EN_LVL3,`EN3_LVL3_HP,5),15,100);
			next(20,newEnemy(`EN_LVL3,`EN3_LVL3_HP,5),15,100);
			next(15,newEnemy(`EN_LVL4,`EN3_LVL4_HP,4),35,100);
			last(15,newEnemy(`EN_LVL4,`EN3_LVL4_HP,4),35);
		end
		23: begin
			first(10,newEnemy(`EN_LVL4,`EN3_LVL4_HP,5),15,100);
			next(20,newEnemy(`EN_LVL3,`EN3_LVL3_HP,6),15,100);
			next(20,newEnemy(`EN_LVL2,`EN3_LVL2_HP,7),15,100);
			last(15,newEnemy(`EN_LVL1,`EN3_LVL1_HP,7),5);
		end
		24: begin
			first(20,newEnemy(`EN_LVL4,`EN3_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL1,`EN3_LVL1_HP,7),5,100);
			next(10,newEnemy(`EN_LVL1,`EN3_LVL1_HP,7),5,100);
			next(15,newEnemy(`EN_LVL4,`EN3_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL1,`EN3_LVL1_HP,7),5,100);
			last(10,newEnemy(`EN_LVL1,`EN3_LVL1_HP,7),5);
		end
		25: begin
			first(20,newEnemy(`EN_LVL4,`EN3_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN3_LVL2_HP,7),5,100);
			next(10,newEnemy(`EN_LVL1,`EN3_LVL1_HP,7),5,100);
			next(15,newEnemy(`EN_LVL4,`EN3_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL1,`EN3_LVL1_HP,7),5,100);
			last(10,newEnemy(`EN_LVL2,`EN3_LVL2_HP,7),5);
		end
		26: begin
			first(20,newEnemy(`EN_LVL4,`EN3_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN3_LVL2_HP,7),5,100);
			next(10,newEnemy(`EN_LVL1,`EN3_LVL1_HP,7),5,100);
			next(15,newEnemy(`EN_LVL4,`EN3_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL1,`EN3_LVL1_HP,7),5,100);
			last(10,newEnemy(`EN_LVL2,`EN3_LVL2_HP,7),5);
		end
		27: begin
			first(20,newEnemy(`EN_LVL4,`EN3_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN3_LVL2_HP,7),5,100);
			next(10,newEnemy(`EN_LVL1,`EN3_LVL1_HP,7),5,100);
			next(20,newEnemy(`EN_LVL4,`EN3_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL1,`EN3_LVL1_HP,7),5,100);
			next(10,newEnemy(`EN_LVL2,`EN3_LVL2_HP,7),5,100);
			next(20,newEnemy(`EN_LVL4,`EN3_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN3_LVL2_HP,7),5,100);
			last(10,newEnemy(`EN_LVL1,`EN3_LVL1_HP,7),5);
		end
		28: begin
			first(20,newEnemy(`EN_LVL4,`EN3_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN3_LVL2_HP,7),5,100);
			next(10,newEnemy(`EN_LVL1,`EN3_LVL1_HP,7),5,100);
			next(15,newEnemy(`EN_LVL4,`EN3_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL1,`EN3_LVL1_HP,7),5,100);
			last(10,newEnemy(`EN_LVL2,`EN3_LVL2_HP,7),5);
		end
		29: begin
			first(15,newEnemy(`EN_LVL4,`EN3_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN3_LVL2_HP,6),15,100);
			next(15,newEnemy(`EN_LVL4,`EN3_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN3_LVL2_HP,6),15,100);
			last(15,newEnemy(`EN_LVL4,`EN3_LVL4_HP,6),15);
		end
		30: begin
			first(20,newEnemy(`EN_LVL4,`EN3_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN3_LVL2_HP,6),15,100);
			next(20,newEnemy(`EN_LVL4,`EN3_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN3_LVL2_HP,6),15,100);
			last(20,newEnemy(`EN_LVL4,`EN3_LVL4_HP,6),15);
		end
		31: begin
			first(25,newEnemy(`EN_LVL4,`EN4_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),15,100);
			next(25,newEnemy(`EN_LVL4,`EN4_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),15,100);
			last(25,newEnemy(`EN_LVL4,`EN4_LVL4_HP,6),15);
		end
		32: begin
			first(40,newEnemy(`EN_LVL4,`EN4_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN4_LVL2_HP,7),15,100);
			last(40,newEnemy(`EN_LVL4,`EN4_LVL4_HP,6),15);
		end
		33: begin
			first(5,newEnemy(`EN_LVL1,`EN4_LVL1_HP,7),10,10);
			next(5,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),10,10);
			next(5,newEnemy(`EN_LVL3,`EN4_LVL3_HP,6),10,10);
			next(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,5),10,10);
			next(5,newEnemy(`EN_LVL1,`EN4_LVL1_HP,7),10,10);
			next(5,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),10,10);
			next(5,newEnemy(`EN_LVL3,`EN4_LVL3_HP,6),10,10);
			next(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,5),10,10);
			next(5,newEnemy(`EN_LVL1,`EN4_LVL1_HP,7),10,10);
			next(5,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),10,10);
			next(5,newEnemy(`EN_LVL3,`EN4_LVL3_HP,6),10,10);
			next(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,5),10,10);
			next(5,newEnemy(`EN_LVL1,`EN4_LVL1_HP,7),10,10);
			next(5,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),10,10);
			next(5,newEnemy(`EN_LVL3,`EN4_LVL3_HP,6),10,10);
			next(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,5),10,10);
			next(5,newEnemy(`EN_LVL1,`EN4_LVL1_HP,7),10,10);
			next(5,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),10,10);
			next(5,newEnemy(`EN_LVL3,`EN4_LVL3_HP,6),10,10);
			next(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,5),10,10);
			next(5,newEnemy(`EN_LVL1,`EN4_LVL1_HP,7),10,10);
			next(5,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),10,10);
			next(5,newEnemy(`EN_LVL3,`EN4_LVL3_HP,6),10,10);
			next(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,5),10,10);
			next(5,newEnemy(`EN_LVL1,`EN4_LVL1_HP,7),10,10);
			next(5,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),10,10);
			next(5,newEnemy(`EN_LVL3,`EN4_LVL3_HP,6),10,10);
			next(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,5),10,10);
			next(5,newEnemy(`EN_LVL1,`EN4_LVL1_HP,7),10,10);
			next(5,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),10,10);
			next(5,newEnemy(`EN_LVL3,`EN4_LVL3_HP,6),10,10);
			last(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,5),10);
		end
		34: begin
			first(40,newEnemy(`EN_LVL4,`EN4_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN4_LVL2_HP,7),15,100);
			last(40,newEnemy(`EN_LVL4,`EN4_LVL4_HP,6),15);
		end
		35: begin
			first(5,newEnemy(`EN_LVL1,`EN4_LVL1_HP,7),10,10);
			next(5,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),10,10);
			next(5,newEnemy(`EN_LVL3,`EN4_LVL3_HP,6),10,10);
			next(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,5),10,10);
			next(5,newEnemy(`EN_LVL1,`EN4_LVL1_HP,7),10,10);
			next(5,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),10,10);
			next(5,newEnemy(`EN_LVL3,`EN4_LVL3_HP,6),10,10);
			next(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,5),10,10);
			next(5,newEnemy(`EN_LVL1,`EN4_LVL1_HP,7),10,10);
			next(5,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),10,10);
			next(5,newEnemy(`EN_LVL3,`EN4_LVL3_HP,6),10,10);
			next(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,5),10,10);
			next(5,newEnemy(`EN_LVL1,`EN4_LVL1_HP,7),10,10);
			next(5,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),10,10);
			next(5,newEnemy(`EN_LVL3,`EN4_LVL3_HP,6),10,10);
			next(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,5),10,10);
			next(5,newEnemy(`EN_LVL1,`EN4_LVL1_HP,7),10,10);
			next(5,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),10,10);
			next(5,newEnemy(`EN_LVL3,`EN4_LVL3_HP,6),10,10);
			next(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,5),10,10);
			next(5,newEnemy(`EN_LVL1,`EN4_LVL1_HP,7),10,10);
			next(5,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),10,10);
			next(5,newEnemy(`EN_LVL3,`EN4_LVL3_HP,6),10,10);
			next(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,5),10,10);
			next(5,newEnemy(`EN_LVL1,`EN4_LVL1_HP,7),10,10);
			next(5,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),10,10);
			next(5,newEnemy(`EN_LVL3,`EN4_LVL3_HP,6),10,10);
			next(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,5),10,10);
			next(5,newEnemy(`EN_LVL1,`EN4_LVL1_HP,7),10,10);
			next(5,newEnemy(`EN_LVL2,`EN4_LVL2_HP,6),10,10);
			next(5,newEnemy(`EN_LVL3,`EN4_LVL3_HP,6),10,10);
			last(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,5),10);
		end
		36: begin
			first(40,newEnemy(`EN_LVL4,`EN4_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN4_LVL2_HP,7),15,100);
			next(40,newEnemy(`EN_LVL4,`EN4_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN4_LVL2_HP,7),15,100);
			last(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,7),3);
		end
		37: begin
			first(40,newEnemy(`EN_LVL4,`EN4_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN4_LVL2_HP,7),15,100);
			next(40,newEnemy(`EN_LVL4,`EN4_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN4_LVL2_HP,7),15,100);
			next(10,newEnemy(`EN_LVL4,`EN4_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN4_LVL2_HP,7),15,100);
			last(5,newEnemy(`EN_LVL4,`EN4_LVL4_HP,7),3);
		end
		38: begin
			first(40,newEnemy(`EN_LVL4,`EN4_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN4_LVL2_HP,5),15,100);
			next(40,newEnemy(`EN_LVL4,`EN4_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN4_LVL2_HP,5),15,100);
			next(10,newEnemy(`EN_LVL4,`EN4_LVL4_HP,6),15,100);
			next(10,newEnemy(`EN_LVL2,`EN4_LVL2_HP,5),15,100);
			last(10,newEnemy(`EN_LVL4,`EN4_LVL4_HP,7),3);
		end
		39: begin
			first(100,newEnemy(`EN_LVL4,`EN4_LVL4_HP,6),15,100);
			last(20,newEnemy(`EN_LVL2,`EN4_LVL2_HP,7),3);
		end
		40: begin
			first(10,newEnemy(`EN_LVL1,`EN4_LVL1_HP,7),30,100);
			next(10,newEnemy(`EN_LVL2,`EN4_LVL2_HP,7),30,100);
			next(10,newEnemy(`EN_LVL3,`EN4_LVL3_HP,7),30,100);
			next(10,newEnemy(`EN_LVL4,`EN4_LVL4_HP,7),30,100);
			last(4,newEnemy(`EN_BOSS,100,4),200);
		end
		
        endcase
    end
    //koniec fal******************************************************
    
    task first(input[6:0] count,input [`ENEMY_MEMORY_BITS-1:0] enm,input[15:0] del,input[15:0] post);
        begin
            maxAddr = count-1;
            if(addr >= 0 && addr <= (count-1)) begin
                data = enm;
                activTick = del;
            end
            else if(addr == count) activTick = post;
        end
    endtask
    task next(input[6:0] count,input [`ENEMY_MEMORY_BITS-1:0] enm,input[15:0] del,input[15:0] post);
        begin
            if(addr >= (maxAddr+1) && addr <= (maxAddr+count)) data = enm;
            if(addr >= (maxAddr+2) && addr <= (maxAddr+count)) activTick = del;
            else if(addr == (maxAddr+count+1)) activTick = post;
            maxAddr = maxAddr + count;
        end
    endtask
    task last(input[6:0] count,input [`ENEMY_MEMORY_BITS-1:0] enm,input[15:0] del);
        begin
            if(addr >= (maxAddr+1) && addr <= (maxAddr+count)) data = enm;
            if(addr >= (maxAddr+2) && addr <= (maxAddr+count)) activTick = del;
            maxAddr = maxAddr + count;
        end
    endtask
	
    function[`ENEMY_MEMORY_BITS-1:0] newEnemy(input[3:0] type,input[6:0] hp,input[2:0] speed);
        begin
            newEnemy = 0;
            newEnemy`ENEMY_FACING = `FACING_LEFT;
            newEnemy`ENEMY_POSX = 605;
            newEnemy`ENEMY_POSY = 75;
            newEnemy`ENEMY_TYPE = type;
            newEnemy`ENEMY_HITPOINTS = hp;
            newEnemy`ENEMY_SPEED = speed;
        end
    endfunction
    
endmodule
