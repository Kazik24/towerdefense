`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.04.2018 21:39:59
// Design Name: 
// Module Name: IteratorXY
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module IteratorXY#(
        parameter X = 4,
        parameter Y = 5
    )(
        input wire clk,
        input wire enable,
        output reg busy=0,
        
        output reg[clog2(X-1)-1:0] cntx=0,
        output reg[clog2(Y-1)-1:0] cnty=0,
        output reg irst=0,
        output wire last
    );
    //sygnal autoresetu
    wire regReset;
    assign regReset = ~enable;
    
    //wykrywanie zbocza narastajacego enable
    reg enable_del;
    always @(posedge clk) enable_del <= regReset ? 0 : enable;
    wire enable_pose;
    assign enable_pose = enable && ~enable_del;
    
    //sygnal last
    assign last = (cntx == X-1) && (cnty == Y-1) && enable;
    
    
    reg[clog2(X-1)-1:0] cntx_nxt;
    reg[clog2(Y-1)-1:0] cnty_nxt;
    wire ctick,crst;
    always @* begin
        cntx_nxt = cntx;
        cnty_nxt = cnty;
        if(~busy || regReset) begin
            cntx_nxt = 0;
            cnty_nxt = 0;
        end else begin
            cntx_nxt = cntx+1;
            if(cntx_nxt == X) begin
                cnty_nxt = cnty+1;
                cntx_nxt = 0;
            end
        end
    end
    wire busy_nxt;
    assign busy_nxt = (regReset || last) ? 0 : enable_pose ? 1 : busy;
    always @(posedge clk) begin
        cntx <= cntx_nxt;
        cnty <= cnty_nxt;
        busy <= busy_nxt;
        irst <= ~busy_nxt;
    end
    
    
    
    
    function integer clog2(input integer number); //bez tego nie dziala symulacja
        begin
            clog2 = 0;
            while(number) begin
                clog2  = clog2 + 1;
                number = number >> 1;
            end
        end
    endfunction
endmodule
