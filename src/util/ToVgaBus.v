`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.03.2018 20:17:04
// Design Name: 
// Module Name: ToVgaBus
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module ToVgaBus(
        input wire`VGA_COUNTING hcount,
        input wire hsync,
        input wire hblnk,
        input wire`VGA_COUNTING vcount,
        input wire vsync,
        input wire vblnk,
        input wire`RGB rgb,
        output wire`VGA busOut
    );
    
    assign busOut = {hcount,vcount,rgb,hsync,vsync,hblnk,vblnk};
    
endmodule
