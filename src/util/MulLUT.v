`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 31.05.2018 00:20:44
// Design Name: 
// Module Name: MulLUT
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MulLUT#(
        parameter AWIDTH = 10,
        parameter BWIDTH = 3,
        parameter OUTWIDTH = 13
    )(
        input wire[AWIDTH-1:0] a,
        input wire[BWIDTH-1:0] b,
        output wire[OUTWIDTH-1:0] mul
    );
    
    reg[OUTWIDTH-1:0] lut[2**(AWIDTH+BWIDTH)-1:0];
    
    
    reg[AWIDTH:0] at;
    reg[BWIDTH:0] bt;
    initial begin
        for(at=0;at<AWIDTH**2;at=at+1) begin
            for(bt=0;bt<AWIDTH**2;bt=bt+1) begin
                lut[{bt[BWIDTH-1:0],at[AWIDTH-1:0]}] = bt*at;
            end
        end
    end
    //assign mul = lut[{b,a}];
    
    assign mul = a*b;
endmodule
