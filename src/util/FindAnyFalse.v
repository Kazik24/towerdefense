`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.03.2018 10:53:50
// Design Name: 
// Module Name: FindAnyFalse
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module FindAnyFalse#(
        parameter COUNT = 16
    )(
        input wire[COUNT-1:0] slots,
        output reg[clog2(COUNT)-1:0] emptyAddress
    );
    `include "mathfuncs.vh"
    localparam BC = 15;
    
    generate
        genvar i;
        localparam STAGES = (COUNT/BC+((COUNT%BC)==0?0:1));
        reg[clog2(BC)-1:0] tempAddr[STAGES-1:0];
        for(i=0;i<COUNT/BC;i=i+1) begin
            wire[14:0] mappedSlot;
            assign mappedSlot = slots[(i+1)*BC-1:i*BC];
            integer cnt1;
            always @* begin
                tempAddr[i] = ~0;//all ones = null
                for(cnt1 = 0;cnt1<BC;cnt1=cnt1+1) begin
                    if(~mappedSlot[cnt1]) tempAddr[i] = cnt1;
                end
            end
        end
        //final unalligned stage
        integer cnt2;
        localparam REST = COUNT%BC;
        if(REST != 0) begin
            wire[REST-1:0] mappedRest;
            assign mappedRest = slots[COUNT-1:COUNT-REST];
            always @* begin
                tempAddr[STAGES-1] = ~0;//all ones = null
                for(cnt2 = 0;cnt2<REST;cnt2=cnt2+1) begin
                    if(~mappedRest[cnt2]) tempAddr[STAGES-1] = cnt2;
                end
            end
        end
        
        //grab first slot
        integer cnt3;
        
        always @* begin
            emptyAddress = ~0;
            for(cnt3 = 0;cnt3<STAGES;cnt3=cnt3+1) begin
                if(~&tempAddr[cnt3]) emptyAddress = tempAddr[cnt3] + cnt3*BC;
            end
        end
    endgenerate
endmodule
