`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 25.03.2018 21:55:45
// Design Name: 
// Module Name: Sequencer
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Sequencer#(
        parameter COUNT = 4//ilosc uzadzen do sychronizaji
    )(
        input wire clk,//zegar
        input wire enable,//uruchomienie sekwencji, jesli zostanie zwolniony przedwczesnie, resetuje uklad
        output reg busy=0,//flaga zajetosci (trwa od enable=1 do konca ostatniego impulsu seq_save_tick)
        
        output reg[COUNT-1:0] seq_enable=0,
        //zegar wyzwalany zaraz przed zwolnieniem enable by mozna bylo zapisac dane do rejestru
        //output reg[COUNT-1:0] seq_save_tick,
        //flaga zajetego uzadzenia, (musi trwac przynajmniej 2 zbocza posedge zegara)
        input wire[COUNT-1:0] seq_busy
    );
    //kabel resetu ukladu
    wire regReset;
    assign regReset = ~enable;
    //wykrywanie zbocza narastajacego enable
    reg enable_del;
    always @(posedge clk) enable_del <= regReset ? 0 : enable;
    wire enable_pose;
    assign enable_pose = enable & ~enable_del;
    //wykrywanie zbocza opadajacego seq_busy
    reg[COUNT-1:0] busy_del;
    always @(posedge clk) busy_del <= regReset ? 0 : seq_busy;
    wire[COUNT-1:0] busy_nege;
    assign busy_nege = ~seq_busy & busy_del;
    //rejestry opozniajace busy_nege
    reg[COUNT-1:0] bn_delayed,bn_delayed2;
    always @(posedge clk) bn_delayed <= regReset ? 0 : busy_nege;
    always @(posedge clk) bn_delayed2 <= regReset ? 0 : bn_delayed;
    //rs latch sterujacy sygnalem busy ukladu
    always @(posedge clk) busy = (regReset || bn_delayed[COUNT-1]) ? 0 : enable_pose ? 1 : busy;
    
    
    //rs latche do seq_enable
    wire[COUNT-1:0] s;
    assign s = {bn_delayed[COUNT-2:0],enable_pose};//sygnal set
    generate
        genvar i;
        for(i=0;i<COUNT;i=i+1) begin
            wire r;
            assign r = regReset || busy_nege[i];
            always @(posedge clk) seq_enable[i] = r ? 0 : s[i] ? 1 : seq_enable[i]; //rs latch (reset priority)
        end
    endgenerate
    
    
endmodule
