`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 31.03.2018 21:59:30
// Design Name: 
// Module Name: Iterator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Iterator#(
        parameter COUNT = 4
    )(
        input wire clk,
        input wire enable,
        output reg busy=0,
        
        output reg[clog2(COUNT-1)-1:0] cnt=0,
        output reg irst=0,
        output wire last
    );
    //sygnal autoresetu
    wire regReset;
    assign regReset = ~enable;
    
    //wykrywanie zbocza narastajacego enable
    reg enable_del;
    always @(posedge clk) enable_del <= regReset ? 0 : enable;
    wire enable_pose;
    assign enable_pose = enable && ~enable_del;
    
    //sygnal last
    assign last = (cnt == COUNT-1) && enable;
    
    wire[clog2(COUNT-1)-1:0] cnt_nxt;
    assign cnt_nxt = (~busy || regReset) ? 0 : (cnt+1);
    
    wire busy_nxt;
    assign busy_nxt = (regReset || last) ? 0 : enable_pose ? 1 : busy;
    always @(posedge clk) begin
        cnt <= cnt_nxt;
        busy <= busy_nxt;
        irst <= ~busy_nxt;
    end
    
    
    
    
    function integer clog2(input integer number); //bez tego nie dziala symulacja
        begin
            clog2 = 0;
            while(number) begin
                clog2  = clog2 + 1;
                number = number >> 1;
            end
        end
    endfunction
endmodule
