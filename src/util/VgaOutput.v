`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.03.2018 13:51:23
// Design Name: 
// Module Name: VgaOutput
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module VgaOutput(
        input wire`VGA vga_in,
        
        output wire vs,
        output wire hs,
        output wire[3:0] r,
        output wire[3:0] g,
        output wire[3:0] b,
        output wire vblank_sync
    );
    
    wire`RGB rgb;
    wire hblnk,vblnk;
    FromVgaBus unpack(
        .busIn(vga_in),
        .vsync(vs),
        .vblnk(vblnk),
        .hsync(hs),
        .hblnk(hblnk),
        .rgb(rgb),
        .hcount(),
        .vcount()
    );
    //dla bezpieczenstwa, jesli blanking to ustawiamy na czarny kolor
    assign {r,g,b} = (hblnk || vblnk) ? 12'h000 : rgb;
    assign vblank_sync = vblnk;
    
endmodule
