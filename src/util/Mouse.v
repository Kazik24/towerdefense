`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.03.2018 09:21:30
// Design Name: 
// Module Name: Mouse
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module Mouse(
        input wire clk100Mhz,
        input wire clk40Mhz,
        inout wire ps2_clk,
        inout wire ps2_data,
        
        input wire frameSync,
        
        output reg`PX_COORD xpos,
        output reg`PX_COORD ypos,
        output reg[3:0] zpos,
        output reg[1:0] button,//wcisniety przycisk
        output reg pressed//1 jestli wcisnieto, trwa 1 ramke
    );
    
    
    wire[11:0] mx,my;
    wire[3:0] mz;
    wire left,middle,right;
    wire[1:0] mb;
    MouseCtl native_mouse(
        .clk(clk100Mhz),
        .rst(0),
        .xpos(mx),
        .ypos(my),
        .zpos(mz),
        .left(left),
        .middle(middle),
        .right(right),
        .new_event(),
        .value(0),
        .setx(0),
        .sety(0),
        .setmax_x(0),
        .setmax_y(0),
        
        .ps2_clk(ps2_clk),
        .ps2_data(ps2_data)
    );
    
    reg frameSync_del,frame_pose_del,frame_pose_del2;
    wire frame_pose;
    always @(posedge clk40Mhz) frameSync_del <= frameSync;
    assign frame_pose = frameSync && ~frameSync_del;
    always @(posedge clk40Mhz) frame_pose_del <= frame_pose;
    
    
    
    reg left_latch,right_latch,middle_latch;
    wire rst;
    reg[2:0] buttons_del,buttons_buf;
    wire[2:0] buttons_pose,buttons_nege;
    always @(posedge left, posedge frame_pose_del) left_latch <= frame_pose_del ? 0 : 1;
    always @(posedge right, posedge frame_pose_del) right_latch <= frame_pose_del ? 0 : 1;
    always @(posedge middle, posedge frame_pose_del) middle_latch <= frame_pose_del ? 0 : 1;
    always @(posedge clk40Mhz) if(frame_pose) buttons_buf <= {left_latch,right_latch,middle_latch};
    always @(posedge clk40Mhz) if(frame_pose) buttons_del <= buttons_buf;
    //assign buttons_nege = buttons_del & ~buttons_buf;
    assign buttons_pose = buttons_buf & ~buttons_del;
    
    
    //kodowanie przycisku na 2 bitowy kod
    assign mb = buttons_buf[2]?`MOUSEBTN_LEFT:(buttons_buf[1]?`MOUSEBTN_RIGHT:(buttons_buf[0]?`MOUSEBTN_MIDDLE:`MOUSEBTN_NONE));
    //wyjscia jako rejestry synchronizowane przez zegar ramki obrazu
    always @(posedge clk40Mhz) if(frame_pose_del) begin
        xpos <= mx[`PX_COORD_SIZE-1:0];
        ypos <= my[`PX_COORD_SIZE-1:0];
        zpos <= mz;
        button <= mb;
        pressed <= |buttons_pose;
    end
    
endmodule
