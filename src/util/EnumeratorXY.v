`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.04.2018 11:23:26
// Design Name: 
// Module Name: EnumeratorXY
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module EnumeratorXY#(
        parameter X = 4,
        parameter Y = 5
    )(
        input wire clk,
        input wire enable,
        output reg busy,
        output reg[clog2(X-1)-1:0] cntx,
        output reg[clog2(Y-1)-1:0] cnty,
        output reg irst,
        output wire last,
        
        output reg subenable,
        input wire subbusy
    );
    wire nen;
    assign #1 nen = ~enable;
    assign #3 last = (cntx == X-1) && (cnty == Y-1) && enable;
    
    //wykrywanie posedge enable i negedge subbusy
    reg enable_del,subbusy_del,sbnege_del;
    wire enable_pose,subbusy_nege;
    always @(posedge clk) enable_del <= nen ? 0 : enable;
    always @(posedge clk) subbusy_del <= nen ? 0 : subbusy;
    assign enable_pose = enable && ~enable_del;
    assign subbusy_nege = subbusy_del && ~subbusy;
    always @(posedge clk) sbnege_del <= nen ? 0 : subbusy_nege;
    
    reg[clog2(X-1)-1:0] cntx_nxt;
    reg[clog2(Y-1)-1:0] cnty_nxt;
    
    wire sb_nxt,busy_nxt;
    assign busy_nxt = nen ? 0 : (enable_pose || ((last && sbnege_del) ? 0 : busy));
    assign sb_nxt = nen ? 0 : (subbusy_nege ? 0 : ((sbnege_del && ~last) || enable_pose) ? 1 : subenable);
    wire crst,ctick;
    assign crst = nen || ~busy_nxt;
    assign ctick = sbnege_del && ~last;
    
    always @* begin
        cntx_nxt = cntx;
        cnty_nxt = cnty;
        if(crst) begin
            cntx_nxt = 0;
            cnty_nxt = 0;
        end else if(ctick) begin
            cntx_nxt = cntx+1;
            if(cntx_nxt == X) begin
                cnty_nxt = cnty+1;
                cntx_nxt = 0;
            end
        end
    end
    
    
    always @(posedge clk) begin
        subenable <= sb_nxt;
        cntx <= cntx_nxt;
        cnty <= cnty_nxt;
        busy <= busy_nxt;
        irst <= ~busy_nxt;
    end
    
    
    function integer clog2(input integer number); //bez tego nie dziala symulacja
        begin
            clog2 = 0;
            while(number) begin
                clog2  = clog2 + 1;
                number = number >> 1;
            end
        end
    endfunction
endmodule
