`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 27.03.2018 22:24:47
// Design Name: 
// Module Name: PassVgaBus
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module PassVgaBus(
        input wire`VGA busIn,
        output wire`VGA busOut,
        output wire`VGA_COUNTING hcount,
        output wire`VGA_COUNTING vcount,
        output wire`RGB rgbSrc,
        input wire`RGB rgbDst
    );
    
    wire hs,vs,hb,vb;
    FromVgaBus unpack(
        .busIn(busIn),
        .vcount(vcount),
        .hcount(hcount),
        .rgb(rgbSrc),
        .vsync(vs),
        .hsync(hs),
        .vblnk(vb),
        .hblnk(hb)
    );
    ToVgaBus pack(
        .vcount(vcount),
        .hcount(hcount),
        .rgb(rgbDst),
        .vsync(vs),
        .hsync(hs),
        .vblnk(vb),
        .hblnk(hb),
        .busOut(busOut)
    );
endmodule
