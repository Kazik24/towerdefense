`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.04.2018 13:24:41
// Design Name: 
// Module Name: Enumerator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Enumerator#(
        parameter COUNT = 4
    )(
        input wire clk,
        input wire enable,
        output reg busy,
        output reg[clog2(COUNT-1)-1:0] cnt,
        output reg irst,
        output wire last,
        
        output reg subenable,
        input wire subbusy
    );
    
    wire nen;
    assign #1 nen = ~enable;
    assign #3 last = (cnt == COUNT-1) && enable;
    
    //wykrywanie posedge enable i negedge subbusy
    reg enable_del,subbusy_del,sbnege_del;
    wire enable_pose,subbusy_nege;
    always @(posedge clk) enable_del <= nen ? 0 : enable;
    always @(posedge clk) subbusy_del <= nen ? 0 : subbusy;
    assign enable_pose = enable && ~enable_del;
    assign subbusy_nege = subbusy_del && ~subbusy;
    always @(posedge clk) sbnege_del <= nen ? 0 : subbusy_nege;
    
    wire[clog2(COUNT-1)-1:0] cnt_nxt;
    
    wire sb_nxt,busy_nxt;
    assign busy_nxt = nen ? 0 : (enable_pose || ((last && sbnege_del) ? 0 : busy));
    assign sb_nxt = nen ? 0 : (subbusy_nege ? 0 : ((sbnege_del && ~last) || enable_pose) ? 1 : subenable);
    assign cnt_nxt = (nen || ~busy_nxt) ? 0 : (sbnege_del && ~last) ? cnt+1 : cnt;
    
    
    always @(posedge clk) begin
        subenable <= sb_nxt;
        cnt <= cnt_nxt;
        busy <= busy_nxt;
        irst <= ~busy_nxt;
    end
    
    
    function integer clog2(input integer number); //bez tego nie dziala symulacja
        begin
            clog2 = 0;
            while(number) begin
                clog2  = clog2 + 1;
                number = number >> 1;
            end
        end
    endfunction
endmodule
