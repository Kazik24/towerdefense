//no include guards

`include "maindefs.vh"

function automatic`RGB blendColors(input`RGB backg,input`RGB col);
	reg[3:0] red,green,blue;
	begin
		//blue
		blue = {1'b0,backg[3:1]} + {1'b0,col[3:1]} + (backg[0] && col[0]);
		//green
		green = {1'b0,backg[7:5]} + {1'b0,col[7:5]} + (backg[4] && col[4]);
		//red
		red = {1'b0,backg[11:9]} + {1'b0,col[11:9]} + (backg[8] && col[8]);
		blendColors = {red,green,blue};
	end
endfunction
//chamfer distance weight 3 approximation
function automatic`PX_COORD approxDistance(input`PX_COORD x1,input`PX_COORD y1,
                                           input`PX_COORD x2,input`PX_COORD y2);
    reg`PX_COORD dx,dy,tx,ty;
    begin
		tx = x1 - x2;
		ty = y1 - y2;
        dx = tx[`PX_COORD_SIZE-1] ? -tx : tx;//absolute value
        dy = ty[`PX_COORD_SIZE-1] ? -ty : ty;
        if(dy < dx) approxDistance = dx + (dy[`PX_COORD_SIZE-1:2] + dy[`PX_COORD_SIZE-1:3]);
        else approxDistance = dy + (dx[`PX_COORD_SIZE-1:2] + dx[`PX_COORD_SIZE-1:3]);
    end
endfunction

function automatic`GRID_XY divBy30(input`VGA_COUNTING num);//x - wynik, y - reszta z dzielenia
    reg`VGA_COUNTING d,r;
    reg`GRID_COORD res;
    integer i;
    begin
        d = num;
        for(i=`GRID_COORD_SIZE-1;i>=0;i=i-1) begin
            r = 30 << i;
            if(r <= d) begin
                d = d - r;
                res = res | (1<<i);
            end
        end
        divBy30`GRID_X = res;
        divBy30`GRID_Y = d`GRID_COORD;
    end
endfunction

function automatic`PX_COORD calcBulletFly(input`PX_COORD diff,input[2:0] div,input[2:0] cnt);
    reg[2:0] mul;
    reg signed`PX_COORD val;
    reg sign;
    reg`PX_COORD acc;
    begin
        mul = div-cnt;
        val = $signed(diff/div);
        sign = val[`PX_COORD_SIZE-1];
        acc = sign ? -val : val;
        calcBulletFly = (mul[0]?acc:0) + (mul[1]?{acc,1'b0}:0) + (mul[2]?{acc,2'b0}:0);
        calcBulletFly = sign ? -calcBulletFly : calcBulletFly;
    end

endfunction

function integer clog2(input integer number);
    begin
        clog2 = 0;
        while(number) begin
            clog2  = clog2 + 1;
            number = number >> 1;
        end
    end
endfunction

function`PX_COORD toTowerOrgin(input`GRID_COORD coord);
    toTowerOrgin = (`GRID_SIZE/2) + ({coord,1'b0}+{coord,2'b0}) +({coord,3'b0}+{coord,4'b0});
endfunction

function[clog2(`GRID_SIZE*`GRID_SIZE)-1:0] gridAddrPos(input`GRID_XY pos);
    gridAddrPos = ({pos`GRID_Y,2'h0}+{pos`GRID_Y,4'h0}) + pos`GRID_X;
endfunction

function[clog2(`GRID_SIZE*`GRID_SIZE)-1:0] gridAddrXY(input`GRID_COORD x,input`GRID_COORD y);
    gridAddrXY = ({y,2'h0}+{y,4'h0}) + x;
endfunction
