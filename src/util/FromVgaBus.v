`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.03.2018 20:20:54
// Design Name: 
// Module Name: FromVgaBus
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module FromVgaBus(
        input wire`VGA busIn,
        output wire`VGA_COUNTING hcount,
        output wire hsync,
        output wire hblnk,
        output wire`VGA_COUNTING vcount,
        output wire vsync,
        output wire vblnk,
        output wire`RGB rgb
    );
    assign {hcount,vcount,rgb,hsync,vsync,hblnk,vblnk} = busIn;
endmodule
