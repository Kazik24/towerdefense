`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.03.2018 18:38:49
// Design Name: 
// Module Name: BusOutRam
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module BusOutRam#(
        parameter COUNT = 14,
        parameter BITS = 32
    )(
        input wire clk,
        input wire[clog2(COUNT-1)-1:0] waddr,
        input wire wen,
        input wire[BITS-1:0] wdata,
        
        input wire[clog2(COUNT-1)-1:0] raddr,
        output wire[BITS-1:0] rdata,
        
        output wire[COUNT*BITS-1:0] mapped
    );
    `include "mathfuncs.vh"
    
    reg[BITS-1:0] memory[COUNT-1:0];
    
    always @(posedge clk) if(wen) memory[waddr] <= wdata;
    assign rdata = memory[raddr];
    
    integer k;
    initial for(k=0;k<COUNT;k=k+1) memory[k] = 0; //init 0 for simulation
    
    generate
        genvar i;
        for(i=0;i<COUNT;i=i+1) begin
            assign mapped[(i+1)*BITS-1:i*BITS] = memory[i];
        end
    endgenerate
    /*localparam CELL = 8;
    localparam SIZE = COUNT/CELL + ((COUNT%CELL != 0)?1:0);
    
    wire[clog2(COUNT-1)-clog2(CELL-1)-1:0] maddrR,maddrW;
    wire[clog2(CELL-1)-1:0] saddrR,saddrW;
    
    
    assign maddrR = raddr[clog2(COUNT-1)-1:clog2(CELL-1)];
    assign maddrW = waddr[clog2(COUNT-1)-1:clog2(CELL-1)];
    assign saddrR = raddr[clog2(CELL-1)-1:0];
    assign saddrW = waddr[clog2(CELL-1)-1:0];
    
    wire[BITS-1:0] readData[SIZE-1:0];
    
    generate
        genvar i;
        for(i=0;i<SIZE;i=i+1) begin
            if(i == SIZE-1 && COUNT%CELL != 0) begin
                BusOutRamCell#(COUNT%CELL,BITS,clog2(CELL-1)) ramCell(
                    .clk(clk),
                    .wen(wen && (maddrW == i)),
                    .waddr(saddrW),
                    .raddr(saddrR),
                    .wdata(wdata),
                    .rdata(readData[i]),
                    .mapped(mapped[COUNT*BITS-1:BITS*CELL*(SIZE-1)])
                );
            end else begin
                BusOutRamCell#(CELL,BITS,clog2(CELL-1)) ramCell(
                    .clk(clk),
                    .wen(wen && (maddrW == i)),
                    .waddr(saddrW),
                    .raddr(saddrR),
                    .wdata(wdata),
                    .rdata(readData[i]),
                    .mapped(mapped[(BITS*CELL*(i+1))-1:BITS*CELL*i])
                );
            end
            
        end
    endgenerate
    
    assign rdata = readData[maddrR];
    */
endmodule
