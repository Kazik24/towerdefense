`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.03.2018 13:22:26
// Design Name: 
// Module Name: ClockGenerator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ClockGenerator(
        input wire clk,
        output wire clk100Mhz,
        output wire clk40Mhz
        //output wire mirror_clk40Mhz //kopia zegara nie jest uzywana
    );
    native_clock_gen gen(
        .clk(clk),
        .clk100Mhz(clk100Mhz),
        .clk40Mhz(clk40Mhz),
        .reset(0),
        .locked()
    );
    
//    ODDR pclk_oddr(
//        .Q(mirror_clk40Mhz),
//        .C(clk40Mhz),
//        .CE(1'b1),
//        .D1(1'b1),
//        .D2(1'b0),
//        .R(1'b0),
//        .S(1'b0)
//    );
endmodule
