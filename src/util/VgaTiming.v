`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.03.2018 23:38:13
// Design Name: 
// Module Name: VgaTiming
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module VgaTiming(
  input wire pclk,
  output wire`VGA vga_bus,
  output wire vblank_sync
  );

  // Describe the actual circuit for the assignment.
  // Video timing controller set for 800x600@60fps
  // using a 40 MHz pixel clock per VESA spec.
  
  localparam HBLNK_START = 800;
  localparam HBLNK_TIME = 256;
  localparam HSYNC_START = 840;
  localparam HSYNC_TIME = 128;
  
  localparam VBLNK_START = 600;
  localparam VBLNK_TIME = 28;
  localparam VSYNC_START = 601;
  localparam VSYNC_TIME = 4;
  
  reg`VGA_COUNTING hc = 0;
  reg`VGA_COUNTING vc = 0;
  
  reg`VGA_COUNTING hc_next;
  reg`VGA_COUNTING vc_next;
  wire hblnk_next,hsync_next,vblnk_next,vsync_next;
  
  
  //combinational part
  always @* begin
    if(hc < (HBLNK_START+HBLNK_TIME-1)) hc_next = hc + 1;
    else hc_next = 0;
    if( vc < (VBLNK_START+VBLNK_TIME-1))
        vc_next = (hc == (HBLNK_START+HBLNK_TIME-1)) ? (vc + 1) : vc;
    else
        vc_next = (hc == (HBLNK_START+HBLNK_TIME-1)) ? 0 : vc;
  end
  assign hblnk_next = hc_next >= HBLNK_START && hc_next < (HBLNK_START+HBLNK_TIME);
  assign hsync_next = hc_next >= HSYNC_START && hc_next < (HSYNC_START+HSYNC_TIME);
  assign vblnk_next = vc_next >= VBLNK_START && vc_next < (VBLNK_START+VBLNK_TIME);
  assign vsync_next = vc_next >= VSYNC_START && vc_next < (VSYNC_START+VSYNC_TIME);
  
  //sequential part
  reg hsync,vsync,hblnk,vblnk;
  always @(posedge pclk) begin
    hc <= hc_next;
    vc <= vc_next;
    hblnk <= hblnk_next;
    hsync <= hsync_next;
    vblnk <= vblnk_next;
    vsync <= vsync_next;
  end
  
  ToVgaBus(
    .hcount(hc),
    .vcount(vc),
    .vsync(vsync),
    .hsync(hsync),
    .vblnk(vblnk),
    .hblnk(hblnk),
    .rgb(12'h000),
    .busOut(vga_bus)
  );
  assign vblank_sync = vblnk;
  

endmodule

