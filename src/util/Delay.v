`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 17.04.2018 12:23:51
// Design Name: 
// Module Name: Delay
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Delay#(
        parameter TICKS = 10
    )(
        input wire clk,
        input wire enable,
        output reg busy
    );
    
    reg[TICKS-1:0] shift;
    wire rst;
    assign rst = ~enable;
    
    always @(posedge clk) begin
        shift <= rst ? 0 : {shift[TICKS-2:0],enable};
        busy <= rst ? 0 : enable & ~shift[TICKS-1];
    end
    
    
    
endmodule
