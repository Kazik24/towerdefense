`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.03.2018 22:41:21
// Design Name: 
// Module Name: Slot20x20
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module Slot20x20#(
        parameter BITS = 8
    )(
        input wire clk,//zegar pamieci
        input wire`GRID_XY wpos,//koordynaty x,y zapisu do komorki
        input wire[BITS-1:0] data,//dane
        input wire wen,//odblokowanie zapisu do pamieci
        input wire`GRID_XY rpos0,//koordynaty x,y odczytu z komorki kanalu 0
        output wire[BITS-1:0] out0,//odczyt kanalu 0
        input wire`GRID_XY rpos1,//koordynaty x,y odczytu z komorki kanalu 1
        output wire[BITS-1:0] out1,//odczyt kanalu 1
        input wire`GRID_XY rpos2,//koordynaty x,y odczytu z komorki kanalu 2
        output wire[BITS-1:0] out2//odczyt kanalu 2
    );
    `include "mathfuncs.vh"
    
    localparam SIZE = `GRID_SIZE*`GRID_SIZE;
    reg[BITS-1:0] memory[SIZE-1:0];
    
    
    wire[clog2(SIZE)-1:0] inAddr;
    wire[clog2(SIZE)-1:0] outAddr0;
    wire[clog2(SIZE)-1:0] outAddr1;
    wire[clog2(SIZE)-1:0] outAddr2;
    //inAddr = wx + 20*wy ale nie uzywa mnozenia
    
    assign inAddr = wpos`GRID_X + ({wpos`GRID_Y,2'h0}+{wpos`GRID_Y,4'h0});
    assign outAddr0 = rpos0`GRID_X + ({rpos0`GRID_Y,2'h0}+{rpos0`GRID_Y,4'h0});
    assign outAddr1 = rpos1`GRID_X + ({rpos1`GRID_Y,2'h0}+{rpos1`GRID_Y,4'h0});
    assign outAddr2 = rpos2`GRID_X + ({rpos2`GRID_Y,2'h0}+{rpos2`GRID_Y,4'h0});
    
    always @(posedge clk) if(wen) memory[inAddr] = data;
    
    assign out0 = memory[outAddr0];
    assign out1 = memory[outAddr1];
    assign out2 = memory[outAddr2];
    
endmodule
