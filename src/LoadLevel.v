`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.05.2018 21:24:46
// Design Name: 
// Module Name: LoadLevel
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module LoadLevel(//laduje level i zeruje pamiec wrogow
        input wire clk,
        input wire enable,
        output wire busy,
        
        input wire`LEVEL level,//nie moze byc zmieniany podczas enable=1 !!!
        
        output wire[clog2(`MAX_ENEMIES-1)-1:0] memEnemy,
        output wire`GRID_XY memPos,
        output wire[`TOWER_MEMORY_BITS-1:0] memTowerWrite,
        output wire memEnemyWEN,
        output wire memGridWEN
    );
    `include "mathfuncs.vh"
    
    
    wire enMap,enEnm,bsMap,bsEnm;
    
    Sequencer#(2) seq(
        .clk(clk),
        .enable(enable),
        .busy(busy),
        .seq_enable({enEnm,enMap}),
        .seq_busy({bsEnm,bsMap})
    );
    
    
    wire`GRID_COORD cntx,cnty;
    wire[clog2(`MAX_ENEMIES-1)-1:0] cntEnm;
    IteratorXY#(`GRID_SIZE,`GRID_SIZE) gloop(
        .clk(clk),.enable(enMap),.busy(bsMap),
        .cntx(cntx),.cnty(cnty),.irst(),.last()
    );
    
    Iterator#(`MAX_ENEMIES) iter(
        .clk(clk),
        .enable(enEnm),
        .busy(bsEnm),
        .cnt(cntEnm),
        .irst(),
        .last()
        
    );
    
   
    
    reg`GRID_XY pos;
    always @* begin
        pos = 0;
        pos`GRID_X = cntx;
        pos`GRID_Y = cnty;
    end
    wire[`TOWER_MEMORY_BITS-1:0] gridData;
    Level1Grid gridRom(
        .level(level),
        .pos(pos),
        .data(gridData)
    );
    
    assign memEnemy = cntEnm;
    assign memPos = pos;
    assign memTowerWrite = gridData & {`TOWER_MEMORY_BITS{enMap}};
    assign memGridWEN = bsMap;
    assign memEnemyWEN = bsEnm;
    
    
endmodule
