`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.03.2018 09:29:58
// Design Name: 
// Module Name: CalcBulletMove
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module CalcBulletMove#(
        parameter COUNT = 16
    )(
        input wire clk,
        input wire enable,
        output wire busy,
        input wire rst,
        input wire[`BULLET_MEMORY_BITS-1:0] push,//data
        input wire pushEnable,//push data
        output wire[COUNT*`BULLET_MEMORY_BITS-1:0] bullets,
        
        output wire[clog2(`MAX_ENEMIES-1)-1:0] enmAddr,
        input wire[`ENEMY_MEMORY_BITS-1:0] enemyRead,
        output wire[`ENEMY_MEMORY_BITS-1:0] enemyWrite,
		output reg`STATISTIC money,
        output wire enemyWEN
    );
    `include "mathfuncs.vh"
    wire pushTick;
    assign pushTick = pushEnable && ~enable;
    
    reg enable_del,enable_pose_del;
    wire enable_pose;
    always @(posedge clk) enable_del <= enable;
    assign enable_pose = enable && ~enable_del;
    always @(posedge clk) enable_pose_del <= enable_pose;
    
    wire subenable,subbusy,irst,last;
    reg subenable_del,subenable_pose_del;
    wire subenable_pose;
    always @(posedge clk) subenable_del <= subenable;
    assign subenable_pose = subenable && ~subenable_del;
    always @(posedge clk) subenable_pose_del <= subenable_pose;
    
    //ktore sloty sa aktywne
    reg[COUNT-1:0] slotActive = 0;
    //znajdz pusty slot pamieci, jesli nie ma zadnego, u�yj 0
    wire[clog2(COUNT)-1:0] emptyAddress;// all ones = null
    FindAnyFalse#(COUNT) findEmptySlot(slotActive,emptyAddress);
    wire[clog2(COUNT-1)-1:0] writeAddr,readAddr,iterAddr;
    assign writeAddr = &emptyAddress ? 0 : emptyAddress[clog2(COUNT-1)-1:0];
    assign iterAddr = enable?readAddr:writeAddr;
    wire[`BULLET_MEMORY_BITS-1:0] readData;
    
    

    wire[`BULLET_MEMORY_BITS-1:0] writeData;
    BusOutRam#(COUNT,`BULLET_MEMORY_BITS) ram(
        .clk(clk),
        .waddr(iterAddr),
        .wen(pushTick || subenable_pose_del),
        .wdata(enable?writeData:push),
        .raddr(readAddr),
        .rdata(readData),
        .mapped(bullets)
    );
    
    
    
    Enumerator#(COUNT) enum(
        .clk(clk),
        .enable(enable),
        .busy(busy),
        .cnt(readAddr),
        .subenable(subenable),
		.irst(irst),
		.last(last),
        .subbusy(subbusy)
    );
    
    
    
    
    wire[clog2(`MAX_ENEMIES-1)-1:0] enmAddrFsm;
    wire[`ENEMY_MEMORY_BITS-1:0] enemyWriteFsm;
    wire enemyWENFsm;
    
    assign enmAddr = enmAddrFsm & {clog2(`MAX_ENEMIES-1){enable}};
    assign enemyWrite = enemyWriteFsm & {`ENEMY_MEMORY_BITS{enable}};
    assign enemyWEN = enemyWENFsm && enable && subenable_pose_del;
    
    wire`PX_COORD explX,explY,distance;
    wire active,tickExplode;
	wire`STATISTIC moneyEarned;
	reg`STATISTIC imoney;
	wire`STATISTIC imoney_nxt;
    FSMBullet bulletAi(
        .inState(readData),
        .outState(writeData),
        .isActive(slotActive[readAddr]),
        .enmAddr(enmAddrFsm),
        .enemyRead(enemyRead),
        .enemyWrite(enemyWriteFsm),
        .enemyWEN(enemyWENFsm),
        
        .explX(explX),
        .explY(explY),
        .distance(distance),
        .tickExplode(tickExplode),
		.moneyEarned(moneyEarned),
        .active(active)
    );
        
    CalcExplosion explode(
        .clk(clk),
        .enable(subenable),
        .busy(subbusy),
        .explX(explX),
        .explY(explY),
        .distance(distance),
        .tickExplode(tickExplode)
    );
	assign imoney_nxt = imoney + moneyEarned;
    
    always @(posedge clk) begin
        if(rst) slotActive <= 0;
        else if(pushTick || subenable_pose_del) begin
            slotActive[iterAddr] <= pushTick || active;
        end
		
		if(irst) imoney <= 0;
		else if(subenable_pose_del) begin
			imoney <= imoney_nxt;
			if(last) money <= imoney_nxt;
		end
    end
    
    
endmodule
