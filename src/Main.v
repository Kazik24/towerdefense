`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.03.2018 13:14:40
// Design Name: 
// Module Name: Main
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module Main(
        input wire clk, //main clock 100mhz
        
        inout wire ps2_clk,//mouse clock
        inout wire ps2_data,//mouse data
        
        output wire vs,//VGA vertical sync
        output wire hs,//VGA horizontal sync
        output wire [3:0] r,//VGA red channel
        output wire [3:0] g,//VGA green channel
        output wire [3:0] b//VGA blue channel
    );
    
    wire logic_clk,pixel_clk,vblank_first,vblank_last,frame_clk;
    assign frame_clk = vblank_first && vblank_last;
    ClockGenerator clockGen(
        .clk(clk),
        .clk100Mhz(logic_clk),
        .clk40Mhz(pixel_clk)
    );
    wire[1:0] mouseButton;
    wire `PX_COORD mouse_xpos, mouse_ypos;
    Mouse mouse(
        .clk100Mhz(logic_clk),
        .clk40Mhz(pixel_clk),
        .ps2_clk(ps2_clk),
        .ps2_data(ps2_data),
        .frameSync(frame_clk),
        .xpos(mouse_xpos),
        .ypos(mouse_ypos),
        .zpos(),
        .button(mouseButton),
        .pressed()
    );
    //vga chain start
    wire`VGA vga_timing_src;
    VgaTiming vga_timing(.pclk(pixel_clk),.vga_bus(vga_timing_src),.vblank_sync(vblank_first));
    
    wire `GRID_XY pointed_grid_addres;
    wire `TOWER_TYPE map_data_from_mem;
    wire`VGA vga_interface;
    wire commandClk;
    wire `COMMAND command;
    wire `GRID_XY new_tower_pos;
    wire `TOWER_TYPE new_tower_type;
	wire `GLOBAL_STATE globals;
	
    GameCore core(
        .pclk(pixel_clk),
        .vga_in(vga_timing_src),
        .vga_out(vga_interface),
        .placePos(new_tower_pos),
        .placeTower({13'b0,new_tower_type}), //  count,upgrades,state,type
        .command(command),
        .commandClk(commandClk),
        .memPos(pointed_grid_addres),
        .memTower({13'b0,map_data_from_mem}),
        .globals(globals)
    );
    
    
    wire`VGA last_vga_output;
    DrawInterface my_DrawInterface(
        .pclk(pixel_clk),
        .command(command),
        .commandClk(commandClk),
        .new_tower_pos(new_tower_pos),
        .new_tower_type(new_tower_type),
        .vga_in(vga_interface),
        .vga_out(last_vga_output),
        .xpos(mouse_xpos),
        .ypos(mouse_ypos),
        .buttons(mouseButton),
        .pointed_grid_addres(pointed_grid_addres),
        .map_data_from_mem(map_data_from_mem),
        .globals(globals)
    );
    
    VgaOutput vga_output(
        .vga_in(last_vga_output),.vblank_sync(vblank_last),
        .vs(vs),.hs(hs),.r(r),.g(g),.b(b)
    );
    
endmodule
