`ifndef _MAINDEFS_VH_
`define _MAINDEFS_VH_

`define VGA_COUNTING_SIZE 11 //hcount/vcount
`define VGA_COUNTING [`VGA_COUNTING_SIZE-1:0] //pole bitowe hcount/vcount
`define VGA_BUS_SIZE 38 //vga bus size
`define VGA_VBLANK [0] //sygnal vblnk z magistrali vga
`define VGA_HBLANK [1]
`define VGA_VSYNC [2]
`define VGA_HSYNC [3]
`define VGA_RGB_LSB 4
`define VGA_RGB_MSB 15
`define VGA_RGB `VGA_RGB_MSB:`VGA_RGB_LSB
`define VGA_VCOUNT_LSB 16
`define VGA_VCOUNT_MSB 26
`define VGA_VCOUNT `VGA_VCOUNT_MSB:`VGA_VCOUNT_LSB
`define VGA_HCOUNT_LSB 27
`define VGA_HCOUNT_MSB 37
`define VGA_HCOUNT `VGA_HCOUNT_MSB:`VGA_HCOUNT_LSB
`define VGA [`VGA_BUS_SIZE-1:0] //vga bus

`define TEXT_BUS_SIZE 16
`define TEXT_ADDR_LSB 0
`define TEXT_ADDR_MSB 10
`define TEXT_ADDR `TEXT_ADDR_MSB:`TEXT_ADDR_LSB
`define TEXT_OFFSET_LSB 11
`define TEXT_OFFSET_MSB 13
`define TEXT_OFFSET `TEXT_OFFSET_MSB:`TEXT_OFFSET_LSB
`define TEXT_ENABLE_BIT 14
`define TEXT_RED_BIT 15


`define GRID_COORD_SIZE 5 //koordynaty na siatce (od 0 do 31)
`define GRID_COORD [`GRID_COORD_SIZE-1:0] //magistrala koordynatow siatki
`define PX_COORD_SIZE 11 //koordynaty pikseli
`define PX_COORD [`PX_COORD_SIZE-1:0] //magistrala koordynatow pikseli
`define RGB_SIZE 12 //rgb
`define RGB [`RGB_SIZE-1:0] //magistrala rgb
`define GRID_SIZE 20 //20x20 siatka wiez, nie zmienia�!!!
`define MOUSEBTN_NONE 2'b00 //przyciski myszy
`define MOUSEBTN_LEFT 2'b01
`define MOUSEBTN_MIDDLE 2'b10
`define MOUSEBTN_RIGHT 2'b11
`define STATISTIC_SIZE 10
`define STATISTIC [`STATISTIC_SIZE-1:0] //liczba okreslajaca dana statystyke
`define STAT_ADDR [4:0] //adres danej statystyki
`define COMMAND [3:0] //komenda dla GameCore
`define GRID_XY [`GRID_COORD_SIZE*2-1:0] //koordynaty x,y pakowane razem
`define GRID_X [`GRID_COORD_SIZE-1:0] //pakowany koordynat x
`define GRID_Y [`GRID_COORD_SIZE*2-1:`GRID_COORD_SIZE] //pakowany koordynat y
`define GRID_ADDR_SIZE clog2(`GRID_SIZE*`GRID_SIZE)
`define GRID_ADDR [GRID_ADDR_SIZE-1:0]

//towers
`define GRID_CELL_PX 30
`define TOWER_TYPE [4:0]
`define TOWER_STATE [8:5]
`define TOWER_UPGRADES [11:9]
`define TOWER_COUNT [17:12]
`define TOWER_MEMORY_BITS 18

`define TOWER1_RESTORE_TIME 60
`define TOWER2_RESTORE_TIME 25
`define FIRE_ANIM_TIME 5


//minions
`define MAX_ENEMIES 128
`define ENEMY_POSX_MSB 9
`define ENEMY_POSX_LSB 0
`define ENEMY_POSX [`ENEMY_POSX_MSB:`ENEMY_POSX_LSB]
`define ENEMY_POSY_MSB 19
`define ENEMY_POSY_LSB 10
`define ENEMY_POSY [`ENEMY_POSY_MSB:`ENEMY_POSY_LSB]
`define ENEMY_TYPE_MSB 23
`define ENEMY_TYPE_LSB 20
`define ENEMY_TYPE [`ENEMY_TYPE_MSB:`ENEMY_TYPE_LSB]
`define ENEMY_HITPOINTS [31:24]
`define ENEMY_COUNTER [33:32]
`define ENEMY_SPEED [36:34]
`define ENEMY_FACING_MSB 38
`define ENEMY_FACING_LSB 37
`define ENEMY_FACING [`ENEMY_FACING_MSB:`ENEMY_FACING_LSB]
`define ENEMY_MEMORY_BITS 39

//enemies_rom_bus
`define ENEMY_LEFT_RIGHT_BIT 17
`define ADDRY_MSB 16
`define ADDRY_LSB 12
`define ADDRY `ADDRY_MSB:`ADDRY_LSB
`define ADDRX_MSB 11
`define ADDRX_LSB 7
`define ADDRX `ADDRX_MSB:`ADDRX_LSB
`define ENABLE_BIT 6
`define ENEM_TYPE_MSB 5
`define ENEM_TYPE_LSB 2
`define ENEM_TYPE `ENEM_TYPE_MSB:`ENEM_TYPE_LSB
`define ENEM_FACING_MSB 1
`define ENEM_FACING_LSB 0
`define ENEM_FACING `ENEM_FACING_MSB:`ENEM_FACING_LSB
`define ENEM_ROM_BUS_SIZE 18
`define ENEM_ROM_BUS [`ENEM_ROM_BUS_SIZE-1:0]

//bullets
`define MAX_BULLETS 32
`define BULLET_TYPE [2:0]
`define BULLET_COUNT [5:3]
`define BULLET_GPOSXY [15:6]
`define BULLET_GPOSX [10:6]
`define BULLET_GPOSY [15:11]
`define BULLET_TARGET [22:16]
`define BULLET_TPOSXY [42:23]
`define BULLET_TPOSX [32:23]
`define BULLET_TPOSY [42:33]

`define BULLET_MEMORY_BITS 43

//global state
`define GLOBAL_STATE_SIZE 33
`define GLOBAL_STATE [`GLOBAL_STATE_SIZE-1:0] //globalny stan gry
`define LEVEL [1:0] //mapa
`define WAVE [5:0] //fala
`define GSTATE_LEVEL [1:0] //indeks zaladowanej mapy
`define GSTATE_WAVE [7:2] //indeks fali
`define GSTATE_LIFES [15:8] //ilosc zyc straconych od poczatku levelu
`define GSTATE_MONEY [31:16] //zarobiona kasa od poczatku levelu
`define GSTATE_RUNNING [32] //1 jesli fala jeszcze sie nie skonczyla

//interface
`define INFO_WINDOW_BITS 2
`define NO_INFO 0
`define TOWER1_INFO 1
`define TOWER2_INFO 2
`define PLACING_INFO 3
`define MONEY_ON_START 130
`define LIFE_ON_START 100

`define  DISP_TOWER_BITS 2
`define  DISP_TOWER_NON 0
`define  DISP_TOWER1 1
`define  DISP_TOWER2 2

`define TOWER1_DIAMETER 60
`define TOWER2_DIAMETER 100
`define TOWER1_COST 70
`define TOWER2_COST 250

`define TOWER_ADDRY_MSB 9
`define TOWER_ADDRY_LSB 5
`define TOWER_ADDRY `TOWER_ADDRY_MSB:`TOWER_ADDRY_LSB
`define TOWER_ADDRX_MSB 4
`define TOWER_ADDRX_LSB 0
`define TOWER_ADDRX `TOWER_ADDRX_MSB:`TOWER_ADDRX_LSB
`define TOWER_ROM_BUS_SIZE 10
`define TOWER_ROM_BUS [`TOWER_ROM_BUS_SIZE-1:0]
`define TOWER1_BUTTON_XPOS 622
`define TOWER1_BUTTON_YPOS 264
`define TOWER1_BUTTON_LENGTH 30
`define TOWER1_BUTTON_HEIGHT 30
`define TOWER2_BUTTON_XPOS 673
`define TOWER2_BUTTON_YPOS 264
`define TOWER2_BUTTON_LENGTH 30
`define TOWER2_BUTTON_HEIGHT 30

//global events
`define GLOBAL_EVENT [15:0]
`define GEVENT_WAVE_FINISHED [0]
`define GEVENT_LEVEL_FINISHED [1]

`endif