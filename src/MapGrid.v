`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.03.2018 19:41:29
// Design Name: 
// Module Name: MapGrid
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 20x20 siatka komorek z ktorych kazda ma 30x30 pikseli, jest wyswietlana
//jako prostokat o rozmiarze 600x600 w pozycji (0,0)
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"
module MapGrid(
        input wire`GRID_XY gpos,//koordynaty x,y zapisu do komorki
        input wire[`TOWER_MEMORY_BITS-1:0] state,//stan komorki do wpisania
        input wire wen,//odblokowanie zapisu do pamieci
        
        input wire`GRID_XY rpos,//koordynaty x,y odczytu z komorki
        output wire[`TOWER_MEMORY_BITS-1:0] readState,//odczytany stan komorki
        
        input wire`GRID_XY rpos2,//koordynaty x,y odczytu z komorki
        output wire[`TOWER_MEMORY_BITS-1:0] readState2,//odczytany stan komorki
        
        input wire pclk,//zegar pikseli i pamieci
        input wire`VGA vga_in,//magistrala vga polaczona w 1 bus (wejsciowa)
        output reg`VGA vga_out//magistrala vga polaczona w 1 bus (wyjsciowa)
    );
    `include "mathfuncs.vh"
    wire`VGA_COUNTING hcount,vcount;
    wire vsync,hsync,vblnk,hblnk;
    wire`RGB rgb_bus;
    FromVgaBus bus_in(.busIn(vga_in),
        .hcount(hcount),
        .vcount(vcount),
        .vsync(vsync),
        .hsync(hsync),
        .vblnk(vblnk),
        .hblnk(hblnk),
        .rgb(rgb_bus)
    );
    wire[`TOWER_MEMORY_BITS-1:0] dispState;
    
    wire`GRID_XY hcountDiv,vcountDiv,currPos;
    assign hcountDiv = divBy30(hcount);
    assign vcountDiv = divBy30(vcount);
    assign currPos`GRID_X = hcountDiv`GRID_X;
    assign currPos`GRID_Y = vcountDiv`GRID_X;
    
    Slot20x20#(`TOWER_MEMORY_BITS) gridMem(
        .clk(pclk),
        .wpos(gpos),
        .data(state),
        .wen(wen),
        .rpos0(currPos),
        .out0(dispState),
        .rpos1(rpos),
        .out1(readState),
        .rpos2(rpos2),
        .out2(readState2)
    );
    wire`RGB cellRgb;
    
    GridCellRender state_cell(
        .pclk(pclk),
        .state(dispState),
        .x(hcountDiv`GRID_Y),
        .y(vcountDiv`GRID_Y),
        .rgb(cellRgb)
    );
    reg`RGB rgb_out;
    reg`VGA vga_out_del,vga_out_del2;
    always @* begin
        if(vga_out_del2`VGA_VBLANK || vga_out_del2`VGA_HBLANK) rgb_out = 12'h000;//blank
        else if(vga_out_del2[`VGA_VCOUNT] >= 600 || vga_out_del2[`VGA_HCOUNT] >= 600) rgb_out = rgb_bus;
        else rgb_out = cellRgb;
    end
    
    wire`VGA vga_out_nxt;
    ToVgaBus bus_out(
        .hcount(hcount),
        .vcount(vcount),
        .vsync(vsync),
        .hsync(hsync),
        .vblnk(vblnk),
        .hblnk(hblnk),
        .rgb(0),
        .busOut(vga_out_nxt)
    );
    
    always @(posedge pclk) begin
        vga_out_del <= vga_out_nxt;
        vga_out_del2 <= vga_out_del;
        
    end
    
    reg`VGA vga_out_del2_nxt;
    always @* begin
        vga_out_del2_nxt = vga_out_del2;
        vga_out_del2_nxt[`VGA_RGB] = rgb_out;
    end
    
    
    always @(posedge pclk) vga_out = vga_out_del2_nxt;
    
    
endmodule
