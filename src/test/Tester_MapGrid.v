`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.04.2018 19:39:18
// Design Name: 
// Module Name: Tester_MapGrid
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module Tester_MapGrid(
        input wire frame_clk,
        output wire[`TOWER_MEMORY_BITS-1:0] gridWrite,
        output reg`GRID_XY gridPos,
        output reg gridWriteClk
    );
    
    reg flip;
    always @(posedge frame_clk) begin
        flip = ~flip;
        if(flip) begin
            gridWriteClk = 0;
            gridPos = gridPos + 1;
        end else begin
            gridWriteClk = 1;
        end
    end
    assign gridWrite = 1;
    always @* begin
        
    end
    
    
endmodule
