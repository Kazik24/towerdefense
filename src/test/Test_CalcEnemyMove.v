`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.03.2018 12:13:38
// Design Name: 
// Module Name: Test_CalcEnemyMove
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module Test_CalcEnemyMove;

    reg enable;
    reg clk = 0;
    always #10 clk = ~clk;
    wire busy;
    wire`GRID_COORD memx;
    wire`GRID_COORD memy;
    reg[`TOWER_MEMORY_BITS-1:0] tower;
    wire[clog2(`MAX_ENEMIES)-1:0] memaddr;
    reg[`ENEMY_MEMORY_BITS-1:0] enemy;
    wire[`ENEMY_MEMORY_BITS-1:0] memWrite;
    wire memWriteClk;
    wire[9:0] hitpointsLost;
    
    CalcEnemyMove test(enable,clk,busy,memx,memy,tower,memaddr,enemy,memWrite,
    memWriteClk,hitpointsLost);
    
    initial begin
        tower = `GR_PATH_H;
        enemy = 10 | (10<<10) | (1<<34) | (`EN_LVL1<<20) | (`FACING_RIGHT<<36);
        #1
        enable = 1;
    end
    
endmodule
