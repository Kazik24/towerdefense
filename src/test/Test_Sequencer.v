`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 27.03.2018 09:30:53
// Design Name: 
// Module Name: Test_Sequencer
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Test_Sequencer;
    
    localparam SIZE = 4;
    reg clk = 0;
    always #20 clk = ~clk;
    reg enable = 0;
    wire busy;
    wire[SIZE-1:0] seq_enable;
    wire[SIZE-1:0] seq_busy;
    
    Sequencer#(SIZE) test(
        .clk(clk),
        .enable(enable),
        .busy(busy),
        .seq_enable(seq_enable),
        .seq_busy(seq_busy)
    );
    
    Delay#(5) del0(clk,seq_enable[0],seq_busy[0]);
    Delay#(5) del1(clk,seq_enable[1],seq_busy[1]);
    Delay#(5) del2(clk,seq_enable[2],seq_busy[2]);
    Delay#(5) del3(clk,seq_enable[3],seq_busy[3]);
    
    initial begin
        #110
        enable = 1;
        
    end
    
endmodule
