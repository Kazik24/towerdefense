`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 31.03.2018 16:58:33
// Design Name: 
// Module Name: Test_Synchronizer
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module Test_Synchronizer;
    function integer clog2(input integer number);
        begin
            clog2 = 0;
            while(number) begin
                clog2  = clog2 + 1;
                number = number >> 1;
            end
        end
    endfunction
    reg clk = 0;
    always #20 clk = ~clk;
    reg genable = 0;
    wire gbusy;
    
    localparam CNT = 6;
    
    wire enable0,busy0,final0,rst0;
    wire[clog2(CNT-1)-1:0] counter0;
    reg[3:0] value0,temp0 = 0;
    wire enable1,busy1,final1,rst1;
    wire[clog2(CNT-1)-1:0] counter1;
    reg[3:0] value1,temp1 = 0;
    Sequencer#(2) seq(clk,genable,gbusy,{enable1,enable0},{busy1,busy0});
    
    
    Iterator#(CNT) sync0(clk,enable0,busy0,counter0,rst0,final0);
    Iterator#(CNT) sync1(clk,enable1,busy1,counter1,rst1,final1);
    
    
    always @(posedge clk) begin
        temp0 <= #2 rst0?0:temp0 + counter0+1;
        temp1 <= #2 rst1?0:temp1 + counter1+1;
        if(final0) value0 <= #2 temp0 + counter0+1;
        if(final1) value1 <= #2 temp1 + counter1+1;
    end
    
    
    
    
    
    initial begin
        #90
        genable = 1;
        
        #990
        genable = 1;
    end
    
    always @(negedge gbusy) #1 genable = 0;
    
endmodule
