`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.04.2018 15:37:12
// Design Name: 
// Module Name: Test_Enumerator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module Test_Enumerator;
    localparam CNT = 4;
    reg clk = 0;
    always #10 clk = ~clk;
    reg genable = 0;
    wire gbusy;
    
    reg clk3 = 0,clk_del = 0;
    always @(posedge clk) begin
        clk3 <= ~clk3;
        //clk3 <= clk_del;
    end
    
    wire[clog2(CNT-1)-1:0] cntx,cnty;
    wire irst1,irst2,last1,last2;
    wire innerEn,innerBs;
    wire subenable;
    wire subbusy;
    
    
    
    Enumerator#(CNT) xLoop(clk,genable,gbusy,cntx,irst1,last1,innerEn,innerBs);
    Enumerator#(CNT) yLoop(clk,innerEn,innerBs,cnty,irst2,last2,subenable,subbusy);
    
    
    
    initial begin
        #96
        genable = 1;
    end
    Delay#(1) del(clk,subenable,subbusy);
//    always @(posedge subenable) begin
//        #1 subbusy = 1;
//        #20 subbusy = 0;
//    end
    always @(negedge gbusy) begin
        #40 genable = 0;
    end
    
    function integer clog2(input integer number); //bez tego nie dziala symulacja
        begin
            clog2 = 0;
            while(number) begin
                clog2  = clog2 + 1;
                number = number >> 1;
            end
        end
    endfunction
endmodule
