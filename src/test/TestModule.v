`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 17.04.2018 17:08:59
// Design Name: 
// Module Name: TestModule
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TestModule(
        input wire clk,
        input wire r,
        input wire s,
        output reg out
    );
    always @(posedge clk) out <= r ? 0 : (s ? 1 : out);
    
endmodule
