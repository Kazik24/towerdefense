`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 15.05.2018 20:34:57
// Design Name: 
// Module Name: Test_Preload
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"
`include "states.vh"

module Test_Preload;
    `include "mathfuncs.vh"
    reg clk = 0;
    always #10 clk = ~clk;
    
    reg enable = 0;
    wire busy;
    reg coreBusy = 0;
    
    //commands
    reg`GRID_XY placePos = 0;
    reg[`TOWER_MEMORY_BITS-1:0] placeTower = 0;
    reg`COMMAND command = 0;
    reg commandEN = 0;
    
    
    wire`GRID_XY memPos;
    wire[clog2(`MAX_ENEMIES-1)-1:0] memEnemy;
    wire[`ENEMY_MEMORY_BITS-1:0] memEnemyWrite;
    wire[`TOWER_MEMORY_BITS-1:0] memTowerWrite;
    wire memEnemyWEN,memGridWEN,clearAll;
    PreloadMemory pre(
        .clk(clk),
        .enable(enable),
        .busy(busy),
        .coreBusy(coreBusy),
        .placePos(placePos),
        .placeTower(placeTower),
        .command(command),
        .commandEN(commandEN),
        .memEnemyRead(0),
        .memTowerRead(0),
        .memPos(memPos),
        .memEnemy(memEnemy),
        .memEnemyWrite(memEnemyWrite),
        .memTowerWrite(memTowerWrite),
        .memEnemyWEN(memEnemyWEN),
        .memGridWEN(memGridWEN),
        .clearAll(clearAll)
        
    );
    
    initial begin
    
        #100
        enable = 1;
        @(negedge busy);
        #5
        enable = 0;
        
        //test start level1
        #30
        command = `CMD_START_LVL1;
        commandEN = 1;
        #20
        commandEN = 0;
        #30
        enable = 1;
        @(negedge busy);
        #5
        enable = 0;
        
        //test start wave
        #30
        command = `CMD_START_WAVE;
        commandEN = 1;
        #20
        commandEN = 0;
        #30
        enable = 1;
        @(negedge busy);
        #5
        enable = 0;
        
        
        #30
        command = 0;
        commandEN = 1;
        #20
        commandEN = 0;
        repeat(60) begin
            #30
            enable = 1;
            @(negedge busy);
            #5
            enable = 0;
        end
        
        
    end
    
endmodule
