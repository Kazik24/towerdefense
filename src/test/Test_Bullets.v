`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.05.2018 22:09:52
// Design Name: 
// Module Name: Test_Bullets
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module Test_Bullets;
    `include "mathfuncs.vh"
    reg clk = 0;
    always #10 clk = ~clk;
    reg enable = 0;
    wire busy;
    reg reset = 0;
    
    reg[`BULLET_MEMORY_BITS-1:0] push;
    reg pushEn = 0;
    wire [16*`BULLET_MEMORY_BITS-1:0] blts;
    wire[6:0] enmAddr;
    reg[`ENEMY_MEMORY_BITS-1:0] enemyRead;
    wire[`ENEMY_MEMORY_BITS-1:0] enemyWrite;
    wire enemyWEN;
    
    CalcBulletMove#(16) calc(
        .clk(clk),
        .enable(enable),
        .busy(busy),
        .rst(reset),
        .push(push),
        .pushEnable(pushEn),
        .bullets(blts),
        .enmAddr(enmAddr),
        .enemyRead(enemyRead),
        .enemyWrite(enemyWrite),
        .enemyWEN(enemyWEN)
    );
    
    initial begin
        
        #100
        push = shootNormal(10'b0010100101,7'b0000001);
        pushEn = 1;
        #20
        pushEn = 0;
        #100
        enable = 1;
    end
    
    always @(enmAddr) begin
        enemyRead = 1;
    end
    
    always @(negedge busy) begin
        #10
        enable = 0;
        #100
        enable = 1;
    end
    
    function[`BULLET_MEMORY_BITS-1:0] shootNormal(input`GRID_XY tpos,input[6:0] target);
        begin
            shootNormal = `BLT_GUN;//normal
            shootNormal`BULLET_COUNT = 5;
            shootNormal`BULLET_GPOSXY = tpos;
            shootNormal`BULLET_TARGET = target;
        end
    endfunction
endmodule
