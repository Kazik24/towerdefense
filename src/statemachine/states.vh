`ifndef _STATES_VH_
`define _STATES_VH_


`define GR_NULL 0
`define GR_GRASS 1 //trawa
`define GR_PATH_V 2 //sciezka pionowa
`define GR_PATH_H 3 //sciezka pozioma
`define GR_PATH_TL 4 //sciezka gora-lewo
`define GR_PATH_TR 5 //sciezka gora-prawo
`define GR_PATH_BL 6 //sciezka dol-lewo
`define GR_PATH_BR 7 //sciezka dol-prawo
`define GR_PATH_CR 8 //sciezka skrzyzowanie
`define GR_GRASS_STONE 9
`define GR_CASTLE_G 10
`define GR_CASTLE_D 11
`define GR_SPAWNER 12
`define GR_TOWER1 13
`define GR_TOWER2 14
`define GR_TREE_LG 15
`define GR_TREE_RG 16
`define GR_TREE_LD 17
`define GR_TREE_RD 18
`define GR_FIRE 19

`define TST_WATCH 0 //wieza obserwuje okolice, jesli w zasiegu znajdzie sie jednostka przechodzi do shoot
`define TST_WAIT 1 //czeka ustalony czas a potem przechodzi do watch
`define TST_IDLE 1 //czeka ustalony czas a potem przechodzi do observe

`define BLT_GUN 1 //zwykly lecacy pocisk
`define BLT_LASER 2 //nie leci, trafia odrazu w cel (animowany jako linia)
`define BLT_EXPLODE 3 //wybucha natychmiast w okreslonym miejscu
`define BLT_BOMB 4 //leci jak zwykly pocisk i na koncu wybucha

`define FACING_UP 0
`define FACING_DOWN 1
`define FACING_LEFT 2
`define FACING_RIGHT 3


`define EN_DEAD 0
`define EN_LVL1 1
`define EN_LVL2 2
`define EN_LVL3 3
`define EN_LVL4 4
`define EN_BOSS 5

//komendy
`define CMD_NONE 0
`define CMD_START_LVL1 1 //rozpoczyna poziom pierwszy,resetuje wszystko, laduje mape, liczbe zyc i czeka na start fali
`define CMD_START_LVL2 5 //analogicznie
`define CMD_START_WAVE 2 //start fali, laduje sekwencyjnie kolejnych wrogow
`define CMD_PLACE_TOWER 3 //wstaw wierzyczke w dane miejsce (uwaga, nadpisuje wszystko w tym miejscu)
`define CMD_RESET_STATS 4 //resetuje wszystkie statystyki

`define LVL_MAP1 0
`define LVL_MAP2 1

`endif