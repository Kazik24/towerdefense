`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 29.03.2018 16:39:05
// Design Name:
// Module Name: GetClosest
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"
`include "mathfuncs.vh"
 
module GetClosest#(
        parameter COUNT = 16
    )(
        input wire[COUNT*`ENEMY_MEMORY_BITS-1:0] enemies,
        input wire`GRID_XY towerGridPos,
       
        output wire[clog2(COUNT-1)-1:0] closestEnemy,
        output wire[`ENEMY_MEMORY_BITS-1:0] choosenEnemy,
        output wire`PX_COORD distance
    );
    `include "mathfuncs.vh"
    wire`PX_COORD towerX,towerY;
    assign towerX = ({towerGridPos`GRID_X,1'h0}+{towerGridPos`GRID_X,2'h0})+(({towerGridPos`GRID_X,3'h0}+{towerGridPos`GRID_X,4'h0})) + `GRID_CELL_PX/2;//30*x+15
    assign towerY = ({towerGridPos`GRID_Y,1'h0}+{towerGridPos`GRID_Y,2'h0})+(({towerGridPos`GRID_Y,3'h0}+{towerGridPos`GRID_Y,4'h0})) + `GRID_CELL_PX/2;
   
    wire[`ENEMY_MEMORY_BITS-1:0] mappedEnemies[COUNT-1:0];
    wire`PX_COORD dists[COUNT-1:0];
    localparam DEPTH = 32;
    
    //obliczanie rozmiaru warstwy drzewa
    function integer sizeForStage(input integer i,input integer count);
        integer c;
        begin
            sizeForStage = count;
            for(c=0;c<(i+1);c=c+1) begin
                sizeForStage = (sizeForStage/2+sizeForStage%2);
            end
        end
    endfunction
    
    generate
        genvar dCnt,i;
        //oblicz dystans kazdego wroga od wiezy
        for(dCnt=0;dCnt<COUNT;dCnt=dCnt+1) begin
            assign mappedEnemies[dCnt] = enemies[(dCnt+1)*`ENEMY_MEMORY_BITS-1:dCnt*`ENEMY_MEMORY_BITS];
            assign dists[dCnt] = |mappedEnemies[dCnt]`ENEMY_TYPE ?
                approxDistance(mappedEnemies[dCnt]`ENEMY_POSX,mappedEnemies[dCnt]`ENEMY_POSY,towerX,towerY) : ~0;
        end
        //wygeneruj drzewo komparatorow
        wire[`ENEMY_MEMORY_BITS-1:0] enm[COUNT-1:0][DEPTH-1:0];
        wire[clog2(COUNT-1)-1:0] adr[COUNT-1:0][DEPTH-1:0];
        wire`PX_COORD dst[COUNT-1:0][DEPTH-1:0];
        for(dCnt=0;dCnt<DEPTH;dCnt=dCnt+1) begin
            if(dCnt == 0) begin
		        //pierwsza warstwa
                for(i=0;i<sizeForStage(dCnt,COUNT);i=i+1) begin
                    if(i*2 == COUNT-1) begin
				        //przepisz ostatni bez zmian (nieparzysty)
                        assign enm[i][dCnt] = mappedEnemies[i*2];
                        assign adr[i][dCnt] = i*2;
                        assign dst[i][dCnt] = dists[i*2];
                    end else begin
                        //komparator wybierajacy blizszy obiekt
                        reg[`ENEMY_MEMORY_BITS-1:0] outTar;
                        reg[clog2(COUNT-1)-1:0] outAddr;
                        reg`PX_COORD outVal;
                        always @* begin
                            if(dists[i*2] < dists[i*2+1]) begin
                                outTar = mappedEnemies[i*2];
                                outAddr = i*2;
                                outVal = dists[i*2];
                            end else begin
                                outTar = mappedEnemies[i*2+1];
                                outAddr = i*2+1;
                                outVal = dists[i*2+1];
                            end
                        end
                        assign enm[i][dCnt] = outTar;
                        assign adr[i][dCnt] = outAddr;
                        assign dst[i][dCnt] = outVal;
//                        CompTwo#(COUNT) cmp(dists[i*2],dists[i*2+1],mappedEnemies[i*2],i*2,mappedEnemies[i*2+1],i*2+1,
//                            enm[i][dCnt],adr[i][dCnt],dst[i][dCnt]);
                    end
                end
            end else begin
			    //reszta warstw
                for(i=0;i<sizeForStage(dCnt,COUNT);i=i+1) begin
                    if(i*2 == sizeForStage(dCnt-1,COUNT)-1) begin
					    //przepisz ostatni bez zmian (nieparzysty)
                        assign enm[i][dCnt] = enm[i*2][dCnt-1];
                        assign adr[i][dCnt] = adr[i*2][dCnt-1];
                        assign dst[i][dCnt] = dst[i*2][dCnt-1];
                    end else begin
                        //komparator wybierajacy blizszy obiekt
                        reg[`ENEMY_MEMORY_BITS-1:0] outTar;
                        reg[clog2(COUNT-1)-1:0] outAddr;
                        reg`PX_COORD outVal;
                        always @* begin
                            if(dst[i*2][dCnt-1] < dst[i*2+1][dCnt-1]) begin
                                outTar = enm[i*2][dCnt-1];
                                outAddr = adr[i*2][dCnt-1];
                                outVal = dst[i*2][dCnt-1];
                            end else begin
                                outTar = enm[i*2+1][dCnt-1];
                                outAddr = adr[i*2+1][dCnt-1];
                                outVal = dst[i*2+1][dCnt-1];
                            end
                        end
                        assign enm[i][dCnt] = outTar;
                        assign adr[i][dCnt] = outAddr;
                        assign dst[i][dCnt] = outVal;
//                        CompTwo#(COUNT) cmp(dst[i*2][dCnt-1],dst[i*2+1][dCnt-1],enm[i*2][dCnt-1],adr[i*2][dCnt-1],
//                            enm[i*2+1][dCnt-1],adr[i*2+1][dCnt-1],
//                            enm[i][dCnt],adr[i][dCnt],dst[i][dCnt]);
                    end
                end
            end
        end
    endgenerate
    
    assign choosenEnemy = enm[0][DEPTH-1];
    assign closestEnemy = adr[0][DEPTH-1];
    assign distance = dst[0][DEPTH-1];
    
endmodule
