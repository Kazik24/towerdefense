`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.04.2018 15:52:20
// Design Name: 
// Module Name: ForEachGetClosest
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module ForEachGetClosest#(
        parameter COUNT = 128,
        parameter PARALLEL = 8
    )(
        input wire clk,
        input wire enable,
        output wire busy,
        
        input wire[COUNT*`ENEMY_MEMORY_BITS-1:0] enemies,
        input wire`GRID_XY towerGridPos,
        
        output reg[clog2(COUNT-1)-1:0] closestEnemy,
        output reg[`ENEMY_MEMORY_BITS-1:0] choosenEnemy,
        output reg`PX_COORD distance
    );
    `include "mathfuncs.vh"
    localparam SIZE = (COUNT/PARALLEL) + (COUNT%PARALLEL!=0?1:0);
    wire clk3;
    assign clk3 = clk;
    
//    reg clk3;//,clk_del;
//    always @(posedge clk) begin
//        //clk_del <= ~clk3;
//        clk3 <= ~clk3;
//    end
    
    
    wire[clog2(SIZE-1)-1:0] cnt;
    wire last,irst;
    wire[1:0] control;
    Iterator#(SIZE) iter(.clk(clk3),.enable(enable),.busy(busy),.irst(irst),.cnt(cnt),.last(last));
    
    
    wire[PARALLEL*`ENEMY_MEMORY_BITS-1:0] mapped[SIZE-1:0];
    generate
        genvar i;
        for(i=0;i<SIZE;i=i+1) begin
            if(i == SIZE-1) begin
                assign mapped[i] = {{(COUNT*`ENEMY_MEMORY_BITS - PARALLEL*`ENEMY_MEMORY_BITS*(i+1)){1'b0}},
                                    enemies[COUNT*`ENEMY_MEMORY_BITS-1:PARALLEL*`ENEMY_MEMORY_BITS*i]};
            end else begin
                assign mapped[i] = enemies[PARALLEL*`ENEMY_MEMORY_BITS*(i+1)-1:PARALLEL*`ENEMY_MEMORY_BITS*i];
            end
        end
    endgenerate
    
    reg[clog2(COUNT-1)-1:0] iAddr;
    reg[`ENEMY_MEMORY_BITS-1:0] iMem;
    
    wire[clog2(PARALLEL)-1:0] address;
    wire[`ENEMY_MEMORY_BITS-1:0] iMem_nxt;
    wire[clog2(COUNT-1)-1:0] iAddr_nxt;
    wire`PX_COORD iDist_nxt;
    GetClosest#(PARALLEL+1) comp(
        .enemies({iMem,mapped[cnt]}),
        .towerGridPos(towerGridPos),
        .closestEnemy(address),
        .choosenEnemy(iMem_nxt),
        .distance(iDist_nxt)
    );
    
    reg[clog2(COUNT-1)-1:0] cntMul;
    integer imul;
    always @* begin //mnozenie przez stala
        cntMul = 0;
        for(imul=1;imul<clog2(PARALLEL);imul=imul+1) begin
            cntMul = ((PARALLEL & (1<<imul))!=0) ? cntMul + (cnt<<imul) : cntMul;
        end
    end
    
    
    assign iAddr_nxt = (address == PARALLEL) ? iAddr : (address + cntMul);
    
    always @(posedge clk3) begin
        iAddr <= irst?0:iAddr_nxt;
        iMem <= irst?0:iMem_nxt;
        if(last) begin
            closestEnemy <= iAddr_nxt;
            choosenEnemy <= iMem_nxt;
            distance <= iDist_nxt;
        end
    end
    
endmodule
