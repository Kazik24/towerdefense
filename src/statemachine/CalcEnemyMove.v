`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 24.03.2018 11:39:04
// Design Name: 
// Module Name: CalcEnemyMove
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"
`include "states.vh"

module CalcEnemyMove(
        input wire enable,
        input wire clk,
        output wire busy,
        
        //memory interface
        output wire`GRID_XY mempos,
        input wire[`TOWER_MEMORY_BITS-1:0] memGridData,
        output wire[clog2(`MAX_ENEMIES-1)-1:0] memReadWriteAddr,
        input wire[`ENEMY_MEMORY_BITS-1:0] memReadData,
        output wire[`ENEMY_MEMORY_BITS-1:0] memWriteData,
        output wire memWriteEn,
        
        output reg`STATISTIC hitpointsLost,
		output reg isAnyAlive
    );
    `include "mathfuncs.vh"
    localparam ADDR_BITS = clog2(`MAX_ENEMIES-1);
    wire reachedExit;
    
    wire[ADDR_BITS-1:0] thisAddr;
    wire[`ENEMY_MEMORY_BITS-1:0] aiOutState;
    wire`GRID_XY aipos;
    FSMEnemyMove moveAi(
        .mempos(aipos),
        .thisAddr(thisAddr),
        .memData(memGridData),
        .inState(memReadData),
        .outState(aiOutState),
        .reachedExit(reachedExit)
    );
    //drivery komunikacyjnych magistrali wyjsciowych (and'owane enable)
    assign memWriteData = aiOutState & {`ENEMY_MEMORY_BITS{enable}};
    assign memReadWriteAddr = thisAddr & {ADDR_BITS{enable}};
    assign mempos = aipos & {`GRID_COORD_SIZE*2{enable}};
    assign memWriteEn = enable && busy;
    
    //zmienne iterowane
    wire`STATISTIC hitpointsLost_nxt;
    reg`STATISTIC ihplost;
	reg ianyAlive;
	wire anyAlive_nxt;
	assign anyAlive_nxt = (aiOutState`ENEMY_TYPE !=`EN_DEAD) || ianyAlive;
	
    assign hitpointsLost_nxt = reachedExit ? (memReadData`ENEMY_HITPOINTS + ihplost+1) : ihplost;
    wire irst,last;
    
    Iterator#(`MAX_ENEMIES) iter(.clk(clk),.enable(enable),.busy(busy),
        .cnt(thisAddr),.irst(irst),.last(last)
    );
    always @(posedge clk) begin
        ihplost <= irst?0:hitpointsLost_nxt;
		ianyAlive <= irst?0:anyAlive_nxt;
        if(last) begin
			hitpointsLost <= hitpointsLost_nxt;
			isAnyAlive <= anyAlive_nxt;
		end
    end
    
endmodule
