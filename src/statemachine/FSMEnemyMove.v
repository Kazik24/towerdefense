`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.03.2018 23:18:31
// Design Name: 
// Module Name: FSMEnemyMove
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"
`include "states.vh"

module FSMEnemyMove(
        output wire`GRID_XY mempos,
        input wire[clog2(`MAX_ENEMIES-1)-1:0] thisAddr,
        input wire[`TOWER_MEMORY_BITS-1:0] memData,
        
        input wire[`ENEMY_MEMORY_BITS-1:0] inState,
        output reg[`ENEMY_MEMORY_BITS-1:0] outState,
        output reg reachedExit
    );
    `include "mathfuncs.vh"
    
    assign mempos`GRID_X = inState`ENEMY_POSX / `GRID_CELL_PX;
    assign mempos`GRID_Y = inState`ENEMY_POSY / `GRID_CELL_PX;
    
    function automatic[4:0] off(input`PX_COORD coord);
        off = coord % 30;
    endfunction
    function automatic`PX_COORD snap(input`PX_COORD coord);
        snap = (coord / 30)*30;
    endfunction
	
	reg[`ENEMY_MEMORY_BITS-1:0] tst;
	reg[4:0] tempOff;
    localparam HALF = `GRID_CELL_PX/2;
//    always @* begin
//        outState = inState;
//        reachedExit = 0;
//        if(inState`ENEMY_TYPE == 0) begin
//            outState`ENEMY_TYPE = {thisAddr[4],thisAddr[0]}+1;
//            outState`ENEMY_POSX = {thisAddr[3:0],5'b0};
//            outState`ENEMY_POSY = {thisAddr[6:4],5'b0};
//        end else begin
//            outState`ENEMY_POSX = inState`ENEMY_POSX + 1;
//            outState`ENEMY_POSY = inState`ENEMY_POSY + 1;
//        end
        
//    end


    //speed 0 - zatrzymane
    //speed 1 - count = 3, add = 1 (15px/s)
    //speed 2 - count = 2, add = 1 (20px/s)
    //speed 3 - count = 1, add = 1 (30px/s)
    //speed 4 - count = 0, add = 1 (60px/s)
    //speed 5 - count = 0, add = 2 (120px/s)
    //speed 6 - count = 0, add = 3 (160px/s)
    //speed 7 - count = 0, add = 4 (220px/s)
    
    reg[2:0] addPixels;//oblicz ilosc pikseli do dodania w ticku
    reg[1:0] resetCount;
    always @* begin//oblicz predkosc
        case(inState`ENEMY_SPEED)
        1: {addPixels,resetCount} = 5'b00111;
        2: {addPixels,resetCount} = 5'b00110;
        3: {addPixels,resetCount} = 5'b00101;
        4: {addPixels,resetCount} = 5'b00100;
        5: {addPixels,resetCount} = 5'b01000;
        6: {addPixels,resetCount} = 5'b01100;
        7: {addPixels,resetCount} = 5'b10000;
        default: {addPixels,resetCount} = 0;
        endcase
    end
    
    always @* begin
        reachedExit = 0;
        tst = inState;
		tempOff = 0;
        if(inState`ENEMY_TYPE == `EN_DEAD) outState = tst;
        else if(inState`ENEMY_COUNTER != 0) begin//zmniejsz licznik ruchu
            tst`ENEMY_COUNTER = inState`ENEMY_COUNTER - 1;
        end else begin //idz za droga
            tst`ENEMY_COUNTER = resetCount;
            if(memData`TOWER_TYPE == `GR_PATH_TL) begin //droga w ksztalcie _|
                case(inState`ENEMY_FACING)
                `FACING_RIGHT: begin
                    tst`ENEMY_POSX = inState`ENEMY_POSX + addPixels;
                    tempOff = off(tst`ENEMY_POSX);
                    if(tempOff >= HALF) begin
                        tst`ENEMY_POSY = tst`ENEMY_POSY - (tempOff - HALF);
                        tst`ENEMY_POSX = snap(tst`ENEMY_POSX) + HALF;
                        tst`ENEMY_FACING = `FACING_UP;
                    end
                end
                `FACING_UP: tst`ENEMY_POSY = inState`ENEMY_POSY - addPixels;
                `FACING_DOWN: begin
                    tst`ENEMY_POSY = inState`ENEMY_POSY + addPixels;
                    tempOff = off(tst`ENEMY_POSY);
                    if(tempOff >= HALF) begin
                        tst`ENEMY_POSX = tst`ENEMY_POSX - (tempOff - HALF);
                        tst`ENEMY_POSY = snap(tst`ENEMY_POSY) + HALF;
                        tst`ENEMY_FACING = `FACING_LEFT;
                    end
                end
                `FACING_LEFT: tst`ENEMY_POSX = inState`ENEMY_POSX - addPixels;
                endcase
            end else if(memData`TOWER_TYPE == `GR_PATH_TR) begin //droga w ksztalcie |_
                case(inState`ENEMY_FACING)
                `FACING_RIGHT: tst`ENEMY_POSX = inState`ENEMY_POSX + addPixels;//w prawo niezaleznie
                `FACING_UP: tst`ENEMY_POSY = inState`ENEMY_POSY - addPixels;//w gore niezaleznie
                `FACING_DOWN: begin//najpierw w dol potem w lewo
                    tst`ENEMY_POSY = inState`ENEMY_POSY + addPixels;
                    tempOff = off(tst`ENEMY_POSY);
                    if(tempOff >= HALF) begin
                        tst`ENEMY_POSX = tst`ENEMY_POSX - (tempOff - HALF);
                        tst`ENEMY_POSY = snap(tst`ENEMY_POSY) + HALF;
                        tst`ENEMY_FACING = `FACING_RIGHT;
                    end
                end
                `FACING_LEFT: begin
                    tst`ENEMY_POSX = inState`ENEMY_POSX - addPixels;
                    tempOff = off(tst`ENEMY_POSX);
                    if(tempOff < HALF) begin
                        tst`ENEMY_POSY = tst`ENEMY_POSY - (HALF - tempOff);
                        tst`ENEMY_POSX = snap(tst`ENEMY_POSX) + HALF;
                        tst`ENEMY_FACING = `FACING_UP;
                    end
                end
                endcase
			end else if(memData`TOWER_TYPE == `GR_PATH_BL) begin //droga w ksztalcie -,
			    case(inState`ENEMY_FACING)
			    `FACING_LEFT: tst`ENEMY_POSX = inState`ENEMY_POSX - addPixels;//w lewo niezaleznie
			    `FACING_DOWN: tst`ENEMY_POSY = inState`ENEMY_POSY + addPixels;//w dol niezaleznie
			    `FACING_RIGHT: begin//najpierw w prawo potem w dol
			        tst`ENEMY_POSX = inState`ENEMY_POSX + addPixels;
                    tempOff = off(tst`ENEMY_POSX);
                    if(tempOff >= HALF) begin
                        tst`ENEMY_POSY = tst`ENEMY_POSY + (tempOff - HALF);
                        tst`ENEMY_POSX = snap(tst`ENEMY_POSX) + HALF;
                        tst`ENEMY_FACING = `FACING_DOWN;
                    end
			    end
			    `FACING_UP: begin
			        tst`ENEMY_POSY = inState`ENEMY_POSY - addPixels;
                    tempOff = off(tst`ENEMY_POSY);
                    if(tempOff < HALF) begin
                        tst`ENEMY_POSX = tst`ENEMY_POSX - (HALF - tempOff);
                        tst`ENEMY_POSY = snap(tst`ENEMY_POSY) + HALF;
                        tst`ENEMY_FACING = `FACING_LEFT;
                    end
			    end
			    endcase
			end else if(memData`TOWER_TYPE == `GR_PATH_BR) begin //droga w ksztalcie ,-
			    case(inState`ENEMY_FACING)
			    `FACING_RIGHT: tst`ENEMY_POSX = inState`ENEMY_POSX + addPixels;//w prawo niezaleznie
                `FACING_DOWN: tst`ENEMY_POSY = inState`ENEMY_POSY + addPixels;//w dol niezaleznie
                `FACING_LEFT: begin//najpierw w lewo potem w dol
                    tst`ENEMY_POSX = inState`ENEMY_POSX - addPixels;
                    tempOff = off(tst`ENEMY_POSX);
                    if(tempOff < HALF) begin
                        tst`ENEMY_POSY = tst`ENEMY_POSY + (HALF - tempOff);
                        tst`ENEMY_POSX = snap(tst`ENEMY_POSX) + HALF;
                        tst`ENEMY_FACING = `FACING_DOWN;
                    end
                end
                `FACING_UP: begin//najpierw w gore potem w prawo
                    tst`ENEMY_POSY = inState`ENEMY_POSY - addPixels;
                    tempOff = off(tst`ENEMY_POSY);
                    if(tempOff < HALF) begin
                        tst`ENEMY_POSX = tst`ENEMY_POSX + (HALF - tempOff);
                        tst`ENEMY_POSY = snap(tst`ENEMY_POSY) + HALF;
                        tst`ENEMY_FACING = `FACING_RIGHT;
                    end
                end
			    endcase
			end else if((memData`TOWER_TYPE == `GR_CASTLE_G) || (memData`TOWER_TYPE == `GR_CASTLE_D)) begin
			    tst`ENEMY_TYPE = `EN_DEAD;
				reachedExit = 1;
			end else begin //skrzyzowanie i wszystko inne
                case(inState`ENEMY_FACING)
                `FACING_RIGHT: tst`ENEMY_POSX = inState`ENEMY_POSX + addPixels;
                `FACING_DOWN: tst`ENEMY_POSY = inState`ENEMY_POSY + addPixels;
                `FACING_LEFT: tst`ENEMY_POSX = inState`ENEMY_POSX - addPixels;
                `FACING_UP: tst`ENEMY_POSY = inState`ENEMY_POSY - addPixels;
                endcase
            end
        end
		outState = tst;
    end//*/
endmodule
