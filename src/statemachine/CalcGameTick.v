`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.04.2018 12:50:14
// Design Name: 
// Module Name: CalcGameTick
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CalcGameTick(
        input wire frame_clk,//zegar ramki oraz sygnal enable dla ukladu
        input wire logic_clk,//zegar logiki
        output wire busy,//jesli zajety to jest caly czas na 1
        //magistrale pamieci gry
        input wire[`MAX_ENEMIES*`ENEMY_MEMORY_BITS-1:0] enemies,
        output wire`GRID_XY memPos,
        output wire[clog2(`MAX_ENEMIES-1)-1:0] memEnemy,
        input wire[`ENEMY_MEMORY_BITS-1:0] memEnemyRead,
        input wire[`TOWER_MEMORY_BITS-1:0] memTowerRead,
        output wire[`ENEMY_MEMORY_BITS-1:0] memEnemyWrite,
        output wire[`TOWER_MEMORY_BITS-1:0] memTowerWrite,
        output wire memEnemyWriteEn,
        output wire memGridWriteEn,
        output wire[`MAX_BULLETS*`BULLET_MEMORY_BITS-1:0] bullets,
        input wire clearBullets,
        //statystyki na koncu ticku
        output wire`STATISTIC hitpointsLost,//ile punktow zycia stracono w tym ticku
        output wire`STATISTIC earnedFactor,//ile jednostek zabily wieze
        output wire isAnyAlive, //czy jakikolwiek wrog jest zywy
		output wire[3:0] status //jesli 1, zakonczono poziom
    );
    `include "mathfuncs.vh"
    //sterownik wszystkich obliczen sekwencyjnych
    wire[2:0] seqEnable,seqBusy;
    Sequencer#(3) sequencer(
        .clk(logic_clk),.enable(frame_clk),.busy(busy),
        .seq_enable(seqEnable),
        .seq_busy(seqBusy)
    );
    //multiplex pamieci
    //enemy move
    wire mvEnWrEn,btEnWrEn;
    wire tsTwWrEn;
    wire`GRID_XY mvPos,tsPos;
    wire[clog2(`MAX_ENEMIES-1)-1:0] mvEnAddr,btEnAddr;
    wire[`ENEMY_MEMORY_BITS-1:0] mvEnWr,btEnWr;
    wire[`TOWER_MEMORY_BITS-1:0] tsTwWr;
    
    assign memEnemy = mvEnAddr | btEnAddr;
    assign memEnemyWrite = mvEnWr | btEnWr;
    assign memPos = mvPos | tsPos;
    assign memEnemyWriteEn = mvEnWrEn | btEnWrEn;
    assign memGridWriteEn = tsTwWrEn;
    assign memTowerWrite = tsTwWr;
    
    //sekwencyjne obliczenia *******************************************
    //*** oblicz ruch jednostek ***
    CalcEnemyMove calcMove(
        .clk(logic_clk),.enable(seqEnable[0]),.busy(seqBusy[0]),
        .mempos(mvPos),
        .memWriteData(mvEnWr),
        .memReadWriteAddr(mvEnAddr),
        .memGridData(memTowerRead),
        .memReadData(memEnemyRead),
        .memWriteEn(mvEnWrEn),
        .hitpointsLost(hitpointsLost),
		.isAnyAlive(isAnyAlive)
    );
    
    //*** oblicz stany wierzyczek i namiezone jednostki, przekaz do pociskow ***
    wire[`BULLET_MEMORY_BITS-1:0] bulletToShoot;
    wire tickBullet;
    
    CalcTowerShoot#(`MAX_ENEMIES) calcTower(
        .clk(logic_clk),.enable(seqEnable[1]),.busy(seqBusy[1]),
        .enemies(enemies),
        .gridPos(tsPos),
        .gridRead(memTowerRead),
        .gridWrite(tsTwWr),
        .gwen(tsTwWrEn),
        .outBullet(bulletToShoot),
        .tickBullet(tickBullet)
    );
    //*** oblicz ruchy pociskow, uderzone jednostki przekaz do obliczania udezen ***
    CalcBulletMove#(`MAX_BULLETS) calcShoot(
        .clk(logic_clk),.enable(seqEnable[2]),.busy(seqBusy[2]),
        .rst(clearBullets),
        .push(bulletToShoot),
        .pushEnable(tickBullet),
        
        .enmAddr(btEnAddr),
        .enemyWrite(btEnWr),
        .enemyRead(memEnemyRead),
        .enemyWEN(btEnWrEn),
        .money(earnedFactor),
        .bullets(bullets)
    );
endmodule
