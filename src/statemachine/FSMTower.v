`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.03.2018 15:30:21
// Design Name: 
// Module Name: FSMTower
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"
`include "states.vh"

module FSMTower(
        input wire[`TOWER_MEMORY_BITS-1:0] inState,
        input wire[clog2(`MAX_ENEMIES-1)-1:0] closestEnemyAddr,
        input wire[`ENEMY_MEMORY_BITS-1:0] choosenEnemy,
        input wire`PX_COORD distance,
        input wire`GRID_XY towerPos,
        
        output reg[`TOWER_MEMORY_BITS-1:0] outState,
        output reg[`BULLET_MEMORY_BITS-1:0] outBullet,
        output reg tickBullet
    );
    `include "mathfuncs.vh"
    function isDecorationTower(input[`TOWER_MEMORY_BITS-1:0] state);
        isDecorationTower = 0;
    endfunction
    function automatic tickForState(input[3:0] state);
        
    endfunction
    
    
    localparam TOWER1_BULLET = 102;
    localparam TOWER2_BULLET = 102;
    
    always @* begin
        outState = inState;
        outBullet = 0;
        tickBullet = 0;
        
        if(inState`TOWER_COUNT != 0) begin//zmniejsz licznik
            outState`TOWER_COUNT = inState`TOWER_COUNT - 1;
        end else begin//wykonaj akcje
            case(inState`TOWER_TYPE)
            `GR_TOWER1: begin
                case(inState`TOWER_STATE)
                `TST_WATCH: begin
                    if(distance <= `TOWER1_DIAMETER) begin
                        tickBullet = 1;
                        outBullet = shootNormal(towerPos,closestEnemyAddr);
                        outState`TOWER_STATE = `TST_WAIT;
                        outState`TOWER_COUNT = `TOWER1_RESTORE_TIME;//ma czekac 1 sekunde
                    end
                end
                `TST_WAIT: outState`TOWER_STATE = `TST_WATCH;
                endcase
            end
            `GR_TOWER2: begin
                case(inState`TOWER_STATE)
                `TST_WATCH: begin
                    if(distance <= `TOWER2_DIAMETER) begin
                        tickBullet = 1;
                        outBullet = shootLaser(towerPos,closestEnemyAddr);
                        outState`TOWER_STATE = `TST_WAIT;
                        outState`TOWER_COUNT = `TOWER2_RESTORE_TIME;//ma czekac 1/3 sekunde
                    end
                end
                `TST_WAIT: outState`TOWER_STATE = `TST_WATCH;
                endcase
            end
            endcase
        end
        
    end
    
    
    function[`BULLET_MEMORY_BITS-1:0] shootLaser(input`GRID_XY tpos,input[clog2(`MAX_ENEMIES-1)-1:0] target);
        begin
            shootLaser = `BLT_LASER;//normal
            shootLaser`BULLET_COUNT = 3;
            shootLaser`BULLET_GPOSXY = tpos;
            shootLaser`BULLET_TARGET = target;
        end
    endfunction
    function[`BULLET_MEMORY_BITS-1:0] shootNormal(input`GRID_XY tpos,input[clog2(`MAX_ENEMIES-1)-1:0] target);
        begin
            shootNormal = `BLT_GUN;//normal
            shootNormal`BULLET_COUNT = 5;
            shootNormal`BULLET_GPOSXY = tpos;
            shootNormal`BULLET_TARGET = target;
        end
    endfunction
endmodule
