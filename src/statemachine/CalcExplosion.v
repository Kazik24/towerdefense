`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.05.2018 13:17:55
// Design Name: 
// Module Name: CalcExplosion
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CalcExplosion(
        input wire clk,
        input wire enable,
        output wire busy,
        
        input wire`PX_COORD explX,
        input wire`PX_COORD explY,
        input wire`PX_COORD distance,
        input wire tickExplode
    );
    
    Delay#(3) del(clk,enable,busy);
endmodule
