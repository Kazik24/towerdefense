`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 31.03.2018 11:50:24
// Design Name: 
// Module Name: CalcTowerShoot
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"

module CalcTowerShoot#(
        parameter ECOUNT = 10
    )(
        input wire clk,
        input wire enable,
        output wire busy,
        
        input wire[ECOUNT*`ENEMY_MEMORY_BITS-1:0] enemies,
        output wire`GRID_XY gridPos,
        input wire[`TOWER_MEMORY_BITS-1:0] gridRead,
        output wire[`TOWER_MEMORY_BITS-1:0] gridWrite,
        output wire gwen,
        
        output wire[`BULLET_MEMORY_BITS-1:0] outBullet,
        output wire tickBullet
    );
    `include "mathfuncs.vh"
    wire[clog2(`GRID_SIZE-1)-1:0] cntx,cnty;
    wire outerEnable,outerBusy,innerEnable,innerBusy;
    reg busyClose_del,busyClose_del2;
    EnumeratorXY#(`GRID_SIZE,`GRID_SIZE) loop(.clk(clk),
        .enable(enable),
        .busy(busy),
        .cnty(cnty),
        .cntx(cntx),
        .subenable(innerEnable),
        .subbusy(busyClose_del),
        .irst(),
        .last()
    );
    
    wire enableClose,busyClose,tickFsm;
    always @(posedge clk) busyClose_del <= busyClose;//tak negedge :P
    always @(posedge clk) busyClose_del2 <= busyClose_del;//aby odpowiednio bramkowac sygnal zegara
    assign tickFsm = ~busyClose && busyClose_del;//wykrywanie zbocza opadajacego
    
    wire[clog2(ECOUNT-1)-1:0] closestEnemy;
    wire[`ENEMY_MEMORY_BITS-1:0] choosenEnemy;
    wire`PX_COORD distance;
    
    wire[`TOWER_MEMORY_BITS-1:0] fsmGrid;
    reg`GRID_XY fsmPos;
    always @* begin
        fsmPos = 0;
        fsmPos`GRID_X = cntx;
        fsmPos`GRID_Y = cnty;
    end
        
    ForEachGetClosest#(ECOUNT,4) closest(.clk(clk),
        .enable(innerEnable),
        .busy(busyClose),
        .enemies(enemies),
        .towerGridPos(fsmPos),
        .closestEnemy(closestEnemy),
        .choosenEnemy(choosenEnemy),
        .distance(distance)
    );
    
    wire gateWclk,gateBulletTick;
    FSMTower tower(
        .inState(gridRead),
        .closestEnemyAddr(closestEnemy),
        .choosenEnemy(choosenEnemy),
        .distance(distance),
        .towerPos(fsmPos),
        .outState(fsmGrid),
        .outBullet(outBullet),
        .tickBullet(gateBulletTick)
    );
    assign tickBullet = gateBulletTick && tickFsm;
    
    //drivery komunikacyjnych magistrali wyjsciowych (and'owane enable)
    assign gwen = tickFsm && enable;
    assign gridPos = fsmPos & {`GRID_COORD_SIZE*2{enable}};
    assign gridWrite = fsmGrid & {`TOWER_MEMORY_BITS{enable}};
    
endmodule
