`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.03.2018 12:29:18
// Design Name: 
// Module Name: FSMBullet
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"
`include "states.vh"

module FSMBullet(
        input wire[`BULLET_MEMORY_BITS-1:0] inState,
        output reg[`BULLET_MEMORY_BITS-1:0] outState,
        input wire isActive,
        
        output wire[clog2(`MAX_ENEMIES-1)-1:0] enmAddr,
        input wire[`ENEMY_MEMORY_BITS-1:0] enemyRead,
        output reg[`ENEMY_MEMORY_BITS-1:0] enemyWrite,
        output reg enemyWEN,
        
        output reg`PX_COORD explX,
        output reg`PX_COORD explY,
        output reg`PX_COORD distance,
        output reg tickExplode,
		output reg`STATISTIC moneyEarned,
        output reg active
    );
    `include "mathfuncs.vh"
    assign enmAddr = inState`BULLET_TARGET;
    
    wire`PX_COORD towerX,towerY;
    assign towerX = toTowerOrgin(inState`BULLET_GPOSX);
    assign towerY = toTowerOrgin(inState`BULLET_GPOSY);
    wire signed `PX_COORD xdiff,ydiff;
    assign xdiff = enemyRead`ENEMY_POSX-towerX;
    assign ydiff = enemyRead`ENEMY_POSY-towerY;
    
    wire[2:0] maxCount;
    assign maxCount = inState`BULLET_TYPE == `BLT_GUN ? 5 : 3;
    
    
    always @* begin
        enemyWrite = enemyRead;
        outState = 0;
        enemyWEN = 0;
        explX = 0;
        explY = 0;
        distance = 0;
        tickExplode = 0;
        active = 0;
        moneyEarned = 0;
		
		outState = inState;
		
		
		if(inState`BULLET_COUNT != 0) begin
		    outState`BULLET_COUNT = inState`BULLET_COUNT - 1;
            active = 1;
		end else begin
            enemyWEN = 1;
            outState = 0;
            active = 0;
            if(enemyRead`ENEMY_HITPOINTS != 0) enemyWrite`ENEMY_HITPOINTS = enemyRead`ENEMY_HITPOINTS - 1;
            else begin
                moneyEarned = enemyRead`ENEMY_TYPE == `EN_BOSS ? 20 : enemyRead`ENEMY_TYPE;
                enemyWrite = `EN_DEAD;
            end
		end
		
		
		case(inState`BULLET_TYPE)
		`BLT_GUN, `BLT_LASER: begin
		      
            //x = xt+((xe-xt)/5)*(5-cnt)
              outState`BULLET_TPOSX = towerX + $signed($signed(xdiff/5) * (5-inState`BULLET_COUNT));
              outState`BULLET_TPOSY = towerY + $signed($signed(ydiff/5) * (5-inState`BULLET_COUNT));
//            outState`BULLET_TPOSX = towerX + calcBulletFly(xdiff,maxCount,inState`BULLET_COUNT);
//            outState`BULLET_TPOSY = towerY + calcBulletFly(ydiff,maxCount,inState`BULLET_COUNT);
		end
//		`BLT_LASER: begin
//            outState`BULLET_TPOSX = enemyRead`ENEMY_POSX;
//            outState`BULLET_TPOSY = enemyRead`ENEMY_POSY;
//		end
		default: begin
			outState = 0;
			enemyWEN = 0;
			active = 0;
			moneyEarned = 0;
		end
		endcase
        if(~isActive || enemyRead`ENEMY_TYPE == `EN_DEAD) begin
            outState = 0;
            enemyWEN = 0;
            active = 0;
            moneyEarned = 0;
        end
    
    end
    
endmodule
