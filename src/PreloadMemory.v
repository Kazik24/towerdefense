`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.04.2018 22:40:39
// Design Name: 
// Module Name: PreloadMemory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "maindefs.vh"
`include "states.vh"

module PreloadMemory(
        input wire clk,
        input wire enable,
        output reg busy,
        input wire coreBusy,
        
        input wire`GRID_XY placePos,
        input wire[`TOWER_MEMORY_BITS-1:0] placeTower,
        input wire`COMMAND command,
        input wire commandEN,
        input wire`STAT_ADDR statAddr,
        output wire`STATISTIC statOut,
        
        output wire`GRID_XY memPos,
        output wire[clog2(`MAX_ENEMIES-1)-1:0] memEnemy,
        input wire[`ENEMY_MEMORY_BITS-1:0] memEnemyRead,
        input wire[`TOWER_MEMORY_BITS-1:0] memTowerRead,
        output wire[`ENEMY_MEMORY_BITS-1:0] memEnemyWrite,
        output wire[`TOWER_MEMORY_BITS-1:0] memTowerWrite,
        output wire memEnemyWEN,
        output wire memGridWEN,
        
        output reg clearAll,//czy wyczyscic wszystkie tymczasowe rzeczy (pociski,tymczasowe statystyki)
        
        input wire`STATISTIC hitpointsLost,//ile punktow zycia stracono w tym ticku
        input wire`STATISTIC earnedFactor,//ile jednostek kasy zarobiono w tym ticku
        input wire anyAlive,
        output reg`GLOBAL_STATE globals
    );
    `include "mathfuncs.vh"
    
    wire nen;
    assign nen = ~enable;
    reg`GRID_XY placePos_lock,placePos_buf;
    reg[`TOWER_MEMORY_BITS-1:0] placeTower_lock,placeTower_buf;
    reg`COMMAND command_lock,command_buf;
    reg enable_del,enable_del2;
    wire enable_pose;
    assign enable_pose = enable && ~enable_del;
    reg enable_pose_del,enable_pose_del2;
    always @(posedge clk) begin
        enable_pose_del <= enable_pose;
        enable_pose_del2 <= enable_pose_del;
        enable_del <= enable;
        enable_del2 <= enable_del;
        
        placePos_buf <= commandEN ? placePos : enable_pose ? 0 : placePos_buf;
        placeTower_buf <= commandEN ? placeTower : enable_pose ? 0 : placeTower_buf;
        command_buf <= commandEN ? command : enable_pose ? 0 : command_buf;
        
        if(enable_pose) begin //aby nie zmienial sie podczas enable
            placePos_lock <= placePos_buf;
            placeTower_lock <= placeTower_buf;
            command_lock <= command_buf;
        end
    end
    
    //command executor
    reg`GRID_XY pos1;
    wire`GRID_XY posLd;
    reg[`TOWER_MEMORY_BITS-1:0] grid1;
    wire[`TOWER_MEMORY_BITS-1:0] gridLd;
    reg gwen1;
    wire gwenLd,ewenLd,ewenWv;
    
    
    reg ben,ldEn;
    wire bbs,ldBs;
    wire[clog2(`MAX_ENEMIES-1)-1:0] enmLd;
    Delay#(6) blankDelay(clk,ben,bbs);//opozniacz dla komend natychniastowych
    
    reg`LEVEL ldLevel;
    LoadLevel load(
        .clk(clk),
        .enable(ldEn),
        .busy(ldBs),
        .level(ldLevel),
        .memPos(posLd),
        .memEnemy(enmLd),
        .memTowerWrite(gridLd),
        .memGridWEN(gwenLd),
        .memEnemyWEN(ewenLd)
    );
    
    //wewnetrzne stany
    reg`LEVEL stLevel=0,stLevel_nxt;
    reg`WAVE stWave=0,stWave_nxt;
    reg[15:0] stCount=0,stCount_nxt;
    reg[clog2(`MAX_ENEMIES-1)-1:0] stEnmIndex=0,stEnmIndex_nxt;
    reg stEnable=0,stEnable_nxt;
    reg stWtiteEnm=0,stWtiteEnm_nxt;
    
    reg resetState;
    reg writeState;
    
    wire[15:0] compCount;
    wire[`ENEMY_MEMORY_BITS-1:0] enmRomData;
    wire[clog2(`MAX_ENEMIES-1)-1:0] maxAddress,addressBound;
    Level1Waves waves(
        .level(stLevel),
        .wave(stWave),
        .addr(stEnmIndex),
        .activTick(compCount),
        .data(enmRomData),
        .maxAddr(maxAddress)
    );
    assign addressBound = maxAddress+1;
    
    always @(posedge clk) begin
        if(resetState) begin
            stLevel <= 0;
            stWave <= 0;
            stCount <= 0;
            stEnmIndex <= 0;
            stEnable <= 0;
            stWtiteEnm <= 0;
        end else if(writeState) begin
            stLevel <= stLevel_nxt;
            stWave <= stWave_nxt;
            stCount <= stCount_nxt;
            stEnmIndex <= stEnmIndex_nxt;
            stEnable <= stEnable_nxt;
            stWtiteEnm <= stWtiteEnm_nxt;
        end
    end
    
    wire nextEnemy;
    assign nextEnemy = stCount == compCount && stEnable;
    
    
    reg resetStats;
    always @* begin
        pos1 = 0;
        grid1 = 0;
        gwen1 = 0;
        busy = bbs;
        resetStats = 0;
        ben = enable_del2;
        ldLevel = 0;
        resetState = 0;
        clearAll = 0;
        ldEn = 0;
        
        //domyslnie przepisz stany i zinkrementuj count jesli odblokowany
        stLevel_nxt = stLevel;
        stWave_nxt = stWave;
		
        stCount_nxt = nextEnemy ? 0 : (stCount + (stEnable ? 1 : 0));
		
        stEnmIndex_nxt = stWtiteEnm ? stEnmIndex+1 : stEnmIndex;
        stEnable_nxt = (stEnmIndex_nxt == addressBound && stWtiteEnm) ? 0 : stEnable;
		stWtiteEnm_nxt = 0;
		if(nextEnemy) stWtiteEnm_nxt = 1;
		if(stWtiteEnm) stWtiteEnm_nxt = 0;
        writeState = enable_pose_del;
        if(command_lock == `CMD_PLACE_TOWER) begin
            pos1 = placePos_lock;
            grid1 = placeTower_lock;
            gwen1 = enable_pose_del;
        end else if(command_lock == `CMD_START_LVL1 || command_lock == `CMD_START_LVL2) begin//todo dodaj reset statystyk
            //multipleksuj na modul LoadLevel
            busy = ldBs;
            resetStats = enable_pose_del;
            ldEn = enable_del2;
            ldLevel = (command_lock == `CMD_START_LVL1) ? `LVL_MAP1 : `LVL_MAP2;
            clearAll = enable_pose_del;
            //wpisz poczatkowe stany dla poziomu
            stLevel_nxt = ldLevel;
            stWave_nxt = 0;
            stCount_nxt = 0;
            stEnmIndex_nxt = 0;
            stEnable_nxt = 0;//zablokuj
        end else if(command_lock == `CMD_START_WAVE) begin
            stCount_nxt = 0;
            stWave_nxt = stWave + 1;
            stEnmIndex_nxt = 0;
            stEnable_nxt = 1;//odblokuj licznik
            clearAll = enable_pose_del;
        end else if(command_lock == `CMD_RESET_STATS) begin
            resetStats = enable_pose_del;
        end
    end
    
    //global state machine
    reg coreBusy_del;
    always @(posedge clk) coreBusy_del <= coreBusy;
    wire coreBusy_nege;
    assign coreBusy_nege = ~coreBusy && coreBusy_del;
    
    reg anyAlive_lock;
    reg[7:0] lives;
    reg[15:0] money;
    always @(posedge clk) begin
        if(coreBusy_nege) anyAlive_lock <= anyAlive;
        if(resetStats) begin
            lives <= 0;
            money <= 0;
        end else if(coreBusy_nege) begin
            lives <= lives + hitpointsLost;
            money <= money + earnedFactor;
        end
    end
    
    //output drivers
    wire[clog2(`MAX_ENEMIES-1)-1:0] eldAddr;
    wire[`ENEMY_MEMORY_BITS-1:0] eldData;
    wire tickEnemy;
    assign tickEnemy = stWtiteEnm && enable_pose_del2;
    assign eldAddr = stEnmIndex & {clog2(`MAX_ENEMIES-1){tickEnemy}};
    assign eldData = enmRomData & {`ENEMY_MEMORY_BITS{tickEnemy}};
    
    
    assign memEnemy = (enmLd | eldAddr) & {clog2(`MAX_ENEMIES-1){enable}};
    assign memPos = (pos1 | posLd) & {`GRID_COORD_SIZE*2{enable}};
    assign memTowerWrite = (grid1 | gridLd) & {`TOWER_MEMORY_BITS{enable}};
    assign memEnemyWrite = (eldData) & {`ENEMY_MEMORY_BITS{enable}};
    assign memGridWEN = (gwen1 | gwenLd) & enable;
    assign memEnemyWEN = (ewenLd | tickEnemy) & enable;
    
    always @* begin//global state
        globals = 0;
        globals`GSTATE_LEVEL = stLevel;
		globals`GSTATE_WAVE = stWave;
        globals`GSTATE_LIFES = lives;
        globals`GSTATE_MONEY = money;//*10;
        globals`GSTATE_RUNNING = stEnable || anyAlive_lock;
    end
    
endmodule
