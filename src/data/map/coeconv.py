

# tu wpisze dane
filename = "logo.data"
radix = 2
fillLen = -1; # do ilu znakow ma wypelniac zerami (-1 - bez wypelniania)


def padStr(s,size,ch):
    if size < 0: return s
    val = size - len(s)
    if val > 0: return str(str(ch)*val)+str(s)
    return str(s)

print("Reading file: %s"%(filename))
with open(filename) as file:
    lines = file.readlines()
lines = [padStr(a.strip(),fillLen,'0') for a in lines]

filename = filename[0:filename.index(".data")]+".coe"
print("Copying file to: %s with radix: %d"%(filename,radix))
with open(filename,"w") as file:
    file.write("memory_initialization_radix=%d;\n"%(radix))
    file.write("memory_initialization_vector=\n");
    for i in range(len(lines)):
        if i == (len(lines)-1):
            file.write("%s;\n"%(lines[i]))
        else:
            file.write("%s,\n"%(lines[i]))

print("Done!")
