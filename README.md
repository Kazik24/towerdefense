# Tower Defense
Chorda stworów zmierza do twojego zamku! Tylko Ty możesz ją powstrzymać. Ustawiaj wieże na mapie tak by zgładzić je wszystkie nim one zgładzą ciebie!

Gra powstała jako projekt z przedmiotu Układy Elektroniki Cyfrowej 2. Napisana została w Verilogu. Do wyboru są dwie mapy, łatwa i trudniejsza, oraz wiele różnych kombinacji fal stworów. Twoim zadaniem jest obronić zamek, bo jeśli za dużo stworów do niego dojdzie... Game Over.

![Zdj 1](https://bitbucket.org/Kazik24/towerdefense/raw/master/doc/zdj1.png)

![Zdj 2](https://bitbucket.org/Kazik24/towerdefense/raw/master/doc/zdj2.png)

![Zdj 3](https://bitbucket.org/Kazik24/towerdefense/raw/master/doc/zdj3.png)